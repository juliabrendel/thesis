\chapter{Introduction}
In the information age it is of major importance to identify meaningful interactions in large unstructured data and measure their informativeness. For example, one might want to know which health indicators were crucial for an outbreak of a disease and how strong was their influence, which factors should be accounted for when planning a company's budget or whether a drop-down in \textit{Facebook} data safety ranking had something to do with the presidential election in the United States. As a matter of fact, interactions of different forms (dependencies, correlations or causalities) occur in every kind of data. 

One way to model them is via a dependency network - a version of a Bayesian network describing processes in data but containing no information about the underlying probability distributions. A dependency network is essentially a directed acyclic graph where nodes represent attributes and edges indicate interactions between those attributes. Once having such a network, one can understand and summarize data much easier and use the gained knowledge for further analysis.

However, the vast majority of the available tools for mining meaningful dependencies and modeling structure of the data have certain requirements or limitations, for example, assuming that data follows a specific distribution. Yet, more often than not, we are given just a data sample of a limited size and no information about the processes that generated it. In order not to be limited to those rare cases when we have some prior knowledge on the data at hand, we aim for a non-parametric approach. 

In addition, most of the existing tools work only on discrete-valued samples or can detect only linear dependencies. However, data usually comes from a continuous domain and interesting interactions are described by non-linear dependencies. Regarding the methods that aim to learn a complete dependency network, the standard approach is to introduce a scoring measure and then select the best fitting network from a specific set, while reconstructing a network optimally has been shown to be NP-hard \citep{chickering1994learning}. Moreover, the existing tools do not pursue the goal of estimating the information content of the constructed network.

Therefore, in this thesis we aim to develop an algorithm for reconstructing a dependency network from continuous-valued data and measure its informativeness  in a purely non-parametric way. We approach this problem from the perspective of information theory, following the observation that presence or absence of a dependence between variables is reflected by their information content. 

The main obstacle on our way is being unable to compute the entropy of a multivariate continuous-valued dataset without having access to the underlying density functions. Therefore, we base our method on the concept of the cumulative entropy \citep{rao2004cumulative, di2009cumulative}, an extension of the Shannon entropy to the continuous domain that works directly on a data sample. We show how to reliably extend it to the multivariate case after inheriting some ideas of \textsc{UDS} \citep{nguyen2016universal} that also uses the cumulative entropy for dependency analysis. However, as we demonstrate in the experiments chapter, even though \textsc{UDS} can reliably estimate the cumulative entropy of a dataset, it is not suitable for learning a dependency network. We estimate the joint cumulative entropy of a multivariate dataset through a decomposition and show that it is strongly connected to the underlying dependency network.

Finally, we introduce \textsc{Grip} - \textbf{G}reedy \textbf{R}econstruction of \textbf{I}nteractions in Data using Entro\textbf{p}y - a greedy iterative approach for constructing from scratch a minimal, in terms of the amount of covered interactions, dependency network though a decomposition of the cumulative entropy. Extensive evaluation of \textsc{Grip} shows that it outperforms the competitors in dependence discovery due to a better entropy estimate and, on the contrary to other methods, can reconstruct dependency networks for most of the dependency types with high accuracy. Moreover, our approach can detect meaningful interactions in real world datasets.

\Cref{intro:energy} shows a dependency network of the energy efficiency dataset \citep{tsanas2012accurate} constructed by our algorithm. The dataset 
describes 8 heating and cooling load requirements for 768 different buildings. The main factors that influence the heating load of a building are the surface area and the roof area. Besides, \textsc{Grip} detected another dependent component, showing an influence of the relative compactness of a building, its overall height and glazing area on the overall distribution of the glazing area in a building. As we can see, even though the reconstructed network contains a small number of edges, it captures the main interactions among the attributes of the dataset.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}
		\tikzstyle{every node} = [ellipse, fill=gray!40]
		\begin{scope}[]
			\node[align=center] (X0) at (-6.5, 8.5) {Relative \\ Compactness};
			\node[align=center] (X1) at (-1.5, 11) {Surface \\ Area};
			\node[align=center] (X2) at (-4.5, 10.5) {Wall \\ Area};
			\node[align=center] (X3) at (1.5, 10.5) {Roof \\ Area};
			\node[align=center] (X4) at (-2, 8) {Overall \\ Height};
			\node[align=center] (X5) at (2.5, 6) {Orientation};
			\node[align=center] (X6) at (-7, 6) {Glazing \\ Area};
			\node[align=center] (X7) at (-3, 4.5) {Glazing Area \\ Distribution};
			\node[align=center] (X8) at (2.5, 7.5) {Heating \\ Load};
			\node[align=center] (X9) at (0.5, 4.5) {Cooling \\ Load};
		\end{scope}
		\draw[->] (X0) -- (X4);
		\draw[->] (X4) -- (X7);
		\draw[->] (X6) -- (X7);
		\draw[->] (X1) -- (X8);
		\draw[->] (X3) -- (X8);
	\end{tikzpicture}
	\caption{Dependency network reconstructed by \textsc{Grip} from the energy efficiency dataset}
	\label{intro:energy}
\end{figure}

The thesis is structured as follows. In \Cref{related} we discuss different approaches for dependency network reconstruction, dependency analysis in general and entropy based discretization. \Cref{back} introduces the main concepts used throughout the thesis: cumulative entropy and universal dependency score. In \Cref{theory} we define a minimal dependency network as well as a minimal entropy decomposition and describe our algorithm in \Cref{alg}. We evaluate the performance of our method in \Cref{exp} and provide a detailed discussion on its shortcomings and limitations in \Cref{discuss}. Finally, we draw conclusions from our work in \Cref{conclusion}.