%TODO add early stopping to appendix
\chapter{Algorithm}\label{alg}
\section{Outline}\label{outline}
In this chapter we present \textsc{Grip}, our algorithm for constructing a minimal dependency network of a multivariate continuous-valued dataset $D$ induced by a minimal cumulative entropy decomposition.

In a nutshell, \textsc{Grip} works as follows. It first preprocesses the data using optimal discretization, see \Cref{sec:entropy} and extracts a candidate set $C$ of dependency relations between pairs of dimensions. The candidates are sorted descending on their dependency scores, meaning that the most informative candidates are placed first. Initially, $\mathit{MDN}$ is an empty network, containing $m$ nodes and no edges and $\hat{h}(X_1,...,X_m)$ is initialized as naive entropy decomposition, see Definition \ref{def:ce_naive}. \textsc{Grip} iteratively extends both entities with dependencies from $C$ minimizing the entropy and the number of edges in the network. The procedure terminates as soon as the candidate set is empty. It outputs a minimal dependency network $\mathit{MDN}$, a minimal CE decomposition $\hat{h}_{\mathit{min}}(X_1,...,X_m)$ that results in an approximate cumulative entropy of the dataset and the dependency score of the dataset $\mathit{UDS}_r$ given the minimal decomposition. 

A brief outline of \textsc{Grip} is shown in \Cref{alg:alg_outline}. In this chapter we explain in more details the three main steps of our method - data preprocessing, construction of a minimal network and extension of dependencies by joint discretization.
\newpage
\begin{algorithm}[H]
	\LinesNumberedHidden
	\KwData{dataset $D \in \mathbb{R}^{n\times{m}}$}
	\KwResult{minimal dependency network $\mathit{MDN}$, minimal entropy decomposition $\hat{h}_{\mathit{min}}(X_1,...,X_m)$, dependency score $\mathit{UDS}_r$}
	$C = \textsc{Preprocessing}(D)$\label{alg:outline_prep}\\
	$\mathit{MDN} = (\{X_1,...,X_m\}, \emptyset)$\\
	$\hat{h}(X_1,...,X_m) = h(X_1) + ... + h(X_m)$\\
	$\mathit{MDN}$, $\hat{h}_{\mathit{min}}(X_1,...,X_m) = \textsc{Reconstruction}(D, C, \mathit{MDN}, \hat{h}(X_1,...,X_m))$\\
	$\mathit{UDS_r} = \mathit{UDS_r}(D, \hat{h}_{\mathit{min}}(X_1,...,X_m))$\\		
	\caption{\textsc{Grip}}    
    \label{alg:alg_outline}
\end{algorithm}

\section{Preprocessing}\label{prep}
The first step of our algorithm is preprocessing the data. It outputs a candidate set $C$ composed of bivariate candidate dependency relations and is utilized at later steps of \textsc{Grip} for constructing a minimal dependency network.

We obtain a candidate set $C$ after applying $\textsc{UDS}_{stop}$\footnote{In order to fasten computations we implemented a variation of \textsc{UDS}, named $\textsc{UDS}_{stop}$. $\textsc{UDS}_{stop}$ terminates after the computation of the first row of matrix $f$ (see \citep{nguyen2016universal} for more details) if the relative difference between the first and last entry of this row is lower than 95\%. This indicates that conditioning $X_i$ on any of the initial bins does not decrease the cumulative entropy of $X_i$ and that $X_i$ and $X_j$ are independent.} to each pair of dimensions $X_i, X_j$ of a dataset $D$. If dimensions $X_i$ and $X_j$ are dependent, $\textsc{UDS}_{stop}$ outputs a candidate $c$ which has a form of a quadruple $c = ((X_i, X_j), \mathit{dsc}_{X_i}(X_j), h(X_i \arrowvert X_j), F(X_j, X_i))$. Each candidate contains a dependency relation $(X_i, X_j)$, an optimal discretization $\mathit{dsc}_{X_i}(X_j)$ of dimension $X_j$ for dimension $X_i$, conditional cumulative entropy $h(X_i \arrowvert X_j)$, obtained when discretization $\mathit{dsc}_{X_i}(X_j)$ is applied, and a dependency score $F(X_j, X_i)$, also known as fraction of information, see \Cref{FI}. 

Since discretization $\mathit{dsc}_{X_i}(X_j)$ induces a probability density function on dimension $X_j$, its conditional cumulative entropy $h(X_i \arrowvert X_j)$ can be computed in a closed-form as shown in \Cref{cce}. We use a variation of \textsc{UDS} for detecting candidate dependencies in a dataset exactly because we need the optimal discretizations and the corresponding entropy values at further steps of our algorithm.

As mentioned above, dependency relation $(X_i, X_j)$ is added to the candidate set $C$ only if it is informative, meaning that conditioning dimension $X_i$ on dimension $X_j$ results in a decrease of entropy, $h(X_i \arrowvert X_j) < h(X_i)$. Furthermore, elements of $C$ are sorted descending on their dependency scores $F(X_j, X_i)$ s.t. the most informative candidates are placed first. Constructing the candidate set in this way is meant to fasten the convergence rate of our algorithm to a local optimum.

The pseudocode of the procedure is shown below in \Cref{alg:alg_preprocessing}. In the next section we take a closer look at the main part of \textsc{Grip} - an iterative greedy search scheme for reconstructing a minimal dependency network.

\begin{algorithm}[H]
	\LinesNumberedHidden
	\KwData{dataset $D \in \mathbb{R}^{n\times{m}}$}
	\KwResult{candidate set $C$ sorted in descending order of $F(X_j, X_i)$}
	
	\Begin{
		$C = \{\}$\\
		\For{$X_i \in \{X_1,...,X_m\}$}{
			$h(X_i) = \textsc{CumulativeEntropy}(X_i)$\\
			\For{$X_j \in \{X_1,...,X_m\}\setminus{X_i}$}{
				$h(X_i \arrowvert X_j), \mathit{dsc}_{X_i}(X_j), F(X_j, X_i) = \textsc{UDS}_{stop}(X_i, X_j)$\\		
				\If{$h(X_i) - h(X_i \arrowvert X_j) > 0$}{
					$c = ((X_i, X_j), \mathit{dsc}_{X_i}(X_j), h(X_i \arrowvert X_j), F(X_j, X_i))$\\
					$C[i,j] = c$\\
				}
			}
		}
		$C = \textsc{Sort}(C)$\\	
	}
	\caption{\textsc{Preprocessing}}
	\label{alg:alg_preprocessing}
\end{algorithm}

\section{Search Scheme}\label{search}
Given a set $C$ of candidate dependencies, our goal is to design an efficient search scheme for reconstructing a minimal dependency network covering just enough dependency relations for a reliable estimation of the cumulative entropy of the data.
%We claim that such dependency list can be represented as a minimum dependency network $\textsc{MDN}_{d(\textbf{X})}$, see \Cref{sec:min_dn}, and induces a minimum decomposition of joint cumulative entropy $\hat{h}_{d^*(\textbf{X})}(X_1,...,X_m)$, see \Cref{sec:decompos}.
%Our goal is to decompose cumulative entropy of multivariate continuous-valued data $h(X_1,...,X_m)$ into lower dimensional terms in order to achieve a reliable approximation $\hat{h}_{d^*(\textbf{X})}(X_1,...,X_m)$ of the entropy value while preserving the structure of the data, and thereby reconstruct the underlying dependency network. \textsc{MCE} addresses this problem in the following way.

To this end, we present the reconstruction algorithm outlined in \Cref{alg:alg_search}. It receives an empty dependency network $\mathit{MDN}=(\{X_1,...,X_m\}, \emptyset)$, naive entropy decomposition $\hat{h}(X_1,...,X_m) = h(X_1) + ... + h(X_m)$ and candidate set $C$ as an input and iteratively extends both entities as long as $C$ in not empty. The search scheme works as follows. In each iteration, the algorithm picks and removes the most informative dependency $(X_i, X_j)$ from the candidate set $C$. It adds an edge $X_j \longrightarrow X_i$ to the network if it does not create cycles (since a dependency network is a DAG) and if the following conditions are fulfilled. Firstly, adding the edge should reduce the entropy of dependency $(X_i, Y_i)$, where $Y_i$ is a parent set of node $X_i$, more specifically $h(X_i \arrowvert Y^{'}_i) < h(X_i \arrowvert Y_i)$, where size of $Y^{'}_i$, the new parent set of $X_i$, is less or equal to the size of $Y_i \cup{X_j}$. Secondly, it should increase the dependency score of $s$, a connected component of $MDN$ that contains nodes $X_i$ and $X_j$, namely, $\mathit{UDS_r}(s\cup{(X_i, X_j)})>\mathit{UDS_r}(s)$. Otherwise, candidate $(X_i, X_j)$ is discarded and the search proceeds to the next iteration. Note that applying these two conditions together is meant to maximize the informativeness of the resulting network.

However, depending on whether the parent set $Y_i$ of node $X_i$ is empty, two scenarios of the network extension are possible. In case $X_i$ has no incoming edges, meaning that in the previous iteration of the reconstruction $X_i$ was considered to be independent of all other dimensions in $D$, the algorithm adds an edge $X_j \longrightarrow X_i$ to the network and the entropy $h(X_i \arrowvert X_j)$ of the candidate replaces $h(X_i)$ in the decomposition. Otherwise, the algorithm extends the existing dependency $(X_i, Y_i)$ with $(X_i, X_j)$. Since we want to keep the size of the network minimized, we need to find a subset of an extended parent set $Y^{'}_i \subseteq Y_i \cup{X_j}$ s.t. $(X_i, Y^{'}_i)$ is at least as informative as $(X_i, Y_i)$. We achieve this by joint discretization of dimensions $Y_i$ and $X_j$, which also provides a trade-off between the size of $Y^{'}_i$, the size of the resulting discretization and the estimation of $h(X \arrowvert Y^{'}_i)$. We explain the procedure in more detail in \Cref{jd}.

\begin{algorithm}[H]
	\LinesNumberedHidden
	\KwData{dataset $D\in \mathbb{R}^{n\times{m}}$, candidate set $C$, $\mathit{MDN}=(\{X_1,...,X_m\}, \emptyset)$, $\hat{h} = h(X_1) + ... + h(X_m)$}
	\KwResult{minimal dependency network $\mathit{MDN}$, minimal CE decomposition $\hat{h}_{\mathit{min}}$}
	\While{$C$ is not empty}{
		$((X_i, X_j), \mathit{dsc}_{X_i}(X_j), h(X_i \arrowvert X_j), F(X_j, X_i)) = \textsc{pop}(C)$\\
		\If{$\textsc{hasCycles}(\mathit{MDN} \cup{(X_i, X_j)})$}{
			\Continue
		}
		$s$ - connected component of $\mathit{MDN}$ that contains nodes $X_i$ and $X_j$ \\
		$Y_i$ - parent set of $X_i$ in $\mathit{MDN}$\\
		\eIf{$Y_i$ is empty}{
			\eIf{$\mathit{UDS_r}(s \cup{(X_i,X_j)}) > \mathit{UDS_r}(s)$}{
			$\mathit{MDN} \longleftarrow \mathit{MDN} \cup{(X_i,X_j)}$\\
			$\hat{h} \longleftarrow \hat{h} - h(X_i) + h(X_i \arrowvert X_j)$\\
			}{\Continue}
		}{
			$Y^{'}_i$, $\mathit{dsc}_{X_i}(Y^{'}_i) = \textsc{JointDiscretization}((X_i, Y_i), (X_i, X_j))$\label{line:jd}\\
			$h(X_i \arrowvert Y^{'}_i) = \textsc{CumulativeEntropy}(X_i, \mathit{dsc}_{X_i}(Y^{'}_{i}))$\\
			\eIf{$h(X_i \arrowvert Y^{'}_i) < h(X_i \arrowvert Y_i) \And \mathit{UDS_r}(s \cup{(X_i,X_j)}) > \mathit{UDS_r}(s)$}{\label{line:multi_check_start}
				$\mathit{MDN} \longleftarrow (\mathit{MDN} \setminus{(X_i, Y_i)})\cup{(X_i, Y^{'}_i)}$\\
				$\hat{h} \longleftarrow \hat{h} - h(X_i \arrowvert Y_i) + h(X_i \arrowvert Y^{'}_i)$\\
			}{\Continue}\label{line:multi:end}
		}	
		$C \longleftarrow{C \setminus{C[j,i]}}$\\
	}
	$\hat{h}_{\mathit{min}} \longleftarrow \hat{h}$	
	\caption{\textsc{Reconstruction}}
	\label{alg:alg_search}
\end{algorithm}

%TODO point out that controlling the local dep. score prevents adding redudant dependencies 

The dependency network and the entropy decomposition are updated accordingly to the output of the joint discretization. Note that we do not consider keeping both $h(X_i \arrowvert Y_i)$ and $h(X_i \arrowvert X_j)$ in the decomposition since in this way multivariate interactions between dimensions $Y_i$ and $X_j$ would be lost, leading to an overestimate in the cumulative entropy. Besides that, when extension is completed, the reverse of the candidate dependency $(X_j, X_i)$ is removed from $C$ in order to avoid creating cycles in the network. The algorithm terminates and outputs a minimal dependency network and a minimal entropy decomposition as soon as the candidate set is empty. The value of the minimal entropy decomposition is the estimated cumulative entropy of the dataset.

\section{Joint Discretization}\label{jd}
Now that we have explained the search scheme for reconstructing a minimal dependency network and estimating the cumulative entropy of a dataset, there is only one part missing for our algorithm to be complete. Namely, the procedure of creating a multivariate dependency $(X_i, Y_i)$ and estimating its cumulative entropy $h(X_i \arrowvert Y_i)$, where $Y_i$ consists of two or more dimensions. We present the joint discretization algorithm that selects the smallest subset $Y^{'}_i$ of a parent set $Y_i$ and an optimal discretization size such that the entropy $h(X_i \arrowvert Y^{'}_i)$ is minimized. The pseudocode is outlined in \Cref{alg:jd}.

Note that here we cannot use optimal discretization, see \Cref{sec:entropy}, as we do at the preprocessing step (\Cref{prep}), since it performs only univariate discretizations $\mathit{dsc}_{X_i}(X_j)$. Therefore, we have to either suggest an efficient algorithm for multivariate discretization of dimensions of $Y_i$ for dimension $X_i$, optimal in a way that the corresponding conditional cumulative entropy $h(X_i \arrowvert Y_i)$ is minimized, or, alternatively, we can make use of univariate discretizations $\mathit{dsc}_{X_i}(Y_{i_1}),...,\mathit{dsc}_{X_i}(Y_{i_k})$ that were constructed and stored at the preprocessing step. Clearly, the second option is more preferable for computational reasons.

A naive way to combine univariate discretizations into a multivariate one would be to simply merge all univariate discretizations $\mathit{dsc}_{X_i}(Y_{i_1}),...,$ $\mathit{dsc}_{X_i}(Y_{i_k}),$ where $k = \arrowvert Y_i \arrowvert$, into one $\mathit{dsc}_{X_i}(Y_i)$ and use it to compute the entropy $h(X_i \arrowvert Y_i)$. This approach is used in $\textsc{UDS}$ \citep{nguyen2016universal} for creating multivariate discretizations in order to estimate the conditional entropy terms $h(X_i \arrowvert X_1,...,X_{i-1})$. There are two major drawbacks of this approach. Firstly, the resulting discretization $\mathit{dsc}_{X_i}(X_1,...,X_{i-1})$ contains too many bins and therefore does not provide a good estimate of the joint pdf $p(X_1,...,X_{i-1})$. Secondly, this approach does not filter out those dimensions of $X_1,...,X_{i-1}$ that are independent of $X_i$. Hence, a dependency network constructed in this way would not be minimal. Our approach, on the other hand, addresses both of these issues. Let us now described it in more detail.

Initially, multivariate discretization $\mathit{dsc}_{X_i}$ contains only one bin, covering all data points of $X_i$, and a parent set $Y^{'}_i$ of node $X_i$ is empty. The general idea of the procedure is to add dimensions to $Y^{'}_i$ one by one, each time extending $\mathit{dsc}_{X_i}$ by one bin from the univariate discretization $\mathit{dsc}_{X_i}(y)$, $y \in Y_i$. Note that if the extension was discarded the algorithm does not consider the same bin again. We repeat this for each dimension in $Y_i$ and select that one which results in the minimum cumulative entropy $h(X_i \arrowvert Y^{'}_i)$. The procedure terminates if no further extensions of the discretization decrease the entropy and outputs the resulting parent set $Y^{'}_i$ and the mutlivariate discretization $\mathit{dsc}_{X_i}(Y^{'}_i)$.

\begin{algorithm}[H]
	\LinesNumberedHidden
	\KwData{current dependency $(X_i, Y_i)$, candidate dependency $(X_i, X_j)$}
	\KwResult{parent set $Y^{'}_i$, discretization $\mathit{dsc}_{X_i}(Y^{'}_i)$, $h(X_i \arrowvert Y^{'}_i)$}
	\Init{$\mathit{dsc}_{X_i} = \{\{1,...,n\}\}$, $Y^{'}_i = \{\}$, $Y_i \longleftarrow Y_i \cup X_j$, $l = \lvert Y_i \rvert$,}\\
	\hspace{4.9em} $\mathit{discretizations} = \{\mathit{dsc}_{X_i}(Y_1),..., \mathit{dsc}_{X_i}(Y_l)\}$\\
	\Begin{
		$\mathit{entropy} = \textsc{CumulativeEntropy}(X_i)$\\
		$\mathit{maxNrBins} = \textsc{max}(\lvert \mathit{dsc}_{X_i} \rvert)$, $\mathit{dsc}_{X_i} \in \mathit{discretizations}$\\
		\For{$\lambda \in \{1,...,\mathit{maxNrBins}\}$}{ \label{line:start_jd}
			$\mathit{tmpEntropy} = \{\}$\\
			\For{$\mathit{dsc}_k \in \mathit{discretizations}$}{
				\If{$\lvert \mathit{dsc}_k \rvert < \lambda$}{
					\Continue				
				}				
				$\hat{\mathit{dsc}}_k = \{\cup_{\lambda^{'}=1}^{\lambda-1} \mathit{bin}_{\lambda^{'}}, \mathit{bin}_\lambda, \cup_{\lambda^{''}=\lambda-1}^{\lvert \mathit{dsc}_k \rvert} \mathit{bin}_{\lambda^{''}} \}$\\
				$\hat{\mathit{dsc}}_{X_i} \longleftarrow \textsc{Merge}(\mathit{dsc}_{X_i}, \hat{\mathit{dsc}}_k)$\\
				$\mathit{tmpEntropy}[k] = \textsc{CumulativeEntropy}(X_i, \hat{\mathit{dsc}}_{X_i})$\\
			}
			$\mathit{minEnt} = \textsc{min}(\mathit{tmpEntropy})$\label{line:min_ce}\\
			\If{$\mathit{minEnt} \not < \mathit{entropy}$}{\label{line:stop1}
				\Return{$\mathit{dsc}_{X_i}$, $Y^{'}_i$, $\mathit{entropy}$}			
			}\label{line:stop2}
			$\mathit{entropy} \longleftarrow \mathit{minEn}t$\\
			$\mathit{dsc}_{\mathit{minEnt}} \longleftarrow \hat{\mathit{dsc}}_{\mathit{minEnt}}$\\
			$\mathit{dsc}_{X_i} \longleftarrow \hat{\mathit{dsc}}_{X_i}$\\
			$Y^{'}_i \longleftarrow Y^{'}_i \cup{Y_{\mathit{minEnt}}}$\\
		}\label{line:end_jd}	
	}	
	\caption{\textsc{JointDiscretization}}
	\label{alg:jd}
\end{algorithm}

All in all, joint discretization provides the following improvements over the naive approach. Firstly, we can control the size of the resulting discretization and avoid creating too many bins which leads to a better cumulative entropy estimation. Secondly, we can control the size of each dependency, keeping the resulting dependency network minimal.

\section{Variations of UDS}\label{sec:var_uds}
In addition to our main algorithm that we just described we also introduce two variations of \textsc{UDS} \citep{nguyen2016universal}. We name them \textsc{UDS\texttt{+}} and \textsc{UDS\texttt{++}}. Both algorithms are enhancements of the original method obtained after integrating the joint discretization step, explained in \Cref{jd}, into \textsc{UDS}. Combining \textsc{UDS} with joint discretization is supposed to improve its performance with respect to the problems that we are solving in this thesis, namely, dependency network reconstruction and cumulative entropy estimation. We evaluate both methods as well as the original \textsc{UDS} via comparison to our approach in \Cref{exp}.

\subsection*{Variation 1: UDS\texttt{+}}
As explained in \Cref{sec:uds}, \textsc{UDS} creates the full entropy decomposition, see Definition \ref{def:ce_full}, by conditioning each consequent dimension $X_i$ on all preceding dimensions $h(X_i \arrowvert X_1,...,X_{i-1})$ w.r.t. a pre-specified ordering. However, for efficiency reasons each dimension is discretized only once and then its discretization is reused for estimating the later conditional entropy terms.

More specifically, dimension $X_{i-1}$ is discretized such that it minimizes the conditional cumulative entropy $h(X_i \arrowvert X_1,...,X_{i-1})$, resulting in $\mathit{dsc}_{X_i}(X_{i-1})$. However, \textsc{UDS} does not discretize $X_{i-1}$ again for minimizing entropy of all subsequent terms where $X_{i-1}$ occurs, i.e., $h(X_{i+1} \arrowvert X_1,...,X_{i-1}, X_{i}),...,h(X_{m} \arrowvert X_1,...,X_{i-1},...,X_{m-1})$. Instead, discretization $\mathit{dsc}_{X_i}(X_{i-1})$ is used all over again, despite the fact that it was meant to estimate only $p(X_1,...,X_i)$, but not $p(X_1,...,X_i),...,p(X_1,...,X_{m-1})$. We suggest applying joint discretization to every CE term $h(X_i \arrowvert X_1,...,X_{i-1})$ in order to obtain a better cumulative entropy estimate through, firstly, filtering out spurious dependencies contained in dependency relation $(X_i, (X_1,...,X_{i-1}))$ and, secondly, making the resulting multivariate discretization $\mathit{dsc}_{X_i}(X_1,...,X_{i-1})$ less sparse.

\subsection*{Variation 2: UDS\texttt{++}}
\textsc{UDS\texttt{++}} resembles the first variation that we explained above, however, here we make an attempt to go even one step further. More specifically, we account for the fact that applying joint discretization to $h(X_i \arrowvert X_1,...,X_{i-1})$ will result in even better cumulative entropy estimate and a , if each dimension $X_i$ is re-discretized particularly for the term where it occurs. In this way multivariate discretization $\mathit{dsc}_{X_i}(X_1,..., X_{i-1})$ will be constructed from $\mathit{dsc}_{X_i}(X_1),...,\mathit{dsc}_{X_i}(X_{i-2})$, $\mathit{dsc}_{X_i}(X_{i-1})$, rather than from $\mathit{dsc}_{X_2}(X_1),...,\mathit{dsc}_{X_{i-1}}(X_{i-2}),$ $\mathit{dsc}_{X_i}(X_{i-1})$. 

\section{Complexity}\label{compl}
We start evaluating the complexity of \textsc{Grip} with the preprocessing step outlined in \Cref{alg:alg_preprocessing}. The cumulative entropy of dimension $X_i$ can be computed in $O(n)$ steps, where $n$ is the number of records in $X_i$, see Definition \ref{def:ce}. Execution of $\textsc{UDS}_{stop}$ takes $O(n)$ or $O(n\log{n})$\footnote{Optimal discretization can be computed in $O(n\log{n} +n\beta^{2})$ steps \citep{nguyen2016universal}, where $\beta$ is the size of the initial equal-frequency discretization. This parameter is usually set to $\beta=20$. The regularization step takes $O(n)$ steps. Therefore, the total complexity of \textsc{UDS} is $O(n\log{n} + n) \approx O(n\log{n})$ steps.}, for dimensions $X_i$ and $X_j$ being independent or dependent, respectively. Since we repeat this step for all pairs of dimensions $X_i, X_j, i \neq j$, in case every pair exhibits a dependency, preprocessing step has a complexity of $O(m^2n\log{n})$. In practice, however, the complexity is much lower, since a significant amount of pairs $X_i, X_j$ are pruned due to independence.

Another component of \textsc{Grip}, joint discretization (\Cref{alg:jd}), greedily constructs a multivariate dependency $(X_i, Y_i)$ and estimates its cumulative entropy while employing the minimal sufficient amount of dimensions and bins. Identifying the size of the largest discretization depends on the number of dimensions in $Y_i$ which is at maximum $m-1$. Since each discretization contains at most $\beta = 20$ bins (number of initial equal-frequent bins, set by optimal disctretization), this step takes $O(m)$ time. Iterating over discretizations, merging bins and computing entropy takes $O(m(n\log{n}) + n) \approx O(mn\log{n})$ steps, as we use the merge step of the merge sort algorithm. The procedure is repeated at most $\beta$ times, therefore in total joint discretization takes $O(\beta(mn\log{n}) + m + n) \approx O(mn\log{n})$ steps.

Complexity of the reconstruction step (\Cref{alg:alg_search}) depends on the size of the candidate set $C$, which is at most $m(m-1)/2$ when each pair of dimensions is dependent. Verifying if a dependency network contains cycles is performed via topological sorting and takes $O(m + m^2)$ steps. Further, extracting a connected component $s$ where nodes $X_i$ and $X_j$ belong takes $O(m^{2})$ steps, as one has to find all paths to those nodes in the network. Extracting the parent set of $X_i$ is constant $O(1)$, since elements of the entropy decomposition are stored ascending on the dimensions ids. Computing $UDS_r$ score takes $O(m)$ steps, see Definition \ref{def:uds_rev}. All in all, complexity of the reconstruction, including the joint discretization step is $O(m^2(n + 2m^2 + mn\log{n})) \approx O(m^3n\log{n})$. Prior to the reconstruction we initialize an empty dependency network and a naive entropy decomposition. This takes $O(m)$ steps.

In total, \textsc{Grip} takes $O(m^2n\log{n} + m + m^3n\log{n}) \approx O(m^3n\log{n})$ steps to converge, what makes is cubic in the number of dimensions and quasilinear in the number of data records.