\chapter{Theory}\label{theory}
In this chapter we describe how to reconstruct a dependency network of a continuous-valued dataset using the cumulative entropy and introduce the concept of an entropy decomposition. Furthermore, we define the dependency score of a reconstructed network based on the entropy decomposition as well as the concept of empirical expected cumulative entropy. Last but not least, we outline the problem statement.

\section{Dependency Network Reconstruction}\label{sec:min_dn}
The goal of this thesis is to non-parametrically reconstruct a dependency network, defined in \Cref{sec:notation}, that describes a minimal number of interactions sufficient for a reliable estimate of the cumulative entropy of a dataset. We call such network a minimal dependency network and denote it as $\mathit{MDN}$. 

In order to reconstruct a dependency network, one usually utilizes probabilistic modeling. In this way, dependencies are identified and estimated directly from the underlying probability distributions, while the size of the network can be minimized by considering only the strongest dependencies. This approach is known as Bayesian modeling. However, there are two issues with this approach. Firstly, Bayesian networks do not estimate the information content of a dataset and thus, do not fulfill our main requirement for a dependency network. Secondly, as we work with continuous-valued data, the main obstacle here is that the true probability density functions are usually not available in practice. Hence, they have to be estimated, for example, via kernel density estimation. While for the univariate case this method provides good results, estimating multivariate conditional distributions using kernels turns into a difficult problem. Moreover, the quality of estimation highly depends on the selection of the smoothing parameters. Since we aim for a non-parametric reconstruction, using kernels is not an option.

Alternatively, we suggest using entropy to detect dependencies and measure the information content of a reconstructed network. Since Shannon entropy requires probability functions as well, we decide to use the cumulative entropy as it can be computed directly from data sample, see Definition \ref{def:ce}. We estimate the multivariate cumulative entropy of a dataset by decomposing it into a sum of lower dimensional univariate and conditional entropy terms. Selecting the correct size and the components of a cumulative entropy decomposition coincides with the problem of minimal dependency network reconstruction.

Conditional entropy terms in a decomposition that correspond to dependency relations in a network can be computed by estimating the density functions via optimal discretization described in \Cref{sec:entropy}. However, the main issue here is that as dimensionalities of the conditional terms grow, the discretizations become more and more sparse and do not result in a reliable multivariate density estimate. Therefore, we suggest a joint discretization scheme for computing conditional entropy terms, introduced in \Cref{alg}, that provides a trade-off between the size of dependency relation, the discretization size and the estimated entropy.

We denote the cumulative entropy decomposition that describes the information content of a minimal network as a minimal cumulative entropy decomposition. Moreover, as we show in \Cref{exp}, a minimal decomposition provides a reliable estimate of the multivariate cumulative entropy. A more detailed discussion as well as the formal definition of a minimal entropy decomposition are given in the next section.

\section{Minimal Cumulative Entropy Decomposition}\label{sec:decompos}
In this section we discuss several approaches to estimate the cumulative entropy of a multivariate dataset through decomposition. First, we introduce the naive decomposition.
\begin{definition}{\textsc{Naive Cumulative Entropy Decomposition}}\label{def:ce_naive}

Multivariate cumulative entropy $h(X_1,...,X_m)$ can be decomposed into a sum of univariate cumulative entropy terms $h(X_i)$ covering each dimension $X_i$ of dataset $D$ as follows $$h(X_1,...,X_m) \approx \hat{h}_{\mathit{naive}}(X_1,...,X_m) = h(X_1) + h(X_2) + ... + h(X_m).$$
\end{definition}
An advantage of the naive approach is that the conditional densities do not have to be estimated - each term $h(X_i)$ can be computed in closed-form on empirical data using \Cref{empirical_ce}. However, since the naive decomposition does not describe any interactions between the dimensions of dataset $D$, it cannot be used for constructing a dependency network. Moreover, the cumulative entropy estimated in this way is, clearly, overestimated, unless all dimensions of $D$ are indeed independent of each other.

An alternative version of entropy decomposition was implicitly suggested by \cite{nguyen2013cmi} and follows from the numerator of the universal dependency score, see Definition \Cref{sec:uds}. Formal definition is presented below.

\begin{definition}{\textsc{Full Cumulative Entropy Decomposition}}\label{def:ce_full}

Multivariate cumulative entropy $h(X_1,...,X_m)$ can be decomposed as a sum of conditional entropy terms, more specifically
\begin{align*}
h(X_1,...,X_m) \approx \hat{h}_{full}(X_1,...,X_m) &= h(X_{\sigma(1)}) + \sum_{i=2}^{m} h(X_{\sigma(i)} \arrowvert X_{\sigma(1)},..., X_{\sigma(i-1)}),
\end{align*}
where $\sigma$ is an ordering on dimensions $X_1,...,X_m$ s.t. $h(X_{\sigma(1)}) \geq ... \geq h(X_{\sigma(m)})$.
\end{definition}
The full CE decomposition provides a more precise entropy estimation in comparison to the naive one.
However, it always implies that each dimension is dependent on all other dimensions in a dataset, regardless of whether a dependency actually exists. Therefore, the full decomposition results in a fully connected dependency network, which is clearly not what we are after.

Note that including term $h(X_i \arrowvert X_j)$ to a decomposition is justified only when dimensions $X_i$ and $X_j$ are dependent. Otherwise, the conditional entropy $h(X_i \arrowvert X_j)$ is equal to $h(X_i)$, since $X_j$ contains no relevant information about $X_i$. In practice, however, given a data sample of a limited size it occurs that $h(X_i \arrowvert X_j) < h(X_i)$ for independent dimensions. Hence, eliminating those entropy terms that represent spurious dependencies and keeping the decomposition brief is the key for constructing a minimal dependency network and obtaining a more reliable cumulative entropy estimation of a multivariate dataset. Next, we give the formal definition of a minimal cumulative entropy decomposition.
%TODO add how and when we construct it
\begin{definition}{\textsc{Minimal Cumulative Entropy Decomposition}}\label{def:ce_decomp}

Given a dataset $D$ with \textit{m} dimensions $X_1, ..., X_m$, a minimal cumulative entropy decomposition is defined as 
\begin{align*}
h(X_1,...,X_m) \approx \hat{h}_{\mathit{min}}(X_1,...,X_m) = \min_{\displaystyle\min \sum_{i=1}^{m} \lvert Y_i\rvert} \sum_{i=1}^m h(X_i \arrowvert Y_i),
\end{align*}
where $Y_i \subseteq \{X_1,...,X_m, \emptyset\} \setminus X_i$.
\end{definition}
Coarsely speaking, a minimal entropy decomposition has to cover all dimensions of $D$ and employ the smallest amount of dependencies sufficient for reflecting the structure of the data. Construction of a minimal entropy decomposition corresponds to the construction of a minimal dependency network.

An improvement of the minimal decomposition over the full one, see Definition \ref{def:ce_full}, is that it allows for cases when some dimensions are independent of all others in a dataset, which happens quite often in practice. Independent dimensions do not belong to any parent set $Y_i$ and are represented in the decomposition as univariate entropy terms $h(X_j)$. Moreover, minimal cumulative entropy decomposition is invariant under permutations of dimensions.

Now that we have introduced a new definition of the cumulative entropy decomposition we also have to adjust the universal dependency score introduced in \Cref{sec:uds}, since it is based on the full entropy decomposition. In the new settlement, the numerator of the score does not contain every element of the entropy decomposition, but only the conditional ones, $h(X_i \arrowvert Y_i)$, where $\lvert Y_i \rvert \neq 0$. The normalization term, on the other hand, contains all dimensions $X_i$ included to the nominator as well as independent dimensions. This means that the revised dependency score, see Definition \ref{def:uds_rev}, is essentially the normalized cumulative mutual information of the dependent subspaces of a dataset. The revised score, on the contrary to the universal score, is computed according to the dependencies represented in a minimal dependency network and, therefore, does not require any ordering on dimensions.

\begin{definition}{\textsc{Revised Universal Dependency Score}}\label{def:uds_rev}
$$\mathit{UDS}_r(X_1,...,X_m) = \frac{\displaystyle\sum_{i=1}^{k} h(X_i) - h(X_i \arrowvert Y_i)}{\displaystyle\sum_{i=1}^{k+l} h(X_i)},$$
where $k$ is the number of conditional entropy terms and $l$ is the number of independent dimensions in the minimal cumulative entropy decomposition $\hat{h}_{\mathit{min}}(X_1,...,X_m)$.
\end{definition}
Note that in order to ensure a fair normalization, we do not include to the denominator those dimensions that belong to the parent sets of other dimensions. 

%Therefore, $k+l \neq m$. Consider an example: let $m=6$ and $\hat{h}_{min}(X_1,...,X_6) = h(X_1) + h(X_2 \arrowvert X_1,X_3) + h(X_3) + h(X_4 \arrowvert X_5) + h(X_5) + h(X_6).$ The revised dependency score is computed as follows $$\textsc{UDS}_r(X_1,...,X_6) = \frac{\displaystyle h(X_2)-h(X_2 \arrowvert X_1, X_3) + h(X_4) - h(X_4 \arrowvert X_5)}{\displaystyle h(X_2) + h(X_4) + h(X_6)}.$$
% The revised universal dependency score has the same properties as the original one \citep{nguyen2016universal}. We postpone the proof to the Appendix.S

\section{Expected Empirical Cumulative Entropy}\label{expected_ce}
In order to assure that a discretization $\mathit{dsc}_{X_i}(X_j)= \{bin_1,...,bin_\lambda\}$ provides a good estimate of the conditional cumulative entropy $h(X_i \arrowvert X_j)$, we introduce a concept of expected conditional cumulative entropy $\mathbb{E}_{\mathit{dsc}}[h(X_i \arrowvert X_j)]$ of dependency relation $(X_i, X_j)$ given the amount of bins in discretization $\mathit{dsc}_{X_i}(X_j)$ and the size of each bin. A direct computation of the expected CE comes down to an average conditional CE over all possible discretizations of $X_j$ that have the same parameters (discretization size and bins sizes) as $\mathit{dsc}_{X_i}(X_j)$. Let $n$ be the amount of records in dataset $D$, then there are $g = {\binom{n}{\lvert bin_1 \rvert} \cdot ... \cdot \binom{n - (\lvert bin_1 \rvert + ... + \lvert bin_{\lambda-1} \rvert)}{\lvert bin_{\lambda} \rvert}}$ possibilities to arrange $n$ records into a discretization of size $\lambda$. Thus, the expected conditional entropy can be computed as
$$\mathbb{E}_{\mathit{dsc}}[h(X_i \arrowvert X_j)]= \frac{1}{g} \cdot \sum_{q=1}^{g} h_{\mathit{dsc_q}}(X_i \arrowvert X_j).$$
Note that for every discretization $dsc_q$ the entropy $h_{dsc_q}(X_i \arrowvert X_j)$ has to be recomputed which is certainly impractical. As such, we perform $\alpha = 1000$ independent permutations of the discretized dimension $\sigma_1(dsc_{X_i}(X_j)),..., \sigma_\alpha(dsc_{X_i}(X_j))$, resulting in $\alpha$ different entropy values $h_{\sigma_1(\mathit{dsc})}(X_i \arrowvert X_j),..., h_{{\sigma_\alpha(\mathit{dsc})}}(X_i \arrowvert X_j)$. The expected empirical cumulative entropy is defined as an average over $\alpha$ cumulative entropies, namely
$$\mathbb{E}_{\mathit{dsc}}^{emp}[h(X_i \arrowvert X_j)] = \frac{1}{\alpha} \sum_{q=1}^{\alpha} h_{\sigma_q(\mathit{dsc})}(X_i \arrowvert X_j).$$
We say that discretization $\mathit{dsc}_{X_i}(X_j)$ provides a good estimate of the conditional entropy $h(X_i \arrowvert X_j)$ if $h_{\mathit{dsc}}(X_i \arrowvert X_j) > \mathbb{E}_{\mathit{dsc}}^{\mathit{emp}}[h(X_i \arrowvert X_j)]$.

\section{The Problem}\label{sec:problem}
Now that we have explained the theory behind our algorithm, we can finally define the problem statement.

\paragraph*{Minimal dependency network reconstruction problem}
\textit{Given a continuous-valued dataset $D \in \mathbb{R}^{n\times{m}}$ find a directed acyclic graph $\mathit{MDN} = (\mathcal{V}, \mathcal{E})$ which corresponds to a minimal cumulative entropy decomposition $\hat{h}_{\mathit{min}}(X_1,...,X_m)$.}

When solving the problem exhaustively, one would need to search over the whole set of directed acyclic graphs of a particular size and select the one with the least number of edges and the smallest cumulative entopy. However, according to \cite{robinson1977counting}, for $m$ labeled nodes the number directed acyclic graphs can be computed through the recursive relation 
$$a_{m} = \sum_{k=1}^m (-1)^{k-1} \binom{m}{k} 2^{k(m-k)}a_{m-k},$$ where $a_0 = 1.$  Clearly, the search space is prohibitively large, as already for 10 nodes the amount of DAGs exceeds $2^{61}$. In fact, it has been proven that the optimal construction of a Bayesian network is NP-hard \citep{chickering1994learning}. This also holds for the construction of a dependency network, as it is a version of a Bayesian network. Hence, in order to solve this problem we resort to heuristics. A detailed description of the proposed approach is presented in the next chapter.