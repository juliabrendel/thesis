\chapter{Background}\label{back}
The goal of this thesis is to reconstruct a dependency network that covers just enough dependency relations to reflect the structure of the data by estimating its information content. In this chapter we introduce notations and concepts essential for developing our approach, such as dependency network, entropy, multivariate dependency score and optimal discretization.

\section{Notation}\label{sec:notation}
We consider an \textit{m}-dimensional dataset $D \in \mathbb{R}^{n\times{m}}$, each dimension $X_i \in D$ contains $n$ records. Let $p(X_i)$ be a probability distribution function and $P(X_i)$ - a cumulative distribution function of dimension $X_i$.

We want to model dependency relations in the data as a directed acyclic graph $DN=(\mathcal{V}, \mathcal{E})$, where the set of nodes $\mathcal{V}$ covers all dimensions $X_1,...,X_m$ of a dataset $D$ and there exists an edge $X_j \longrightarrow X_i$ if dimension $X_i$ is dependent on dimension $X_j$. Disconnected nodes represent dimensions which are conditionally independent of all others. We denote $X_i$ and all dimensions dependent on $X_i$ as a dependency relation $(X_i, Y_i)$. In the graph, $Y_i$ is represented as a parent set of $X_i$. The strength of each dependency $(X_i, Y_i)$ is equivalent to its informativeness, i.e., the proportional reduction of uncertainty about $X_i$ by observing $Y_i$, also known as fraction of information, see \Cref{FI}. We call such graph a dependency network of $D$. 

%Furthermore, we define a minimal dependency network as a maximally informative network containing a minimum number of edges. Our approach for learning a minimal dependency network from data is described in \Cref{alg}.

We summarize the most important notations used throughout this thesis in \Cref{fig:notation}.

\begin{table}[H]
    \centering
    \begin{tabular}[H]{l l c c} \arrayrulecolor{black} \toprule
        Notation & Description \\ \midrule
        $D$	 &	continuous-valued dataset	\\
        $X_i$ & dimension (variable), a column of $D$	\\
        $(X_i, Y_i)$ &	dependency relation between dimension $X_i$ and a set of dimensions $Y_i$\\
        $h(X_1,...,X_m)$ &	cumulative entropy (CE) of $D$	\\
        $\hat{h}(X_1,...,X_m)$ &	decomposition of the cumulative entropy of $D$ \\
        $h(X_i)$ &	cumulative entropy of dimension $X_i$ \\
        $h(X_i \arrowvert Y_i)$ & conditional cumulative entropy of dependency relation $(X_i, Y_i)$ \\
        $p(X_i)$	&	probability distribution function (pdf) of dimension $X_i$	\\
        $P(X_i)$	&	cumulative distribution function (cdf) of dimension $X_i$		\\
        $C$&		candidate set, contains candidate dependency relations of $D$	\\
        $\mathit{DN} = (\mathcal{V},\mathcal{E})$ &	dependency network of $D$	\\
        $\mathit{MDN} = (\mathcal{V},\mathcal{E})$ &	minimal dependency network of $D$	\\
        $\mathit{UDS}(X_1,...,X_m)$ &	universal dependency score of $D$	\\
        $\mathit{UDS}_r(X_1,...,X_m)$ &	revised universal dependency score of $D$	\\
        $F(X_j, X_i)$ & fraction of information of dimensions $X_i$ and $X_j$ \\
        $dsc_{X_i}(X_j)$ &	optimal discretization of dimension $X_j$ for dimension $X_i$		\\ \bottomrule
    \end{tabular}
    \caption{Summary of the commonly used notations}
    \label{fig:notation}
\end{table}

\section{Entropy}\label{sec:entropy}
We suggest using entropy to construct a dependency network. When speaking about entropy, one usually refers to the Shannon entropy \citep{shannon1948} that represents the expected amount of information in some discrete random variable $X$. Formal definition is given below.
%Shannon entropy is measured in bits and also represents the compression rate of a random variable.
%Further we provide background on such notions as Shannon and cumulative entropy, as well as cumulative mutual information, which are essential for our algorithm. We start with introducing the Shannon entropy which represents the amount of information in bits contained in a discrete random vector $X = (x_1,...,x_n)$.

\begin{definition}\textsc{Shannon Entropy}

Let $X$ be a discrete random variable with a sample $\{x_1,...,x_n\}$ and a probability distribution function $p(X)$. Then the Shannon entropy of $X$ is given as 
\begin{equation}
	H(X) = -\sum_{i=1}^{n} p(x_i)\log{p(x_i)}.
\end{equation}
\end{definition}
Shannon entropy is a well-studied, powerful measure and a core concept of the information theory. However, it cannot be applied to our task, since it works exclusively on discrete data, unless the underlying probability density function is known. We, on the contrary, aim to non-parametrically estimate information content of a multivariate continuous-valued dateset.

Attempts to extend Shannon entropy for continuous data include differential \citep{shannon1948} and cumulative entropy \citep{rao2004cumulative, di2009cumulative}. While the differential entropy was suggested by Shannon as a naive extension of the discrete entropy and lacks a number of essential properties, the cumulative entropy turned out to be a much more solid measure. Its main idea is to replace the probability density function $p(X)$ by a cumulative density function $P(X)$ and compute the value as an integral over the whole domain of variable $X$, see Definition \ref{def:ce}.

\begin{definition}\textsc{Cumulative Entropy (CE)}\label{def:ce}

Let $X$ be a continuous-valued random variable and $P(X)$ its cumulative distribution function. The cumulative entropy of $X$ is
\begin{equation}
	h(X) = -\int P(X)\log{P(X)}dx.
\end{equation}
\end{definition}
Unless the true cdf $P(X)$ is known, cumulative entropy can be computed in a closed-form from a data sample $\{x_1,...,x_n\}$  as follows
\begin{equation}\label{empirical_ce}
h(X) = -\displaystyle\sum_{i=1}^{n-1} (x_{i+1} - x_i)\frac{i}{n} \log \frac{i}{n},
\end{equation}
where records are stored in ascending order $x_1 \leq ... \leq x_n$. 

Now that we are able to compute the entropy of a continuous random variable $X$, we also want to know how its information content will change if there is a dependency relation between $X$ and some other random variable $Y$. This value is defined as conditional cumulative entropy and essentially corresponds to the information content of the edge $Y \longrightarrow X$ of a dependency network. It can be computed as follows 
\begin{equation}
	h(X \arrowvert Y) = \displaystyle\int h(X \arrowvert y)p(y)dy,
\end{equation}
where $p(Y)$ is the probability distribution function of $Y$.

%TODO explain joint discretization in more details!!!
An efficient way to compute conditional cumulative entropy on empirical data without knowing the distribution was suggested by \cite{nguyen2014multivariate}. Here, a pdf of $Y$ is induced by an optimal discretization of $Y$ into several bins $\{b_1,...,b_\lambda\}$, resulting in a minimal value of the conditional entropy, formally
\begin{equation}\label{cce}
h(X \arrowvert Y) = \displaystyle\sum_{k=1}^{\lambda} h(X \arrowvert b_k) \frac{\lvert b_k \rvert}{n}.
\end{equation}
Optimal discretization works as follows. A discretization is initialized containing some pre-specified number of equal-frequency bins. The conditional cumulative entropy of $X$ given $Y$ is computed for each permissible number of bins via a dynamic programming approach. The optimal value of $\lambda$ is then selected through a regularization scheme and provides a trade-off between the discretization size and the quality of entropy estimation. The resulting dicretization is projected onto dimension $X$ w.r.t. the records' ids in a dataset and the cumulative entropy of each bin $h(X \arrowvert b_k)$ is computed using \Cref{empirical_ce}. 

We evaluate the relative informativeness of a dependency $(X, Y)$ w.r.t. the case when variable $X$ is independent. This essentially coincides with the concept of fraction of information \citep{cavallo1987theory, reimherr2013quantifying} between variables $X$ and $Y$, formally
\begin{equation}\label{FI}
	F(Y, X) = \frac{h(X) - h(X \arrowvert Y)}{h(X)}.
\end{equation}
%TODO refer to multivariate dependency terms, motivate the need for CMI and a normalization - UDS
Next, we introduce a dependency score of a multivariate continuous-valued dataset and explain how to compute the cumulative entropy of a dependency $(X, Y)$ if the parent set of $X$ contains more than one variable.

\section{Dependency Score}\label{sec:uds}
Once a network is reconstructed, we want tos compute a multivariate dependency score of the dataset with regard to the dependency relations described by the network. To this end, we suggest following the idea of \textsc{UDS} introduced by \cite{nguyen2016universal}. It is also based on the cumulative entropy and provides a natural extension of the fraction of information to the multivariate case. It can be computed as follows.

\begin{definition}\textsc{Universal Dependency Score ($\mathit{UDS}$)}\label{def:uds}

Given a dataset $D$ with \textit{m} continuous-valued dimensions, $\mathit{UDS}$ of $X_1,...,X_m$ is a maximal normalized dependency score over all permutations of dimensions, formally
$$\mathit{UDS}(X_1,...,X_m) = \max_{\sigma \in \mathcal{F}_m} \frac{ \displaystyle \sum_{i=2}^{m} h(X_{\sigma(i)}) - h(X_{\sigma(i)} \arrowvert X_{\sigma(1)},...,X_{\sigma(i-1)})}{\displaystyle\sum_{i=2}^{m} h(X_{\sigma(i)})},$$
where $\mathcal{F}_m$ is a set of all permutations of dimensions $X_1,...,X_m$.
\end{definition}

In order to avoid an exhaustive search over each possible permutation of dimensions, the authors sort the dimensions descending on their cumulative entropy values.

Essentially, $\mathit{UDS}$ also implicitly constructs a dependency network when calculating the score. However, the parent set of each dimension is filled according to the pre-specified order and its size increases in each iteration. Hence, the resulting network is fully connected and may contains many spurious dependencies. We confirm this in \Cref{exp}. Since our network is constructed differently to that of $\mathit{UDS}$, we cannot use this score directly. Therefore, we suggest a modification of the universal dependency score in \Cref{theory}.

Here, the cumulative entropy of a multivariate dependency relation $(X_i \arrowvert X_1,...,X_{i-1})$ is computed after inducing the joint pdf $p(X_1,...,X_{i-1})$ by merging the corresponding optimal discretizations of dimensions $X_1,...,X_{i-1}$. We discuss pros and cons of this approach and suggest an alternative way for computing the cumulative entropy of a multivariate dependency in \Cref{jd}.

%Clearly, an exhaustive search over the whole set $\mathcal{F}_m, \lvert \mathcal{F}_m \rvert = m!$, is impractical, especially when working with high-dimensional data. Therefore, the authors resort to heuristics and introduce a practical version of the original \textsc{UDS}. Namely, a permutation of dimensions $\sigma \in \mathcal{F}_m$ is fixed, i.e. the dimensions are sorted in the descending order of their cumulative entropies, so that the most informative ones are placed first, $h(X_{\sigma(1)}) \geq ... \geq h(X_{\sigma(m)}).$ This approach, according to \cite{nguyen2016universal}, maximizes the dependency score and eliminates the need to search over all permutations of dimensions.

%Note that dimensions are continuous-valued, therefore computing the CE terms $h(X_{\sigma(i)})$ directly is impossible, unless one has access to the underlying cumulative distribution functions. Otherwise, CEs can be computed in closed-form on empirical data, as described in \Cref{sec:entropy}. While computation of the denominator of \textsc{UDS} is quite straightforward, the numerator is more tricky. Here, for each conditional term $h(X_{\sigma(i)} \arrowvert X_{\sigma(1)},...,X_{\sigma(i-1)})$ one has to estimate the joint conditional cdf $P(X_{\sigma(i)} \arrowvert X_{\sigma(1)},...,X_{\sigma(i-1)})$, which is a very difficult problem. Thus, the authors suggest an alternative way to do it, namely by discretizing each of the given dimensions $X_{\sigma(1)},...,X_{\sigma(i-1)}$ for dimension $X_{\sigma(i)}$ such that the corresponding conditional CE terms are minimized and the numerator is thereby maximized. The procedure is called \textit{optimal discretization}, it outputs a list of discretizations $dsc_{X_{\sigma(i)}}(X_{\sigma(1)}),...,dsc_{X_{\sigma(i)}}(X_{\sigma(i-1)})$ of each given dimension. Each discretization $dsc_{X_i}(X_j)$ splits the conditioned dimension $X_i$ into a set of bins $\{bin_1,...,bin_\lambda\}$, s.t. $\bigcup\limits_{k=1}^{\lambda} \lvert bin_i \rvert = n$, and thus induces a probability density function on $X_i$ which enables a closed-form computation of the term $h(X_i \arrowvert X_j)$, see \Cref{sec:entropy}.

%Note that for computational efficiency each dimension is discretized only once and then this discretization is propagated to the later terms of the numerator. Pros and cons of this approach are discussed in \Cref{dis}.

%We utilize \textsc{UDS} at the preprocessing step of our algorithm for constructing a candidate set composed of dependent pairs of dimensions $(X_i, X_j)$ as well as corresponding optimal discretizations $dsc_{X_i}(X_j)$, which we use later on for creating minimal joint CE decomposition. 
%For computational efficiency we introduce a criterion for early stopping of \textsc{UDS} if dimensions $X_i$ and $X_j$ are independent, see Appendix for more details.
