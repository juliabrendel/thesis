\contentsline {chapter}{Abstract}{vi}{dummy.2}
\vspace {1em}
\contentsline {chapter}{Acknowledgements}{viii}{dummy.3}
\vspace {1em}
\vspace {2em}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.5}
\contentsline {chapter}{\numberline {2}Related work}{5}{chapter.7}
\contentsline {paragraph}{Cumulative entropy}{5}{section*.8}
\contentsline {paragraph}{Dependency network reconstruction}{6}{section*.9}
\contentsline {paragraph}{Discovering and measuring dependencies}{6}{section*.10}
\contentsline {paragraph}{Entropy based discretization}{10}{section*.12}
\contentsline {chapter}{\numberline {3}Background}{11}{chapter.13}
\contentsline {section}{\numberline {3.1}Notation}{11}{section.14}
\contentsline {section}{\numberline {3.2}Entropy}{12}{section.16}
\contentsline {section}{\numberline {3.3}Dependency Score}{14}{section.25}
\contentsline {chapter}{\numberline {4}Theory}{17}{chapter.27}
\contentsline {section}{\numberline {4.1}Dependency Network Reconstruction}{17}{section.28}
\contentsline {section}{\numberline {4.2}Minimal Cumulative Entropy Decomposition}{18}{section.29}
\contentsline {section}{\numberline {4.3}Expected Empirical Cumulative Entropy}{20}{section.34}
\contentsline {section}{\numberline {4.4}The Problem}{21}{section.35}
\contentsline {chapter}{\numberline {5}Algorithm}{23}{chapter.37}
\contentsline {section}{\numberline {5.1}Outline}{23}{section.38}
\contentsline {section}{\numberline {5.2}Preprocessing}{24}{section.41}
\contentsline {section}{\numberline {5.3}Search Scheme}{25}{section.45}
\contentsline {section}{\numberline {5.4}Joint Discretization}{27}{section.48}
\contentsline {section}{\numberline {5.5}Variations of UDS}{29}{section.51}
\contentsline {section}{\numberline {5.6}Complexity}{30}{section.54}
\contentsline {chapter}{\numberline {6}Experiments}{31}{chapter.56}
\contentsline {section}{\numberline {6.1}Setup}{31}{section.57}
\contentsline {section}{\numberline {6.2}Quality of Entropy and Dependency Score Estimation}{32}{section.58}
\contentsline {section}{\numberline {6.3}Quality of Dependency Network Reconstruction}{36}{section.67}
\contentsline {section}{\numberline {6.4}Performance on Real Data}{40}{section.82}
\contentsline {chapter}{\numberline {7}Discussion}{47}{chapter.92}
\contentsline {chapter}{\numberline {8}Conclusion}{53}{chapter.95}
\vspace {2em}
\contentsline {chapter}{List of Figures}{54}{dummy.96}
\contentsline {chapter}{List of Tables}{57}{dummy.98}
\contentsline {chapter}{\numberline {A}More Results on Synthetic Data}{59}{appendix.100}
\contentsline {section}{\numberline {A.1}Cumulative Entropy and Dependency Score Estimation}{59}{section.101}
\contentsline {section}{\numberline {A.2}Dependency network reconstruction}{62}{section.106}
\vspace {2em}
\contentsline {chapter}{Bibliography}{67}{dummy.133}
