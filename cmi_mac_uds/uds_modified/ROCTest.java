import java.util.HashSet;

import uds.roc.ROCComp;

public class ROCTest {
	public static void main(String[] args) {
		try {
			String fileInput = args[0];
			String fileOutliers = args[1];
			String fileOutput = args[2];
			HashSet<String> outlierLabels = ROCComp.readOutlierLabels(fileOutliers);
			ROCComp.process(fileInput, fileOutput, outlierLabels);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}