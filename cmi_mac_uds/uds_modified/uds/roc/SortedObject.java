package uds.roc;

public class SortedObject implements Comparable<SortedObject> {
	public String classLabel;
	public double value;
	
	public SortedObject(String classLabel, double value) {
		this.classLabel = classLabel;
		this.value = value;
	}

	@Override
	public int compareTo(SortedObject o) {
		if (value > o.value) {
			return 1;
		} else if (value == o.value) {
			return 0;
		} else {
			return -1;
		}
	}
}