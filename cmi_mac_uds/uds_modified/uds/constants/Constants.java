package uds.constants;

import java.util.ArrayList;

import uds.score.Bin;

public class Constants {
	public static final int LOG_BASE = 2;
	public static final double MAX_ERROR = 0.00001;
	public static int BETA = 20;
	
	public static int METHOD = 1;
	public static final int UDS = 1;
	public static final int UDSR = 2;
	public static final int CMI = 3;
	public static final int MAC = 4;
	public static final int HICS = 5;
	
	public static ArrayList<Bin>[] FORMAC = null;
}