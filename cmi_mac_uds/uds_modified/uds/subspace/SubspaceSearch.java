package uds.subspace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.PriorityQueue;

public class SubspaceSearch {
	private static final int MAX_LEVEL = 100;
	
	public static Subspace[] find(double[][] data, int[][] rank, int W, int K) {
		int c = data[0].length;
		
		PriorityQueue<Subspace> R = new PriorityQueue<Subspace>();
		ArrayList<Subspace> Beam = new ArrayList<Subspace>();
		
		// create 2-dimensional subspaces
		PriorityQueue<Subspace> Cands = new PriorityQueue<Subspace>();
		for (int i = 0; i < c; i++) {
			for (int j = i + 1; j < c; j++) {
				ArrayList<Integer> dims = new ArrayList<Integer>();
				dims.add(i);
				dims.add(j);
				Subspace sub = new Subspace(dims);
				sub.setCorr(data, rank);
				
				if (Cands.size() < W) {
					Cands.offer(sub);
				} else {
					if (sub.corr > Cands.peek().corr) {
						Cands.poll();
						Cands.offer(sub);
					}
				}
			}
		}
		Beam.addAll(Cands);
		
		// update top K
		for (Subspace sub : Cands) {
			if (R.size() < K) {
				R.offer(sub);
			} else {
				if (sub.corr > R.peek().corr) {
					R.poll();
					R.offer(sub);
				}
			}
		}
		
		// mine higher dimensional subspaces
		int level = 3;
		while (Beam.size() != 0 && level <= MAX_LEVEL) {
			Cands = new PriorityQueue<Subspace>();
			generateCandidate(data, rank, Beam, Cands, W);
			
			// update top K
			for (Subspace sub : Cands) {
				if (R.size() < K) {
					R.offer(sub);
				} else {
					if (sub.corr > R.peek().corr) {
						R.poll();
						R.offer(sub);
					}
				}
			}
			
			Beam.clear();
			Beam.addAll(Cands);
			level++;
		}
		
		// return the result
		Subspace[] ret = new Subspace[R.size()];
		ret = R.toArray(ret);
		Arrays.sort(ret, Collections.reverseOrder());
		return ret;
	}
	
	private static void generateCandidate(double[][] data, int[][] rank, ArrayList<Subspace> Beam, PriorityQueue<Subspace> Cands, int W) {
		int n = Beam.size();
		HashSet<Key> map = new HashSet<Key>();
		for (int i = 0; i < n; i++) {
			map.add(new Key(Beam.get(i).dims));
		}
		
		HashSet<Key> addedSubspaces = new HashSet<Key>();
		for (int i = 0; i < n; i++) {
			ArrayList<Integer> a = Beam.get(i).dims;
			for (int j = i + 1; j < n; j++) {
				ArrayList<Integer> dims = new ArrayList<Integer>();
				dims.addAll(Beam.get(j).dims);
				dims.removeAll(a);
				if (dims.size() == 1) {
					ArrayList<Integer> b = new ArrayList<Integer>();
					b.addAll(a);
					b.add(dims.get(0));
					Collections.sort(b);
					
					Key key = new Key(b);
					if (addedSubspaces.contains(key)) {
						continue;
					} else {
						addedSubspaces.add(key);
					}
					
					boolean ok = true;
					for (int k = 0; k < b.size(); k++) {
						int t = b.remove(k);
						if (!map.contains(new Key(b))) {
							ok = false;
							break;
						}
						
						b.add(k, t);
					}
					if (!ok) {
						continue;
					}
					
					Subspace sub = new Subspace(b);
					sub.setCorr(data, rank);
					
					if (Cands.size() < W) {
						Cands.offer(sub);
					} else {
						if (sub.corr > Cands.peek().corr) {
							Cands.poll();
							Cands.offer(sub);
						}
					}
				}
			}
		}
	}
}