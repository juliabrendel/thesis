package uds.subspace;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.PriorityQueue;

import uds.constants.Constants;
import uds.cmi.CMIFunction;
import uds.hics.HICSFunction;
import uds.mac.MACFunction;
import uds.score.UDSFunction;

public class ScalableSearch {
	public static void find(double[][] data, int[][] rank, int K, String graphFileOutput,
						 	String graphCliqueOutput, String graphBinaryOutput, String fileOut) throws Exception {
		int c = data[0].length;
		System.out.println("generate graph...");
		generateGraph(data, rank, K, graphFileOutput);
		System.out.println("mine cliques...");
		ArrayList<Integer> vals = GraphMining.FindCliques(c, graphFileOutput, graphCliqueOutput, graphBinaryOutput);
		int numCliques = vals.get(0);
		int numDims = vals.get(1);
		System.out.println("merge cliques...");
		MDLMerge.mergeCliques(numDims, numCliques, graphBinaryOutput, graphCliqueOutput, fileOut);
	}
	
	private static void generateGraph(double[][] data, int[][] rank, int K, String graphFileOutput) throws IOException {
		int r = data.length;
		int c = data[0].length;

		PriorityQueue<Double> pq = new PriorityQueue<Double>();
		double[][] corr = new double[c][c];
		for (int i = 0; i < c; i++) {
			for (int j = i + 1; j < c; j++) {
				double[][] d = new double[r][2];
				int[][] ank = new int[r][2];
				for (int x = 0; x < r; x++) {
					d[x][0] = data[x][i];
					d[x][1] = data[x][j];
					ank[x][0] = rank[x][i];
					ank[x][1] = rank[x][j];
				}
				
				if (Constants.METHOD == Constants.CMI) {
					corr[i][j] = CMIFunction.computeScore(d, ank);
				} else if (Constants.METHOD == Constants.MAC) {
					corr[i][j] = MACFunction.computeScore(d, ank);
				} else if (Constants.METHOD == Constants.HICS) {
					corr[i][j] = HICSFunction.computeScore(d, ank);
				} else {
					corr[i][j] = UDSFunction.computeScore(d, ank);
				}
				corr[j][i] = corr[i][j];
				
				System.out.printf(String.format("finish for %d - %d\n\n", i, j));
				
				if (pq.size() < K) {
					pq.offer(corr[i][j]);
				} else if (corr[i][j] > pq.peek()) {
					pq.poll();
					pq.offer(corr[i][j]);
				}
			}
		}

		// binarize the coefficients
		double cutoff = pq.peek();
		for (int i = 0; i < c; i++) {
			for (int j = i + 1; j < c; j++) {
				if (corr[i][j] >= cutoff) {
					corr[i][j] = 1;
				} else {
					corr[i][j] = 0;
				}
			}
		}
		
		// write the graph to output file
		BufferedWriter writerGraph = new BufferedWriter(new FileWriter(new File(graphFileOutput)));
		for (int i = 0; i < c; i++) {
			for (int j = 0; j < c; j++) {
				if (corr[i][j] == 1) {
					writerGraph.write(Integer.toString(j) + " ");
				}
			} // end for
			
			writerGraph.newLine();
		} // end for
		
		writerGraph.close();
	}
}