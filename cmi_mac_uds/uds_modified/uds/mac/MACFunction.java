package uds.mac;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import uds.constants.Constants;
import uds.score.Bin;
import uds.score.Key;
import uds.score.UDSFunction;

public class MACFunction {
	public static double computeScore(double[][] data, int[][] preRank) {
		int r = data.length;
		int c = data[0].length;
		int[][] rank = preRank;
		if (rank == null) {
			rank = UDSFunction.generateRank(data);
		}
		int[][] discrete = new int[r][c];
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				discrete[i][j] = -1;
			}
		}
		
		// compute ces
		int[] colOrder = new int[c];
		double[] ces = new double[c];
		for (int j = 0; j < c; j++) {
			colOrder[j] = j;
			
			double[] a = new double[r];
			for (int i = 0; i < r; i++) {
				a[i] = data[rank[i][j]][j];
			}
			ces[j] = UDSFunction.computeCE(a, true);
		}
		
		// identify the first two dimensions
		double maxGlobal = Double.NEGATIVE_INFINITY;
		int dimX = -1;
		int dimY = -1;
		ArrayList<Bin> binsX = null;
		ArrayList<Bin> binsY = null;
		for (int i = 0; i < c; i++) {
			for (int j = i + 1; j < c; j++) {
				// get the optimal binnings for x
				performCEDisc(data, rank, ces, i, j);
				ArrayList<Bin>[] x = Constants.FORMAC;
				
				// get the optimal binnings for y
				performCEDisc(data, rank, ces, j, i);
				ArrayList<Bin>[] y = Constants.FORMAC;
				
				// loop through the binnings of x and y to identify the best combination
				double maxLocal = Double.NEGATIVE_INFINITY;
				int idX = -1;
				int idY = -1;
				int startX = (x.length == 1) ? 0 : 1;
				int startY = (y.length == 1) ? 0 : 1;
				for (int t = startX; t < x.length; t++) {
					int[][] disc = new int[r][2];
					ArrayList<Integer> dims = new ArrayList<Integer>();
					dims.add(0);
					dims.add(1);
					
					// update disc with the bin indices of x[t]
					populate(x[t], disc, 0);
					
					double entropyX = entropy(x[t], r);
					for (int s = startY; s < y.length; s++) {
						// update disc with the bin indices of y[s]
						populate(y[s], disc, 1);
						
						// compute the score
						double entropyY = entropy(y[s], r);
						double jointEntropy = entropy(disc, dims);
						double score = (entropyX + entropyY - jointEntropy) / Math.min(Math.log(x[t].size()) / Math.log(2), Math.log(y[s].size()) / Math.log(2));
						if (Math.abs(entropyX + entropyY - jointEntropy) < Constants.MAX_ERROR && (x[t].size() == 1 || y[s].size() == 1)) {
							score = 0;
						}
						if (score > maxLocal) {
							maxLocal = score;
							idX = t;
							idY = s;
						}
					}
				}
				
				if (maxLocal > maxGlobal) {
					dimX = i;
					dimY = j;
					binsX = x[idX];
					binsY = y[idY];
				}
			}
		}
		System.out.printf("bins of X = %d, bins of Y = %d\n\n", binsX.size(), binsY.size());
		populate(binsX, discrete, dimX);
		populate(binsY, discrete, dimY);
		double logX = Math.log(binsX.size()) / Math.log(2);
		double logY = Math.log(binsY.size()) / Math.log(2);
		double sumLog = logX + logY;
		double maxLog = Math.max(logX, logY);
		double sumEntropy = entropy(binsX, r) + entropy(binsY, r);
		ArrayList<Integer> procDims = new ArrayList<Integer>();
		procDims.add(dimX);
		procDims.add(dimY);
		
		// get the remaining dimensions
		ArrayList<Integer> dims = new ArrayList<Integer>();
		for (int i = 0; i < c; i++) {
			if (i != dimX && i != dimY) {
				dims.add(i);
			}
		}
		
		// process the remaining dimensions
		while (dims.size() > 0) {
			// compute joint entropy of dimensions already discretized
			HashMap<Key, ArrayList<Integer>> map = getMap(discrete, procDims);
			double jointEntropy = entropy(map, r);
			
			// identify the next dimension to be discretized
			double max = Double.NEGATIVE_INFINITY;
			int d = -1;
			for (int i = 0; i < dims.size(); i++) {
				double score = conditionalCE(data, discrete, rank, dims.get(i), procDims);
				score = (sumEntropy + ces[dims.get(i)] - score - jointEntropy) / (ces[dims.get(i)] + sumLog - maxLog);
				if (score > max) {
					max = score;
					d = i;
				}
			}
			
			double[] ret = computeScore(data, discrete, rank, colOrder, dims.get(d), procDims, sumEntropy, sumLog, maxLog, Constants.BETA);
			sumLog = ret[0];
			maxLog = ret[1];
			sumEntropy += ret[2];
			dims.remove(d);
		}
		
		// compute the final score
		ArrayList<Integer> allDims = new ArrayList<Integer>();
		for (int i = 0; i < c; i++) {
			allDims.add(i);
		}
		double jointEntropy = entropy(discrete, allDims);
		double t = (sumEntropy - jointEntropy) / (sumLog - maxLog);
		if (Math.abs(sumEntropy - jointEntropy) < Constants.MAX_ERROR && Math.abs(sumLog - maxLog) < Constants.MAX_ERROR) {
			t = 0;
		}
		
		// control check
		if ((t < 0 && Math.abs(t) > Constants.MAX_ERROR) || (t > 1 && Math.abs(t - 1) > Constants.MAX_ERROR)) {
			throw new ArithmeticException(String.format("Wrong score = %f, sumEntropy = %f, jointEntropy = %f, sumLog = %f, maxLog = %f", t, sumEntropy, jointEntropy, sumLog, maxLog));
		}
		
		return t;
	}
	
	private static double conditionalCE(double[][] data, HashMap<Key, ArrayList<Integer>> map, int x) {
		double sum = 0;
		Collection<ArrayList<Integer>> values = map.values();
		for (ArrayList<Integer> pids : values) {
			double[] a = new double[pids.size()];
			for (int i = 0; i < a.length; i++) {
				a[i] = data[pids.get(i)][x];
			}
			
			sum += pids.size() * UDSFunction.computeCE(a, true) / data.length;
		}
		
		return sum;
	}
	
	private static double conditionalCE(double[][] data, int[][] discrete, int[][] rank, int x, ArrayList<Integer> dims) {
		HashMap<Key, ArrayList<Integer>> map = new HashMap<Key, ArrayList<Integer>>();
		for (int i = 0; i < data.length; i++) {
			int actualID = rank[i][x];
			
			int[] vals = new int[dims.size()];
			for (int j = 0; j < vals.length; j++) {
				vals[j] = discrete[actualID][dims.get(j)];
			}
			
			ArrayList<Integer> pids = null;
			Key k = new Key(vals);
			if (map.containsKey(k)) {
				pids = map.get(k);
			} else {
				pids = new ArrayList<Integer>();
			}
			
			pids.add(actualID);
			map.put(k, pids);
		}
		
		return conditionalCE(data, map, x);
	}
	
	private static void populate(ArrayList<Bin> bins, int[][] discrete, int col) {
		int idx = 0;
		for (Bin b : bins) {
			for (Integer pid : b.getPoints()) {
				discrete[pid][col] = idx;
			}
			
			idx++;
		}
	}
	
	private static double entropy(ArrayList<Bin> bins, int r) {
		double sum = 0;
		for (Bin b : bins) {
			double freq = b.getPoints().size() * 1.0 / r;
			sum += -freq * Math.log(freq) / Math.log(2);
		}
		return sum;
	}
	
	private static double entropy(HashMap<Key, ArrayList<Integer>> map, int r) {
		double sum = 0;
		Collection<ArrayList<Integer>> values = map.values();
		for (ArrayList<Integer> pids : values) {
			double freq = pids.size() * 1.0 / r;
			sum += -freq * Math.log(freq) / Math.log(2);
		}
		
		return sum;
	}
	
	private static double entropy(int[][] discrete, ArrayList<Integer> dims) {
		double sum = 0;
		HashMap<Key, ArrayList<Integer>> map = getMap(discrete, dims);
		Collection<ArrayList<Integer>> values = map.values();
		for (ArrayList<Integer> pids : values) {
			double freq = pids.size() * 1.0 / discrete.length;
			sum += -freq * Math.log(freq) / Math.log(2);
		}
		
		return sum;
	}
	
	private static HashMap<Key, ArrayList<Integer>> getMap(int[][] discrete, ArrayList<Integer> dims) {
		HashMap<Key, ArrayList<Integer>> map = new HashMap<Key, ArrayList<Integer>>();
		for (int i = 0; i < discrete.length; i++) {
			int[] val = new int[dims.size()];
			for (int j = 0; j < val.length; j++) {
				val[j] = discrete[i][dims.get(j)];
			}
			
			Key k = new Key(val);
			ArrayList<Integer> pids = null;
			if (map.containsKey(k)) {
				pids = map.get(k);
			} else {
				pids = new ArrayList<Integer>();
			}
			pids.add(i);
			map.put(k, pids);
		}
		
		return map;
	}
	
	private static void performCEDisc(double[][] data, int[][] rank, double[] ces, int i, int j) {
		int r = data.length;
		
		double[][] d = new double[r][2];
		int[][] ra = new int[r][2];
		double[] ce = new double[2];
		int[] order = new int[2];
		
		for (int k = 0; k < r; k++) {
			d[k][0] = data[k][i];
			d[k][1] = data[k][j];
			ra[k][0] = rank[k][i];
			ra[k][1] = rank[k][j];
			ce[0] = ces[i];
			ce[1] = ces[j];
			order[0] = 0;
			order[1] = 1;
		}
		int[][] tmpDisc = new int[r][2];
		UDSFunction.computeScore2(d, tmpDisc, ra, ce, order, 0, 1, 0, Constants.BETA, false);
	}
	
	@SuppressWarnings("unchecked")
	private static double[] computeScore(double[][] data, int[][] discrete, int[][] rank, int[] colOrder, int X, ArrayList<Integer> C,
										 double sumEntropy, double sumLog, double maxLog, int numDesiredBins) {
		ArrayList<Bin> a = UDSFunction.formInitialBins(data, rank, colOrder, X, numDesiredBins);
		int beta = a.size();
		int actualCol = colOrder[X];
		int r = data.length;
		
		int sum = 0;
		int[] s = new int[beta];
		for (int i = 0; i < beta; i++) {
			ArrayList<Integer> pids = a.get(i).getPoints();
			sum += pids.size();
			s[i] = sum;
			
			for (Integer id : pids) {
				discrete[id][actualCol] = i;
			}
		}
		
		double[][] f = computeF(data, discrete, rank, X, C, beta);
		
		double[][] val = new double[beta + 1][beta];
		ArrayList<Bin>[][] b = new ArrayList[beta + 1][beta];
		Bin tmp = null;
		for (int i = 0; i < beta; i++) {
			b[1][i] = new ArrayList<Bin>();
			if (i == 0) {
				tmp = new Bin(a.get(0).getPoints());
				b[1][i].add(tmp);
			} else {
				ArrayList<Integer> pids = new ArrayList<Integer>(b[1][i - 1].get(0).getPoints());
				pids.addAll(a.get(i).getPoints());
				tmp = new Bin(pids);
				b[1][i].add(tmp);
			}
			
			val[1][i] = -f[0][i];
		}
		
		for (int i = 1; i < beta; i++) {
			int pos = -1;
			double max = Double.NEGATIVE_INFINITY;
			for (int j = 0; j < i; j++) {
				double t = s[j] * val[1][j] / s[i] - (s[i] - s[j]) * f[j + 1][i] / s[i];
				
				if (t > max) {
					max = t;
					pos = j;
				}
			}
			val[2][i] = max;
			
			b[2][i] = new ArrayList<Bin>();
			b[2][i].addAll(b[1][pos]);
			ArrayList<Integer> pids = new ArrayList<Integer>();
			for (int k = pos + 1; k <= i; k++) {
				pids.addAll(a.get(k).getPoints());
			}
			tmp = new Bin(pids);
			b[2][i].add(tmp);
		}
		
		for (int delta = 3; delta <= beta; delta++) {
			for (int i = delta - 1; i < beta; i++) {
				int pos = -1;
				double max = Double.NEGATIVE_INFINITY;
				for (int j = delta - 2; j < i; j++) {
					double t = s[j] * val[delta - 1][j] / s[i] - (s[i] - s[j]) * f[j + 1][i] / s[i];
					
					if (t > max) {
						max = t;
						pos = j;
					}
				}
				val[delta][i] = max;
				
				b[delta][i] = new ArrayList<Bin>();
				b[delta][i].addAll(b[delta - 1][pos]);
				ArrayList<Integer> pids = new ArrayList<Integer>();
				for (int k = pos + 1; k <= i; k++) {
					pids.addAll(a.get(k).getPoints());
				}
				tmp = new Bin(pids);
				b[delta][i].add(tmp);
			}
		}
		
		// perform regularization
		C.add(actualCol);
		double logBase = Math.log(Constants.LOG_BASE);
		double max = Double.NEGATIVE_INFINITY;
		int deltaMax = -1;
		for (int delta = 2; delta <= beta; delta++) {
			// update discrete data
			ArrayList<Bin> finalBins = b[delta][beta - 1];
			if (finalBins.size() != delta) {
				throw new ArithmeticException(String.format("Wrong number of bins: finalBins.size() = %d, delta = %d", finalBins.size(), delta));
			}
			
			int c = 0;
			for (Bin bin : finalBins) {
				ArrayList<Integer> pids = bin.getPoints();
				for (Integer id : pids) {
					discrete[id][actualCol] = c;
				}
				c++;
			}
			
			// compute entropy of X
			double entropyX = entropy(finalBins, r);
			
			// compute joint entropy
			double jointEntropy = entropy(discrete, C);
			
			// pick the one with minimum cost
			double log = Math.log(delta) / logBase;
			double cost = (sumEntropy + entropyX - jointEntropy) / (log + sumLog - Math.max(log, maxLog));
			if (cost > max) {
				max = cost;
				deltaMax = delta;
			}
		}
		
		// control check
		if ((max < 0 && Math.abs(max) > Constants.MAX_ERROR) || (max > 1 && Math.abs(max - 1) > Constants.MAX_ERROR)) {
			throw new ArithmeticException(String.format("Wrong score = %f", max));
		}
		
		// update discrete data
		ArrayList<Bin> finalBins = b[deltaMax][beta - 1];
		int c = 0;
		for (Bin bin : finalBins) {
			ArrayList<Integer> pids = bin.getPoints();
			for (Integer id : pids) {
				discrete[id][actualCol] = c;
			}
			
			c++;
		}
		
		double log = Math.log(deltaMax) / logBase;
		double[] ret = new double[3];
		ret[0] = sumLog + log;
		ret[1] = Math.max(log, maxLog);
		ret[2] = entropy(finalBins, r);
		System.out.printf("deltaMax = %d, number of final bins = %d\n\n", deltaMax, b[deltaMax][beta - 1].size());
		return ret;
	}
	
	private static double[][] computeF(double[][] data, int[][] discrete, int[][] rank, int X, ArrayList<Integer> C, int beta) {
		// create hypercubes
		int[] count = new int[beta];
		HashMap<Key, Integer>[] map = createHypercubes(discrete, rank, X, C, beta, count);
		
		// pre-computation for f[i][i]
		double[][] f = new double[beta][beta];
		for (int i = 0; i < beta; i++) {
			Collection<Integer> values = map[i].values();
			for (Integer n : values) {
				double freq = n * 1.0 / count[i];
				f[i][i] += -freq * Math.log(freq) / Math.log(2);
			}
			
			if (f[i][i] < 0 && Math.abs(f[i][i]) > Constants.MAX_ERROR) {
				throw new ArithmeticException(String.format("Wrong score f[i][i] = %f", f[i][i]));
			}
		}
		
		// pre-computation for f[j][i]
		for (int j = 0; j < beta; j++) {
			// initialize the hash
			HashMap<Key, Integer> tmpMap = new HashMap<Key, Integer>();
			Set<Key> keys = map[j].keySet();
			for (Key k : keys) {
				tmpMap.put(k, map[j].get(k));
			}
			
			// populate hash incrementally
			int c = count[j];
			for (int i = j + 1; i < beta; i++) {
				c += count[i];
				keys = map[i].keySet();
				for (Key k : keys) {
					int n = 0;
					if (tmpMap.containsKey(k)) { // key exists
						n = tmpMap.get(k) + map[i].get(k);
					} else {
						n = map[i].get(k);
					}
					tmpMap.put(k, n);
				}
				
				Collection<Integer> values = tmpMap.values();
				for (Integer n : values) {
					double freq = n * 1.0 / c;
					f[j][i] += -freq * Math.log(freq) / Math.log(2);
				}
				
				if (f[j][i] < 0 && Math.abs(f[j][i]) > Constants.MAX_ERROR) {
					throw new ArithmeticException(String.format("Wrong score f[j][i] = %f", f[j][i]));
				}
			}
		}
		
		return f;
	}
	
	@SuppressWarnings({ "unchecked" })
	public static HashMap<Key, Integer>[] createHypercubes(int[][] discrete, int[][] rank, int X, ArrayList<Integer> C, int beta, int[] count) {
		HashMap<Key, Integer>[] map = new HashMap[beta];
		int r = discrete.length;
		int c = 0;
		for (int i = 0; i < r; i++) {
			// cube of already discretized dimensions
			int[] vals = new int[C.size()];
			for (int j = 0; j < vals.length; j++) {
				vals[j] = discrete[i][C.get(j)];
			}
			
			// bin of X
			int idx = discrete[i][X];
			
			// create hash if it is not there yet
			if (map[idx] == null) {
				map[idx] = new HashMap<Key, Integer>();
			}
			
			// retrieve number of points
			Key k = new Key(vals);
			int n = 0;
			if (map[idx].containsKey(k)) {
				n = map[idx].get(k);
			}
			n++;
			
			// add list of ids to hash
			map[idx].put(k, n);
			count[idx]++;
			c++;
		}
		
		if (c != r) {
			throw new ArithmeticException("some records are missing!");
		}
		
		return map;
	}
}