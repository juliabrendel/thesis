package uds.hics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

import uds.score.UDSFunction;

public class HICSFunction {
	private static final int M = 50;
	private static final double ALPHA = 0.1;
	
	public static double computeScore(double[][] data, int[][] preRank) {
		int r = data.length;
		int c = data[0].length;
		int[][] rank = preRank;
		if (rank == null) {
			rank = UDSFunction.generateRank(data);
		}
		int[][] discrete = new int[r][c];
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				discrete[i][j] = -1;
			}
		}
		
		// pre-compute cumulative distribution function
		ArrayList<double[][]> cums = new ArrayList<double[][]>();
		for (int j = 0; j < c; j++) {
			double[] vals = new double[r];
			for (int i = 0; i < r; i++) {
				vals[i] = data[rank[i][j]][j];
			}
			cums.add(cumulativeDistribution(vals, false));
		}
		
		// compute contrast score using a Monte Carlo approach
		double score = 0;
		Random rand = new Random();
		int sampleSize = (int)(r * Math.pow(ALPHA, 1.0 / c));
		for (int n = 0; n < M; n++) {
			int x = rand.nextInt(c);
			HashSet<Integer> set = null;
			for (int j = 0; j < c; j++) {
				if (j != x) {
					HashSet<Integer> newSet = new HashSet<Integer>();
					int start = rand.nextInt(r - sampleSize);
					int end = start + sampleSize - 1;
					for (int i = start; i <= end; i++) {
						int actualID = rank[i][j];
						if (set == null || set.contains(actualID)) {
							newSet.add(actualID);
						}
					}
					set = newSet;
				}
			}
			
			double[] vals = new double[set.size()];
			int idx = 0;
			for (Integer pid : set) {
				vals[idx] = data[pid][x];
				idx++;
			}
			
			score += KolmogorovSmirnovTest(cums.get(x), vals);
		}
		
		return score / M;
	}
	
	/* compute the Kolmogorov-Smirnov distance between two sets of values */
	public static double KolmogorovSmirnovTest(double[][] cumulativeA,  double[] b) {
		// compute cumulative distributions of b
		double[][] cumulativeB = cumulativeDistribution(b, false);
				
		int posB;
		double max = Double.NEGATIVE_INFINITY;
		double val = -1;
		for (int i = 0; i < cumulativeA[0].length; i++) {
			posB = -1;
			for (int j = 0; j < cumulativeB[0].length; j++) {
				if (cumulativeB[0][j] == cumulativeA[0][i]) {
					posB = j;
					break;
				} else if (cumulativeB[0][j] > cumulativeA[0][i]) {
					posB = j - 1;
					break;
				}
				
				posB++;
			}
			
			if (posB != -1) {
				val = Math.abs(cumulativeA[1][i] - cumulativeB[1][posB]);
			} else {
				val = cumulativeA[1][i];
			}
			
			if (val > max) {
				max = val;
			}
		} // end for
		
		return max;
	}
	
	public static double[][] cumulativeDistribution(double[] a, boolean hasSorted) {
		if (hasSorted == false) {
			Arrays.sort(a);
		}
		
		int num_items = a.length;
		ArrayList<Double> distinctVals = new ArrayList<Double>();
		ArrayList<Integer> cumulativeCounts = new ArrayList<Integer>();
		for (int i = 0; i < num_items; i++) {
			distinctVals.add(new Double(a[i]));
			for (int j = i; j < num_items; j++) {
				if (a[j] > a[i])
				{
					i = j;
					break;
				}
			}
			
			cumulativeCounts.add(new Integer(i));
		}		
		
		int size = cumulativeCounts.size();
		double[][] ret = new double[2][size];
		for (int i = 0; i < size; i++) {
			ret[0][i] = distinctVals.get(i).doubleValue();
			ret[1][i] = cumulativeCounts.get(i).intValue() / (1.0 * a.length);
		}
		
		return ret;
	}
}