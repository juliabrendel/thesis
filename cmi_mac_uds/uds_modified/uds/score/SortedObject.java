package uds.score;

public class SortedObject implements Comparable<SortedObject> {
	public int id;
	public double value;
	
	public SortedObject(int id, double value) {
		this.id = id;
		this.value = value;
	}

	@Override
	public int compareTo(SortedObject o) {
		if (this.value > o.value) {
			return 1;
		} else if (this.value == o.value) {
			return 0;
		} else {
			return -1;
		}
	}
}