package uds.discovery;

public class Attribute {
	private int id;
	private Object lb;
	private Object ub;
	
	public Attribute(int id, Object lb, Object ub) {
		this.id = id;
		this.lb = lb;
		this.ub = ub;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public Object getLb() {
		return lb;
	}
	public void setLb(Object lb) {
		this.lb = lb;
	}
	
	public Object getUb() {
		return ub;
	}
	public void setUb(Object ub) {
		this.ub = ub;
	}
	
	@Override
	public String toString() {
		String s = Integer.toString(id) + ":" + lb.toString() + "," + ub.toString();
		return s;
	}
}