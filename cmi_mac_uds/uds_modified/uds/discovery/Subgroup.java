package uds.discovery;

import java.util.ArrayList;

import uds.cmi.CMIFunction;
import uds.constants.Constants;
import uds.hics.HICSFunction;
import uds.mac.MACFunction;
import uds.score.UDSFunction;

public class Subgroup implements Comparable<Subgroup> {
	private ArrayList<Attribute> cons;
	private ArrayList<Integer> pids;
	private double quality = -10;
	
	public Subgroup() {
		cons = new ArrayList<Attribute>();
		pids = new ArrayList<Integer>();
	}
	
	public ArrayList<Attribute> getCons() {
		return cons;
	}
	public void setCons(ArrayList<Attribute> cons) {
		this.cons = cons;
	}
	
	public ArrayList<Integer> getPoints() {
		return pids;
	}
	public void setPoints(ArrayList<Integer> pids) {
		this.pids = pids;
	}
	
	public double getQuality() {
		return quality;
	}
	public void setQuality(double quality) {
		this.quality = quality;
	}
	public void setQuality(Database db, double globalCorr) {
		int r = pids.size();
		int c = db.getNumNumericTarget();
		double[][] data = new double[r][c];
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				data[i][j] = db.getPoints().get(i).getNumericTarget()[j];
			}
		}
		
		if (Constants.METHOD == Constants.CMI) {
			this.quality = CMIFunction.computeScore(data, null) - globalCorr;
		} else if (Constants.METHOD == Constants.MAC) {
			this.quality = MACFunction.computeScore(data, null) - globalCorr;
		} else if (Constants.METHOD == Constants.HICS) {
			this.quality = HICSFunction.computeScore(data, null) - globalCorr;
		} else {
			this.quality = UDSFunction.computeScore(data, null) - globalCorr;
		}
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		for (Attribute att : cons) {
			s.append(att.toString());
			s.append("+++");
		}
		s.append(Integer.toString(pids.size()) + ";");
		s.append(Double.toString(quality));
		
		return s.toString();
	}

	@Override
	public int compareTo(Subgroup o) {
		if (this.getQuality() > o.getQuality()) {
			return 1;
		} else if (this.getQuality() == o.getQuality()) {
			return 0;
		}
		
		return -1;
	}
}