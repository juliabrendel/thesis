package uds.outlier;

public class OUTConstants {
	public static String FILEIN;
	public static String FILESUB;
	public static String FILEOUT;
	public static int COLS;
	public static int K;
	
	public static double EPSILON = 0.02;
	public static int MIN_PTS = 5;
	public static double OVR = 0.5;
}