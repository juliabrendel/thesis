package uds.outlier;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

public class DBScan {
	public static void mineClusters(double[][] data, ArrayList<ArrayList<Integer>> subspaces, String fileOutput) throws Exception {
		// load data
		int numPoints = data.length;
		int numDims = data[0].length;
		
		// mine clusters
		ArrayList<Cluster> candClusters = new ArrayList<Cluster>();
		ArrayList<Cluster> tmpClusters = null;
		int numSubspaces = subspaces.size();
		ArrayList<Integer> outliers = new ArrayList<Integer>();
		int numClusters;
		int nc;
		Cluster C1 = null;
		Cluster C2 = null;
		int csize1, csize2, cdims1, cdims2;
		int csizeovr, cdimovr;
		boolean isAdd;
		for (int sid = 0; sid < numSubspaces; sid++) {
			tmpClusters = computeClusters(data, numPoints, subspaces.get(sid), outliers);
			numClusters = tmpClusters.size();
			for (int j = 0; j < numClusters; j++) {
				isAdd = true;
				C1 = tmpClusters.get(j);
				cdims1 = C1.dims.size();
				csize1 = C1.pointIDs.size();
				//if (csize1 < Constants.MIN_PTS * 3)
				//	continue;
				nc = candClusters.size();
				for (int cid = 0; cid < nc; cid++) {
					C2 = candClusters.get(cid);
					cdims2 = C2.dims.size();
					csize2 = C2.pointIDs.size();
					
					// count overlapping dimensions
					cdimovr = 0;
					for (int d1 = 0; d1 < cdims1; d1++) {
						for (int d2 = 0; d2 < cdims2; d2++) {
							if (C1.dims.get(d1).intValue() == C2.dims.get(d2).intValue()) {
								cdimovr++;
							}
						}
					}
					if (cdimovr < OUTConstants.OVR * Math.min(cdims1, cdims2)) {
						continue;
					}
					
					// count overlapping points
					csizeovr = 0;
					for (int p1 = 0; p1 < cdims1; p1++) {
						for (int p2 = 0; p2 < cdims2; p2++) {
							if (C1.pointIDs.get(p1).intValue() == C2.pointIDs.get(p2).intValue()) {
								csizeovr++;
							}
						}
					}
					if (csizeovr < OUTConstants.OVR * Math.min(csize1, csize2)) {
						continue;
					}
					
					// there is overlap, keep the one with higher dimensionality
					if (cdims1 < cdims2) {
						tmpClusters.remove(j);
						j--;
						numClusters--;
						isAdd = false;
						break;
					} else if (cdims1 > cdims2) {
						candClusters.remove(cid);
						cid--;
						nc--;
					} else {
						if (csize1 <= csize2) {
							tmpClusters.remove(j);
							j--;
							numClusters--;
							isAdd = false;
							break;
						} else {
							candClusters.remove(cid);
							cid--;
							nc--;
						}
					} // end if
				} // end for
				if (isAdd) {
					candClusters.add(C1);
				}
			}
				
			System.out.println("subspace " + sid);
		}
		
		// write clusters
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File(fileOutput)));
		writer.write("OUTPUT CLUSTERS");
		writer.newLine();
		numClusters = candClusters.size();
		Cluster C = null;
		int clusterSize;
		for (int cid = 0; cid < numClusters; cid++) {
			C = candClusters.get(cid);
			for (int k = 0; k < numDims; k++) {
				if (C.dims.contains(k)) {
					writer.write("1 ");
				} else {
					writer.write("0 ");
				}
			}
			
			clusterSize = C.pointIDs.size();
			writer.write(Integer.toString(clusterSize) + " ");
			for (int k = 0; k < clusterSize; k++) {
				if (k != clusterSize - 1) {
					writer.write(Integer.toString(C.pointIDs.get(k)) + " ");
				} else {
					writer.write(Integer.toString(C.pointIDs.get(k)));
				}
			}
			
			//if (cid != numClusters - 1) {
			writer.newLine();
			//}
		} // end for
		
		writer.flush();
		writer.close();
	}
	
	public static ArrayList<Cluster> computeClusters(double[][] data, int numPoints, ArrayList<Integer> dims, ArrayList<Integer> outliers) {
		ArrayList<Cluster> ret = new ArrayList<Cluster>();
		boolean[] visited = new boolean[numPoints];
		boolean[] isClusterMember = new boolean[numPoints];
		for (int i = 0; i < numPoints; i++) {
			visited[i] = false;
			isClusterMember[i] = false;
		}
		
		// perform clustering
		int numDims = dims.size();
		ArrayList<Integer> tmpNeighbors = null;
		double epsilon = OUTConstants.EPSILON;
		epsilon = epsilon * Math.pow(numPoints, 1 - 1.0 / numDims);
		System.out.println(epsilon);
		//epsilon = epsilon * optimalHForSubspace(rows, numDims) / optimalHForSubspace(rows, 2);
		int MinPts = OUTConstants.MIN_PTS;
		Cluster C = null;
		for (int i = 0; i < numPoints; i++) {
			if (visited[i] == false) {
				visited[i] = true;
				tmpNeighbors = regionQuery(i, numPoints, data, dims, epsilon);
				if (tmpNeighbors.size() < MinPts) {
					outliers.add(new Integer(i));
					isClusterMember[i] = true;
				} else {
					C = new Cluster();
					C.dims = new ArrayList<Integer>();
					for (int j = 0; j < numDims; j++) {
						C.dims.add(dims.get(j));
					}
					
					expandCluster(i, tmpNeighbors, C, epsilon, MinPts, numPoints, data, dims, visited, isClusterMember);
					if (C.pointIDs.size() >= MinPts * 3) {
						ret.add(C);
					}
				}
			}
		}
		
		return ret;
	}
	
	public static void expandCluster(int pointID, ArrayList<Integer> neighbors, Cluster C, double epsilon, int MinPts, int numPoints,
									 double[][] data, ArrayList<Integer> S, boolean[] visited, boolean[] isClusterMember) {
		C.pointIDs.add(new Integer(pointID));
		isClusterMember[pointID] = true;
		int numNeighbors = neighbors.size();
		int curID;
		ArrayList<Integer> curNeighbors = null;
		int curNumNeighbors;
		for (int i = 0; i < numNeighbors; i++) {
			curID = neighbors.get(i).intValue();
			if (visited[curID] == false && curID != pointID) {
				visited[curID] = true;
				curNeighbors = regionQuery(curID, numPoints, data, S, epsilon);
				curNumNeighbors = curNeighbors.size();
				if (curNumNeighbors >= MinPts) {
					for (int j = 0; j < curNumNeighbors; j++)
						neighbors.add(curNeighbors.get(j).intValue());
					
					numNeighbors += curNumNeighbors;
				}
			} // end if
			
			if (isClusterMember[curID] == false) {
				C.pointIDs.add(new Integer(curID));
				isClusterMember[curID] = true;
			} // end if
		} // end for
	}
	
	public static ArrayList<Integer> regionQuery(int pid, int numPoints, double[][] data, ArrayList<Integer> S, double epsilon) {
		ArrayList<Integer> ret = new ArrayList<Integer>();
		double dist;
		for (int i = 0; i < numPoints; i++) {
			dist = Distance(S, data, pid, i);
			if (dist <= epsilon) {
				ret.add(new Integer(i));
			}
		}
		
		return ret;
	}
	
	public static double Distance(ArrayList<Integer> useddims, double[][] data, int pid, int qid) {
		int numDims = useddims.size();
		double sum = 0;
		int curdim;
		for (int i = 0; i < numDims; i++) {
			curdim = useddims.get(i);
			sum += Math.pow(Math.abs(data[pid][curdim] - data[qid][curdim]), 2);
		}
		
		return Math.pow(sum, 1.0 / 2);
	}
	
	public static double logGamma(double x) {
		double tmp = (x - 0.5) * Math.log(x + 4.5) - (x + 4.5);
		double ser = 1.0 + 76.18009173    / (x + 0)   - 86.50532033    / (x + 1)
                     	 + 24.01409822    / (x + 2)   -  1.231739516   / (x + 3)
                     	 +  0.00120858003 / (x + 4)   -  0.00000536382 / (x + 5);
		return tmp + Math.log(ser * Math.sqrt(2 * Math.PI));
	}
	
	public static double gamma(double x) { 
		return Math.exp(logGamma(x)); 
	}
	
	public static double optimalHForSubspace(int n, int numDims) {
		return ((8 * gamma(numDims / 2.0 + 1) / Math.pow(Math.PI, numDims / 2.0)) * (numDims + 4) * Math.pow(2 * Math.sqrt(Math.PI), numDims)) * Math.pow(n, -1.0 / (numDims + 4));
	}
}