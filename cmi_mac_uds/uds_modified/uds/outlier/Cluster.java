package uds.outlier;

import java.util.ArrayList;

public class Cluster {
	public ArrayList<Integer> dims;
	public ArrayList<Integer> pointIDs;
	
	public Cluster() {
		pointIDs = new ArrayList<Integer>();
	}
}