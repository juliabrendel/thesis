package uds.outlier;

import java.util.ArrayList;

public class LOF {
	public static ArrayList<Double> computeLOF(ArrayList<DataPoint> data, ArrayList<Integer> dims) {
		ArrayList<Double> ret = new ArrayList<Double>();
		computeNearestNeighbors(data, dims);
		computeLRD(data, dims);
		DataPoint curPoint = null;
		int numNeighbors;
		double sum;
		for (int i = 0; i < data.size(); i++) {
			curPoint = data.get(i);
			numNeighbors = curPoint.neighbors.size();
			sum = 0;
			for (int j = 0; j < numNeighbors; j++) {
				sum += data.get(curPoint.neighbors.get(j)).lrd;
			}
			
			if (curPoint.lrd == 0 || Double.isNaN(curPoint.lrd) || Double.isInfinite(curPoint.lrd) || Double.isNaN(sum) || Double.isInfinite(sum)) {
				ret.add(new Double(0));
			} else {
				ret.add(new Double(sum / (numNeighbors * curPoint.lrd)));
			}
		}
		
		return ret;
	}
	
	public static void computeLRD(ArrayList<DataPoint> data, ArrayList<Integer> dims) {
		DataPoint curPoint = null;
		int numNeighbors;
		double sum;
		for (int i = 0; i < data.size(); i++) {
			curPoint = data.get(i);
			numNeighbors = curPoint.neighbors.size();
			sum = 0;
			for (int j = 0; j < numNeighbors; j++) {
				sum += reachabilityDistance(dims, curPoint, data.get(curPoint.neighbors.get(j)));
			}
			
			curPoint.lrd = numNeighbors / sum;
		}
	}
	
	public static void computeNearestNeighbors(ArrayList<DataPoint> data, ArrayList<Integer> dims) {
		DataPoint curPoint = null;
		for (int i = 0; i < data.size(); i++) {
			curPoint = data.get(i);
			curPoint.neighbors.clear();
			curPoint.distToNeighbors.clear();
			curPoint.lrd = 0;
			for (int j = 0; j < data.size(); j++) {
				if (j != i) {
					curPoint.insertNeighbors(dims, data.get(j));
				}
			}
		}
	}
	
	public static double reachabilityDistance(ArrayList<Integer> dims, DataPoint a, DataPoint b) {
		double bKthDist = b.distToNeighbors.get(OUTConstants.K - 1).doubleValue();
		double dist = DataPoint.distanceLNorm(2, dims, a, b);
		return Math.max(bKthDist, dist);
	}
}