import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.opencsv.CSVReader;

public class ReadCsvToArray {
	
	
	public String[][] readData(String pathToCSVFile) {
		
		try (CSVReader reader = new CSVReader(new BufferedReader(
		          new FileReader(pathToCSVFile)));) {

		    List<String[]> lines = reader.readAll();
		    return lines.toArray(new String[lines.size()][]);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static void main(String[] args) {
		
		String path = "/Users/julia/Documents/studying/master/Master Thesis/thesis/CMI, MAC, UDS/mac/data/diagnosis.csv";
		String [][] data =  new ReadCsvToArray().readData(path);
	}

}
