import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import uds.constants.Constants;
import uds.outlier.DBScan;
import uds.outlier.OUTConstants;


public class DBTest {
	public static void main(String[] args) {
		try {
			readInputString(args);
			String file = OUTConstants.FILEIN;
			String fileSub = OUTConstants.FILESUB;
			String fileOut = OUTConstants.FILEOUT;
			int cols = OUTConstants.COLS;
			double[][] data = readData(file, cols);
			ArrayList<ArrayList<Integer>> subspaces = LOFTest.readSubspaces(fileSub);
			
			DBScan.mineClusters(data, subspaces, fileOut);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static double[][] readData(String file, int cols) throws IOException, IllegalArgumentException {
		Scanner scan = null;
		try {
			ArrayList<double[]> data = new ArrayList<double[]>();
			scan = new Scanner(new File(file));
			while (scan.hasNext()) {
				String[] vals = scan.nextLine().split(",");
				if (vals.length != cols + 1) {
					throw new IllegalArgumentException("missing data!");
				}
				
				double[] t = new double[cols];
				for (int j = 0; j < cols; j++) {
					t[j] = Double.parseDouble(vals[j]);
				}
				data.add(t);
			}
			
			double[][] ret = new double[data.size()][cols];
			for (int i = 0; i < ret.length; i++) {
				for (int j = 0; j < cols; j++) {
					ret[i][j] = data.get(i)[j];
				}
			}
			
			return ret;
		} catch (IOException e) {
			throw e;
		} finally {
			if (scan != null) {
				scan.close();
			}
		}
	}
	
	public static void readInputString(String[] args) throws Exception {	
		int i;
		int total = args.length - 1;
		
		// take FILEIN
		boolean found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILEIN"))
			{
				OUTConstants.FILEIN = args[i + 1];
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -FILEIN");
		}
		
		
		// take FILESUB
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILESUB"))
			{
				OUTConstants.FILESUB = args[i + 1];
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -FILESUB");
		}
		
		// take FILEOUT
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILEOUT"))
			{
				OUTConstants.FILEOUT = args[i + 1];
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -FILEOUT");
		}
		
		// take COLS
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-COLS"))
			{
				OUTConstants.COLS = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -COLS");
		}
		
		// take EPSILON
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-EPSILON"))
			{
				OUTConstants.EPSILON = Double.parseDouble(args[i + 1]);
				found = true;
				break;
			}
		
		// take MIN_PTS
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-MIN_PTS"))
			{
				OUTConstants.MIN_PTS = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		
		// take OVR
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-OVR"))
			{
				OUTConstants.OVR = Double.parseDouble(args[i + 1]);
				found = true;
				break;
			}
		
		// take METHOD
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-METHOD"))
			{
				Constants.METHOD = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
	} // end method
}