import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import uds.cmi.CMIFunction;
import uds.constants.Constants;
import uds.hics.HICSFunction;
import uds.mac.MACFunction;
import uds.score.UDSFunction;

/*
public class RuntimeTest {
	private static final double NOISE = 0.1;
	
	public static void main(String[] args) {
		try {
			String fileOut = args[0];
			int[] size = {1000, 2000, 4000, 6000, 8000, 10000};
			int[] dim = {2, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50};
			int[] methods = {1, 2, 3, 5, 4};
			int type = 3;
			
			double[][] X = null;
			BufferedWriter writer = null;
			double score = 0;
			for (int r : size) {
				for (int c : dim) {
					X = Simulation.createCorrelatedData(r, c, type, NOISE);
					for (int method : methods) {
						Constants.METHOD = method;
						long start = System.currentTimeMillis();
						String name = null;
						if (method == Constants.CMI) {
							score = CMIFunction.computeScore(X, null);
							name = "CMI";
						} else if (method == Constants.MAC) {
							score = MACFunction.computeScore(X, null);
							name = "MAC";
						} else if (method == Constants.HICS) {
							score = HICSFunction.computeScore(X, null);
							name = "HICS";
						} else {
							score = UDSFunction.computeScore(X, null);
							if (method == Constants.UDS) {
								name = "UDS";
							} else {
								name = "UDSR";
							}
						}
						long end = System.currentTimeMillis();
						writer = new BufferedWriter(new FileWriter(new File(fileOut + "_" + name + "_" + Integer.toString(r) + "_" + Integer.toString(c) + ".txt")));
						writer.write(String.format("%s - %d - %d: %f\n", name, r, c, (end - start) / 1000.0));
						writer.flush();
						writer.close();
						System.out.println(score);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}*/