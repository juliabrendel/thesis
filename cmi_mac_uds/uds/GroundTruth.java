import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;

public class GroundTruth {
	public static void main(String[] args) {
		try {
			read(args[0], args[1], Integer.parseInt(args[2]));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void read(String fileIn, String fileOut, int c) throws IOException {
		HashMap<String, ArrayList<Integer>> map = new HashMap<String, ArrayList<Integer>>();
		
		Scanner scan = new Scanner(new File(fileIn));
		int idx = 0;
		while (scan.hasNext()) {
			String s = scan.nextLine();
			String[] vals = s.split(",");
			String label = vals[c];
			ArrayList<Integer> pids = null;
			if (map.containsKey(label)) {
				pids = map.get(label);
			} else {
				pids = new ArrayList<Integer>();
			}
			pids.add(idx);
			map.put(label, pids);
			idx++;
		}
		scan.close();
		
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File(fileOut)));
		writer.write("TRUE CLUSTER");
		writer.newLine();
		Collection<ArrayList<Integer>> values = map.values();
		for (ArrayList<Integer> pids : values) {
			for (int i = 0; i < c; i++) {
				writer.write("1 ");
			}
			writer.write(Integer.toString(pids.size()));
			for (Integer id : pids) {
				writer.write(String.format(" %d", id));
			}
			writer.newLine();
		}
		writer.flush();
		writer.close();
	}
}