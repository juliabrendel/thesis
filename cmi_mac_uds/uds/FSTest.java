import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import uds.constants.Constants;
import uds.score.UDSFunction;
import uds.subspace.SSConstants;
import uds.subspace.ScalableSearch;


public class FSTest {
	public static void main(String[] args) {
		try {
			readInputString(args);
			String file = SSConstants.FILEIN;
			String fileOut = SSConstants.FILEOUT;
			String fileG = SSConstants.FILEG;
			String fileB = SSConstants.FILEB;
			String fileC = SSConstants.FILEC;
			int cols = SSConstants.COLS;
			double[][] data = readData(file, cols);
			int[][] rank = UDSFunction.generateRank(data);
			
			//long start = System.currentTimeMillis();
			int K = (int)(cols * Math.log(data.length) / Math.log(10));
			ScalableSearch.find(data, rank, K, fileG, fileC, fileB, fileOut);
			//long end = System.currentTimeMillis();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static double[][] readData(String file, int cols) throws IOException, IllegalArgumentException {
		Scanner scan = null;
		try {
			ArrayList<double[]> data = new ArrayList<double[]>();
			scan = new Scanner(new File(file));
			while (scan.hasNext()) {
				String s = scan.nextLine();
				if (s.contains(",,")) {
					continue;
				}
				
				String[] vals = s.split(",");
				if (vals.length != cols + 1) {
					throw new IllegalArgumentException("missing data!");
				}
				
				double[] t = new double[cols];
				for (int j = 0; j < cols; j++) {
					t[j] = Double.parseDouble(vals[j]);
				}
				data.add(t);
			}
			
			double[][] ret = new double[data.size()][cols];
			for (int i = 0; i < ret.length; i++) {
				for (int j = 0; j < cols; j++) {
					ret[i][j] = data.get(i)[j];
				}
			}
			
			return ret;
		} catch (IOException e) {
			throw e;
		} finally {
			if (scan != null) {
				scan.close();
			}
		}
	}
	
	public static void readInputString(String[] args) throws Exception {	
		int i;
		int total = args.length - 1;
		
		// take FILEIN
		boolean found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILEIN"))
			{
				SSConstants.FILEIN = args[i + 1];
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -FILEIN");
		}
		
		// take FILEOUT
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILEOUT"))
			{
				SSConstants.FILEOUT = args[i + 1];
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -FILEOUT");
		}
		
		// take FILEG
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILEG"))
			{
				SSConstants.FILEG = args[i + 1];
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -FILEG");
		}
		
		// take FILEB
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILEB"))
			{
				SSConstants.FILEB = args[i + 1];
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -FILEB");
		}
		
		// take FILEC
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILEC"))
			{
				SSConstants.FILEC = args[i + 1];
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -FILEC");
		}
		
		// take COLS
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-COLS"))
			{
				SSConstants.COLS = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -COLS");
		}
		
		// take METHOD
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-METHOD"))
			{
				Constants.METHOD = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
	} // end method
}