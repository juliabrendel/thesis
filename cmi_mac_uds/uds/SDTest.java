import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import uds.cmi.CMIFunction;
import uds.constants.Constants;
import uds.discovery.DataPoint;
import uds.discovery.Database;
import uds.discovery.SDConstants;
import uds.discovery.SubDisc;
import uds.discovery.Subgroup;
import uds.score.UDSFunction;

public class SDTest {
	public static void main(String[] args) {
		try {
			readInputString(args);
			String file = SDConstants.FILEIN;
			String fileOut = SDConstants.FILEOUT;
			int numNumeric = SDConstants.NUMERIC;
			int numOrdinal = SDConstants.ORDINAL;
			int numCategoric = SDConstants.CATEGORIC;
			int numNumericTarget = SDConstants.NUMTARGET;
			int numOrdinalTarget = SDConstants.ORDTARGET;
			int numCategoricTarget = SDConstants.CATTARGET;
			
			Database db = readData(file, numNumeric, numOrdinal, numCategoric, numNumericTarget, numOrdinalTarget, numCategoricTarget);
			long start = System.currentTimeMillis();
			double[][] data = new double[db.getPoints().size()][numNumericTarget];
			for (int i = 0; i < data.length; i++) {
				for (int j = 0; j < numNumericTarget; j++) {
					data[i][j] = db.getPoints().get(i).getNumericTarget()[j];
				}
			}
			double globalCorr = 0;
			if (Constants.METHOD == Constants.CMI) {
				globalCorr = CMIFunction.computeScore(data, null);
			} else {
				globalCorr = UDSFunction.computeScore(data, null);
			}
			
			Subgroup[] result = SubDisc.find(db, globalCorr);
			long end = System.currentTimeMillis();
			
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(fileOut)));
			for (Subgroup sub : result) {
				writer.write(sub.toString());
				writer.newLine();
			}
			writer.write(Long.toString(end - start));
			writer.newLine();
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static Database readData(String file, int numNumeric, int numOrdinal, int numCategoric, int numNumericTarget, int numOrdinalTarget, int numCategoricTarget) 
			throws IOException, IllegalArgumentException {
		Scanner scan = null;
		try {
			scan = new Scanner(new File(file));
			
			ArrayList<DataPoint> points = new ArrayList<DataPoint>();
			String s;
			String[] vals;
			double[] numeric;
			int[] ordinal;
			String[] categoric;
			double[] numericTarget;
			int[] ordinalTarget;
			String[] categoricTarget;
			DataPoint p = null;
			int n = numNumeric + numOrdinal + numCategoric + numNumericTarget + numOrdinalTarget + numCategoricTarget;
			while (scan.hasNext()) {
				s = scan.nextLine();
				vals = s.split(",");
				if (vals.length != n) {
					System.out.println(s);
					throw new IllegalArgumentException("missing data!");
				}
				
				numeric = new double[numNumeric];
				ordinal = new int[numOrdinal];
				categoric = new String[numCategoric];
				numericTarget = new double[numNumericTarget];
				ordinalTarget = new int[numOrdinalTarget];
				categoricTarget = new String[numCategoricTarget];
				
				for (int i = 0; i < numNumeric; i++) {
					numeric[i] = Double.parseDouble(vals[i]);
				}
				
				for (int i = 0; i < numOrdinal; i++) {
					ordinal[i] = Integer.parseInt(vals[i + numNumeric]);
				}
				
				for (int i = 0; i < numCategoric; i++) {
					categoric[i] = vals[i + numNumeric + numOrdinal];
				}
				
				for (int i = 0; i < numNumericTarget; i++) {
					numericTarget[i] = Double.parseDouble(vals[i + numNumeric + numOrdinal + numCategoric]);
				}
				
				for (int i = 0; i < numOrdinalTarget; i++) {
					ordinalTarget[i] = Integer.parseInt(vals[i + numNumeric + numOrdinal + numCategoric + numNumericTarget]);
				}
				
				for (int i = 0; i < numCategoricTarget; i++) {
					categoricTarget[i] = vals[i + numNumeric + numOrdinal + numCategoric + numNumericTarget + numOrdinalTarget];
				}
				
				p = new DataPoint(numeric, ordinal, categoric, numericTarget, ordinalTarget, categoricTarget);
				points.add(p);
			}
			
			Database db = new Database(points);
			db.setNumNumeric(numNumeric);
			db.setNumOrdinal(numOrdinal);
			db.setNumCategoric(numCategoric);
			db.setNumNumericTarget(numNumericTarget);
			db.setNumOrdinalTarget(numOrdinalTarget);
			db.createOrdinalTargetCount();
			db.createCategoricTargetCount();
			db.createIndividualCategoricTargetCount();
			db.createNumericTargetMax();
			return db;
		} catch (IOException e) {
			throw e;
		} finally {
			if (scan != null) {
				scan.close();
			}
		}
	}
	
	public static void readInputString(String[] args) throws Exception
	{	
		int i;
		int total = args.length - 1;
		
		// take FILEIN
		boolean found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILEIN"))
			{
				SDConstants.FILEIN = args[i + 1];
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -FILEIN");
		
		// take FILEOUT
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILEOUT"))
			{
				SDConstants.FILEOUT = args[i + 1];
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -FILEOUT");
		
		// take NUMERIC
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-NUMERIC"))
			{
				SDConstants.NUMERIC = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -NUMERIC");
		
		// take ORDINAL
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-ORDINAL"))
			{
				SDConstants.ORDINAL = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -ORDINAL");
		
		// take CATEGORIC
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-CATEGORIC"))
			{
				SDConstants.CATEGORIC = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -CATEGORIC");
		
		// take NUMTARGET
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-NUMTARGET"))
			{
				SDConstants.NUMTARGET = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -NUMTARGET");
		
		// take ORDTARGET
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-ORDTARGET"))
			{
				SDConstants.ORDTARGET = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -ORDTARGET");
		
		// take CATTARGET
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-CATTARGET"))
			{
				SDConstants.CATTARGET = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -CATTARGET");
		
		// take METHOD
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-METHOD"))
			{
				Constants.METHOD = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		
		// take MIN_COV
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-MIN_COV"))
			{
				SDConstants.MIN_COV = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		
		// take MAX_DEPTH
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-MAX_DEPTH"))
			{
				SDConstants.MAX_DEPTH = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		
		// take W
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-W"))
			{
				SDConstants.W = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		
		// take K
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-K"))
			{
				SDConstants.K = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
	} // end method
}