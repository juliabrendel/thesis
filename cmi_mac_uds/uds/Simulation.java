import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Random;

import uds.cmi.CMIFunction;
import uds.constants.Constants;
import uds.hics.HICSFunction;
import uds.mac.MACFunction;
import uds.score.UDSFunction;


public class Simulation {
	private static final double NOISE = 0.1;
	private static final int M = 100;
	
	public static void main(String[] args) {
		try {
			String fileOut = args[0];
			int r = Integer.parseInt(args[1]);
			int c = Integer.parseInt(args[2]);
			int type = Integer.parseInt(args[3]);
			Constants.METHOD = 2;
			
			int extra = 0;
			if (args.length > 4) {
				extra = Integer.parseInt(args[4]);
			}
			System.out.printf("extra = %d\n", extra);
			
			if (args.length > 5) {
				Constants.BETA = Integer.parseInt(args[5]);
			}
			System.out.printf("beta = %d\n", Constants.BETA);
			
			// generate data with no correlation and compute their scores
			double[] res = new double[M];
			double[][] X = null;
			for (int i = 0; i < M; i++) {
				X = createIndependentData(r, c + extra);
				if (Constants.METHOD == Constants.CMI) {
					res[i] = CMIFunction.computeScore(X, null);
				} else if (Constants.METHOD == Constants.MAC) {
					res[i] = MACFunction.computeScore(X, null);
				} else if (Constants.METHOD == Constants.HICS) {
					res[i] = HICSFunction.computeScore(X, null);
				} else {
					res[i] = UDSFunction.computeScore(X, null);
				}
			}
			
			// calculate the cutoff
			Arrays.sort(res);
			int pos = (int)(M * 0.75);
			double cutoff = res[pos];
			
			// generate data with correlation and compute their scores
			double[] scores = new double[M];
			for (int i = 0; i < M; i++) {
				X = createCorrelatedData(r, c, type, NOISE);
				if (Constants.METHOD == Constants.CMI) {
					scores[i] = CMIFunction.computeScore(X, null);
				} else if (Constants.METHOD == Constants.MAC) {
					scores[i] = MACFunction.computeScore(X, null);
				} else if (Constants.METHOD == Constants.HICS) {
					scores[i] = HICSFunction.computeScore(X, null);
				} else {
					scores[i] = UDSFunction.computeScore(X, null);
				}
			}
			
			// compute power
			int count = 0;
			for (int i = 0; i < M; i++) {
				if (scores[i] > cutoff) {
					count++;
				}
			}
			double t = count * 1.0 / M;
			
			// write the result
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(fileOut)));
			writer.write(Double.toString(t));
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static double[][] createIndependentData(int r, int c) throws Exception {
		Random rand = new Random();
		double[][] X = new double[r][c];
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				X[i][j] = rand.nextDouble();
			}
		}
		
		return X;
	}
	
	public static double[][] createCorrelatedData(int r, int c, int type, double sigmanoise) throws Exception {
		Random rand = new Random();
		double[][] X = new double[r][c];
		
		int l = c / 2;
		double[][] A = new double[l][l];
		double[][] B = new double[l][l];
		for (int i = 0; i < l; i++) {
			for (int j = 0; j < l; j++) {
				A[i][j] = rand.nextDouble();
				B[i][j] = rand.nextDouble() * 0.5;
			}
		}
		
		for (int i = 0; i < r; i++) {
			double[] Z = new double[l];
			double[] W = new double[l];
			for (int j = 0; j < l; j++) {
				Z[j] = rand.nextGaussian();
			}
			
			for (int j = 0; j < l; j++) {
				X[i][j] = 0;
				for (int k = 0; k < l; k++) {
					X[i][j] += A[j][k] * Z[k];
				}
			}
			
			for (int j = 0; j < l; j++) {
				W[j] = 0;
				for (int k = 0; k < l; k++) {
					W[j] += B[j][k] * X[i][k];
				}
			}
			
			for (int j = 0; j < l; j++) {
				if (type == 0) {
					X[i][j + l] = 2 * W[j] + 1 + rand.nextGaussian() * sigmanoise;
				} else if (type == 1) {
					X[i][j + l] = W[j] * W[j] - 2 * W[j] + rand.nextGaussian() * sigmanoise;
				} else if (type == 2) {
					X[i][j + l] = Math.log(Math.abs(W[j] + 1)) + rand.nextGaussian() * sigmanoise;
				} else if (type == 3) {
					X[i][j + l] = Math.sin(2 * W[j]) + rand.nextGaussian() * sigmanoise;
				}
			}
		}
		
		return X;
	}
}