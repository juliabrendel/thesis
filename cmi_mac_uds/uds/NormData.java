import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


public class NormData {
	private static final int MAXVAL = 10;
	
	public static void main(String[] args) {
		try {
			String file = args[0];
			String fileOut = args[1];
			int cols = Integer.parseInt(args[2]);
			int start = Integer.parseInt(args[3]);
			int end = Integer.parseInt(args[4]);
			processData(file, fileOut, cols, start, end);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void processData(String file, String fileOut, int cols, int start, int end) throws IOException, IllegalArgumentException {
		Scanner scan = null;
		try {
			ArrayList<String[]> prefix = new ArrayList<String[]>();
			ArrayList<double[]> data = new ArrayList<double[]>();
			ArrayList<String[]> suffix = new ArrayList<String[]>();
			
			double[] min = new double[end - start + 1];
			double[] max = new double[end - start + 1];
			for (int i = start; i <= end; i++) {
				min[i - start] = Double.POSITIVE_INFINITY;
				max[i - start] = Double.NEGATIVE_INFINITY;
			}
			
			scan = new Scanner(new File(file));
			while (scan.hasNext()) {
				String s = scan.nextLine();
				if (s.contains(",,")) {
					continue;
				}
				
				String[] vals = s.split(",");
				if (vals.length != cols) {
					throw new IllegalArgumentException("missing data!");
				}
				
				String[] pref = new String[start];
				double[] t = new double[end - start + 1];
				String[] suf = new String[cols - end];
				for (int j = 0; j < cols; j++) {
					if (j < start) {
						pref[j] = vals[j];
					} else if (j <= end) {
						t[j - start] = Double.parseDouble(vals[j]);
						
						if (t[j - start] < min[j - start]) {
							min[j - start] = t[j - start];
						}
						
						if (t[j - start] > max[j - start]) {
							max[j - start] = t[j - start];
						}
					} else {
						suf[j - end - 1] = vals[j];
					}
				}
				prefix.add(pref);
				data.add(t);
				suffix.add(suf);
			}
			
			int r = data.size();
			for (int i = 0; i < r; i++) {
				double[] point = data.get(i);
				for (int j = start; j <= end; j++) {
					if (max[j - start] == min[j - start]) {
						point[j - start] = 0;
					} else {
						point[j - start] = MAXVAL * (point[j - start] - min[j - start]) / (max[j - start] - min[j - start]);
					}
				}
			}
			
			// write data
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(fileOut)));
			for (int i = 0; i < r; i++) {
				String[] pref = prefix.get(i);
				double[] point = data.get(i);
				String[] suf = suffix.get(i);
				for (int j = 0; j < cols; j++) {
					if (j < start) {
						writer.write(pref[j] + ",");
					} else if (j <= end) {
						writer.write(Double.toString(point[j - start]));
						if (j != cols - 1) {
							writer.write(",");
						}
					} else {
						writer.write(suf[j - end - 1]);
						if (j != cols - 1) {
							writer.write(",");
						}
					}
				}
				
				if (i != r - 1) {
					writer.newLine();
				}
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			throw e;
		} finally {
			if (scan != null) {
				scan.close();
			}
		}
	}
}