import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import uds.constants.Constants;
import uds.outlier.DataPoint;
import uds.outlier.LOF;
import uds.outlier.OUTConstants;

public class LOFTest {
	public static void main(String[] args) {
		try {
			readInputString(args);
			String file = OUTConstants.FILEIN;
			String fileSub = OUTConstants.FILESUB;
			String fileOut = OUTConstants.FILEOUT;
			int cols = OUTConstants.COLS;
			ArrayList<DataPoint> data = readData(file, cols);
			ArrayList<ArrayList<Integer>> subspaces = readSubspaces(fileSub);
			
			int rows = data.size();
			double[] overallScores = new double[rows];
			BufferedWriter writerOverallScores = new BufferedWriter(new FileWriter(new File(fileOut)));
			int numSubspaces = subspaces.size();
			for (int i = 0; i < numSubspaces; i++) {
				ArrayList<Integer> tmpSpace = subspaces.get(i);
				ArrayList<Double> scores = LOF.computeLOF(data, tmpSpace);

				if (scores != null) {
					for (int j = 0; j < rows; j++) {
						overallScores[j] += scores.get(j);
					}
				}

			} // end for
			System.out.println("end computing outlier scores...");

			for (int i = 0; i < rows; i++) {
				writerOverallScores.write(Integer.toString(i) + " "
						+ Double.toString(overallScores[i]) + " "
						+ data.get(i).classID);
				writerOverallScores.newLine();
			}

			writerOverallScores.flush();
			writerOverallScores.close();
			System.out.println("end computing outlier scores...");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static ArrayList<ArrayList<Integer>> readSubspaces(String file) throws IOException, IllegalArgumentException {
		Scanner scan = null;
		try {
			ArrayList<ArrayList<Integer>> subspaces = new ArrayList<ArrayList<Integer>>();
			scan = new Scanner(new File(file));
			String s = null;
			while (scan.hasNext()) {
				s = scan.nextLine();
				String[] vals = s.split(";")[0].split(",");
				ArrayList<Integer> sub = new ArrayList<Integer>();
				for (int j = 0; j < vals.length; j++) {
					sub.add(Integer.parseInt(vals[j]));
				}
				
				subspaces.add(sub);
			}
			
			return subspaces;
		} catch (IOException e) {
			throw e;
		} finally {
			if (scan != null) {
				scan.close();
			}
		}
	}
	
	public static ArrayList<DataPoint> readData(String file, int cols) throws IOException, IllegalArgumentException {
		Scanner scan = null;
		try {
			ArrayList<DataPoint> data = new ArrayList<DataPoint>();
			scan = new Scanner(new File(file));
			int id = 0;
			while (scan.hasNext()) {
				String[] vals = scan.nextLine().split(",");
				if (vals.length != cols + 1) {
					throw new IllegalArgumentException("missing data!");
				}
				
				double[] t = new double[cols];
				for (int j = 0; j < cols; j++) {
					t[j] = Double.parseDouble(vals[j]);
				}
				
				DataPoint point = new DataPoint(id, t, vals[cols]);
				data.add(point);
				id++;
			}
			
			return data;
		} catch (IOException e) {
			throw e;
		} finally {
			if (scan != null) {
				scan.close();
			}
		}
	}
	
	public static void readInputString(String[] args) throws Exception {	
		int i;
		int total = args.length - 1;
		
		// take FILEIN
		boolean found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILEIN"))
			{
				OUTConstants.FILEIN = args[i + 1];
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -FILEIN");
		}
		
		
		// take FILESUB
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILESUB"))
			{
				OUTConstants.FILESUB = args[i + 1];
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -FILESUB");
		}
		
		// take FILEOUT
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILEOUT"))
			{
				OUTConstants.FILEOUT = args[i + 1];
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -FILEOUT");
		}
		
		// take COLS
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-COLS"))
			{
				OUTConstants.COLS = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -COLS");
		}
		
		// take K
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-K"))
			{
				OUTConstants.K = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		
		// take METHOD
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-METHOD"))
			{
				Constants.METHOD = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
	} // end method
}