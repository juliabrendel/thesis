import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import uds.constants.Constants;
import uds.score.UDSFunction;
import uds.subspace.SSConstants;
import uds.subspace.Subspace;
import uds.subspace.SubspaceSearch;


public class SSTest {
	public static void main(String[] args) {
		try {
			readInputString(args);
			String file = SSConstants.FILEIN;
			String fileOut = SSConstants.FILEOUT;
			int cols = SSConstants.COLS;
			int W = SSConstants.W;
			int K = SSConstants.K;
			double[][] data = readData(file, cols);
			long startScore = System.currentTimeMillis();
			double score = UDSFunction.computeScore(data, null);
			long endScore = System.currentTimeMillis();
			System.out.println("score: ");
			System.out.println(score);
			System.out.println("runtime in milliseconds: ");
			System.out.println(endScore - startScore);
			int[][] rank = UDSFunction.generateRank(data);
			
			long start = System.currentTimeMillis();
			Subspace[] result = SubspaceSearch.find(data, rank, W, K);
			long end = System.currentTimeMillis();
			
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(fileOut)));
			for (Subspace sub : result) {
				writer.write(sub.toString());
				writer.newLine();
			}
			//writer.write(Long.toString(end - start));
			writer.newLine();
			writer.flush();
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static double[][] readData(String file, int cols) throws IOException, IllegalArgumentException {
		Scanner scan = null;
		try {
			ArrayList<double[]> data = new ArrayList<double[]>();
			scan = new Scanner(new File(file));
			while (scan.hasNext()) {
				String[] vals = scan.nextLine().split(";");
				if (vals.length != cols + 1) {
					System.out.println(vals.length);
					throw new IllegalArgumentException("missing data!");
				}
				
				double[] t = new double[cols];
				for (int j = 0; j < cols; j++) {
					t[j] = Double.parseDouble(vals[j]);
				}
				data.add(t);
			}
			
			double[][] ret = new double[data.size()][cols];
			for (int i = 0; i < ret.length; i++) {
				for (int j = 0; j < cols; j++) {
					ret[i][j] = data.get(i)[j];
				}
			}
			
			return ret;
		} catch (IOException e) {
			throw e;
		} finally {
			if (scan != null) {
				scan.close();
			}
		}
	}
	
	public static void readInputString(String[] args) throws Exception {	
		int i;
		int total = args.length - 1;
		
		// take FILEIN
		boolean found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILEIN"))
			{
				SSConstants.FILEIN = args[i + 1];
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -FILEIN");
		}
		
		// take FILEOUT
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILEOUT"))
			{
				SSConstants.FILEOUT = args[i + 1];
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -FILEOUT");
		}
		
		// take COLS
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-COLS"))
			{
				SSConstants.COLS = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		if (found == false) {
			throw new Exception("Missing -COLS");
		}
		
		// take W
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-W"))
			{
				SSConstants.W = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		
		// take K
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-K"))
			{
				SSConstants.K = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		
		// take METHOD
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-METHOD"))
			{
				Constants.METHOD = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
	} // end method
}