package uds.score;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import uds.constants.Constants;

public class UDSFunction {
	public static double computeScore(double[][] data, int[][] preRank) {
		int r = data.length;
		int c = data[0].length;
		int[][] rank = preRank;
		if (rank == null) {
			rank = UDSFunction.generateRank(data);
		}
		int[][] discrete = new int[r][c];
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				discrete[i][j] = -1;
			}
		}
		
		// compute ces
		double[] ces = new double[c];
		for (int j = 0; j < c; j++) {
			double[] a = new double[r];
			for (int i = 0; i < r; i++) {
				a[i] = data[rank[i][j]][j];
			}
			ces[j] = computeCE(a, true);
		}
		
		// rank columns
		int[] colOrder = rankColumns(ces);
		
		// compute score
		double sumCES = 0;
		double sumScore = 0;
		double sumLog = 0;
		double[] ret = null;
		for (int j = 1; j < c; j++) {
			if (Constants.METHOD == Constants.UDS) {
				ret = computeScore(data, discrete, rank, ces, colOrder, j - 1, j, Constants.BETA);
			} else if (Constants.METHOD == Constants.UDSR) {
				ret = computeScore2(data, discrete, rank, ces, colOrder, j - 1, j, sumLog, Constants.BETA, true);
			}
			sumScore += ret[0];
			sumLog += ret[1];
			sumCES += ces[j];
		}
		
		double t = sumScore / sumCES;
		if ((t < 0 && Math.abs(t) > Constants.MAX_ERROR) || (t > 1 && Math.abs(t - 1) > Constants.MAX_ERROR)) { // control check
			throw new ArithmeticException(String.format("Wrong score t = %f", t));
		}
		
		return t;
	}
	
	@SuppressWarnings("unchecked")
	private static double[] computeScore(double[][] data, int[][] discrete, int[][] rank, double[] ces, int[] colOrder, int X, int Y, int numDesiredBins) {
		ArrayList<Bin> a = formInitialBins(data, rank, colOrder, X, numDesiredBins);
		int beta = a.size();
		int actualCol = colOrder[X];
		
		int sum = 0;
		int[] s = new int[beta];
		for (int i = 0; i < beta; i++) {
			ArrayList<Integer> pids = a.get(i).getPoints();
			sum += pids.size();
			s[i] = sum;
			
			for (Integer id : pids) {
				discrete[id][actualCol] = i;
			}
		}
		
		double[][] f = computeF(data, discrete, rank, colOrder, X, Y, beta);
		
		double[] val = new double[beta];
		ArrayList<Bin>[] b = new ArrayList[beta];
		val[0] = f[0][0];
		b[0] = new ArrayList<Bin>();
		b[0].add(a.get(0));
		for (int i = 1; i < beta; i++) {
			int pos = -1;
			val[i] = Double.POSITIVE_INFINITY;
			for (int j = 0; j < i; j++) {
				double t = (s[i] - s[j]) * f[j + 1][i] / s[i] + s[j] * val[j] / s[i];
				if (t < val[i]) {
					val[i] = t;
					pos = j;
				}
			}
			if (f[0][i] < val[i]) {
				val[i] = f[0][i];
				pos = -1;
			}
			
			if (val[i] < 0 && Math.abs(val[i]) > Constants.MAX_ERROR) {
				throw new ArithmeticException(String.format("Wrong score val[i] = %f", val[i]));
			}
			
			b[i] = new ArrayList<Bin>();
			if (pos != -1) {
				b[i].addAll(b[pos]);
			}
			
			ArrayList<Integer> pids = new ArrayList<Integer>();
			for (int k = pos + 1; k <= i; k++) {
				pids.addAll(a.get(k).getPoints());
			}
			Bin tmp = new Bin(pids);
			b[i].add(tmp);
		}
		
		ArrayList<Bin> finalBins = b[beta - 1];
		int c = 0;
		for (Bin tmp : finalBins) {
			ArrayList<Integer> pids = tmp.getPoints();
			for (Integer id : pids) {
				discrete[id][actualCol] = c;
			}
			
			c++;
		}
		
		double[] ret = new double[2];
		ret[0] = ces[colOrder[Y]] - val[beta - 1];
		ret[1] = Math.log(b[beta - 1].size()) / Math.log(Constants.LOG_BASE);
		//System.out.printf("number of final bins = %d\n\n", b[beta - 1].size());
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	public static double[] computeScore2(double[][] data, int[][] discrete, int[][] rank, double[] ces, int[] colOrder,
										 int X, int Y, double sumLog, int numDesiredBins, boolean doRegularization) {
		int actualCol = colOrder[X];
		int r = data.length;
		ArrayList<Bin> a = formInitialBins(data, rank, colOrder, X, numDesiredBins);
		int beta = a.size();
		
		// control check in case all values of the dimension are the same
		if (beta == 1) {
			double[] ret = null;
			if (doRegularization) {
				ret = new double[2];
				ret[0] = ces[colOrder[Y]];
				ret[1] = 0;
				System.out.printf("no discretization happens: number of final bins = %d\n\n", 1);
			} else {
				Constants.FORMAC = new ArrayList[1];
				Constants.FORMAC[0] = new ArrayList<Bin>();
				ArrayList<Integer> pids = new ArrayList<Integer>();
				for (int i = 0; i < r; i++) {
					pids.add(i);
				}
				Constants.FORMAC[0].add(new Bin(pids));
			}
			return ret;
		}
		
		int sum = 0;
		int[] s = new int[beta];
		for (int i = 0; i < beta; i++) {
			ArrayList<Integer> pids = a.get(i).getPoints();
			sum += pids.size();
			s[i] = sum;
			
			for (Integer id : pids) {
				discrete[id][actualCol] = i;
			}
		}
		
		double[][] f = computeF(data, discrete, rank, colOrder, X, Y, beta);
		
		double[][] val = new double[beta + 1][beta];
		ArrayList<Bin>[][] b = new ArrayList[beta + 1][beta];
		Bin tmp = null;
		for (int i = 0; i < beta; i++) {
			b[1][i] = new ArrayList<Bin>();
			if (i == 0) {
				tmp = new Bin(a.get(0).getPoints());
				b[1][i].add(tmp);
			} else {
				ArrayList<Integer> pids = new ArrayList<Integer>(b[1][i - 1].get(0).getPoints());
				pids.addAll(a.get(i).getPoints());
				tmp = new Bin(pids);
				b[1][i].add(tmp);
			}
			
			val[1][i] = f[0][i];
		}
		
		for (int i = 1; i < beta; i++) {
			int pos = -1;
			double min = Double.POSITIVE_INFINITY;
			for (int j = 0; j < i; j++) {
				double t = (s[i] - s[j]) * f[j + 1][i] / s[i] + s[j] * val[1][j] / s[i];
				
				if (t < min) {
					min = t;
					pos = j;
				}
			}
			
			val[2][i] = min;
			if (val[2][i] < 0 && Math.abs(val[2][i]) > Constants.MAX_ERROR) {
				throw new ArithmeticException(String.format("Wrong score val[2][i] = %f", val[2][i]));
			}
			
			b[2][i] = new ArrayList<Bin>();
			b[2][i].addAll(b[1][pos]);
			ArrayList<Integer> pids = new ArrayList<Integer>();
			for (int k = pos + 1; k <= i; k++) {
				pids.addAll(a.get(k).getPoints());
			}
			tmp = new Bin(pids);
			b[2][i].add(tmp);
		}
		
		for (int delta = 3; delta <= beta; delta++) {
			for (int i = delta - 1; i < beta; i++) {
				int pos = -1;
				double min = Double.POSITIVE_INFINITY;
				for (int j = delta - 2; j < i; j++) {
					double t = (s[i] - s[j]) * f[j + 1][i] / s[i] + s[j] * val[delta - 1][j] / s[i];
					
					if (t < min) {
						min = t;
						pos = j;
					}
				}
				
				val[delta][i] = min;
				if (val[delta][i] < 0 && Math.abs(val[delta][i]) > Constants.MAX_ERROR) {
					throw new ArithmeticException(String.format("Wrong score val[delta][i] = %f", val[delta][i]));
				}
				
				b[delta][i] = new ArrayList<Bin>();
				b[delta][i].addAll(b[delta - 1][pos]);
				ArrayList<Integer> pids = new ArrayList<Integer>();
				for (int k = pos + 1; k <= i; k++) {
					pids.addAll(a.get(k).getPoints());
				}
				tmp = new Bin(pids);
				b[delta][i].add(tmp);
			}
		}
		
		// perform regularization
		double[] ret = null;
		if (doRegularization) {
			double logBase = Math.log(Constants.LOG_BASE);
			double min = Double.POSITIVE_INFINITY;
			int deltaMin = -1;
			Constants.FORMAC = new ArrayList[beta];
			for (int delta = 1; delta <= beta; delta++) {
				// update discrete data
				ArrayList<Bin> finalBins = b[delta][beta - 1];
				Constants.FORMAC[delta - 1] = finalBins;
				
				int c = 0;
				for (Bin bin : finalBins) {
					ArrayList<Integer> pids = bin.getPoints();
					for (Integer id : pids) {
						discrete[id][actualCol] = c;
					}
					
					c++;
				}
				
				// compute distinct combination
				HashMap<Key, Integer> hash = new HashMap<Key, Integer>();
				for (int i = 0; i < r; i++) {
					int[] z = new int[X + 1];
					for (int j = 0; j <= X; j++) {
						z[j] = discrete[i][colOrder[j]];
					}
					
					int p;
					Key key = new Key(z);
					if (hash.containsKey(key)) {
						p = hash.get(key);
						p++;
					} else {
						p = 1;
					}
					
					hash.put(key, p);
				}
				
				// compute entropy
				double q = 0;
				for (Integer n : hash.values()) {
					q += - (n / (r * 1.0)) * Math.log(n / (r * 1.0)) / logBase;
				}
				
				// pick the one with minimum cost
				if (Math.abs(ces[colOrder[Y]]) < Constants.MAX_ERROR) {
					ces[colOrder[Y]] = 0;
				}
				if (Math.abs(val[delta][beta - 1]) < Constants.MAX_ERROR) {
					val[delta][beta - 1] = 0;
				}
				if (ces[colOrder[Y]] == 0) {
					if (val[delta][beta - 1] == 0) {
						q = q / (Math.log(beta) / logBase + sumLog);
					} else {
						q = Double.POSITIVE_INFINITY;
					}
				} else {
					q = val[delta][beta - 1] / ces[colOrder[Y]] + q / (Math.log(beta) / logBase + sumLog);
				}
				//System.out.printf(String.format("val[delta][beta - 1] = %f, ces[colOrder[Y]] = %f, beta = %d, sumLog = %f\n", val[delta][beta - 1], ces[colOrder[Y]], beta, sumLog));
				if (q < min) {
					min = q;
					deltaMin = delta;
				}
			}
			
			// update discrete data
			ArrayList<Bin> finalBins = b[deltaMin][beta - 1];
			int c = 0;
			for (Bin bin : finalBins) {
				ArrayList<Integer> pids = bin.getPoints();
				for (Integer id : pids) {
					discrete[id][actualCol] = c;
				}
				
				c++;
			}
			
			ret = new double[2];
			ret[0] = ces[colOrder[Y]] - val[deltaMin][beta - 1];
			ret[1] = Math.log(b[deltaMin][beta - 1].size()) / Math.log(Constants.LOG_BASE);
			//System.out.printf("number of final bins = %d\n\n", b[deltaMin][beta - 1].size());
		} else {
			Constants.FORMAC = new ArrayList[beta];
			for (int delta = 1; delta <= beta; delta++) {
				Constants.FORMAC[delta - 1] = b[delta][beta - 1];
			}
		}
		
		return ret;
	}
	
	private static double[][] computeF(double[][] data, int[][] discrete, int[][] rank, int[] colOrder, int X, int Y, int beta) {
		int actualCol = colOrder[Y];
		
		// create hypercubes
		int[] count = new int[beta];
		HashMap<Key, ArrayList<Integer>>[] map = createHypercubes(discrete, rank, colOrder, X, Y, beta, count);
		
		// pre-computation for f[i][i]
		double[][] f = new double[beta][beta];
		for (int i = 0; i < beta; i++) {
			Collection<ArrayList<Integer>> values = map[i].values();
			for (ArrayList<Integer> pids : values) {
				double[] v = new double[pids.size()];
				int idx = 0;
				for (Integer id : pids) {
					v[idx++] = data[id][actualCol];
				}
				double ce = computeCE(v, true);
				f[i][i] += v.length * ce / count[i];
			}
			
			if (f[i][i] < 0 && Math.abs(f[i][i]) > Constants.MAX_ERROR) {
				throw new ArithmeticException(String.format("Wrong score f[i][i] = %f", f[i][i]));
			}
		}
		
		// pre-computation for f[j][i]
		ArrayList<Double> d = null;
		for (int j = 0; j < beta; j++) {
			// initialize the hash
			HashMap<Key, ArrayList<Double>> tmpMap = new HashMap<Key, ArrayList<Double>>();
			Set<Key> keys = map[j].keySet();
			for (Key k : keys) {
				d = extractValues(data, actualCol, map[j], k);
				tmpMap.put(k, d);
			}
			
			// populate hash incrementally
			int c = count[j];
			for (int i = j + 1; i < beta; i++) {
				c += count[i];
				keys = map[i].keySet();
				for (Key k : keys) {
					d = extractValues(data, actualCol, map[i], k);
					ArrayList<Double> cur = tmpMap.get(k);
					if (cur == null) { // key is not there
						cur = d;
					} else {
						cur = merge(cur, d);
					}
					tmpMap.put(k, cur);
				}
				
				Collection<ArrayList<Double>> values = tmpMap.values();
				for (ArrayList<Double> val : values) {
					double ce = computeCE(val, true);
					f[j][i] += val.size() * ce / c;
				}
				
				if (f[j][i] < 0 && Math.abs(f[j][i]) > Constants.MAX_ERROR) {
					throw new ArithmeticException(String.format("Wrong score f[j][i] = %f", f[j][i]));
				}
			}
		}
		
		return f;
	}
	
	private static ArrayList<Double> merge(ArrayList<Double> a, ArrayList<Double> b) {
		ArrayList<Double> ret = new ArrayList<Double>();
		int idxA = 0;
		int idxB = 0;
		int sizeA = a.size();
		int sizeB = b.size();
		
		while (idxA < sizeA && idxB < sizeB) {
			if (a.get(idxA) < b.get(idxB)) {
				ret.add(a.get(idxA));
				idxA++;
			} else {
				ret.add(b.get(idxB));
				idxB++;
			}
		}
		
		if (idxA >= sizeA) {
			for (int i = idxB; i < sizeB; i++) {
				ret.add(b.get(i));
			}
		} else {
			for (int i = idxA; i < sizeA; i++) {
				ret.add(a.get(i));
			}
		}
		
		return ret;
	}
	
	private static ArrayList<Double> extractValues(double[][] data, int actualCol, HashMap<Key, ArrayList<Integer>> map, Key k) {
		ArrayList<Integer> pids = map.get(k);
		ArrayList<Double> d = new ArrayList<Double>();
		for (Integer id : pids) {
			d.add(data[id][actualCol]);
		}
		return d;
	}
	
	@SuppressWarnings({ "unchecked" })
	public static HashMap<Key, ArrayList<Integer>>[] createHypercubes(int[][] discrete, int[][] rank, int[] colOrder, int X, int Y, int beta, int[] count) {
		HashMap<Key, ArrayList<Integer>>[] map = new HashMap[beta];
		int r = discrete.length;
		int c = 0;
		int actualID;
		for (int i = 0; i < r; i++) {
			actualID = rank[i][colOrder[Y]];
			
			// cube of already discretized dimensions
			int[] vals = new int[X];
			for (int j = 0; j < X; j++) {
				vals[j] = discrete[actualID][colOrder[j]];
			}
			
			// bin of X
			int idx = discrete[actualID][colOrder[X]];
			
			// create hash if it is not there yet
			if (map[idx] == null) {
				map[idx] = new HashMap<Key, ArrayList<Integer>>();
			}
			
			// retrieve point ids
			ArrayList<Integer> pids = null;
			Key key = new Key(vals);
			if (map[idx].containsKey(key)) {
				pids = map[idx].get(key);
			} else {
				pids = new ArrayList<Integer>();
			}
			pids.add(actualID);
			
			// add list of ids to hash
			map[idx].put(key, pids);
			count[idx]++;
			c++;
		}
		
		if (c != r) {
			throw new ArithmeticException("some records are missing!");
		}
		
		return map;
	}
	
	private static ArrayList<Integer> join(ArrayList<Integer> a, ArrayList<Integer> b) {
		HashSet<Integer> set = new HashSet<Integer>();
		
		for (Integer n : a) {
			set.add(n);
		}
		
		ArrayList<Integer> ret = new ArrayList<Integer>();
		for (Integer n : b) {
			if (set.contains(n)) {
				ret.add(n);
			}
		}
		return ret;
	}
	
	public static int[] rankColumns(double[] ces) {
		int c = ces.length;
		int[] order = new int[c];
		
		SortedObject[] sos = new SortedObject[c];
		for (int j = 0; j < c; j++) {
			sos[j] = new SortedObject(j, ces[j]);
		}
		Arrays.sort(sos, Collections.reverseOrder());
		
		for (int j = 0; j < c; j++) {
			order[j] = sos[j].id;
		}
		
		return order;
	}
	
	public static int[][] generateRank(double[][] data) {
		int r = data.length;
		int c = data[0].length;
		int[][] rank = new int[r][c];
		
		// sort data per dimension
		for (int j = 0; j < c; j++) {
			SortedObject[] sos = new SortedObject[r];
			for (int i = 0; i < r; i++) {
				sos[i] = new SortedObject(i, data[i][j]);
			}
			Arrays.sort(sos);
			for (int i = 0; i < r; i++) {
				rank[i][j] = sos[i].id;
			}
		}
		
		return rank;
	}
	
	public static ArrayList<Bin> formInitialBins(double[][] data, int[][] rank, int[] colOrder, int X, int numDesiredBins) {
		int r = data.length;
		ArrayList<Bin> ret = new ArrayList<Bin>();
		int actualCol = colOrder[X];
		//System.out.printf("numDesiredBins = %d\n", numDesiredBins);
		
		// extract distinct values
		HashMap<Double, ArrayList<Integer>> distinctValues = new HashMap<Double, ArrayList<Integer>>();
		ArrayList<Double> sortedKeys = new ArrayList<Double>();
		for (int i = 0; i < r; i++) {
			ArrayList<Integer> pids = null;
			double k = data[rank[i][actualCol]][actualCol];
			if (distinctValues.containsKey(k)) {
				pids = distinctValues.get(k);
			} else {
				sortedKeys.add(k);
				pids = new ArrayList<Integer>();
			}
			
			pids.add(rank[i][actualCol]);
			distinctValues.put(k, pids);
		}
		Collections.sort(sortedKeys);
		
		// number of distinct values per bin
		int numRows = r;
		int totalNumBins = numDesiredBins;
		int desiredBinSize = (int)Math.floor(numRows * 1.0 / totalNumBins);
		//System.out.printf("numDesiredBins = %d\n", numDesiredBins);
		
		// discretize
		int[] indices = new int[distinctValues.size()];
		int i = 1;
		int curDistinctVal = 0;
		int curBinIndex = 1;
		int count;
		while (curDistinctVal < indices.length) {
			count = 0;
			for (int j = 0; j < indices.length; j++) {
				if (indices[j] == curBinIndex) {
					count = count + distinctValues.get(sortedKeys.get(j)).size();
				}
			}
			
			if (count != 0 && Math.abs(count + distinctValues.get(sortedKeys.get(curDistinctVal)).size() - desiredBinSize) >= Math.abs(count - desiredBinSize)) {
				curBinIndex++;
				desiredBinSize = (int)((numRows - i + 1) * 1.0) / (totalNumBins - curBinIndex + 1);
			}
			indices[curDistinctVal] = curBinIndex;
			i = i + distinctValues.get(sortedKeys.get(curDistinctVal)).size();
			curDistinctVal++;
		}
		//System.out.println("curBinIndex = " + curBinIndex);
		
		// create bin
		Bin tmpBin = null;
		curBinIndex = 1;
		curDistinctVal = 0;
		int newDistinctVal;
		ArrayList<Integer> ds = null;
		while (curDistinctVal < indices.length) {
			for (i = curDistinctVal + 1; i < indices.length; i++) {
				if (indices[i] != curBinIndex) {
					break;
				}
			}
			newDistinctVal = i;
			
			// get current value and add the corresponding points to the bin
			ds = new ArrayList<Integer>();
			for (i = curDistinctVal; i < newDistinctVal; i++) {
				ds.addAll(distinctValues.get(sortedKeys.get(i)));
			}
			tmpBin = new Bin(ds);
			
			// add the bin to the list of bins
			ret.add(tmpBin);
			
			curDistinctVal = newDistinctVal;
			curBinIndex++;
		} // end for
		
		// control check
		count = 0;
		for (Bin bin : ret) {
			count += bin.getPoints().size();
		}
		if (count != numRows) {
			throw new ArithmeticException("some point is missing!");
		}
		
		//System.out.printf("number of initial bins = %d\n", ret.size());
			
		return ret;
	}
	
	public static double computeCE(double[] vals, boolean hasSorted) {
		if (vals.length <= 1) {
			return 0;
		}
		
		if (!hasSorted) {
			Arrays.sort(vals);
		}
			
		int num_items = vals.length;
		
		double ce = 0;
		double logBase = Math.log(Constants.LOG_BASE);
		for (int i = 0; i < num_items - 1; i++) {
			if (vals[i + 1] < vals[i]) {
				throw new ArithmeticException(String.format("non-decreasing order is violated: vals[i] = %f, vals[i + 1] = %f", vals[i], vals[i + 1]));
			}
			
			if (vals[i + 1] != vals[i]) {
				ce += (vals[i + 1] - vals[i]) * ((i + 1) / (1.0 * num_items)) * Math.log((i + 1) / (1.0 * num_items)) / logBase;
			}
		}
		
		return -ce;
	}
	
	public static double computeCE(ArrayList<Double> vals, boolean hasSorted) {
		if (vals.size() <= 1) {
			return 0;
		}
		
		if (!hasSorted) {
			Collections.sort(vals);
		}
			
		int num_items = vals.size();
		
		double ce = 0;
		double logBase = Math.log(Constants.LOG_BASE);
		for (int i = 0; i < num_items - 1; i++) {
			if (vals.get(i + 1) < vals.get(i)) {
				throw new ArithmeticException(String.format("non-decreasing order is violated: vals.get(i) = %f, vals.get(i + 1) = %f", vals.get(i), vals.get(i + 1)));
			}
			
			if (vals.get(i + 1).doubleValue() != vals.get(i).doubleValue()) {
				ce += (vals.get(i + 1) - vals.get(i)) * ((i + 1) / (1.0 * num_items)) * Math.log((i + 1) / (1.0 * num_items)) / logBase;
			}
		}
		
		return -ce;
	}
	
	private static double computeCE(double[][] data, int c) {
		if (data.length <= 1) {
			return 0;
		}
			
		int num_items = data.length;
		
		double ce = 0;
		double logBase = Math.log(Constants.LOG_BASE);
		for (int i = 0; i < num_items - 1; i++) {
			if (data[i + 1][c] < data[i][c]) {
				throw new ArithmeticException(String.format("non-decreasing order is violated: data[i][c] = %f, data[i + 1][c] = %f", data[i][c], data[i + 1][c]));
			}
			
			if (data[i + 1][c] != data[i][c]) {
				ce += (data[i + 1][c] - data[i][c]) * ((i + 1) / (1.0 * num_items)) * Math.log((i + 1) / (1.0 * num_items)) / logBase;
			}
		}
		
		return -ce;
	}
}