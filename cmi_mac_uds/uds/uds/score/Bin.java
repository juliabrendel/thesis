package uds.score;

import java.util.ArrayList;

public class Bin {
	private ArrayList<Integer> pids;
	
	public Bin(ArrayList<Integer> pids) {
		setPoints(pids);
	}
	
	public ArrayList<Integer> getPoints() {
		return pids;
	}
	public void setPoints(ArrayList<Integer> pids) {
		this.pids = pids;
	}
}