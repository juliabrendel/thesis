package uds.score;

public class Key {
	public int[] val;
	
	public Key(int[] val) {
		this.val = val;
	}
	
	@Override
    public boolean equals(Object object) {
        if (!(object instanceof Key)) {
            return false;
        }

        Key otherKey = (Key) object;
        if (val.length != otherKey.val.length) {
        	return false;
        }
        
        for (int i = 0; i < val.length; i++) {
        	if (val[i] != otherKey.val[i]) {
        		return false;
        	}
        }
        
        return true;
    }

	@Override
    public int hashCode() {
        int result = 17; // any prime number
        for (int i = 0; i < val.length; i++) {
        	result = 31 * result + Double.valueOf(val[i]).hashCode();
        }
        return result;
    }
}