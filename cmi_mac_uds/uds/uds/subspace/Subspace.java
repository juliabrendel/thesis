package uds.subspace;

import java.util.ArrayList;

import uds.cmi.CMIFunction;
import uds.constants.Constants;
import uds.hics.HICSFunction;
import uds.mac.MACFunction;
import uds.score.UDSFunction;

public class Subspace implements Comparable<Subspace> {
	public ArrayList<Integer> dims;
	public double corr;
	
	public Subspace(ArrayList<Integer> dims) {
		this.dims = dims;
	}
	
	public void setCorr(double[][] data, int[][] rank) {
		int r = data.length;
		int c = dims.size();
		double[][] a = new double[r][c];
		int[][] s = new int[r][c];
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				a[i][j] = data[i][dims.get(j)];
				s[i][j] = rank[i][dims.get(j)];
			}
		}
		
		if (Constants.METHOD == Constants.CMI) {
			this.corr = CMIFunction.computeScore(a, s);
		} else if (Constants.METHOD == Constants.MAC) {
			this.corr = MACFunction.computeScore(a, s);
		} else if (Constants.METHOD == Constants.HICS) {
			this.corr = HICSFunction.computeScore(a, s);
		} else {
			this.corr = UDSFunction.computeScore(a, s);
		}
	}
	
	@Override
	public int compareTo(Subspace o) {
		if (this.corr > o.corr) {
			return 1;
		} else if (this.corr == o.corr) {
			return 0;
		} else {
			return -1;
		}
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		int c = 0;
		for (Integer d : dims) {
			if (c == 0) {
				s.append(Integer.toString(d));
			} else {
				s.append(",");
				s.append(Integer.toString(d));
			}
			c++;
		}
		s.append(";");
		s.append(Double.toString(corr));
		
		return s.toString();
	}
}