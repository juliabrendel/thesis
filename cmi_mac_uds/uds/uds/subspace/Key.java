package uds.subspace;

import java.util.ArrayList;

public class Key {
	public ArrayList<Integer> val;
	
	public Key(ArrayList<Integer> val) {
		this.val = val;
	}
	
	@Override
    public boolean equals(Object object) {
        if (!(object instanceof Key)) {
            return false;
        }

        Key otherKey = (Key) object;
        if (val.size() != otherKey.val.size()) {
        	return false;
        }
        
        for (int i = 0; i < val.size(); i++) {
        	if (val.get(i).intValue() != otherKey.val.get(i).intValue()) {
        		return false;
        	}
        }
        
        return true;
    }

	@Override
    public int hashCode() {
        int result = 17; // any prime number
        for (int i = 0; i < val.size(); i++) {
        	result = 31 * result + Double.valueOf(val.get(i)).hashCode();
        }
        return result;
    }
}