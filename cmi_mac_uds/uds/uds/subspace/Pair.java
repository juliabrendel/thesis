package uds.subspace;

public class Pair implements Comparable<Pair> {
	public int x;
	public int y;
	public double corr;
	
	public Pair(int x, int y, double corr) {
		this.x = x;
		this.y = y;
		this.corr = corr;
	}
	
	@Override
	public int compareTo(Pair a) {
		if (this.corr > a.corr) {
			return 1;
		} else if (this.corr == a.corr) {
			return 0;
		}
		
		return -1;
	}
}