package uds.subspace;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class MDLMerge {
	public static void readInputData(int numPoints, int numDims, String fileInput, int[][] data) throws Exception {
		System.out.println("numDims = " + numPoints);
		Scanner scanner = new Scanner(new FileInputStream(fileInput));
		String s;
		String[] vals = null;
		for (int row = 0; row < numPoints; row++) {
			s = scanner.nextLine();
			vals = s.split(" ");
			for (int col = 0; col < numDims; col++) {
				data[row][col] = Integer.parseInt(vals[col]);
			}
		}
		
		scanner.close();
	}
	
	public static ArrayList<ArrayList<Integer>> loadCliques(int numCliques, String cliqueFile) throws Exception {
		ArrayList<ArrayList<Integer>> ret = new ArrayList<ArrayList<Integer>>();
		
		Scanner scanner = new Scanner(new FileInputStream(cliqueFile));
		String data;
		String[] vals;
		ArrayList<Integer> vertices = null;
		for (int dim = 0; dim < numCliques; dim++) {
			vertices = new ArrayList<Integer>();
			data = scanner.nextLine();
			vals = data.split(",");
			for (int i = 0; i < vals.length; i++) {
				vertices.add(new Integer(vals[i]));
			}
			ret.add(vertices);
		} // end for
		
		scanner.close();
		return ret;
	}
	
	public static int mergeCliques(int numPoints, int numDims, String fileInput, String cliqueFile, String finalCliqueFile) throws Exception {
		ArrayList<ArrayList<Integer>> cliqueClusters = mergeAtts(numPoints, numDims, fileInput);
		ArrayList<ArrayList<Integer>> cliques = loadCliques(numDims, cliqueFile);
		
		// write final cliques
		BufferedWriter writerClique = new BufferedWriter(new FileWriter(new File(finalCliqueFile)));
		int nc = cliqueClusters.size();
		ArrayList<Integer> cliqueids = null;
		ArrayList<Integer> clique = null;
		int cs;
		int cid;
		int nd;
		ArrayList<Integer> newClique = new ArrayList<Integer>();
		int vertex;
		for (int i = 0; i < nc; i++) {
			cliqueids = cliqueClusters.get(i);
			cs = cliqueids.size();
			newClique.clear();
			for (int j = 0; j < cs; j++) {
				cid = cliqueids.get(j);
				clique = cliques.get(cid);
				nd = clique.size();
				for (int k = 0; k < nd; k++) {
					vertex = clique.get(k);
					if (!newClique.contains(vertex)) {
						newClique.add(vertex);
					}
				}
			}
			
			nd = newClique.size();
			for (int j = 0; j < nd; j++) {
				if (j == 0) {
					writerClique.write(Integer.toString(newClique.get(j)));
				} else {
					writerClique.write("," + Integer.toString(newClique.get(j)));
				}
			}
			writerClique.write(";");
			writerClique.newLine();
		}
		writerClique.flush();
		writerClique.close();
		
		return nc;
	}
	
	public static ArrayList<ArrayList<Integer>> mergeAttsCountItems(int numPoints, int numDims, String fileInput) throws Exception {
		// load data
		int[][] data = new int[numPoints][numDims];
		readInputData(numPoints, numDims, fileInput, data);
		
		// create clusters of singleton attribute
		ArrayList<ArrayList<Integer>> clusters = new ArrayList<ArrayList<Integer>>();
		ArrayList<ArrayList<Integer>> bestClusters = new ArrayList<ArrayList<Integer>>();
		ArrayList<Double> entropies = new ArrayList<Double>();
		ArrayList<Double> ctsizes = new ArrayList<Double>();
		ArrayList<Integer> c = null;
		double logbase = Math.log(2);
		double loglogD = Math.log(Math.log(numPoints) / logbase) / logbase;
		int numZeros;
		int numOnes;
		double freqZero;
		double freqOne;
		double entropy;
		double ctsize;
		double cc = 0;
		for (int dim = 0; dim < numDims; dim++) {
			c = new ArrayList<Integer>();
			c.add(dim);
			clusters.add(c);
			bestClusters.add(c);
			
			// compute code table
			numZeros = 0;
			numOnes = 0;
			for (int r = 0; r < numPoints; r++) {
				if (data[r][dim] == 0) {
					numZeros++;
				} else {
					numOnes++;
				}
			}
			freqZero = numZeros * 1.0 / numPoints;
			freqOne = numOnes * 1.0 / numPoints;
			
			// compute entropy and size of code table
			entropy = 0;
			ctsize = 0;
			if (freqZero != 0) {
				entropy += -freqZero * Math.log(freqZero) / logbase;
				ctsize += 1;
				ctsize += loglogD;
				ctsize -= Math.log(freqZero) / logbase;
			}
			if (freqOne != 0) {
				entropy += -freqOne * Math.log(freqOne) / logbase;
				ctsize += 1;
				ctsize += loglogD;
				ctsize -= Math.log(freqOne) / logbase;
			}
			entropies.add(entropy);
			ctsizes.add(ctsize);
			cc += numPoints * entropy;
			cc += ctsize;
		}
		
		// compute cluster similarities
		ArrayList<ArrayList<double[]>> cs = new ArrayList<ArrayList<double[]>>();
		ArrayList<double[]> tmp = null;
		ArrayList<Integer> cluster = null;
		ArrayList<Integer> cluster1 = null;
		ArrayList<Integer> cluster2 = null;
		double ent1, ent2, ctsize1, ctsize2;
		int nc = clusters.size();
		for (int clid1 = 0; clid1 < nc - 1; clid1++) {
			tmp = new ArrayList<double[]>();
			cluster1 = clusters.get(clid1);
			ent1 = entropies.get(clid1);
			ctsize1 = ctsizes.get(clid1);
			for (int clid2 = clid1 + 1; clid2 < nc; clid2++) {
				cluster2 = clusters.get(clid2);
				ent2 = entropies.get(clid2);
				ctsize2 = ctsizes.get(clid2);
				tmp.add(computeClusterSimilarity(data, numPoints, cluster1, ent1, ctsize1, cluster2, ent2, ctsize2));
			}
			cs.add(tmp);
		}
		
		// mine the best clusters
		double mincost = cc;
		double prevcost = cc;
		int cl1, cl2;
		double maxsofar;
		double maxsimsofar;
		double[] v = null;
		while (clusters.size() > 1) {
			// find the merge reducing the largest cost
			nc = clusters.size();
			maxsimsofar = -Double.MAX_VALUE;
			maxsofar = -Double.MAX_VALUE;
			cl1 = -1;
			cl2 = -1;
			for (int clid1 = 0; clid1 < nc - 1; clid1++) {
				tmp = cs.get(clid1);
				for (int clid2 = clid1 + 1; clid2 < nc; clid2++) {
					v = tmp.get(clid2 - clid1 - 1);
					if (v[3] > maxsimsofar) {
						cl1 = clid1;
						cl2 = clid2;
						maxsimsofar = v[3];
						maxsofar = v[0];
					}
				}
			} // end for
			
			if (maxsofar < 0) {
				System.out.println("maxsimsofar = " + maxsimsofar);
				System.out.println("maxsofar = " + maxsofar);
				break;
			}
			
			// delete old clusters
			cluster1 = clusters.get(cl1);
			cluster2 = clusters.get(cl2);
			clusters.remove(cl2); // is this correct???
			clusters.remove(cl1);
			entropies.remove(cl2);
			entropies.remove(cl1);
			ctsizes.remove(cl2);
			ctsizes.remove(cl1);
			
			// create the new cluster and add it to the list of clusters
			cluster = new ArrayList<Integer>();
			int clustersize = cluster1.size();
			for (int attid = 0; attid < clustersize; attid++) {
				cluster.add(cluster1.get(attid).intValue());
			}
			clustersize = cluster2.size();
			for (int attid = 0; attid < clustersize; attid++) {
				cluster.add(cluster2.get(attid).intValue());
			}
			entropy = cs.get(cl1).get(cl2 - cl1 - 1)[1];
			ctsize = cs.get(cl1).get(cl2 - cl1 - 1)[2];
			clusters.add(cluster);
			entropies.add(entropy);
			ctsizes.add(ctsize);
			
			// remove similarities to old clusters
			for (int cid = 0; cid < cl1; cid++) {
				tmp = cs.get(cid);
				tmp.remove(cl2 - cid - 1);
				tmp.remove(cl1 - cid - 1);
			}
			for (int cid = cl1 + 1; cid < cl2; cid++) {
				tmp = cs.get(cid);
				tmp.remove(cl2 - cid - 1);
			}
			if (cl2 < nc - 1) {
				cs.remove(cl2);
				cs.add(new ArrayList<double[]>());
			}
			cs.remove(cl1);
			
			// compute similarities of the new cluster to existing clusters
			nc = clusters.size();
			for (int cid = 0; cid < nc - 1; cid++) {
				cs.get(cid).add(computeClusterSimilarity(data, numPoints, clusters.get(cid), entropies.get(cid), ctsizes.get(cid), clusters.get(nc - 1), entropies.get(nc - 1), ctsizes.get(nc - 1)));
			}
				
			// update best clustering
			prevcost -= maxsofar;
			if (prevcost < mincost) {
				bestClusters.clear();
				mincost = prevcost;
				for (int cid = 0; cid < nc; cid++) {
					bestClusters.add(clusters.get(cid));
				}
			}
			
			System.out.println(clusters.size());
		} // end while
		
		return bestClusters;
	}
	
	public static ArrayList<ArrayList<Integer>> mergeAtts(int numPoints, int numDims, String fileInput) throws Exception {
		// load data
		int[][] data = new int[numPoints][numDims];
		readInputData(numPoints, numDims, fileInput, data);
		
		// pre-compute Bell numbers
		/*int[] bellNumbers = new int[numDims + 1];
		bellNumbers[0] = 1;
		for (int n = 1; n <= numDims; n++)
		{
			bellNumbers[n] = 0;
			for (int k = 0; k < n; k++)
				bellNumbers[n] += C(n - 1, k) * bellNumbers[k];
		}*/
		
		// create clusters of singleton attribute
		ArrayList<ArrayList<Integer>> clusters = new ArrayList<ArrayList<Integer>>();
		ArrayList<ArrayList<Integer>> bestClusters = new ArrayList<ArrayList<Integer>>();
		ArrayList<Double> entropies = new ArrayList<Double>();
		ArrayList<Double> ctsizes = new ArrayList<Double>();
		ArrayList<Integer> c = null;
		double logbase = Math.log(2);
		double loglogD = Math.log(Math.log(numPoints) / logbase) / logbase;
		int numZeros;
		int numOnes;
		double freqZero;
		double freqOne;
		double entropy;
		double ctsize;
		double cc = 0;
		for (int dim = 0; dim < numDims; dim++) {
			c = new ArrayList<Integer>();
			c.add(dim);
			clusters.add(c);
			bestClusters.add(c);
			
			// compute code table
			numZeros = 0;
			numOnes = 0;
			for (int r = 0; r < numPoints; r++) {
				if (data[r][dim] == 0) {
					numZeros++;
				} else {
					numOnes++;
				}
			}
			freqZero = numZeros * 1.0 / numPoints;
			freqOne = numOnes * 1.0 / numPoints;
			
			// compute entropy and size of code table
			entropy = 0;
			ctsize = 0;
			if (freqZero != 0) {
				entropy += -freqZero * Math.log(freqZero) / logbase;
				ctsize += 1;
				ctsize += loglogD;
				ctsize -= Math.log(freqZero) / logbase;
			}
			if (freqOne != 0) {
				entropy += -freqOne * Math.log(freqOne) / logbase;
				ctsize += 1;
				ctsize += loglogD;
				ctsize -= Math.log(freqOne) / logbase;
			}
			entropies.add(entropy);
			ctsizes.add(ctsize);
			cc += numPoints * entropy;
			cc += ctsize;
		}
		
		// compute cluster similarities
		ArrayList<ArrayList<double[]>> cs = new ArrayList<ArrayList<double[]>>();
		ArrayList<double[]> tmp = null;
		ArrayList<Integer> cluster = null;
		ArrayList<Integer> cluster1 = null;
		ArrayList<Integer> cluster2 = null;
		double ent1, ent2, ctsize1, ctsize2;
		int nc = clusters.size();
		for (int clid1 = 0; clid1 < nc - 1; clid1++) {
			tmp = new ArrayList<double[]>();
			cluster1 = clusters.get(clid1);
			ent1 = entropies.get(clid1);
			ctsize1 = ctsizes.get(clid1);
			for (int clid2 = clid1 + 1; clid2 < nc; clid2++) {
				cluster2 = clusters.get(clid2);
				ent2 = entropies.get(clid2);
				ctsize2 = ctsizes.get(clid2);
				tmp.add(computeClusterSimilarity(data, numPoints, cluster1, ent1, ctsize1, cluster2, ent2, ctsize2));
			}
			cs.add(tmp);
		}
		
		// mine the best clusters
		double mincost = cc;
		double prevcost = cc;
		int cl1, cl2;
		double maxsofar;
		double[] v = null;
		while (clusters.size() > 1) {
			// find the merge reducing the largest cost
			nc = clusters.size();
			maxsofar = -Double.MAX_VALUE;
			cl1 = -1;
			cl2 = -1;
			for (int clid1 = 0; clid1 < nc - 1; clid1++) {
				tmp = cs.get(clid1);
				for (int clid2 = clid1 + 1; clid2 < nc; clid2++) {
					v = tmp.get(clid2 - clid1 - 1);
					if (v[0] > maxsofar) {
						cl1 = clid1;
						cl2 = clid2;
						maxsofar = v[0];
					}
				}
			} // end for
			
			if (maxsofar < 0) {
				System.out.println(maxsofar);
				break;
			}
			
			// delete old clusters
			cluster1 = clusters.get(cl1);
			cluster2 = clusters.get(cl2);
			clusters.remove(cl2); // is this correct???
			clusters.remove(cl1);
			entropies.remove(cl2);
			entropies.remove(cl1);
			ctsizes.remove(cl2);
			ctsizes.remove(cl1);
			
			// create the new cluster and add it to the list of clusters
			cluster = new ArrayList<Integer>();
			int clustersize = cluster1.size();
			for (int attid = 0; attid < clustersize; attid++) {
				cluster.add(cluster1.get(attid).intValue());
			}
			clustersize = cluster2.size();
			for (int attid = 0; attid < clustersize; attid++) {
				cluster.add(cluster2.get(attid).intValue());
			}
			entropy = cs.get(cl1).get(cl2 - cl1 - 1)[1];
			ctsize = cs.get(cl1).get(cl2 - cl1 - 1)[2];
			clusters.add(cluster);
			entropies.add(entropy);
			ctsizes.add(ctsize);
			
			// remove similarities to old clusters
			for (int cid = 0; cid < cl1; cid++) {
				tmp = cs.get(cid);
				tmp.remove(cl2 - cid - 1);
				tmp.remove(cl1 - cid - 1);
			}
			for (int cid = cl1 + 1; cid < cl2; cid++) {
				tmp = cs.get(cid);
				tmp.remove(cl2 - cid - 1);
			}
			if (cl2 < nc - 1) {
				cs.remove(cl2);
				cs.add(new ArrayList<double[]>());
			}
			cs.remove(cl1);
			
			// compute similarities of the new cluster to existing clusters
			nc = clusters.size();
			for (int cid = 0; cid < nc - 1; cid++) {
				cs.get(cid).add(computeClusterSimilarity(data, numPoints, clusters.get(cid), entropies.get(cid), ctsizes.get(cid), clusters.get(nc - 1), entropies.get(nc - 1), ctsizes.get(nc - 1)));
			}
				
			// update best clustering
			prevcost -= maxsofar;
			if (prevcost < mincost) {
				bestClusters.clear();
				mincost = prevcost;
				for (int cid = 0; cid < nc; cid++) {
					bestClusters.add(clusters.get(cid));
				}
			}
			
			System.out.println(clusters.size());
		} // end while
		
		return bestClusters;
	}
	
	public static double[] computeClusterSimilarity(int[][] data, int numPoints, ArrayList<Integer> c1,
													double ent1, double ctsize1, ArrayList<Integer> c2, double ent2, double ctsize2) throws Exception {
		double[] ret = new double[4];
		
		// compute the joint code table
		ArrayList<ArrayList<Integer>> ct = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> sps = new ArrayList<Integer>();
		ArrayList<Boolean> hascommon = new ArrayList<Boolean>();
		ArrayList<Integer> c = null;
		int numatt1 = c1.size();
		int numatt2 = c2.size();
		int size;
		int cursp;
		boolean exists;
		boolean match;
		boolean hasit1, hasit2;
		for (int r = 0; r < numPoints; r++) {
			size = ct.size();
			exists = false;
			for (int cid = 0; cid < size; cid++) {
				c = ct.get(cid);
				match = true;
				for (int attid = 0; attid < numatt1; attid++)   {
					if (data[r][c1.get(attid)] != c.get(attid)) {
						match = false;
						break;
					}
				}
				
				if (match == true) {
					for (int attid = 0; attid < numatt2; attid++) {
						if (data[r][c2.get(attid)] != c.get(attid + numatt1)) {
							match = false;
							break;
						}
					}
				}
				
				// if itemset matches an existing one, increase the corresponding support and break the loop of code table
				if (match == true) {
					exists = true;
					cursp = sps.get(cid);
					cursp++;
					sps.remove(cid);
					sps.add(cid, cursp);
					break;
				}
			}
			
			// itemset not exist, add it
			if (exists == false) {
				hasit1 = false;
				hasit2 = false;
				c = new ArrayList<Integer>();
				for (int attid = 0; attid < numatt1; attid++) {
					c.add(data[r][c1.get(attid)]);
					if (data[r][c1.get(attid)] == 1) {
						hasit1 = true;
					}
				}
				for (int attid = 0; attid < numatt2; attid++) {
					c.add(data[r][c2.get(attid)]);
					if (data[r][c2.get(attid)] == 1) {
						hasit2 = true;
					}
				}
				ct.add(c);
				sps.add(1);
				if (hasit1 == true && hasit2 == true) {
					hascommon.add(true);
				} else {
					hascommon.add(false);
				}
			}
		}
		
		// compute the new code table's entropy and size
		double ent = 0;
		double ctsize = 0;
		double freq;
		double logbase = Math.log(2);
		double loglogD = Math.log(Math.log(numPoints) / logbase) / logbase;
		int count = 0;
		size = ct.size();
		for (int cid = 0; cid < size; cid++) {
			freq = sps.get(cid) * 1.0 / numPoints;
			ent += -freq * Math.log(freq) / logbase;
			ctsize += numatt1 + numatt2;
			ctsize += loglogD;
			ctsize -= Math.log(freq) / logbase;
			if (hascommon.get(cid) == true) {
				count += sps.get(cid);
			}
		}
		if (ent < 0 || ctsize < 0) {
			throw new Exception("error in computing entropy and code table size");
		}
		
		// populate ret
		ret[0] = numPoints * (ent1 + ent2 - ent) + ctsize1 + ctsize2 - ctsize;
		ret[1] = ent;
		ret[2] = ctsize;
		ret[3] = count;
		
		return ret;
	}
	
	public static double C(int N, int k) {
		double val1 = 1;
		double val2 = 1;
		
		int lowerBound = N - k + 1;
		for (int i = lowerBound; i <= N; i++) {
			val1 *= i;
		}
		
		for (int i = 1; i <= k; i++) {
			val2 *= i;
		}
		
		return (int)(val1 / val2);
	}
}