package uds.subspace;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class GraphMining {
	public static ArrayList<Integer> FindCliques(int numDims, String graphFileInput, String cliqueFileOutput, String binaryFileOutput) throws Exception {
		GraphAndDegree ret = loadGraph(numDims, graphFileInput, true);
		ArrayList<Integer> isoVertices = ret.isoVertices;
		int[] vertices = generateOrder(numDims, ret.neighborLists, ret.D, ret.count);
		ret = loadGraph(numDims, graphFileInput, false);
		ArrayList<ArrayList<Integer>> cliques = BronKerboschDegeneracy(numDims, vertices, ret.neighborLists, ret.D, ret.count, cliqueFileOutput);
		int size = cliques.size();
		outputBinaryCliques(numDims, isoVertices, cliques, binaryFileOutput);
		ArrayList<Integer> vals = new ArrayList<Integer>();
		vals.add(size);
		vals.add(numDims - isoVertices.size());
		return vals;
	}
	
	@SuppressWarnings("unchecked")
	public static GraphAndDegree loadGraph(int numDims, String graphFileOutput, boolean loadDegrees) throws FileNotFoundException {
		GraphAndDegree ret = new GraphAndDegree();
		ret.neighborLists = new ArrayList[numDims];
		if (loadDegrees) {
			ret.D = new ArrayList[numDims];
			ret.isoVertices = new ArrayList<Integer>();
		}
		
		Scanner scanner = new Scanner(new FileInputStream(graphFileOutput));
		String data;
		String[] vertices;
		int count = 0;
		for (int dim = 0; dim < numDims; dim++) {
			if (loadDegrees) {
				if (ret.D[dim] == null) {
					ret.D[dim] = new ArrayList<Integer>();
				}
			}
			
			ret.neighborLists[dim] = new ArrayList<Integer>();
			data = scanner.nextLine();
			if (!data.trim().equals("")) {
				count++;
				vertices = data.split(" ");
				for (int i = 0; i < vertices.length; i++) {
					ret.neighborLists[dim].add(new Integer(vertices[i]));
				}
				
				if (loadDegrees) {
					if (ret.D[vertices.length] == null) {
						ret.D[vertices.length] = new ArrayList<Integer>();
					}
					ret.D[vertices.length].add(dim);
				}
			} else {
				if (loadDegrees) {
					ret.isoVertices.add(dim);
				}
			} // end if
		} // end for
		ret.count = count;
		
		scanner.close();
		return ret;
	}
	
	public static ArrayList<ArrayList<Integer>> BronKerboschDegeneracy(int numDims, int[] vertices, ArrayList<Integer>[] neighborLists,
																	   ArrayList<Integer>[] D, int numUsedVertices, String cliqueFileOutput) throws IOException {
		ArrayList<ArrayList<Integer>> cliques = new ArrayList<ArrayList<Integer>>(); 
		ArrayList<Integer> P = new ArrayList<Integer>(); 
		ArrayList<Integer> R = new ArrayList<Integer>(); 
		ArrayList<Integer> X = new ArrayList<Integer>();
		
		// get the degeneracy order
		//int[] vertices = generateOrder(numDims, neighborLists, D, numUsedVertices);
		int numNNs;
		int v;
		int vi;
		System.out.println(vertices.length);
		for (int i = 0; i < vertices.length; i++) {
			P.clear();
			R.clear();
			X.clear();
			
			vi = vertices[i];
			numNNs = neighborLists[vi].size();
			for (int j = 0; j < numNNs; j++) {
				// populate P
				v = neighborLists[vi].get(j).intValue();
				for (int z = i + 1; z < vertices.length; z++) {
					if (v == vertices[z]) {
						P.add(new Integer(v));
						break;
					}
				}
				
				// populate X
				for (int z = 0; z <= i - 1; z++) {
					if (v == vertices[z]) {
						X.add(new Integer(v));
						break;
					}
				}
			} // end for
			
			// populate R
			R.add(new Integer(vi));
			
			// mine cliques
			BronKerboschPivot(neighborLists, cliques, P, R, X);
			//System.out.println("done with " + vertices[i]);
		} // end for
		
		// write the cliques to output file
		BufferedWriter writerClique = new BufferedWriter(new FileWriter(new File(cliqueFileOutput)));
		int numCliques = cliques.size();
		int numNodes;
		ArrayList<Integer> nodes = null;
		System.out.println("numCliques = " + numCliques);
		for (int i = 0; i < numCliques; i++) {
			nodes = cliques.get(i);
			numNodes = nodes.size();
			for (int j = 0; j < numNodes; j++) {
				if (j == 0) {
					writerClique.write(Integer.toString(nodes.get(j)));
				} else {
					writerClique.write("," + Integer.toString(nodes.get(j)));
				}
			} // end for
			
			if (i != numCliques - 1) {
				writerClique.newLine();
			}
		} // end for
		
		writerClique.flush();
		writerClique.close();
		
		return cliques;
	}
	
	public static void outputBinaryCliques(int numDims, ArrayList<Integer> isoVertices,
										   ArrayList<ArrayList<Integer>> cliques, String binaryFileOutput) throws Exception {
		BufferedWriter writerClique = new BufferedWriter(new FileWriter(new File(binaryFileOutput)));
		int numCliques = cliques.size();
		for (int dim = 0; dim < numDims; dim++) {
			if (isoVertices.contains(dim)) {
				/*for (int cid = 0; cid < numCliques; cid++)
				{
					if (cid == 0)
						writerClique.write("0");
					else
						writerClique.write(" 0");
				}*/
			} else {
				for (int cid = 0; cid < numCliques; cid++) {
					if (cliques.get(cid).contains(dim)) {
						if (cid == 0) {
							writerClique.write("1");
						} else {
							writerClique.write(" 1");
						}
					} else {
						if (cid == 0) {
							writerClique.write("0");
						} else {
							writerClique.write(" 0");
						}
					}
				}
				
				if (dim != numDims - 1) {
					writerClique.newLine();
				}
			}
		}
		
		writerClique.flush();
		writerClique.close();
	}
	
	public static void BronKerboschPivot(ArrayList<Integer>[] neighborLists, ArrayList<ArrayList<Integer>> cliques,
										 ArrayList<Integer> P, ArrayList<Integer> R, ArrayList<Integer> X) {
		int sizeP = P.size();
		int sizeR = R.size();
		int sizeX = X.size();
		int vertex;
		
		if (sizeP == 0) {
			if (sizeX == 0) {
				ArrayList<Integer> secondR = new ArrayList<Integer>();
				for (int i = 0; i < sizeR; i++) {
					vertex = R.get(i).intValue();
					secondR.add(new Integer(vertex));
				}
				cliques.add(secondR);
			}
			
			return;
		}
		
		int v;
		int numNNs;
		int numMatches;
		int maxMatches = -Integer.MAX_VALUE;
		int maxVertex = -1;
		ArrayList<Integer> secondP = new ArrayList<Integer>();
		ArrayList<Integer> secondX = new ArrayList<Integer>();
		
		// loop through all vertices in P
		for (int i = 0; i < sizeP; i++) {
			vertex = P.get(i).intValue();
			secondP.add(new Integer(vertex));
			numMatches = 0;
			numNNs = neighborLists[vertex].size();
			for (int j = 0; j < numNNs; j++) {
				v = neighborLists[vertex].get(j).intValue();
				for (int k = 0; k < sizeP; k++) {
					if (v == P.get(k).intValue()) {
						numMatches++;
						break;
					}
				}
			}
			
			if (numMatches > maxMatches) {
				maxMatches = numMatches;
				maxVertex = vertex;
			}
		}
		
		// loop through all vertices in X
		for (int i = 0; i < sizeX; i++) {
			vertex = X.get(i).intValue();
			secondX.add(new Integer(vertex));
			numMatches = 0;
			numNNs = neighborLists[vertex].size();
			for (int j = 0; j < numNNs; j++) {
				v = neighborLists[vertex].get(j).intValue();
				for (int k = 0; k < sizeP; k++) {
					if (v == P.get(k).intValue()) {
						numMatches++;
						break;
					}
				}
			}
			
			if (numMatches > maxMatches) {
				maxMatches = numMatches;
				maxVertex = vertex;
			}
		}
		
		// identify all vertices in P that are not in NN(maxVertex)
		secondP.removeAll(neighborLists[maxVertex]);
		int sizeSecondP = secondP.size();
		ArrayList<Integer> keepP = new ArrayList<Integer>();
		ArrayList<Integer> keepX = new ArrayList<Integer>();
		ArrayList<Integer> secondR = new ArrayList<Integer>();
		for (int i = 0; i < sizeSecondP; i++) {
			vertex = secondP.get(i).intValue();
			numNNs = neighborLists[vertex].size();
			keepP.clear();
			keepX.clear();
			secondR.clear();
			
			// find the intersection between NN(vertex) and P and X
			for (int j = 0; j < numNNs; j++) {
				v = neighborLists[vertex].get(j).intValue();
				
				// intersection with P
				for (int z = 0; z < sizeP; z++) {
					if (v == P.get(z).intValue()) {
						if (v == vertex) {
							throw new ArithmeticException("duplicate vertex");
						}
						
						keepP.add(new Integer(v));
						break;
					}
				}
				
				// intersection with X
				for (int z = 0; z < sizeX; z++) {
					if (v == X.get(z).intValue()) {
						keepX.add(new Integer(v));
						break;
					}
				}
			}
			
			// add vertex to R
			for (int id = 0; id < sizeR; id++) {
				v = R.get(id).intValue();
				secondR.add(new Integer(v));
			}
			secondR.add(new Integer(vertex));
			
			// recursive call
			BronKerboschPivot(neighborLists, cliques, keepP, secondR, keepX);
			
			for (int j = 0; j < sizeP; j++) {
				if (P.get(j).intValue() == vertex) {
					P.remove(j);
					sizeP--;
					break;
				}
			}
			X.add(new Integer(vertex));
			sizeX++;
		}
	}
	
	public static int[] generateOrder(int numDims, ArrayList<Integer>[] neighborLists, ArrayList<Integer>[] D, int numUsedVertices) {
		int[] ret = new int[numUsedVertices];
		
		int numNNs;
		int vertex;
		int numVertexNNs;
		int numVertices;
		for (int i = 0; i < ret.length; i++) {
			for (int j = 0; j < numDims; j++) {
				if (D[j].size() > 0) {
					// remove the first vertex of D[j]
					ret[i] = D[j].get(0).intValue();
					D[j].remove(0);
					
					// loop through the list of neighbors of the removed vertex
					numNNs = neighborLists[ret[i]].size();
					for (int k = 0; k < numNNs; k++) {
						// get the vertex
						vertex = neighborLists[ret[i]].get(k).intValue();
						
						// update the neighbors of the vertex
						numVertexNNs = neighborLists[vertex].size();
						for (int z = 0; z < numVertexNNs; z++) {
							if (neighborLists[vertex].get(z).intValue() == ret[i]) {
								neighborLists[vertex].remove(z);
								break;
							}
						}
						
						// remove the vertex from the old list
						numVertices = D[numVertexNNs].size();
						for (int z = 0; z < numVertices; z++) {
							if (D[numVertexNNs].get(z).intValue() == vertex) {
								D[numVertexNNs].remove(z);
								break;
							}
						}
						
						// add the vertex to the new list
						if (numVertexNNs >= 1) {
							D[numVertexNNs - 1].add(new Integer(vertex));
						}
					}
					
					break;
				} // end for
			} // end for
			
			//System.out.print(ret[i] + "---");
		} // end for
		System.out.println("\n\n--------------------------");
		
		return ret;
	}
}