package uds.cmi;

import java.util.ArrayList;

import uds.constants.Constants;
import uds.score.UDSFunction;

public class CMIFunction {
	private static final int NUM_SEEDS = 10;
	
	public static double computeScore(double[][] data, int[][] preRank) {
		int r = data.length;
		int c = data[0].length;
		int[][] rank = preRank;
		if (rank == null) {
			rank = UDSFunction.generateRank(data);
		}
		int[][] discrete = new int[r][c];
		double[] means = new double[c];
		double[] devs = new double[c];
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < c; j++) {
				discrete[i][j] = -1;
				means[j] += data[i][j] / r;
			}
		}
		
		// compute ces
		double[] ces = new double[c];
		for (int j = 0; j < c; j++) {
			double[] a = new double[r];
			for (int i = 0; i < r; i++) {
				a[i] = data[rank[i][j]][j];
				
				devs[j] += (data[i][j] - means[j]) * (data[i][j] - means[j]); 
			}
			ces[j] = UDSFunction.computeCE(a, true);
			
			if (r == 0) {
				devs[j] = 0;
			} else {
				devs[j] = Math.sqrt(devs[j] / (r - 1));
			}
		}
		
		// rank columns
		int[] colOrder = UDSFunction.rankColumns(ces);
		
		// compute score
		double sumScore = 0;
		for (int j = 1; j < c; j++) {
			sumScore += computeContrastSpecial(data, colOrder, j, ces, NUM_SEEDS, means, devs);
		}
		
		// control check
		if (sumScore < 0 && Math.abs(sumScore) > Constants.MAX_ERROR) {
			throw new ArithmeticException(String.format("Wrong score sumScore = %f", sumScore));
		}
		
		return sumScore;
	}
	
	public static double computeContrastSpecial(double[][] data, int[] colOrder, int X, double[] ces, int NumSeeds, double[] means, double[] devs) {
		int r = data.length;
		ArrayList<Integer>[] clusters = incrementalClusteringNewSeed(data, colOrder, X, NumSeeds, means, devs);
		
		double maxCE = Double.NEGATIVE_INFINITY;
		double ce = 0;
		ArrayList<Integer> curCluster = null;
		int curNumPoints;
		double[] vals = null;
		for (int j = 0; j < NumSeeds; j++) {
			curCluster = clusters[j];
			curNumPoints = curCluster.size();
			
			vals = new double[curNumPoints];
			for (int k = 0; k < curNumPoints; k++) {
				vals[k] = data[curCluster.get(k)][colOrder[X]];
			}
		
			ce += curNumPoints * UDSFunction.computeCE(vals, false) / r;
		} // end for
		
		maxCE = ces[colOrder[X]] - ce;
		if (maxCE < 0 && Math.abs(maxCE) > Constants.MAX_ERROR) {
			System.out.println("ces[" + colOrder[X] + "] = " + ces[colOrder[X]]);
			System.out.println("maxCE = " + maxCE);
			throw new ArithmeticException("wrong contrast!");
		}
		
		return Math.max(maxCE, 0);
	}
	
	public static ArrayList<Integer>[] incrementalClusteringNewSeed(double[][] data, int[] colOrder, int X, int NumSeeds, double[] means, double[] devs)
	{
		@SuppressWarnings("unchecked")
		ArrayList<Integer>[] ret = new ArrayList[NumSeeds];
		for (int i = 0; i < NumSeeds; i++)
			ret[i] = new ArrayList<Integer>();
		
		int r = data.length;
		double[][] seeds = new double[NumSeeds][X];
		int curDim;
		for (int i = 0; i < NumSeeds; i++) {
			for (int j = 0; j < X; j++) {
				curDim = colOrder[j];
				if (j % 2 == 0) {
					seeds[i][j] = means[curDim] + i * devs[curDim] / (X * (NumSeeds - 1));
				} else {
					seeds[i][j] = means[curDim] - i * devs[curDim] / (X * (NumSeeds - 1));
				}
			}
		}
		
		double[] curPoint = null;
		double[] curSeed = null;
		double min;
		int minSeedId;
		double dist;
		int numMSteps = (int)Math.sqrt(r);
		for (int scan = 0; scan < 3; scan++) {
			for (int seedID = 0; seedID < NumSeeds; seedID++) {
				ret[seedID].clear();
			}
			
			for (int i = 0; i < r; i++) {
				curPoint = data[i];
				min = Double.MAX_VALUE;
				minSeedId = -1;
				for (int j = 0; j < NumSeeds; j++) {
					curSeed = seeds[j];
					dist = distance(curPoint, curSeed, colOrder);
					if (dist < min) {
						min = dist;
						minSeedId = j;
					} // end if
				} // end for
				
				ret[minSeedId].add(new Integer(i));
				
				if (i % numMSteps == 0 && scan == 0) {
					performMStep(data, colOrder, X, seeds, ret);
				}
			} // end for
			
			performMStep(data, colOrder, X, seeds, ret);
		}
		
		return ret;
	}
	
	public static void performMStep(double[][] data, int[] colOrder, int X, double[][] seeds, ArrayList<Integer>[] members)
	{
		int numSeeds = seeds.length;
		int numPoints;
		double[] curPoint  = null;
		int curDim;
		for (int i = 0; i < numSeeds; i++) {
			numPoints = members[i].size();
			for (int j = 0; j < numPoints; j++) {
				curPoint = data[members[i].get(j)];
				for (int k = 0; k < X; k++) {
					curDim = colOrder[k];
					if (j == 0) {
						seeds[i][k] = curPoint[curDim] / numPoints;
					} else {
						seeds[i][k] += curPoint[curDim] / numPoints;
					}
				}
			} // end for
		} // end for
	}
	
	private static double distance(double[] a, double[] b, int[] colOrder) {
		double sum = 0;
		for (int i = 0; i < b.length; i++) {
			sum += (a[colOrder[i]] - b[i]) * (a[colOrder[i]] - b[i]);
		}
		
		return Math.sqrt(sum);
	}
}