package uds.roc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class ROCComp {
	public static HashSet<String> readOutlierLabels(String fileOutliers) throws IOException {
		Scanner scan = null;
		try {
			HashSet<String> labels = new HashSet<String>();
			scan = new Scanner(new File(fileOutliers));
			String[] vals = scan.nextLine().split(",");
			for (String v : vals) {
				if (!labels.contains(v)) {
					labels.add(v);
				}
			}
			
			return labels;
		} catch (IOException e) {
			throw e;
		} finally {
			if (scan != null) {
				scan.close();
			}
		}
	}
	
	public static void process(String fileInput, String fileOutput, HashSet<String> outlierLabels) throws Exception {
		try {
			// this is used for reading data from the input file
			Scanner scanner = new Scanner(new FileInputStream(fileInput));
			
			// this is used for writing normalized data to the output file
			BufferedWriter writer = new BufferedWriter(new FileWriter(new File(fileOutput)));
			
			String s = null;
			int count  = 0;
			int numOutliers = 0;
			SortedObject tmp = null;
			ArrayList<SortedObject> scoreLabels = new ArrayList<SortedObject>();
			String[] vals = null;
			double score;
			while (scanner.hasNextLine()) {
				s = scanner.nextLine();
				
				if (s.trim().length() == 0) {
					break;
				}
				
				vals = s.split(" ");
				score = 0;
				if (!vals[1].equals("Infinity") && !vals[1].equals("NaN")) {
					score = Double.parseDouble(vals[1]);
				}
				tmp = new SortedObject(vals[2], score);
				scoreLabels.add(tmp);
				if (outlierLabels.contains(vals[2])) {
					numOutliers++;
				}
				count++;
			}
			
			SortedObject[] arScoreLabels = new SortedObject[count];
			for (int i = 0; i < count; i++) {
				arScoreLabels[i] = scoreLabels.get(i);
			}
			Arrays.sort(arScoreLabels);
			
			double[] x = new double[count];
			double[] y = new double[count];
			int numNormalPoints = count - numOutliers;
			String curLabel;
			int numOutliersSoFar = 0;
			int numNormalPointsSoFar = 0;
			double auc = 0;
			for (int i = count - 1; i >= 0; i--) {
				curLabel = arScoreLabels[i].classLabel;
				if (!outlierLabels.contains(curLabel)) {
					numNormalPointsSoFar++;
				} else {
					numOutliersSoFar++;
				}
				
				x[count - 1 - i] = numNormalPointsSoFar / (1.0 * numNormalPoints);
				y[count - 1 - i] = numOutliersSoFar / (1.0 * numOutliers);
			}
			
			for (int i = 0; i < count; i++) {
				if (i > 0) {
					auc += (x[i] - x[i - 1]) * y[i];
				}
				writer.write(Double.toString(x[i]) + ";" + Double.toString(y[i]));
				writer.newLine();
			}
			writer.write(Double.toString(auc));
			
			// close input and output streams
			scanner.close();
			writer.flush();
			writer.close();
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}
}