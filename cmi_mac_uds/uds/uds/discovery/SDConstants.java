package uds.discovery;

public class SDConstants {
	public static String FILEIN;
	public static String FILEOUT;
	public static int ORDINAL;
	public static int CATEGORIC;
	public static int NUMTARGET;
	public static int ORDTARGET;
	public static int CATTARGET;
	public static int NUMERIC;
	public static int MIN_COV = 500;
	public static int MAX_DEPTH = 3;
	public static int W = 100;
	public static int K = 10;
}