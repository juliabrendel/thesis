package uds.discovery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Database {
	private int numCategoric;
	private int numOrdinal;
	private int numNumeric;
	private int numOrdinalTarget;
	private int numNumericTarget;
	private int numCategoricTarget;
	private ArrayList<DataPoint> points;
	private Map<Integer, Integer>[] ordinalTargetCount;
	private Map<String[], Integer> categoricTargetCount;
	private Map<String, Integer>[] individualCategoricTargetCount;
	private double[] numericTargetMax;
	
	public Database() {
		points = new ArrayList<DataPoint>();
	}
	public Database(ArrayList<DataPoint> points) {
		setPoints(points);
	}
	
	public int getNumCategoric() {
		return numCategoric;
	}
	public void setNumCategoric(int n) {
		numCategoric = n;
	}
	
	public int getNumOrdinal() {
		return numOrdinal;
	}
	public void setNumOrdinal(int n) {
		numOrdinal = n;
	}
	
	public int getNumNumeric() {
		return numNumeric;
	}
	public void setNumNumeric(int n) {
		numNumeric = n;
	}
	
	public int getNumOrdinalTarget() {
		return numOrdinalTarget;
	}
	public void setNumOrdinalTarget(int n) {
		numOrdinalTarget = n;
	}
	
	public int getNumNumericTarget() {
		return numNumericTarget;
	}
	public void setNumNumericTarget(int n) {
		numNumericTarget = n;
	}
	
	public int getNumCategoricTarget() {
		return numCategoricTarget;
	}
	public void setNumCategoricTarget(int n) {
		numCategoricTarget = n;
	}
	
	public ArrayList<DataPoint> getPoints() {
		return points;
	}
	public void setPoints(ArrayList<DataPoint> points) {
		this.points = points;
	}
	
	public Map<Integer, Integer>[] getOrdinalTargetCount() {
		return ordinalTargetCount;
	}
	
	public Map<String[], Integer> getCategoricTargetCount() {
		return categoricTargetCount;
	}
	
	public Map<String, Integer>[] getIndividualCategoricTargetCount() {
		return individualCategoricTargetCount;
	}
	
	public double[] getNumericTargetMax() {
		return numericTargetMax;
	}
	
	@SuppressWarnings("unchecked")
	public void createOrdinalTargetCount() {
		ordinalTargetCount = new Map[numOrdinalTarget];
		
		for (int i = 0; i < numOrdinalTarget; i++) {
			ordinalTargetCount[i] = new HashMap<Integer, Integer>();
			
			// collect set of distinct values of ordinal target i
			HashSet<Integer> set = new HashSet<Integer>();
			for (DataPoint p : points) {
				set.add(p.getOrdinalTarget()[i]);
			}
			
			// sort these values
			ArrayList<Integer> data = new ArrayList<Integer>();
			for (Integer v : set) {
				data.add(v);
			}
			Collections.sort(data);
			
			// record their statistics
			int size = data.size();
			for (int c = size - 1; c >= 0; c--) {
				ordinalTargetCount[i].put(data.get(c), size - c);
			}
		}
	}
	
	public void createNumericTargetMax() {
		numericTargetMax = new double[numNumericTarget];
		double val;
		for (int i = 0; i < numNumericTarget; i++) {
			numericTargetMax[i] = Double.NEGATIVE_INFINITY;
			
			for (DataPoint p : points) {
				val = p.getNumericTarget()[i];
				if (val > numericTargetMax[i]) {
					numericTargetMax[i] = val;
				}
			}
		}
	}
	
	public void createCategoricTargetCount() {
		categoricTargetCount = new HashMap<String[], Integer>();
		for (DataPoint p : points) {
			if (categoricTargetCount.containsKey(p.getCategoricTarget())) {
				int a = categoricTargetCount.get(p.getCategoricTarget()) + 1;
				categoricTargetCount.put(p.getCategoricTarget(), a);
			} else {
				categoricTargetCount.put(p.getCategoricTarget(), 1);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void createIndividualCategoricTargetCount() {
		individualCategoricTargetCount = new Map[numCategoricTarget];
		
		for (int i = 0; i < numCategoricTarget; i++) {
			individualCategoricTargetCount[i] = new HashMap<String, Integer>();
			
			for (DataPoint p : points) {
				if (individualCategoricTargetCount[i].containsKey(p.getCategoricTarget()[i])) {
					int a = individualCategoricTargetCount[i].get(p.getCategoricTarget()[i]) + 1;
					individualCategoricTargetCount[i].put(p.getCategoricTarget()[i], a);
				} else {
					individualCategoricTargetCount[i].put(p.getCategoricTarget()[i], 1);
				}
			}
		}
	}
}