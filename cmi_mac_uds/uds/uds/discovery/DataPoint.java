package uds.discovery;

public class DataPoint {
	private String[] categoric;
	private int[] ordinal;
	private double[] numeric;
	private int[] ordinalTarget;
	private double[] numericTarget;
	private String[] categoricTarget;
	
	public DataPoint(double[] numeric, int[] ordinal, String[] categoric, double[] numericTarget, int[] ordinalTarget, String[] categoricTarget) {
		setNumeric(numeric);
		setOrdinal(ordinal);
		setCategoric(categoric);
		setNumericTarget(numericTarget);
		setOrdinalTarget(ordinalTarget);
		setCategoricTarget(categoricTarget);
	}
	
	public double[] getNumeric() {
		return numeric;
	}
	public void setNumeric(double[] numeric) {
		this.numeric = numeric;
	}
	
	public int[] getOrdinal() {
		return ordinal;
	}
	public void setOrdinal(int[] ordinal) {
		this.ordinal = ordinal;
	}
	
	public String[] getCategoric() {
		return categoric;
	}
	public void setCategoric(String[] categoric) {
		this.categoric = categoric;
	}
	
	public double[] getNumericTarget() {
		return numericTarget;
	}
	public void setNumericTarget(double[] numericTarget) {
		this.numericTarget = numericTarget;
	}
	
	public int[] getOrdinalTarget() {
		return ordinalTarget;
	}
	public void setOrdinalTarget(int[] ordinalTarget) {
		this.ordinalTarget = ordinalTarget;
	}
	
	public String[] getCategoricTarget() {
		return categoricTarget;
	}
	public void setCategoricTarget(String[] categoricTarget) {
		this.categoricTarget = categoricTarget;
	}
}