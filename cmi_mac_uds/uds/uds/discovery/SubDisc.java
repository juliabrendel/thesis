package uds.discovery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;

public class SubDisc {
	public static Subgroup[] find(Database db, double globalCorr) throws Exception {
		int m = db.getPoints().size();
		ArrayList<Integer> pids = new ArrayList<Integer>();
		for (int i = 0; i < m; i++) {
			pids.add(i);
		}
		
		PriorityQueue<Subgroup> R = new PriorityQueue<Subgroup>();
		ArrayList<Subgroup> Beam = new ArrayList<Subgroup>();
		int depth = 1;
		int n = db.getNumNumeric() + db.getNumOrdinal() + db.getNumCategoric();
		
		while (depth <= SDConstants.MAX_DEPTH) {
			PriorityQueue<Subgroup> Cands = new PriorityQueue<Subgroup>();
			if (Beam.size() == 0) {
				for (int i = 0; i < n; i++) {
					ArrayList<Subgroup> tmp = Discretization.generateRefinements(db, pids, i, globalCorr);
					for (Subgroup sub : tmp) {
						if (sub.getPoints().size() > SDConstants.MIN_COV) {
							if (Cands.size() < SDConstants.W) {
								Cands.offer(sub);
							} else {
								if (sub.getQuality() > Cands.peek().getQuality()) {
									Cands.poll();
									Cands.offer(sub);
								}
							}
						}
					}
				}
			} else {
				for (Subgroup sub : Beam) {
					int lastAttributeId = sub.getCons().get(sub.getCons().size() - 1).getId();
					for (int i = lastAttributeId + 1; i < n; i++) {
						ArrayList<Subgroup> tmp = Discretization.generateRefinements(db, sub.getPoints(), i, globalCorr);
						
						for (Subgroup d : tmp) {
							Subgroup newSub = new Subgroup();
							newSub.getCons().addAll(sub.getCons());
							newSub.getCons().add(d.getCons().get(0));
							newSub.setPoints(d.getPoints());
							
							if (newSub.getPoints().size() > SDConstants.MIN_COV) {
								newSub.setQuality(db, globalCorr);
								
								if (Cands.size() < SDConstants.W) {
									Cands.offer(newSub);
								} else {
									if (newSub.getQuality() > Cands.peek().getQuality()) {
										Cands.poll();
										Cands.offer(newSub);
									}
								}
							}
						}
					}
				}
			}
			
			// update top K
			for (Subgroup sub : Cands) {
				if (R.size() < SDConstants.K) {
					R.offer(sub);
				} else {
					if (sub.getQuality() > R.peek().getQuality()) {
						R.poll();
						R.offer(sub);
					}
				}
			}
			
			Beam.clear();
			Beam.addAll(Cands);
			depth++;
		}
		
		Subgroup[] ret = new Subgroup[R.size()];
		ret = R.toArray(ret);
		Arrays.sort(ret, Collections.reverseOrder());
			
		return ret;
	}
}