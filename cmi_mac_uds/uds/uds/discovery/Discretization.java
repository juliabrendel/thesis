package uds.discovery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import uds.score.Bin;

public class Discretization {
	private static final int NUM_BINS = 5;
	private static final boolean EW = false;
	
	public static ArrayList<Subgroup> generateRefinements(Database db, ArrayList<Integer> usedPids, int dim, double globalCorr) throws Exception {
		ArrayList<Subgroup> refs = new ArrayList<Subgroup>();
		ArrayList<Bin> bins = null;

		if (dim < db.getNumNumeric() + db.getNumOrdinal()) {
			int numDesiredBins = NUM_BINS;
			bins = formInitialBins(db, usedPids, dim, numDesiredBins, EW, globalCorr);

			System.out.printf("number of final bins = %d\n\n", bins.size());
			for (Bin b : bins) {
				Subgroup sub = new Subgroup();
				sub.setPoints(b.getPoints());
				Double min = Double.POSITIVE_INFINITY;
				Double max = Double.NEGATIVE_INFINITY;
				double val;
				for (Integer p : sub.getPoints()) {
					if (dim < db.getNumNumeric()) {
						val = db.getPoints().get(p).getNumeric()[dim];
					} else {
						val = db.getPoints().get(p).getOrdinal()[dim
								- db.getNumNumeric()];
					}

					if (val < min) {
						min = val;
					}

					if (val > max) {
						max = val;
					}
				}

				Attribute att = new Attribute(dim, min, max);
				sub.getCons().add(att);
				sub.setQuality(db, globalCorr);
				refs.add(sub);
			}
		} else {
			HashMap<String, ArrayList<Integer>> map = new HashMap<String, ArrayList<Integer>>();
			String val;
			ArrayList<Integer> pids = null;
			for (Integer p : usedPids) {
				val = db.getPoints().get(p).getCategoric()[dim
						- db.getNumNumeric() - db.getNumOrdinal()];
				if (map.containsKey(val)) {
					pids = map.get(val);
				} else {
					pids = new ArrayList<Integer>();
				}

				pids.add(p);
				map.put(val, pids);
			}

			for (String key : map.keySet()) {
				Subgroup sub = new Subgroup();
				sub.setPoints(map.get(key));
				Attribute att = new Attribute(dim, key, key);
				sub.getCons().add(att);
				sub.setQuality(db, globalCorr);
				refs.add(sub);
			}
		}

		return refs;
	}
	
	private static ArrayList<Bin> formInitialBins(Database db, ArrayList<Integer> pids, int dim, int numDesiredBins, boolean equalWidth, double globalCorr) {
		ArrayList<SortedKey> sortedKeys = new ArrayList<SortedKey>();
		SortedKey key = null;
		for (Integer p : pids) {
			key = new SortedKey();
			if (dim < db.getNumNumeric()) {
				key.value = db.getPoints().get(p).getNumeric()[dim];
			} else {
				key.value = db.getPoints().get(p).getOrdinal()[dim - db.getNumNumeric()];
			}
			key.pid = p;
			sortedKeys.add(key);
		}
		//Collections.sort(sortedKeys);
		
		if (!equalWidth) {
			return formInitialBins(sortedKeys, numDesiredBins);
		} else {
			return formEqualWidth(sortedKeys, numDesiredBins);
		}
	}
	
	private static ArrayList<Bin> formInitialBins(ArrayList<SortedKey> keys, int numDesiredBins) {
		ArrayList<Bin> ret = new ArrayList<Bin>();
		//System.out.printf("numDesiredBins = %d\n", numDesiredBins);
		
		// extract distinct values
		HashMap<Double, ArrayList<Integer>> distinctValues = new HashMap<Double, ArrayList<Integer>>();
		ArrayList<Double> sortedKeys = new ArrayList<Double>();
		for (SortedKey key : keys) {
			ArrayList<Integer> pids = null;
			if (distinctValues.containsKey(key.value)) {
				pids = distinctValues.get(key.value);
			} else {
				sortedKeys.add(key.value);
				pids = new ArrayList<Integer>();
			}
			
			pids.add(key.pid);
			distinctValues.put(key.value, pids);
		}
		Collections.sort(sortedKeys);
		
		// number of distinct values per bin
		int numRows = keys.size();
		int totalNumBins = numDesiredBins;
		int desiredBinSize = (int)Math.floor(numRows * 1.0 / totalNumBins);
		//System.out.printf("numDesiredBins = %d\n", numDesiredBins);
		
		// discretize
		int[] indices = new int[distinctValues.size()];
		int i = 1;
		int curDistinctVal = 0;
		int curBinIndex = 1;
		int count;
		while (curDistinctVal < indices.length) {
			count = 0;
			for (int j = 0; j < indices.length; j++) {
				if (indices[j] == curBinIndex) {
					count = count + distinctValues.get(sortedKeys.get(j)).size();
				}
			}
			
			if (count != 0 && Math.abs(count + distinctValues.get(sortedKeys.get(curDistinctVal)).size() - desiredBinSize) >= Math.abs(count - desiredBinSize)) {
				curBinIndex++;
				desiredBinSize = (int)((numRows - i + 1) * 1.0) / (totalNumBins - curBinIndex + 1);
			}
			indices[curDistinctVal] = curBinIndex;
			i = i + distinctValues.get(sortedKeys.get(curDistinctVal)).size();
			curDistinctVal++;
		}
		//System.out.println("curBinIndex = " + curBinIndex);
		
		// create bin
		Bin tmpBin = null;
		curBinIndex = 1;
		curDistinctVal = 0;
		int newDistinctVal;
		ArrayList<Integer> ds = null;
		while (curDistinctVal < indices.length) {
			for (i = curDistinctVal + 1; i < indices.length; i++) {
				if (indices[i] != curBinIndex) {
					break;
				}
			}
			newDistinctVal = i;
			
			// get current value and add the corresponding points to the bin
			ds = new ArrayList<Integer>();
			for (i = curDistinctVal; i < newDistinctVal; i++) {
				ds.addAll(distinctValues.get(sortedKeys.get(i)));
			}
			tmpBin = new Bin(ds);
			
			// add the bin to the list of bins
			ret.add(tmpBin);
			
			curDistinctVal = newDistinctVal;
			curBinIndex++;
		} // end for
		
		// subsampling and control check
		count = 0;
		for (Bin bin : ret) {
			count += bin.getPoints().size();
		}
		if (count != numRows) {
			throw new ArithmeticException("some point is missing!");
		}
		
		System.out.printf("number of initial bins = %d\n", ret.size());
			
		return ret;
	}
	
	private static <T extends Number> ArrayList<Bin> formEqualWidth(ArrayList<SortedKey> keys, int numDesiredBins) {
		Collections.sort(keys);
		double range = (keys.get(keys.size() - 1).value - keys.get(0).value) / numDesiredBins;
		ArrayList<Bin> ret = new ArrayList<Bin>();
		Bin tmp = null;
		double lower, upper;
		int start = 0;
		for (int i = 0; i < numDesiredBins; i++) {
			lower = i * range + keys.get(0).value;
			upper = (i + 1) * range + keys.get(0).value;
			if (i == numDesiredBins - 1) {
				upper = upper + 1;
			}
			
			ArrayList<Integer> pids = new ArrayList<Integer>();
			for (int j = start; j < keys.size(); j++) {
				if (keys.get(j).value >= lower && keys.get(j).value < upper) {
					pids.add(keys.get(j).pid);
				} else {
					start = j;
					break;
				}
			}
			
			if (pids.size() > 0) {
				tmp = new Bin(pids);
				ret.add(tmp);
			}
		}
		
		return ret;
	}
}