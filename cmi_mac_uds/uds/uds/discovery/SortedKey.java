package uds.discovery;

public class SortedKey implements Comparable<SortedKey> {
	public double value;
	public int pid;
	
	@Override
	public int compareTo(SortedKey o) {
		if (value > o.value) {
			return 1;
		} else if (value == o.value) {
			return 0;
		}
		
		return -1;
	}
}