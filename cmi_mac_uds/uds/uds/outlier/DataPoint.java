package uds.outlier;

import java.util.ArrayList;

public class DataPoint 
{
	public int id;
	public double[] measures;
	
	public ArrayList<Integer> neighbors;					
	public ArrayList<Double> distToNeighbors;				
	public double lrd;										
	public String classID;									
	
	public DataPoint(int id, double[] measures, String classID) {
		this.id = id;
		this.measures = measures;
		this.classID = classID;
		
		neighbors = new ArrayList<Integer>();
		distToNeighbors = new ArrayList<Double>();
	}
	
	public void insertNeighbors(ArrayList<Integer> dims, DataPoint q) {
		double dist = DataPoint.distanceLNorm(2, dims, this, q);
		int numNeighbors = this.neighbors.size();
		int pos = numNeighbors;
		
		double tmpDist;
		for (int i = numNeighbors - 1; i >= 0; i--) {
			tmpDist = this.distToNeighbors.get(i).doubleValue();
			if (dist >= tmpDist) {
				break;
			}
			
			pos = i;
		}
		
		this.neighbors.add(pos, q.id);
		this.distToNeighbors.add(pos, dist);
		numNeighbors++;
		
		for (int i = numNeighbors - 1; i >= OUTConstants.K; i--) {
			this.neighbors.remove(i);
			this.distToNeighbors.remove(i);
		}
	}
	
	public static double distanceLNorm(int expo, ArrayList<Integer> dims, DataPoint p, DataPoint q) {
		int numMeasuresCols = dims.size();
		double dist = 0;
		int curCol;
		for (int i = 0; i < numMeasuresCols; i++) {
			curCol = dims.get(i).intValue();
			dist += Math.pow(Math.abs(p.measures[curCol] - q.measures[curCol]), expo);
		}
		
		return Math.pow(dist, 1.0 / expo);
	}
}