import java.util.*;
import java.io.*;

public class MainClass 
{
	@SuppressWarnings("unchecked")
	public static void main(String[] args)
	{
		try
		{
			//Get the jvm heap size.
	        long heapSize = Runtime.getRuntime().totalMemory();
	        System.out.println("Heap Size = " + heapSize);
	        
	        readInputString(args);
			DataMatrix dataMatrix = new DataMatrix(Constants.NUM_ROWS, Constants.NUM_MEASURE_COLS);
			
			System.out.println("start reading input data...");
			DataReader.readData(Constants.FILE_INPUT, dataMatrix);
			System.out.println("end reading input data...");
			
			System.out.println("start discretization...");
			BufferedWriter writerCP = new BufferedWriter(new FileWriter(new File(Constants.FILE_CP_OUTPUT)));
			BufferedWriter writerRuntime = new BufferedWriter(new FileWriter(new File(Constants.FILE_RUNTIME_OUTPUT)));
			
			long start = System.currentTimeMillis();
			ArrayList<MacroBin>[] finalBins = null;
			finalBins = MMIC.discretizeData(dataMatrix, Constants.CRES);
			long end = System.currentTimeMillis();
			writerRuntime.write("MTC = " + Double.toString(Constants.MTC));
			writerRuntime.newLine();
			writerRuntime.write(Long.toString(end - start));
			writerRuntime.flush();
			writerRuntime.close();
			
			int numCols = Constants.NUM_MEASURE_COLS;
			int tmpNumMacroBins;
			MacroBin tmpMacroBin = null;
			MacroBin tmpPrevMacroBin = null;
			ArrayList<MacroBin>[] outputBins = new ArrayList[numCols];
			for (int dim = 0; dim < numCols; dim++)
			{
				outputBins[dim] = new ArrayList<MacroBin>();
				tmpNumMacroBins = finalBins[dim].size();
				tmpPrevMacroBin = finalBins[dim].get(0);
				outputBins[dim].add(tmpPrevMacroBin);
				for (int binIndex = 1; binIndex < tmpNumMacroBins; binIndex++)
				{
					tmpMacroBin = finalBins[dim].get(binIndex);
					if (tmpMacroBin.upperBound > tmpPrevMacroBin.upperBound)
						outputBins[dim].add(tmpMacroBin);
					tmpPrevMacroBin = tmpMacroBin;
				}
				tmpNumMacroBins = outputBins[dim].size();
				
				writerCP.write("dimension " + Integer.toString(dim) + " (" + tmpNumMacroBins + " bins)");
				writerCP.newLine();
				for (int binIndex = 0; binIndex < tmpNumMacroBins; binIndex++)
				{
					tmpMacroBin = outputBins[dim].get(binIndex);
					writerCP.write(Double.toString(tmpMacroBin.upperBound));
					writerCP.newLine();
				}
				writerCP.write("-------------------------------------");
				writerCP.newLine();
			}
			System.out.println("end discretization...");
			
			writerCP.flush();
			writerCP.close();
			
			// write discretized data
			BufferedWriter writerData = new BufferedWriter(new FileWriter(new File(Constants.FILE_DATA_OUTPUT)));
			writerData.write("@relation DB");
			writerData.newLine();
			writerData.newLine();
			
			// write discretized continuous data
			int numRows = Constants.NUM_ROWS;
			double tmpLowerBound;
			double tmpUpperBound;
			String[][] outputData = new String[numRows][numCols];
			String[] pointLabels = new String[numRows];
			DataPoint tmpPoint = null;
			String prefix = null;
			String attName = null;
			int nominalIndex = 1;
			for (int dim = 0; dim < numCols; dim++)
			{
				prefix = Integer.toString(dim);
				writerData.write("@attribute dim" + prefix + " {");
				tmpNumMacroBins = outputBins[dim].size();
				for (int binIndex = 0; binIndex < tmpNumMacroBins; binIndex++)
				{
					tmpMacroBin = outputBins[dim].get(binIndex);
					attName = Integer.toString(nominalIndex);
					tmpMacroBin.name = attName;
					if (binIndex == 0)
						writerData.write(attName);
					else
						writerData.write("," + attName);
					nominalIndex++;
				} // end for
				
				writerData.write("}");
				writerData.newLine();
			} // end for
			
			// discretize data
			boolean attNameExists;
			for (int i = 0; i < numRows; i++)
			{
				tmpPoint = dataMatrix.data.get(i);
				pointLabels[i] = new String(tmpPoint.classID);
				for (int dim = 0; dim < numCols; dim++)
				{
					attNameExists = false;
					tmpNumMacroBins = outputBins[dim].size();
					for (int binIndex = 0; binIndex < tmpNumMacroBins; binIndex++)
					{
						tmpMacroBin = outputBins[dim].get(binIndex);
						tmpLowerBound = tmpMacroBin.lowerBound;
						tmpUpperBound = tmpMacroBin.upperBound;
						if (tmpPoint.measures[dim] <= tmpUpperBound && tmpPoint.measures[dim] >= tmpLowerBound)
						{
							outputData[i][dim] = tmpMacroBin.name;
							attNameExists = true;
							break;
						}
					} // end for
					
					if (attNameExists == false)
					{
						System.out.println(i + " --- " + dim + " --- " + tmpPoint.measures[dim]);
						throw new Exception("Attribute nominal value not found");
					}
				} // end for
			} // end for
			
			// write class labels
			writerData.write("@attribute class {");
			int numLabels = Constants.CLASS_LABELS.size();
			for (int i = 0; i < numLabels; i++)
			{
				if (i == 0)
					writerData.write("\"" + Constants.CLASS_LABELS.get(i) + "\"");
				else
					writerData.write("," + "\"" +  Constants.CLASS_LABELS.get(i) + "\"");
			}
			writerData.write("}");
			writerData.newLine();
			writerData.newLine();
			
			// write the actual data
			writerData.write("@data");
			writerData.newLine();
			for (int i = 0; i < numRows; i++)
			{
				tmpPoint = dataMatrix.data.get(i);
				for (int j = 0; j < numCols; j++)
				{
					if (outputData[i][j] == null)
						throw new Exception("Null value");
						
					writerData.write(outputData[i][j] + ",");
				}
				writerData.write("\"" + pointLabels[i] + "\"");
				
				if (i != numRows - 1)
					writerData.newLine();
			}
			
			writerData.flush();
			writerData.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public static void readInputString(String[] args) throws Exception
	{	
		int i;
		int total = args.length - 1;
		
		// take FILE_INPUT
		boolean found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILE_INPUT"))
			{
				Constants.FILE_INPUT = args[i + 1];
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -FILE_INPUT");
		
		// take FILE_CP_OUTPUT
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILE_CP_OUTPUT"))
			{
				Constants.FILE_CP_OUTPUT = args[i + 1];
				File f = new File(Constants.FILE_CP_OUTPUT);
				if (f.exists())
					f.delete();
				f.createNewFile();
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -FILE_CP_OUTPUT");
		
		// take FILE_RUNTIME_OUTPUT
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILE_RUNTIME_OUTPUT"))
			{
				Constants.FILE_RUNTIME_OUTPUT = args[i + 1];
				File f = new File(Constants.FILE_RUNTIME_OUTPUT);
				if (f.exists())
					f.delete();
				f.createNewFile();
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -FILE_RUNTIME_OUTPUT");
		
		// take FILE_DATA_OUTPUT
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FILE_DATA_OUTPUT"))
			{
				Constants.FILE_DATA_OUTPUT = args[i + 1];
				File f = new File(Constants.FILE_DATA_OUTPUT);
				if (f.exists())
					f.delete();
				f.createNewFile();
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -FILE_DATA_OUTPUT");
		
		// take NUM_ROWS
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-NUM_ROWS"))
			{
				Constants.NUM_ROWS = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -NUM_ROWS");
		
		// take NUM_MEASURE_COLS
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-NUM_MEASURE_COLS"))
			{
				Constants.NUM_MEASURE_COLS = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
		if (found == false)
			throw new Exception("Missing -NUM_MEASURE_COLS");
		
		// take FIELD_DELIMITER
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-FIELD_DELIMITER"))
			{
				Constants.FIELD_DELIMITER = args[i + 1];
				found = true;
				break;
			}
		
		// take ALPHA
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-ALPHA"))
			{
				Constants.ALPHA = Double.parseDouble(args[i + 1]);
				found = true;
				break;
			}
		
		// take CLUMPS
		found = false;
		for (i = 0; i < total; i++)
			if (args[i].equals("-CLUMPS"))
			{
				Constants.CLUMPS = Integer.parseInt(args[i + 1]);
				found = true;
				break;
			}
	} // end method
}