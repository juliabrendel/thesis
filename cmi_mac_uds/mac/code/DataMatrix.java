import java.util.*;

public class DataMatrix 
{
	public int rows;
	public int cols;
	public ArrayList<DataPoint> data;
	
	public DataMatrix()
	{
		// do nothing
	}
	
	public DataMatrix(int rows, int cols)
	{
		this.rows = rows;
		this.cols = cols;
		this.data = new ArrayList<DataPoint>();
	}
	
	public DataMatrix(int rows, int cols, ArrayList<DataPoint> data)
	{
		this.rows = rows;
		this.cols = cols;
		this.data = data;
	}
}