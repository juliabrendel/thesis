\Chapter{Solution}{The road to Isengard}
\newcommand{\NLTK}{\textsc{NLTK}\xspace}
\newcommand{\StanfordNLP}{\textsc{Stanford CoreNLP}\xspace}
In this thesis we analyse several datasets consisting of books or articles. We look at \textit{Moby Dick}, \textit{Hobbit} and \textit{Lord Of The Rings} trilogy, as well as a~set of few hundreds articles from \NYT. 

Since we are working with real world text, there is some preprocessing necessary in order to ensure compatibility with the computer understandable format. First, we treat every chapter of a book and every article as a separate entry point for the algorithm, ie. a~singleton storyline. Moreover, every sentence is a sequence in \SQS terminology. We use \StanfordNLP\!\footnote{\url{http://stanfordnlp.github.io/CoreNLP/}} and \NLTK\!\footnote{\url{http://www.nltk.org/}} library to split files into sentences. We also remove all punctuation and treat all words as lower-case.

We can also go a little further in our preprocessing to minimise number of distinct patterns that are actually different variants of the same phrase. Let us mention \textit{"Bilbo Baggins"} and \textit{"Bilbo Baggins's"} as an example of such pattern. There are a couple of approaches to achieve it. We decided to try stemming and lemmatisation. 

Stemming is the~process of reducing inflected words to their word stem, in this thesis we achieve it by stripping word suffix. Lemmatisation aims to group together the different inflected forms of a word, for instance words \textit{was}, \textit{been} and \textit{are} all become \textit{be}. We use \NLTK to perform stemming and \StanfordNLP for lemmatising. 

One last thing that we considered was removing stop words. On one hand, this might improve the results by eliminating meaningless patterns, however a~stop word might be an important part of a~pattern and hence removing it might not be desired. 

We discuss all results achieved with different preprocessing techniques with more details in \Cref{sec:eval_preproc}. Let us now introduce and explain in more detail $\Spaghetti$, our algorithm.
\section{$\Spaghetti$ outline}
In order to find a~set of good storylines we proceed as in \Cref{alg:algo_outline}. First,  in lines \ref{alg:outline_preproc} and \ref{alg:outline_glbsl} we perform some initial processing as explained in the beginning of this chapter, and we find global storyline for the input data set (as explained in \Cref{sec:global_sl}).

Next, in lines \ref{alg:outline_start} and \ref{alg:outline_start2}, we create initial single-document storylines (as explained in \Cref{sec:notation}) as starting points of the algorithm, and then proceed to check possible candidates for new storylines (lines \ref{alg:outline_main_start} to \ref{alg:outline_main_end}).

\begin{algorithm}[H]
    \KwData{set of documents $D$}
    \KwResult{set of storylines $\SLSm$}
    $D= \textsc{Preprocess}(D)$\label{alg:outline_preproc}\\
    $global_{SL} = \textsc{MakeGlobalStoryline}(D)$\label{alg:outline_glbsl}\\
    $\SLSm = \emptyset$			\\
    \ForEach {$d \in D$} { \label{alg:outline_start}
        $\SLSm = \SLSm \cup \textsc{MakeStoryline}(d, global_{SL})$ \label{alg:outline_start2}
    }
    \ForEach {$storyline_1 \in \SLSm$} { \label{alg:outline_main_start}
        \ForEach {$d \in D$} {
            $candidate_{SL} = \textsc{Merge}(storyline_1, d)$	\\  \label{alg:outline_sl_merge}
            \If{$\textsc{IsGoodStoryline}(candidate_{SL})$} {  \label{alg:outline_good_sl}
                $\SLSm = \SLSm \cup candidate_{SL}$
            }
        }
    } \label{alg:outline_main_end}
    \caption{$\Spaghetti$}
    \label{alg:algo_outline}
\end{algorithm}

Having presented the high level algorithm, let us now explain each of its components. We start with finding global storylines.
\section{In search of a global storyline}
\label{sec:global_sl}

One of the things to note is that some of the patterns describing articles well, tend to repeat between storylines. In certain cases, especially when the~storyline data is short, this might lead to falsely merged storylines, even though they have nothing in common.

These patterns consist mostly of common phrases found in a language, such as \textit{"she~said"}, \textit{"in~the"}, etc. It might be tempting to simply remove stop words from the data to overcome this problem but this might be in some cases undesired as we can accidentally remove important patterns, most often because a~certain stop word is part of the phrase, eg. \textit{"The~Mountain"}. Thus we keep a separate storyline consisting of all these common patterns and accommodate for its presence when calculating the encoded length.

We present two approaches to creating such storylines:
\begin{enumerate}
\item  \textbf{Top-down}: In this approach we start from patterns for the whole dataset and update them with file specific patterns. \Cref{alg:top_down_gls} shows pseudocode for this procedure. 

First, we find global patterns for the whole dataset at once and then go over patterns found for individual files to remove patterns that are not generic but still made it to the global code table. We do it by computing frequency for each of the globally found patterns and leaving the most commonly occurring patterns from the input code table. 

This yields mostly satisfactory results except for when there is a pattern common for many documents in the corpus but quite specific in general. Some examples of such patterns include  \textit{"Moby Dick"} for  \textit{Moby Dick}, \textit{"Hillary Clinton"} for \NYT corpus or \textit{"Minas Tirith"} for \LOTR.

\begin{algorithm}[H]
 \KwData{Set of documents $D$}
 \KwResult{Global storyline of patterns common in the input set}
$D= \textsc{Preprocess}(D)$	\\
$globalPatterns= \textsc{SQS}(\bigcup\limits_{d \in D} d)$			\\
\ForEach {$d \in D$} {
	$documentPatterns = \textsc{SQS}(d)$	\\
	\ForEach {$p \in documentPatterns$} {
		$\textsc{UpdateFrequency}(globalPatterns, p)$
		}
	}
\Return{$\textsc{Filter}(globalPatterns)$}

\caption{Top-down approach to finding global storylines}
\label{alg:top_down_gls}
\end{algorithm}

\item \textbf{Bottom-up}: In this case we start with patterns for separate documents and iteratively build our way up to find the global patterns. Pseudocode is shown in \Cref{alg:bottom_up_gls}.

We go through code table for each of the documents and compute term frequency for each of the occurring patterns. Then we again take the most frequent ones. Unfortunately, this approach also tends to include the~aforementioned semi-general patterns.

\begin{algorithm}[H]
    \KwData{Set of documents $D$}
    \KwResult{Global storyline of patterns common in the input set}
    $D= \textsc{Preprocess}(D)$	\\
    $globalPatterns= \{ \}$			\\
    \ForEach {$d \in D$} {
        $documentPatterns = \textsc{SQS}(d)$	\\
        \ForEach {$p \in documentPatterns$} {
            $\textsc{UpdateFrequency}(globalPatterns, p)$
        }
    }
    \Return{$\textsc{Filter}(globalPatterns)$}
    
    \caption{Bottom-up approach to finding global storylines}
    \label{alg:bottom_up_gls}
\end{algorithm}

\end{enumerate}

In order to avoid including too specific patterns in the global storyline we perform bottom-up and top-down search on several corpora simultaneously. In this case we favour only the patterns that are common to all datasets and not any of them in particular. By combining multiple datasets we decrease relative frequency of data-dependent common patterns and increase frequency of globally common patterns. 

Let us now describe the most important part of \Spaghetti, that is merging the storylines.

%\section{Finding good patterns}
%To find set of good patterns describing a~document we use SQS.
\section{Merging storylines}
Extending storylines is the main challenge of the algorithm. The correct way of managing the set of patterns that are relevant for the storyline decides on the performance of the algorithm. In our algorithm we use three different methods of merging and compare their results (more about it in \Cref{sec:merge_types_comp}):
\begin{enumerate}[label={\bf \arabic*.}]
    \item \textbf{Incremental merge.} This is the simplest and most straightforward approach. Upon every merge we add all patterns from the tested document to the patternset of the storyline.
    
    This approach makes storylines more and more general as new documents are added, what might lead to eventually matching each new document. However, the mechanism of punishing pattern changes prevents to some extent this undesirable outcome. %We discuss it more in \Cref{sec:merge_types_comp}. %\Cref{sec:merge_types_comp_sum}.

    \item \textbf{Adaptive merge.} This is quite the opposite of the previous approach. In this case we add and remove patterns from the storyline upon every new joined document. We leave only patterns that occur in the newly merged document. 
    
    This could also lead to merging all new documents but punishing pattern adds and deletes restrains this process so that if the patterns in the document differ too much from the storyline's ones, it does not pay off to perform the merge.
    
    \item \textbf{Pattern mining based merge.} In this setting every time we try to extend the storyline we recalculate patterns optimal for the whole currently relevant set of documents at once. 
    
    This algorithm might either overfit, if the set of documents is very closely related or it might make storylines more generic over time and end up matching new documents basing only on non-informative patterns.
\end{enumerate}

To test if a~storyline is worth adding to the model, after all the initial processing and pattern update, we check if extending this storyline improves the score, ie. if the encoded length of documents decreases. If so, we add the storyline to our set $\SLSm$ and update patterns of our newly added document.

Having described all the parts of \Spaghetti let us now discuss shortly a~more practical aspect of the algorithm which is its runtime.
\section{Complexity}
The main bottle neck of our algorithm are the parts using \SQS because of its complexity equal to $O(|\Cands|^3 ||\DB||)$, where $\Cands$ is a~set of all possible candidate patterns. However, thanks to used heuristics and \MDL limiting number of patterns, in practice execution time of the algorithm is much faster than expected (it typically takes up to a~few minutes for a~single document, depending on its length). Nonetheless, finding the global storyline requires multiple runs of \SQS and that might take a~long time even despite the optimisations. However, these calculations have to be performed only once at the beginning, and can be cached what speeds up the algorithm significantly.

Merging storylines is linear in terms of $|\Patterns|$, where $|\Patterns|$ is a~set of all patterns related to the storyline. Although, if we use the \SQS variant, its complexity is the complexity of \SQS. Despite that not particularly good theoretical complexity, this step is usually very fast because, thanks to \MDL, there are not many patterns that need to be changed upon update, and the total number of patterns is also very small.

The last part of the algorithm to describe is the whole main loop. We start the algorithm with $n=|\DB|$ initial storylines. We try to extend each of them with every $\doc \in \DB$ what can generate another $n$ storylines. Hence, this step takes $O(n^2)$ iterations. Next, we try to extend the newly found storylines with every $\doc \in \DB$ what again takes $O(n^2)$ iterations. We have at most $n$ such steps what leads to complexity of $O(n^3)$. 

Therefore, the complete complexity of \Spaghetti (without global storylines, which can be found separately) is $O(n^3\; T)$, where $T=O(|\Patterns|)$ or $T=O(|\Cands|^3 ||\DB||)$ depending if the merge method used was incremental or adaptive, or \SQS respectively.

Let us now explain parameters of the algorithm.

\section{Parameters}
Although \Spaghetti is for the most part a~parameter free algorithm, it does however have a~few parameters. One of them is how many patterns should we include in the global storyline. 

The bigger the number, the bigger the chance of incidentally including patterns that would have been helpful in identifying related documents. On the oher hand, if number of patterns is too small, global storyline does not fulfill its purpose. We estimate that including about $20\%$ of the patterns might be a~reasonable choice.

Having discussed all aspects of our algorithm, let use now show the results we obtained for both synthetic and real data.

\iffalse
\section{Old}
There are two ways of solving the problem: top-down and bottom-up approach. In the bottom-up case we first find pattern set for each document in the corpora and then try to merge individual documents into larger sets forming a~storyline. An example outline of such algorithm is shown in \Cref{alg:algo_outline} (details further explained in \Cref{alg:preprocess}, \Cref{alg:make_storylines} and \Cref{alg:merge_storylines}).

In the top-down approach we first find patterns for the entire document set and then look for specific patterns occurring only within a~certain time period. We try to find the maximum stretch for such patterns which are later extracted from the general into a~separate, more specific storyline. The process might be repeated recursively (to be determined during the implementation).

The exact approach used in the algorithm is to be determined after implementation and comparison of the two.

\textsc{FindTimestamps}(D) divides timestamps of documents in set $D$ into several groups that will be considered separately.

Other helper methods:

\begin{algorithm}[H]
 \KwData{set of documents $D$}
 \KwResult{preprocessed set of documents $D'$ and probabilities for words in the corpus $Pr_D$}
 $D' = \emptyset$\\
 $Pr_D = \emptyset$ \\
 \ForEach{$d \in D$} {
 	$d' = \textsc{RemoveStopWordsAndPunctuation}(d)$\\
	$\textsc{RecogniseAndDisambiguateNamedEntities}(d')$\\
	$\textsc{UpdateWordsProbabilities}(Pr_D, d)$ \\
	$D' = D' \cup \{d'\}$
 }
 \Return $D'$, $Pr_D$
 \caption{\textsc{Preprocess}}
 \label{alg:preprocess}
\end{algorithm}

\begin{algorithm}[H]
 \KwData{set of documents $D$, timestamp $t$}
 \KwResult{set $\SLSm_t$ of documents from $D$ belonging to timestamp $t$}
 $\SLSm_t = \emptyset$\\

 \ForEach{$d \in D$} {
	\If{\textsc{HappenedWithin}(d, t)}{
   		SL - new storyline	\\
		SL.startTime = t	\\
		SL.finishTime = t	\\
		SL.patternSet = \textsc{Encoded}(d)	 // using some variant of \textsc{Sqs}\\
		$\SLSm_t = \SLSm_t \cup \{SL\}$ \\
	}
 }
 \Return $\SLSm_t$
 \caption{\textsc{MakeStorylines}}
 \label{alg:make_storylines}
\end{algorithm}

\begin{algorithm}[H]
 \KwData{storyline $SL_1$ and storyline $SL_{2}$ that is to be merged with $SL_1$}
 \KwResult{the resulting storyline $SL_{new}$}
$SL_{new}.startTime = \min(SL_1.startTime, SL_2.startTime)$	\\
$SL_{new}.finishTime = \max(SL_1.finishTime, SL_2.finishTime)$	\\
$SL_{new}.patternSet = \textsc{UpdatePatterns}(SL_1, SL_2)$	  // this is where the \textsc{magic} happens	\\

 \Return $SL_{new}$
 \caption{\textsc{MergeStorylines}}
 \label{alg:merge_storylines}
\end{algorithm}
\fi

