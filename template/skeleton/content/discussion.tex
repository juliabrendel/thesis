\Chapter{Discussion}{The Last Debate}
%\begin{itemize}
%    \item Draw conclusions on experiments
%    \item Future work, critical discussion
%\end{itemize}

In this section we present observations and conclusions that we came to while working on the problem, as well as identify possible ways to improve our algorithm.

\section{General observations}
First, one of the most distinctive characteristics of our algorithm is that it performs much better on datasets with longer documents. Number of documents does not matter that much but their content can greatly impact the results. 

The reason behind is that we are trying to find the most frequent patterns that summarise a~document well. However, journalists and writers are doing their best to avoid using repetitive phrases. As a~consequence, we might have some difficulties trying to find descriptive patterns in datasets with shorter documents, like for example in corpus of \NYT articles.

Small number of patterns found in single documents is also a~consequence of using \SQS as a~method for discovering patterns in documents. One way to overcome these shortcomings is to use for instance co-reference resolution to make patterns stand out more~--~we discuss it in more details in \Cref{sec:future_work}. Alternatively, we could also alter \SQS to be less strict about returned patterns.

Initially, we assumed that extending the set of patterns for a~document  with ones from a~global storyline (containing patterns commonly occurring in the corpus) will improve the results. As it turned out, it did in a~sense: using a~global storyline finds many more connections between documents. Unfortunately, these connections are not always meaningful, as it might happen that global storyline contained patterns relevant for only some of the documents and yet they are now present everywhere. Moreover, if global storyline is too small (but likely avoids including too specific patterns), its use does not help with finding related documents.

On the other hand, what we initially though was preventing us from finding interesting connections but turned out to be a~good idea, is removing stop words. Again, it does not help with shorter documents where we are depending on patterns like \textit{The Mountain} because we are unable to find them without the word \textit{the}. However, this approach works quite well on corpora with longer documents where we are able to find many helpful patterns, alternative to the ones inevitably lost by removing stop words. Nonetheless, it is important to note that both \Hobbit, \LOTR and \textit{Moby Dick} have a~structure dense with multi-word patterns (like \textit{Minas Tirith}, \textit{Fili and Kili} or \textit{Moby Dick} itself) what helps with identifying related documents.

\section{Future work}\label{sec:future_work}
\paragraph{Relation to topic models}
The problem that we are trying to solve can also be solved using topic models. They use a~different approach to finding commonly occurring patterns, i.e. everything is based on word frequency in the document. 

This allows for discovering different type of patterns, especially ones consisting of words rare in the corpus but commonly occurring in a~particular document. Topic models allow also for finding single-word descriptive patterns, while our algorithm is able to pick up only the ones of length two or more. Therefore, in topic models the notion of global storyline is irrelevant, also removing stop words should not cause a~decrease in performance.

One of the directions to explore in future work would be to apply concepts from topic models to our algorithm to be able to detect more document-specific patterns as well as the single-word ones.

\paragraph{Prequential codes}
We use fixed length prefix codes to calculate lengths of the patterns we work with. It has a~lot of advantages, it is easy to compute and efficient to work with just to name a~few. Alas, it also has a~couple of downsides, the biggest one being lack of ability to reflect changes in the data. Also, we have no other means to actually calculate the needed probabilities than simply computing them from the algorithm outcome.

Alternatively we could use prequential codes which we explain in detail in \Cref{sec:prequential_coding}. Using these codes would allow us to be more flexible with pattern lengths, as it does not require knowing words probabilities in advance~-~they are calculated on the fly, as pattern set is updated. Moreover, using prequential coding eliminates the need for encoding word frequencies in the model directly, what leads to making more fair comparisons between them.

Unfortunately, the big downside of prequential codes is increased implementation complexity. Our initial goal in this thesis was to use prequential codes throughout the algorithm but because of overhead associated with implementing them as well as time constraints, in the end we decided to stay with prefix codes.

\paragraph{Dynamic corpora}
Another interesting extension to our algorithm would be adding support for dynamically changing corpora, where new documents can be added as time passes by. Theoretically this is quite an easy modification but it is not currently possible, unless we are using local frequencies. However, in this setting, we might not be able to discover all the important connections between the data. Another alternative would be to use the aforementioned prequential codes that automatically and accurately reflect changes in structure of the data, and changing word distribution in the corpora does not disrupt its performance.

\paragraph{Co-reference resolution}
In our experiments we discovered that sometimes the algorithm picked up several patterns of very similar structure and meaning and yet they were treated separately. That happened most often with patterns related to people or places. Examples of such patterns include \textit{"Mrs Clinton"} and \textit{"Hillary Clinton"}, or \textit{"Big Apple"} and \textit{"New York"}. It is natural for real world text data to use different names for the same entity but it causes problems to our algorithm.

A~conceptually simple way to overcome it is to use co-reference resolution to derive a~canonical form for the entity and then use it throughout the corpus. However, in practice it is not a~trivial task and it is very difficult to perform effective substitutions. Also, we would not like to change all possible forms of how an~entity is called~---~some descriptive names (like \textit{Republican's candidate for president}) should be left in their original form.

\paragraph{Improving the runtime}
In most cases \Spaghetti works quite fast, given size of the data we are dealing with and complexity of the problem at hand. However, this is not true in the case of pattern mining merge where computations take days to complete because we need to redo mining over and over again. 

Yet, computing time could be greatly improved by implementing parallelisation in the algorithm. Checking candidates for storylines in disjoint subsets of the data is an independent procedure that could be easily done in separate threads, thus allowing us to check multiple candidates at the time. However, this thesis is more of a~proof of concept than an actual working solution and therefore we leave it for future work.
