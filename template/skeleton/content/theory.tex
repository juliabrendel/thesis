%\newcommand{\abcpatternshort}{\ensuremath{\mathit{abc}}}
\newcommand{\abcpatternshort}{\begin{tikzpicture}\node[draw=limpetshell, fill=limpetshell!60, pattern_node] (12) at (0,-0.5)  {abc};\end{tikzpicture}}
\newcommand{\cdepatternshort}{\begin{tikzpicture}\node[draw=peachecho, fill=peachecho!60, pattern_node] (12) at (0,-0.5)  {cde};\end{tikzpicture}}


%How are we computing the score?
%Complexity of the problem
\Chapter{Theory}{Out Of The Frying-Pan Into The Fire}
In this chapter we present the theory behind our solution. First we define length of the encoded model, and then follow to define encoded length of the data, to build a~complete system ready to use with MDL.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Encoded length of the model} % L(M)

Every set of storylines, given a timestamp represents a~set of patterns. Having the desired properties listed above, we can define encoded length in bits of the description of elements present in the model.

First, we need to introduce universal code for integers crucial for defining encodings of entities present in the algorithm:

\begin{definition}{\textsc{Universal code for integers}}
	
For $n \geq 1$ is defined as \citep{bib:rissanen}
\[
L_\mathbb{N}(z) = \log^*z  + \log c_0 \]with $\log^*(z) = \log z + \log \log z + \dots$ (summing only over the positive components).\\
To satisfy the Kraft equality we set $c_0 = 2.865064$.
\end{definition}

Let us now introduce concepts useful in defining the encodings:

\paragraph{Pattern alphabet} It is the size of the set of all words $W$, $||D|| = \sum_{d \in D} |d|$ which is a~total length of all documents in the document set and the number of all possible set of words made from words found in documents in $D$. Its length can be defined as 
\[L(\Omega) = L_\mathbb{N}(|W|) + L_\mathbb{N}(||D||) + \log {{||D||-1} \choose {|W| - 1}}\]

Having defined the above we can follow with defining length of the encoding of elements of the model:

\paragraph{Change between the points of the storyline}\label{def:delta}

Each $\delta$ consists of encoding of the time the change happened, length of the sets of patterns added to the storyline as well as length of each of these involved patterns. The last component is number of deleted patterns, and their encoding that can be specified as one of the subsets of $P^{t-1}$ (set of patterns from the previous timestamp). Thus the whole sum becomes equal to
\begin{equation}
	L(\delta)=L(t) + L_\mathbb{N}(|P^t_+| + 1) + \displaystyle \sum_{X \in P^t_+} L(X)+ L_\mathbb{N}(|P^t_-| + 1) + \log {{|P^{t-1}|} \choose {|P^t_-|}}
	\label{eqn:delta}
\end{equation}
where $L(X) = L_{\mathbb{N}}(|X|) + \sum_{w \in X} -\log Pr(w \mid \Omega)$ is a~description length of a~pattern expressed as sum of the number of words involved and optimal code for each of the words computed based on the background knowledge about the language.

Term $L(t)$ can be omitted if we choose to include $\delta$ in each storyline for each possible timestamp, thus having some of them empty. To avoid problems with encoding empty pattern sets we add one to the encoding of their size.

\paragraph{Storylines}

	Encoding of a~single storyline based on the properties from \Cref{sec:storyline_struct} consists of encoding of the timestamp at the beginning and end of the storyline as well as encoded number of all changes between points in the storyline followed by the length of each of the changes involved (\Cref{eqn:delta}), resulting in
	\[
	L(SL) = \log{{t_{end}}\choose{2}} + L_\mathbb{N}(|\Delta|) + \sum_{\delta \in \mathbf{\Delta}} L(\delta)
	\]
	where $t_{end}$ is the global maximal timestamp, and storyline's start and end times are chosen from the set of all possible pairs of timestamps. This way we avoid penalising stories based on their length.
	From the definition of $P^t$ follows that for non--empty storylines there exists at least one $\delta$~so $|\Delta| \geq 1$.

Lastly, description length of the storylines model in bits is simply a~sum of length of the alphabet and lengths of each of the storylines,
\begin{equation}
L(\SLSm) = L(\Omega) + \displaystyle \sum_{SL \in \; \SLSm} L(SL)
\label{eqn:model_desc}
\end{equation}
$L(\Omega)$ is a~constant term and thus can be omitted during optimisation.

Next, we describe how to encode data $\DB$ given a~model $\SLSm$, and in particular how to encode $L(\DB, \SLSm)$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Encoded length of the data} % L(D | M)
 \label{sec:len_of_the_data}

We encode the data using fixed length prefix codes, which means that probability for each pattern is constant throughout the algorithm and does not need to be recalculated. The length of the optimal prefix code is given by Shannon entropy \citep{bib:elements_of_information_theory}. Prefix codes have are chosen because they are easy to compute and analyse, just to name a~few. A~theoretical downside is that in order to use these codes, recipient has to know all probabilities in advance. This might require making arbitrary choices and lead to incorrect distribution.

One way to avoid these problems is to use prequential codes \Citep{bib:grunwald}. The main idea behind them is that the recipient does not know probabilities in advance and assumes that in the beginning every word is equally likely. Probabilities are updated on-the-fly, as new patterns arrive, what allows us to have a~valid pattern distribution at every stage. However, that makes them much more complex to implement. We discuss them in more details in \Cref{sec:prequential_coding}.

Here, we are using a~more simple approach to calculate probabilities. We assume them to be known, what is a~good approximation, especially for large data, where word distribution does not significantly change between files.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
\subsection{Cover by patterns with gaps}
In our setting we allow patterns to have gaps within them, similarly to \citep{bib:sqs}. To be able to detect if the next word in the encoding is the next word from the current pattern or a~singleton gap-pattern we use two pattern covers: one to keep track of the patterns, $C_p$, and a~separate one to keep track of the gaps, $C_g$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{Pattern stream}

We can determine length of the encoded pattern stream using the formula
\begin{eqnarray*}
L_f(C_p(d)) = \sum_{X \in P^d} usg_X L_f(code(X) | P^d) &=& - \sum_{X \in P^d} usg_X \log P(X | \Omega) \\&=& - \sum_{X \in P^d} usg_X \log \left({\frac{usg_{X}}{usg_{P^d}}}\right)\quad.
\end{eqnarray*}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{Gap stream}

In order to encode it we write $gaps(X)$ to refer to the number of gaps within the usages of the pattern $X$~and $fills(X)$ to indicate the number of non--gap codes in the usage of $X$,~that is $fills(X) = usg(X)(|X|-1)$. We need to encode presence or absence of the gap, for which we use $L(code_g(X \mid P^d))$ and $L(code_n(X \mid P^d))$ respectively, defined as
\[
L\left(code_n\left(X \mid P^d\right)\right)  = -\log \left( \frac{fills(X)}{gaps(X) + fills(X)} \right) 
\] and
\[
\;\;\;\;\;L\left(code_g\left(X \mid P^d\right)\right) = -\log \left( \frac{gaps(X)}{gaps(X) + fills(X)} \right)\quad.
\]
From the lengths of the individual codes we can calculate the length of the whole gap cover:
\[
L_f(C_g(d)) = \sum_{X \in P^d} \left( gaps(X)L(code_g(X \mid P^d)) + fills(X)L(code_n(X \mid P^d)) \right)
\]

\paragraph{Document length}

We define the encoded length of a~document $d \in D$ given patterns associated with $d$ as
\[
L(d \mid P^d) = L_\mathbb{N}(|d|) + L(C_p(d))+ L(C_g(d))\quad
\]
where we first encode length of the document and then each of the patterns in its pattern and gap cover respectively.

We use a~separate encoding for the document given a~model \SLS. It consists of encoded timestamp of the document (which is constant and can be omitted during optimisation), number of the storylines, index of the subset of storylines corresponding to $SL(d)$ (assuming a~canonical order on \SLS) and the encoded length of $d$ given its patterns,
\begin{equation}
L(d \mid \; \SLSm) = L(t) + \log |\SLSm| + \log {{|\SLSm|} \choose {|SL(d)|}} + L(d \mid P^d)
\label{eqn:doc_given_ls}
\end{equation}

Hence, for the encoded length of set of all documents, $D$, we get 
\begin{equation}
L(D \mid \; \SLSm) = L_\mathbb{N}(|D|) + \displaystyle \sum_{d \in D} L(d \mid \; \SLSm)
\label{eq:all_docs_given_ls}
\end{equation}
where $L(\SLSm)$ is defined as in \Cref{eqn:model_desc}.

%where 
%\[L(P_d) = L_{\mathbb{N}}(|P_d|) + \sum_{W \in P_d} L(W)\] and
%\[L(text_d) =  L_{\mathbb{N}}(|text_d|) + \sum_{w \in text_d} -\log Pr(w \mid D)\]
% (I'm actually not sure about this encoding because it seems to favor shorter documents).
%L_\mathbb{N}(|s(d)|) + \sum_{SL_d \in s(d)} L(SL)

%$\displaystyle \sum_{k=1} z^{-L_\mathbb{N}(k)} \leq 1$

Now that our theory is complete we can introduce the problem.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The problem}	

The total encoded length of set of documents $D$ and a~storyline model \SLS\xspace consists of encoded length of the number of documents in the dataset and encoded length of all the documents (\Cref{eq:all_docs_given_ls}) and is defined as
\[
L(D, \; \SLSm) = L(D \mid \; \SLSm) + L(\SLSm) \quad.
\]
By the MDL principle, we are looking for the~model that minimises the total encoded length.

\paragraph{Minimum set of storylines problem}
\emph{Let $D$ be a~set of documents over an~alphabet $\Omega$. Find the smallest set of storylines \SLS\xspace that best describes $D$, such that the sum of the number of bits to describe the storyline set $L(\SLSm)$, and the encoded size of the document set $L(D\mid\; \SLSm)$, is minimal.
%$L(d\mid P^d)$ should be minimal, which means finding the optimal alignment/encoding of $d$ given patterns $P^d$ and $\Omega$.
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

To better see the scale of the problem we now define complexity of the search space. Let us start with a~single document $d$. Let $l$ denote the length of $d$ in words. Number of all possible patterns that can appear in the cover of $d$ is equal to $O(2^l)$.

Let us now count the ways we can cover $d$ by $k$ non-overlapping patterns without gaps~---~this is the lower bound for the complexity as it is just a~special case of the pattern cover. This number is equal to ${l-1}\choose{k-1}$. Taking into consideration all possible $k$ we get $\sum_{k=1}^{l} {{l-1}\choose{k-1}} = \sum_{k=0}^{l-1} {{l-1}\choose{k}} = 2^{l-1} = O\left(2^l\right)$.

This number grows if we allow interleaving of the patterns. The resulting problem is equivalent to exact set cover, one of the $NP-$complete problems. Number of possibilities to cover a~document of $l$ words with $k$ patterns is thus equal to Stirling number of the second kind, defined as \[\stirling{l}{k} = \frac{1}{k!} \sum_{j=0}^{k} (-1)^{k-j} {{k}\choose{j}}j^l\] Counting the ways to cover the document with any feasible number of patterns (between $1$ and $l$) results in a~so-called Bell number, $B_l = \sum_{k=0}^{l}\stirling{l}{k}$ less one, since we cannot have a~zero-pattern cover. As shown in \citep{bell_estimate} \[B_l < \left( \frac{0.792l}{\ln(l+1)} \right)^l \text{ thus } B_l = O\left(\left(\frac{l}{ln(l)}\right)^l\right)\quad.\]

As we can see, the search space is exponential for a~single document already. To find the optimal cover for $n$ documents we have to search through all possible covers of each of the documents involved. If we denote the total length of all documents as $L_n = \sum_{i=1}^{n} l_i$, then the number of possibilities for patterns without gaps becomes $O\left( 2^{L_n} \right)$, and $O\left(\left(\frac{L_n}{ln(L_n)}\right)^{L_n}\right)$ for the interleaving patterns.

Adding a~specific subset $A$ of available patterns to the document cover set might increase the total encoded length. However, adding a~subset consisting of the same patterns as~$A$ except for one might significantly decrease the encoded length. Given that we cannot determine in advance which subsets would decrease the encoded length, the only solution is to check all possible subsets.

Besides, when we decide to extend the document set, encoding for the smaller set might not prove optimal for a~bigger one so in this case it is also necessary to check each possible subset.

%\input{./tikz_pics/counterexample}


To ease the calculations we would like to have some structure in the optimised problem. However, this is not the case what we prove by considering one of the possible cover functions, $f$, and a~simple example from \Cref {fig:toy}. If the chosen function does not have any desired properties, then we cannot assume that any function from the family of cover functions has them. Let $f(A) = |A| + cover_S(A)$ be the considered function being sum of the size of the pattern set $A$ and minimum number of patterns used in cover of the sequence $S$.

 First, let us remind that for the finite set $S$, function $f:2^S \rightarrow \mathbb{R}$ is supermodular if for any $A \subset B \subset C$ and $x \in S \setminus B$, $f(A \cup \{x\})  f(A) \leq f(B \cup \{x\}) - f(B)$. In our setting $f$ is size of the set of patterns and length of the encoded data.

\Cref{subfig:orig_seq} shows the sequence to be covered and \Cref{subfig:orig_AB} patterns in sets $A$ and $B$, as well as $S$ covered by them. As we can see, $A \subset B$, what stays true throughout the example. We also have $f(A) = 12$ and $f(B) = 11$. After adding a~pattern \abcpatternshort, as in \Cref{subfig:AB_with_one} we obtain $f(A \cup \{\abcpatternshort\}) = 11$ and $f(B \cup \{\abcpatternshort\}) = 11$, and $f(A \cup \{\abcpatternshort\}) - f(A) = -1$ and $f(B \cup \{\abcpatternshort\}) - f(B) = 0$ so the supermodularity condition holds.

However, when we set $A'=A \cup \{\abcpatternshort\}$ and $B'=B \cup \{\abcpatternshort\}$, and extend $A'$ and $B'$ with pattern \cdepatternshort\xspace to $A''=A' \cup \{\cdepatternshort\}$ and $B''=B' \cup \{\cdepatternshort\}$~---~the setting from \Cref{subfig:AB_with_two}~---~we obtain  $f(A'') = 12$ and $f(B'') = 11$, so $f(A'') - f(A') = 1$ while $f(B'') - f(B') = 0$ and the supermodularity condition does not hold anymore. Thus $f$ is neither sub- nor supermodular.
 
Also, as we can see from observing values of $f$ for $A$, $A'$ and $A''$, even though $A \subset A' \subset A''$, $f(A) > f(A')$ but $f(A') < f(A'')$ so $f$ is not in any way monotonous either.

Having estimated the possible number of subsets as exponential in terms of the total length of the dataset, and having shown that the problem has no properties that could be exploited to ease the computations, we conclude that the direct and exact approach is infeasible in practice. Hence we resort to heuristics and we give more details in the next chapter.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%