\Chapter{Preliminaries}{Riddles in the Dark}
The goal of this work is to detect storylines in news articles or books, such as \textit{Lord Of The Rings}. We also want to detect when multiple storylines relate to the same topic and merge them. In order to do that we first want to detect topic of each document in our dataset. We achieve it using the \MDL Principle explained in \Cref{sec:mdl_principle} and a~pattern mining method described in \Cref{sec:sqs_explained}. Having discovered the topics, we want to join them to create coherent storylines. 

In this section we introduce the concepts and notation that we are going to use throughout the thesis.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Notation}\label{sec:notation}
Our data is a~set of documents $\DB$ over an alphabet of words $\AB$. Each document $\doc \in \DB$ is a~sequence of $|\doc|$ events $\word \in \AB$ and has an~assigned timestamp $\timestamp$. We summarise the most important notation in \Cref{fig:notation}.

\begin{table}[H]
    \centering
    \begin{tabular}[H]{cl} \arrayrulecolor{serenity} \toprule
        Symbol & Description \\ \midrule
        $\DB$ 			&	set of all documents	\\
        $\doc$ 			& 	document, element of $D$  \\
        \SLS 			&	set of all storylines \\
        $\SL$ 			&	single storyline, $\SL \in \; \SLSm$ \\
        $\Delta_{\SL}$		&	storyline--specific global history (also $\Delta$ when clear) \\
        $\timestamp$ 				&	time index relative to the storyline \\
        $\delta^t_{\SL}$	&	single step in the history of $\SL$ at time $t$ (also simply $\delta$ when clear) \\
        $P_-^{\timestamp}$			& 	set of patterns deleted from the storyline at time $t$ \\
        $P_+^{\timestamp}$		& 	set of patterns added to the storyline at time $t$ \\
        $\SLpt$		&	set of patterns describing the storyline $\SL$ at time $t$ (also $P^t$ when clear) \\
        $P^d$			&	set of patterns from all storylines that $d$ is involved in \\
        $\SL(d)$			&	set of storylines associated with $d$ \\
        $\AB$ 			&	set of all words $w$ \\
        $\word$ 			&	word, element of $W$ \\
        $X$				&	pattern, sequence of words, $X \in W^{|X|}$ \\ \bottomrule
    \end{tabular}
    \caption{Summary of the commonly used symbols}
    \label{fig:notation}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{Document structure}
Each document $\doc \in \DB$ consists of several important features. Apart from the timestamp and sequence of words, it also has a~(non--empty) set of storylines associated with it, $\SL(d)$.

We denote by $P^d$ the~set of patterns from every storyline including $\doc$, \[P^d~=~\bigcup_{\SL \in \SL(\doc)} P_{\SL}^\timestamp\] where $t$~is the timestamp of~$\doc$.
%Furthermore, all the documents are encoded using the $\MDL$ principle, thus having a~document--specific set of patterns $P_d$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{Storyline structure}\label{sec:storyline_struct}
Every storyline has~a timespan $\left[t_s, t_e\right]$ where $t_s$ is a timestamp index of the first document belonging to the storyline and $t_e$ is timestamp index of the last of such documents.

Storyline characteristics are altered at every time step what is reflected in the set $\Delta_{\SL}$~---~a~global storyline history. Each $\delta_{\SL}^{t} \in \Delta_{\SL}$ is a tuple $(P_-^t, P_+^t)$, where $P_-^t$ is a~set of patterns deleted from the storyline in time $t$ and $P_+^t$ are the patterns added in this time step. %Whenever the storyline clear from context we write $P_-^t$ and $P_+^t$.

%Each storyline has an initial $\delta$ created from the documents added at time $t_s$~---~in this case $P_+$ is the set based on patterns from the documents added at $t_s$ and $P_-$ is an empty set.

At each timestamp $t$ there is also a~current set of patterns, $P^t$ defined as follows:
\[
P^0 = \emptyset
\]
\[
P^t = \left( P^{t-1} \cup P^t_+\right) \setminus P_-^t, \text{for } t > 0
\]
%\left( \bigcup_{\delta = (t', P_-, P_+) \in \Delta, t' \leq t}  P_+ \right) \setminus \left( \bigcup_{\delta = (t', P_-, P_+) \in \Delta, t' \leq t} P_- \right)
Thus each non-empty storyline has at least one $\delta_{\SL}$. %Thus $P^{t_s}$ is $P_+$ for the initial $\delta$.

We want to create a~set of storylines $\SLSm$ such that for every $\SL \in \; \SLSm$ exists at least one $\doc \in \DB$ such that $\SL \in \SL(d)$, where $\SL(d)$ is a~set of storylines associated with $d$. Moreover, for each $\doc \in \DB$ exists such storyline $\SL_\doc \in \; \SLSm$ that consists of only this document and $\SL_d \in s(\doc)$.

In the reminder of this section we describe the concepts we use to achieve our goal.
\section{The \MDL principle}\label{sec:mdl_principle}
The Minimum Description Length Principle (\MDL) was first introduced by J. Rissanen in \citep{bib:rissanen_78_mdl}. It is a~practical version of Kolmogorov complexity~\citep{bib:vitanyi_93_book} viewing learning as data compression. It follows from the observation that regularity may be identified with ability to compress \citep{bib:grunwald}.

Given a~set of models \Models, the best model $M \in \ModelsM$ is the one that minimises 
\[
L(M) + L(D \mid M)
\]
where $L(M)$ is length, in bits, of the description of model $M$ and $L(D \mid M)$ is length, in bits, of the description of the data encoded by $M$.

We use a~crude \MDL, which is a~variant where data and model are encoded separately. There also exists a~refined version, where both model and data are encoded together. It uses an ad-hoc encoding of the model what results in more fair comparison between models, as it is judging them only by the encoded length.

However, this type of \MDL is only available for simple model classes yet our is more complex. We are also interested in patterns that the model contains and therefore we use the two-part \MDL.

Let us now describe \SQS, the algorithm using \MDL that we base our work on.
%This type of \MDL is called crude or two--part \MDL since it encodes data and model separately. We use this model instead of the refined \MDL (where model and data are encoded together) because we are interested in finding the best model, that is the best set of patterns describing a~storyline.
\section{Finding good patterns}\label{sec:sqs_explained}
One part of our algorithm requires finding patterns that describe documents in the database well. To achieve it we are using \SQS \citep{bib:sqs}, that summarises sequential data with small but descriptive set of serial episodes. 

\SQS is a~complex algorithm with several different variants but we are using only a~small subset of its functionality. We give a~high-level outline of \SQS in \Cref{alg:sqs_outline}. In next paragraphs we are going to explain in more detail parts relevant for our algorithm.

\paragraph{Data representation}
Each serial episode consists of a~sequence of events (or in our case words) that are a~subsequence of our data. It also allows gaps occurring between events of the episode. Allowed gaps are singleton patterns.

\SQS uses a~code table to describe the data model. Each pattern has a~separate entry in the code table and it consists of several parts:
%\renewcommand\labelitemi{\tiny$\bullet$}
%\begin{itemize}[label=\raisebox{0.25ex}{\tiny$\bullet$}]
\begin{enumerate}[a.]
    \item Patterns, that is words that the entry consists of, for instance \textit{Bilbo Baggins}
    \item Codes that allow identification of the pattern.
    \item Gap stream to identify positions of gaps in code occurences.
    \item Pattern stream to identify positions of words from the pattern.
\end{enumerate}
%\end{itemize}
In the beginning the code table consists only of singletons (line~\ref{alg:sqs_get_sing}~of \Cref{alg:sqs_outline}) to ensure that the data can be encoded. As the algorithm progresses singletons start to be gradually replaced by more informative, data specific patterns that improve the \MDL score.

Also the encoding of a~document consists of two separate streams: a~pattern and a~gap one. Length of this encoding depends on usages of each code in the cover of the document.

\begin{algorithm}[H]
    \KwData{A document $\doc$}
    \KwResult{Patterns forming an~optimal encoding of the data, $\Patterns$}
    $\Patterns = GetSingletons(\doc)$ \\ \label{alg:sqs_get_sing}
    \Do {$\textsc{EncodedLengthChanges}(\doc, \Patterns)$} { \label{alg:sqs_main_start}
        $\textsc{Align}(\Patterns) $			\\ \label{alg:sqs_align}
        $candidatePatterns = \textsc{MinePatterns}(\doc, \Patterns)$	\\ \label{alg:sqs_mine_pats}
        $\Patterns = \textsc{PrunePatterns}(candidatePatterns)$	\\ \label{alg:sqs_prune_pats}
    }\label{alg:sqs_main_end}
    \Return{$\Patterns$}
    
    \caption{Outline of \SQS}
    \label{alg:sqs_outline}
\end{algorithm}

\paragraph{Finding the optimal cover}
Let us now describe how to find the optimal cover of the data with patterns. This is a~non-trivial task if there are patterns other than singletons because there might be many possible encodings of the same data. \SQS is therefore looking for the encoding that minimises the \MDL score.

Given the complex relation between the chosen alignment and respective code lengths, \SQS uses heuristics to find the optimal solution. First, it sets the code lengths and then tries to optimise the alignment (line~\ref{alg:sqs_align}~of \Cref{alg:sqs_outline}).

Based on the results, it updates the code lengths and proceeds with aligning patterns again. It continues this process until convergence is reached, ie. until the code lengths stop changing after the alignment.

\paragraph{Mining pattern candidates}
It is also important to know how to find the right patterns to perform the alignment on. \SQS provides two mechanisms to achieve it. Firstly, the patterns might be mined using a~dynamic algorithm, and secondly they can be provided as input of the algorithm. In our solution we focus on the former version, presented in line~\ref{alg:sqs_mine_pats}~of \Cref{alg:sqs_outline}.
%
%To find the best set of possible patterns, \SQS uses a~heuristic based of the idea that if we are given a~set of patterns $\Patterns$, new patterns can be in a~form $XY$, where $X,Y \in \Patterns$.

Given a~set of patterns $\Patterns$, \SQS looks for new patterns in a~form of~$XY$, where $X,Y \in \Patterns \cap \AB$. Next, pattern that produces the biggest gain in the encoded length is added to the current pattern set $\Patterns$. The procedure is repeated until there are no more patterns that could improve the score. 

Despite its simplicity, this approach is not feasible in practice because of its time complexity. Hence, \SQS uses a~linear time heuristic to find possible patterns.

\paragraph{Choosing the best patterns}
Given a~set of patterns that might belong to the optimal encoding, we have to decide which ones are actually worth keeping. This is done in line~\ref{alg:sqs_prune_pats}~of~\Cref{alg:sqs_outline}.

To do this, \SQS iterates over patterns adding them one by one in order of decreasing impact on the data length. Pattern is added to the codedable, if and only if including it in the cover improves the total encoded length.

It might happen that over time some of the previously added patterns are no longer useful and, in fact, worsen the encoding. Therefore, after adding a~new pattern, \SQS checks every pattern if cost of keeping it in the code table is higher than the gain in encoded length that this pattern induces. If that is the case, it does not pay off to keep it and the pattern is removed.

Even though \SQS was designed with another type of data in mind than the text data we are working with, it performs very well in finding relevant patterns for a~given data set. 

For instance, given abstracts of papers from the Journal of Machine Learning Research (\textit{JMLR}) website\!\footnote{\url{http://jmlr.csail.mit.edu/}} it found patterns like `\textit{non neg matrix factor}', `\textit{isotrop log concav distribut}', and `\textit{reproduc[ing] kernel Hilbert space}'.

Now that we described all the concepts crucial to understanding the rest of this work, we proceed with explaining the theory behind our algorithm.