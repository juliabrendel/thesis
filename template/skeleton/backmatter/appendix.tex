\chapter{Prequential coding}\label{sec:prequential_coding}
\subsection{Prequential codes}
To avoid difficulties mentioned in \Cref{sec:len_of_the_data}, we can use prequential coding scheme \citep{bib:rissanen, bib:grunwald}. This encoding, as opposed to fixed length optimal prefix codes, does not require knowing probabilities of every pattern in advance. Pattern usages are initialised to $\varepsilon$ and recalculated on-the-fly when new patterns arrive. This approach allows us to have a~valid pattern distribution at every stage and thus send optimal prefix codes.

To calculate the necessary encodings let us assume that $S$ is a~sequence of arriving codes over pattern set $P$. Also let $k=|S|$ be the length of the sequence and $l=|P|$ be the number of patterns in the set. We also introduce \Cref{lemma:total_usage}.

\begin{lemma}
	
	Let $U_j = \sum_{p_m \in P} usg_j(p_m)$ denote the total usage of patterns in $P$. If for all $p_i \in P$ $usg_0(p_i) = \varepsilon$ and $usg_j(p_i) = \varepsilon + |\{z \mid s_z = p_i, z \in [1, j]\}|$ then $U_j = \varepsilon l + j$.
	\begin{proof}
		\begin{eqnarray*}
			U_j = \sum_{p_m \in P} usg_{j}(p_m) & = & \sum_{p_m \in P} \varepsilon + \left|\{z \mid s_z = p_m, m \in [1, j]\}\right| 
			\\ & = & \varepsilon l + \sum_{p_m \in P} |\{z \mid s_z = p_m, m \in [1, j]\}| \\ & = & \varepsilon l + j
		\end{eqnarray*}
	\end{proof}
	\label{lemma:total_usage}
\end{lemma}
In the beginning usage of each $p_i \in P$ is initialized to a~small constant $\varepsilon$, i.e. $usg_0(p_i) = \varepsilon$ and transmitting any of $p_i$ takes $-\log \frac{usg_0(p_i)}{U_0} = -\log \frac{usg_0(p_i)}{\varepsilon l} = \log l$ bits. 

After sending $j$ messages from the sequence, usage of $p_i$ is equal to the initial usage and number of sent messages equal to $p_i$: $usg_j(p_i) = \varepsilon + |\{z \mid s_z = p_i, z \in [1, j]\}|$ and transmitting $p_i$ requires (using \Cref{lemma:total_usage}) $-\log \frac{usg_{j}(p_i)}{U_j}=-\log \frac{usg_{j}(p_i)}{ \varepsilon l + j}$.

Let us define $usg_{p_i} = usg_k(p_i)$ as the count of pattern $p_i$ after transmitting all codes from $S$. Using the dependencies from above we can encode the length of the whole sequence $L(S)$ as the sum of lengths of all codes transmitted at their respective times,
\begin{eqnarray*}
	L_p(S) = \sum_{i = 1}^{k} -\log \frac{usg_i(s_i)}{U_{i-1}} & = & - \log \frac{\prod\limits_{i=1}^{k} usg_i(s_i)}{\prod\limits_{j=0}^{k-1} U_j} 
	= -\log \frac{\prod\limits_{i=1}^{l} \prod\limits_{j=0}^{usg_{p_i} - 1} (\varepsilon + j)}{\prod\limits_{j=0}^{k-1} (\varepsilon l + j)}
	\\ & = & \log \frac{(\varepsilon l + k - 1)(\varepsilon l + k - 2) \dots (\varepsilon l + 1)(\varepsilon l)}{\prod\limits_{i=1}^{l}(\varepsilon + usg_{p_i} - 1)(\varepsilon + usg_{p_i} - 2)\dots(\varepsilon + 1)(\varepsilon)}
	\\ & = & \log \frac{\Gamma(\varepsilon l + k)/\Gamma(\varepsilon l)}{\prod\limits_{i=1}^{l} \Gamma(\varepsilon + usg_{p_i})/\Gamma(\varepsilon)}
	\\ & = & \log \Gamma(\varepsilon l + k) - \log \Gamma(\varepsilon l) + \sum _{i=1}^{l} \left[ \log \Gamma(\varepsilon + usg_{p_i}) - \log \Gamma(\varepsilon)\right]
\end{eqnarray*}
% \prod\limits_{i=1}^{l} \Gamma(\varepsilon + \usg_{p_i})\Gamma(\varepsilon)}
% = \log \frac{\prod\limits_{j=0}^{k-1} (\varepsilon l + j)}{\prod\limits_{i=1}^{l} \prod\limits_{j=0}^{usg_{p_i} - 1} (\varepsilon + j)}


A~natural and often used value for $\varepsilon$ is $0.5$, hence we get
\begin{eqnarray*}
	L_p(S) &=& \log \Gamma(0.5 l + k) - \log \Gamma(0.5 l) + \sum _{i=1}^{l} \left[ \log \Gamma(0.5 + usg_{p_i}) - \log \Gamma(0.5)\right] 
	\\ &=& \log \Gamma(0.5 l + k) - \log \Gamma(0.5 l) + \sum _{i=1}^{l} \left[\log{\frac{(2 usg_{p_i} - 1)!! \sqrt{\pi}}{2^{usg_{p_i}}}} \right]
	\\ &=& \log \Gamma(0.5 l + k) - \log \Gamma(0.5 l) \\&+& \sum _{i=1}^{l} \left[\log{\{(2 usg_{p_i} - 1)!!\}} + \log{\sqrt{\pi}} - \log(2^{usg_{p_i}}) - \log{\sqrt{\pi}} \right]
	\\ &=&  \log \Gamma(0.5 l + k) - \log \Gamma(0.5 l) + \sum _{i=1}^{l} \left[\log{\{(2 usg_{p_i} - 1)!!\}} - usg_{p_i} \right]
\end{eqnarray*}
where $n!!= \prod_{i=0}^{k}(n-2i)$ such that $k = \ceil{n/2} - 1$ is the double factorial of $n$ and $\Gamma$ is the Gamma function, an extension of the factorial function to the complex plane, that is $\Gamma(x + 1) = x\Gamma(x)$, with relevant base cases $\Gamma(1) =1$, $\Gamma(0.5) = \sqrt{\pi}$ and $\Gamma(0.5 + n) = \frac{(2n - 1)!! \sqrt{\pi}}{2^{n}}$. For numerical calculations, we can use Stirling's approximation for the logarithm of gamma and double factorial.

%\begin{definition}{\textsc{Patterns}}\label{def:patterns} are encoded using the optimal prefix code length as defined by Shannon entropy ($-\log Pr(X)$). To compute it we need to know the probability of a~pattern $X$ in the encoding $P^t$ of the storyline at time $t$. We find the~probability $Pr_t(X)$ of the pattern using the aforementioned~prequential coding scheme. 
%	Let $usage_{SL}(X, P^t) = |\{ d \in SL \mid X \in cover(d, P^t) \}|$. Then the relative occurrence of a~pattern in a~storyline encoding at time $t$ is
%	\[
%	Pr_t(X) = \frac{usage_{SL}(X, P^t)}{\sum_{R \in P^t} usage_{SL}(R, P^t)}
%	\]
%The encoded length of the pattern is
%\[
%L(code(X) \mid P^t) = -\log Pr_t(X)
%\]
%\end{definition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
\subsection{Cover by patterns with gaps using prequential coding}
In this section we extend concepts introduced in \Cref{sec:len_of_the_data} to operate on prequential codes. Let us remind that we allow our patterns to contain gaps, similarly to \citep{bib:sqs}. Hence, we need to pattern covers: pattern cover $C_p$, and gap cover $C_g$. 

We now derive formulas for the both covers using prequential codes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{Pattern stream}

We can determine length of the encoded pattern stream using the formula
\begin{eqnarray*}
	L_f(C_p(d)) = \sum_{X \in P^d} usg_X L_f(code(X) | P^d) &=& - \sum_{X \in P^d} usg_X \log P(X | \Omega) \\&=& - \sum_{X \in P^d} usg_X \log \left({\frac{usg_{X}}{usg_{P^d}}}\right)\quad.
\end{eqnarray*}
Using the prequential coding scheme we can aggregate lengths of all patterns and rewrite $L_f(C_p(d))$. Let $usg_{P^d} = \sum\limits_{X \in P^d} usg_X$. Hence, for the length of the encoded pattern stream we have
\begin{eqnarray*}
	L_p(C_p(d)) & = & L_\mathbb{N}(|d|) + \log \frac{\prod\limits_{j=0}^{usg_{P^d}-1} (\varepsilon |P^d| + j)}{\prod\limits_{X \in P^d} \prod\limits_{j=0}^{usg_{X} - 1} (\varepsilon + j)}
	\\ & = & \log \Gamma(\varepsilon |P^d| + usg_{P^d}) - \log \Gamma(\varepsilon l) + \sum _{X \in P^d} \left[ \log \Gamma(\varepsilon + usg_{X}) - \log \Gamma(\varepsilon)\right]
	\\ &=& \log \Gamma(0.5 |P^d| + usg_{P^d}) - \log \Gamma(0.5 l) + \sum _{X \in P^d} \left[\log{\{(2 usg_{X} - 1)!!\}} - usg_{X} \right] \quad.
\end{eqnarray*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\paragraph{Gap stream}
We write $gaps(X)$ to refer to the number of gaps within the usages of the pattern $X$~and $fills(X)$ to indicate the number of non--gap codes in the usage of $X$,~that is $fills(X) = usg(X)(|X|-1)$. We encode presence or absence of the gap, for which we use $L(code_g(X \mid P^d))$ and $L(code_n(X \mid P^d))$ respectively, defined as
\[
L\left(code_n\left(X \mid P^d\right)\right) = -\log \left( \frac{fills(X)}{gaps(X) + fills(X)} \right) 
\] and
\[
\;\;\;\;\;\;L\left(code_g\left(X \mid P^d\right)\right) = -\log \left( \frac{gaps(X)}{gaps(X) + fills(X)} \right)\quad.
\] % Lame hack but it works!
From the lengths of the individual codes we can calculate the length of the whole gap cover:
\[
L_f(C_g(d)) = \sum_{X \in P^d} \left( gaps(X)L(code_g(X \mid P^d)) + fills(X)L(code_n(X \mid P^d)) \right)
\]

To use the prequential coding we first initialise both gap count, $gaps_0(X)$ and fill count, $fills_0(X)$ to $\varepsilon$. Thus, at this stage transmitting either gap or no-gap code requires the same length, $- \log\left( \frac{\varepsilon}{2\varepsilon} \right)= 1$.

% j is global for the pattern in document!
After sending $j$ symbols within the context $ctx$ of $X$, $fills_j(X) = \varepsilon + |\{q \in C_g^j(X) \mid q = fill \}|$ and $gaps_j(x) = \varepsilon + |\{q \in C_g^j(X) \mid q = gap \}|$ where $C_g^j(X)=\{q \in C_g^j(d) \mid ctx(q) = X\}$ and $C_g^j(d) = \{q_i \in C_g(d) \mid i \leq j\}$, hence 

\[L_p\left(code_n\left(X \mid P^d\right)\right) = -\log \left( \frac{ \varepsilon +  |\{q \in C_g^j(X) \mid q = fill \}|}{2\varepsilon + j}\right)\] and 

\[\;\;\;\;\;L_p\left(code_g\left(X \mid P^d\right)\right) = -\log \left( \frac{ \varepsilon +  |\{q \in C_g^j(X) \mid q = gap \}|}{2\varepsilon + j}\right)\quad .\]

Let $fills_X$ denote the count of all fills in $X$ after transmitting the whole document and $gaps_X$ count of gaps in $X$ after transmitting the whole document. $|C_g^j(X)| - |C_g^{j-1}(X)|$ is a~frequency of the symbol transmitted at time $j$ within $X$ (a~gap or no-gap). We can hence encode the gap cover for the entire document as
\begin{eqnarray*}
	L_p(C_g(d)) &=& \sum_{X \in P^d} \sum_{j=1}^{fills_X + gaps_X} -\log \left(\frac{|C_g^j(X)| - |C_g^{j-1}(X)|}{2\varepsilon + j} \right)\\&=& \sum_{X \in P^d} - \log \left( \frac{\prod\limits_{i=0}^{fills_X}(\varepsilon + j) \prod\limits_{k=0}^{gaps_X}(\varepsilon + k) }{\prod\limits_{j=0}^{fills_X + gaps_X - 1} (2\varepsilon + j)} \right)
	\\ &=& \sum_{X \in P^d} - \log \left( \frac{(\Gamma(\varepsilon + fills_X)/\Gamma(\epsilon)) (\Gamma(\varepsilon + gaps_X)/\Gamma(\varepsilon)) }{\Gamma(2\varepsilon + fills_X + gaps_X)/\Gamma(2\epsilon)} \right)
\end{eqnarray*}

We can use $\varepsilon=0.5$ what simplifies the above formula to
\begin{eqnarray*}
	L_p(C_g(d)) &=& \sum_{X \in P^d} - \log \left( \frac{(\Gamma(0.5 + fills_X)/\Gamma(0.5)) (\Gamma(0.5 + gaps_X)/\Gamma(0.5)) }{\Gamma(1 + fills_X + gaps_X)/\Gamma(1)} \right)
	\\ & = & \sum_{X \in P^d} - \log \left( \frac{(\Gamma(0.5 + fills_X) \Gamma(0.5 + gaps_X) }{\Gamma(1 + fills_X + gaps_X)\pi} \right)
	\\ & = & \sum_{X \in P^d} - \log \frac{(2 fills_X - 1)!!(2 gaps_X -1)!! \sqrt{fills_X \cdot gaps_X}}{ \pi \;\Gamma(1 + fills_X + gaps_X) 2^{fills_X + gaps_X}}
	\\ & = & \sum_{X \in P^d} fills_X + gaps_X + \log{\pi} + \log{\Gamma(1 + fills_X + gaps_X)} \\ &-& \frac{1}{2} \log(fills_X \cdot gaps_X) - \log \left[(2 fills_X - 1)!!(2 gaps_X -1)!! \right] \quad .
\end{eqnarray*}
where again $n!!= \prod_{i=0}^{k}(n-2i)$ such that $k = \ceil{n/2} - 1$ is the double factorial of $n$ and $\Gamma$ is the Gamma function, an extension of the factorial function to the complex plane, that is $\Gamma(x + 1) = x\Gamma(x)$, with relevant base cases $\Gamma(1) =1$, $\Gamma(0.5) = \sqrt{\pi}$ and $\Gamma(0.5 + n) = \frac{(2n - 1)!! \sqrt{\pi}}{2^{n}}$. 