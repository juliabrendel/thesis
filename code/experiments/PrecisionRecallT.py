from __future__ import division
from scipy.special import binom
import numpy as np
from experiments.dependencyGraph import basic_functions as basic


def compare_connected_components(true_combination, estimated_combination):
    true_combination = sorted(true_combination.copy())
    estimated_combination = sorted(estimated_combination.copy())
    sum_scores = 0

    for c2_id in range(len(estimated_combination)):
        max_tp = -np.inf
        score = 0

        for c1_id in range(len(true_combination)):
            true_c = true_combination[c1_id]
            est_c = estimated_combination[c2_id]
            tp_ = len(np.intersect1d(true_c, est_c))
            if tp_ <= max_tp: continue
            if tp_ <= 1 and len(true_c) != 1: continue
            fp_ = len(np.setdiff1d(est_c, true_c))
            if fp_ != 0: continue
            score = tp_/len(true_c)
            max_tp = tp_
        sum_scores += score
    # accuracy = sum_scores/len(estimated_combination)
    accuracy = sum_scores / len(true_combination)
    # print(accuracy)
    return accuracy

"""
def compare_graphs(true_combination, resulting_combination, number_of_nodes):
    if isinstance(true_combination, dict):
        dependencies = break_down_into_pairwise(list(true_combination.keys()))
    else:
        dependencies = break_down_into_pairwise(true_combination)
    modified_combination = break_down_into_pairwise(resulting_combination)
    if len(set(dependencies)) >= len(set(modified_combination)):
        set1 = set(dependencies)
        set2 = set(modified_combination)
    else:
        set1 = set(modified_combination)
        set2 = set(dependencies)
    true_positives = set1.intersection(set2)
    false_positives = set(modified_combination).difference(set(dependencies))
    false_negatives = set(dependencies).difference(set(modified_combination))
    for i in modified_combination:
        for j in modified_combination:
            if i == j:
                continue
            if """


def pr_r_directed(candidates, resulting_combination, number_of_nodes):
    if isinstance(candidates, list): dependencies = basic.reformat_dependencies(basic.break_down(candidates), noisy_case=True)
    else: dependencies = basic.reformat_dependencies(list(candidates.keys()), noisy_case=True)
    outliers = [d for d in dependencies if len(d) == 1]
    true_pos = 0
    # false_pos = 0
    post_iteration = False
    repeat = True
    modified_combination = basic.reformat_dependencies(basic.break_down(resulting_combination), noisy_case=True)
    singletons = [s for s in list(range(number_of_nodes)) if (s,) not in outliers]
    discovered_deps = len(modified_combination)
    if len(dependencies) == 0 and len(modified_combination) == 0: return 1, 1
    implied_combinations = []
    while repeat:
        if post_iteration:
            discovered_deps += len(implied_combinations)
            combination = implied_combinations
            repeat = False
        else:
            combination = modified_combination
            post_iteration = True
        for c in combination:
            if len(c) == 2:
                # both are not outliers, remove from singletons tn + 2
                if c[0] in singletons: singletons.remove(c[0])
                if c[1] in singletons: singletons.remove(c[1])
            if c not in dependencies: continue
            elif c in dependencies:
                true_pos += 1
                dependencies.remove(c)
    false_neg = len(dependencies)
    false_pos = discovered_deps - true_pos
    T = true_pos + false_neg
    number_of_edges = (number_of_nodes * (number_of_nodes - 1)) / 2
    total_combs = number_of_edges + number_of_nodes
    false_singletons = number_of_nodes - len(outliers) - len(singletons)
    # print("false singl: ", false_singletons)
    # tn - all truly missing edges and all truly missing singletons!  ... - |false singletons|
    true_neg = total_combs - discovered_deps - false_neg - false_singletons
    if true_neg < 0:
        true_neg = total_combs - discovered_deps - false_neg
    """
    print("amount of discoverded edges: ", discovered_deps)
    print("true pos: ", true_pos)
    print("false pos: ", false_pos)
    print("false neg: ", false_neg, "\n")"""
    # print("all: ", total_combs, "\n")

    assert true_pos >= 0, str(true_pos)
    assert false_pos >= 0, str(false_pos)
    assert false_neg >= 0, str(false_neg)
    assert true_neg >= 0, str(true_neg)

    # print("tp: ", true_pos, ", fp: ", false_pos, ", tn: ", true_neg, ", fn: ", false_neg)

    accuracy = (true_pos + true_neg)/(true_pos + true_neg + false_pos + false_neg)
    assert (0 <= accuracy <= 1), str(accuracy)
    if true_pos + false_pos == 0:
        precision = 0
    else:
        precision = true_pos/(true_pos + false_pos)
    if true_pos + false_neg == 0:
        recall = 0
    else:
        recall = true_pos/(true_pos + false_neg)

    # p robability = sum_prob(T, total_combs - T, true_pos, false_pos, total_combs)
    probability1 = prob(true_pos, false_pos, T, total_combs - T)
    probability2 = sum_prob(true_pos, false_pos, T, total_combs - T)
    assert precision <= 1
    assert precision <= 1

    print("precision: ", round(precision, 3), " recall: ", round(recall, 3))
    return accuracy, probability1, probability2, precision, recall


def pr_r(candidates, resulting_combination, number_of_nodes):

    if isinstance(candidates, list):
        dependencies = basic.reformat_dependencies(basic.break_down(candidates), noisy_case=True)
    else:
        dependencies = basic.reformat_dependencies(list(candidates.keys()), noisy_case=True)

    outliers = [d for d in dependencies if len(d) == 1]
    true_pos = 0
    # false_pos = 0
    post_iteration = False
    repeat = True

    modified_combination = basic.reformat_dependencies(basic.break_down(resulting_combination), noisy_case=True)
    singletons = [s for s in list(range(number_of_nodes)) if (s,) not in outliers]
    discovered_deps = len(modified_combination)
    if len(dependencies) == 0 and len(modified_combination) == 0:
        return 1, 1

    # todo optimize
    implied_combinations = []
    while repeat:
        if post_iteration:
            discovered_deps += len(implied_combinations)
            combination = implied_combinations
            repeat = False
        else:
            combination = modified_combination
            post_iteration = True
        for c in combination:
            reverse = c[::-1]

            # if len(c) == 1:  # if an outlier
            # if c[0] in singletons: singletons.remove(c[0])  # remove also from singletons, tp + 1
            # else:  # if an edge
            if len(c) == 2:
                if c[0] in singletons:
                    singletons.remove(c[0])  # both are not outliers, remove from singletons tn + 2
                if c[1] in singletons:
                    singletons.remove(c[1])

            if c not in dependencies and reverse not in dependencies:
                # false_pos += 1
                # todo singletons
                continue
            if c in dependencies and reverse not in dependencies:
                true_pos += 1
                dependencies.remove(c)
            elif reverse in dependencies and c not in dependencies:
                true_pos += 1
                dependencies.remove(reverse)
            elif c in dependencies and reverse in dependencies:
                true_pos += 1
                dependencies.remove(c)
                if len(c) != 1:
                    dependencies.remove(reverse)
    false_neg = len(dependencies)
    false_pos = discovered_deps - true_pos
    T = true_pos + false_neg
    number_of_edges = (number_of_nodes * (number_of_nodes - 1)) / 2
    total_combs = number_of_edges + number_of_nodes
    # true_neg = total_combs - T - false_pos
    false_singletons = number_of_nodes - len(outliers) - len(singletons)
    # print("false singl: ", false_singletons)
    # tn - all truly missing edges and all truly missing singletons!  ... - |false singletons|
    true_neg = total_combs - discovered_deps - false_neg - false_singletons
    """
    print("amount of discoverded edges: ", discovered_deps)
    print("true pos: ", true_pos)
    print("false pos: ", false_pos)
    print("false neg: ", false_neg, "\n")"""
    # print("all: ", total_combs, "\n")

    assert true_pos >= 0, str(true_pos)
    assert false_pos >= 0, str(false_pos)
    assert false_neg >= 0, str(false_neg)
    assert true_neg >= 0, str(true_neg)

    # print("tp: ", true_pos, ", fp: ", false_pos, ", tn: ", true_neg, ", fn: ", false_neg)

    accuracy = (true_pos + true_neg)/(true_pos + true_neg + false_pos + false_neg)
    assert (0 <= accuracy <= 1), str(accuracy)
    if true_pos + false_pos == 0:
        precision = 0
    else:
        precision = true_pos/(true_pos + false_pos)
    if true_pos + false_neg == 0:
        recall = 0
    else:
        recall = true_pos/(true_pos + false_neg)

    # p robability = sum_prob(T, total_combs - T, true_pos, false_pos, total_combs)
    probability1 = prob(true_pos, false_pos, T, total_combs - T)
    probability2 = sum_prob(true_pos, false_pos, T, total_combs - T)
    assert precision <= 1
    assert precision <= 1

    print("precision: ", round(precision, 3), " recall: ", round(recall, 3))
    return accuracy, probability1, probability2, precision, recall


def prob(true_pos, false_pos, T, F):
    assert true_pos <= T and false_pos <= F
    p = (binom(T, true_pos) * binom(F, false_pos)) / binom(T + F, true_pos + false_pos)
    if not np.isnan(p):
        assert p <= 1, str(p)
    return p


def sum_prob(true_pos, false_pos, T, F):
    tp_range = np.arange(true_pos, T + 1)
    fp_range = np.arange(false_pos, -1, -1)
    tp_range_all = np.arange(0, T + 1)
    fp_range_all = np.arange(F, -1, -1)
    X, Y = np.meshgrid(tp_range, fp_range)
    X_all, Y_all = np.meshgrid(tp_range_all, fp_range_all)
    p_normalizer = sum([prob(tp, fp, T, F) for tp, fp in zip(np.ravel(X_all), np.ravel(Y_all))])
    p_total = sum([prob(tp, fp, T, F) for tp, fp in zip(np.ravel(X), np.ravel(Y))])
    p = p_total / p_normalizer
    if not np.isnan(p):
        assert p <= 1
    return p


if __name__ == "__main__":

    # a = [[1, 2, 3], [4, 5], [6, 7, 8], [9], [10], [11], [12]]
    a = [(0, (1, 2)), (3, 4), (5, (6, 7)), (8,), (9,), (10,), (11,)]

    # b = [(0, (3, 4, 5, 6, 7, 8, 9, 10, 11, 12)), (1, (2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)),
    #     (2, (1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)), (3, (0, 1, 2, 5, 6, 7, 8, 9, 10, 11, 12)),
    #     (4, (0, 1, 2, 5, 6, 7, 8, 9, 10, 11, 12)), (5, (0, 1, 2, 3, 4, 8, 9, 10, 11, 12)),
    #     (6, (0, 1, 2, 3, 4, 7, 8, 9, 10, 11, 12)), (7, (0, 1, 2, 3, 4, 6, 8, 9, 10, 11, 12)),
    #     (8, (0, 1, 2, 3, 4, 5, 6, 7, 9, 10, 11, 12)), (9, (0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12)),
    #     (10, (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12)), (11, (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12)),
    #     (12, (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11))]
    b = [(0,), (1,), (2,), (3,), (4,), (5,), (6,), (7,), (8,), (9, 10), (10, 11), (11, 9)]
    # b = [[1], [4], [6]]
    pr_r(a, b, 12)

    """
        if transitivity:
            for i in modified_combination:
                inter = set(c).intersection(set(i))
                if i != c and len(inter) != 0:
                    diff = tuple(set(c).symmetric_difference(set(i)))
                    reverse_diff = diff[::-1]
                    if diff in implied_combinations or reverse_diff in implied_combinations:
                        continue
                    if diff not in dependencies and reverse_diff not in dependencies:
                        continue
                    elif diff in dependencies and reverse_diff not in dependencies:
                        if diff not in combination:
                            implied_combinations.append(diff)
                    elif reverse_diff in dependencies and diff not in dependencies:
                        if reverse_diff not in combination:
                            implied_combinations.append(reverse_diff)
                    elif diff in dependencies and reverse_diff in dependencies:
                        if diff not in combination and reverse_diff not in combination:
                            implied_combinations.append(diff) """
