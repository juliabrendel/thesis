from os import path

import pandas as pd
import time

from experiments.discretizationEval import DiscrEvaluation as evaluation
from main import MceEstimation, MceWithoutJD, Uds, preprocessing, read_data


class EntropyEstimation:
    def __init__(self, number_of_bins, data_file, results_file):
        self.data = read_data.read(data_file)
        print("main file, data size: ", self.data.shape)
        self.results_file = results_file
        self.number_of_bins = number_of_bins
        self.cumulative_entropies = preprocessing.ce_per_dimension(self.data)
        self.ordered_dimensions = preprocessing.rank_dimensions(self.cumulative_entropies)
        self.initial_I = [pd.DataFrame(self.data).index]

        # import time
        # start_time1 = time.time()
        self.candidate_set_values, self.candidate_set_disrs, self.conditional_ces, self.amount_of_combinations = \
            preprocessing.generate_candidates(self.data, self.number_of_bins, self.initial_I, early_stopping=True)

        """
        print("early stopping")
        print("--- %s seconds ---" % (time.time() - start_time1))
        print("\n")

        start_time2 = time.time()

        self.candidate_set_values_full, self.candidate_set_disrs_full, self.conditional_ces_full, \
        self.amount_of_combinations_full = preprocessing.generate_candidates(self.data, self.number_of_bins,
                                                                             self.initial_I, early_stopping=False)
        
        print("full algo")
        print("--- %s seconds ---" % (time.time() - start_time2))
        print("\n")
        #print("amount of candidates, full = ", str(self.amount_of_combinations_full))
        #print("amount of candidates, early stopping = ", str(self.amount_of_combinations), "\n")
        """

        self.precisions = []
        self.ce_values = []
        self.total_score_values = []

        self.multiv_results = []
        self.mce_scores = []

    def mce(self):
        mce = MceEstimation.MCE(self, do_exp_check=False)
        mce.mce_estimation(do_jd=True)
        print("score: ", mce.mce_uds_score)
        print("entropy: ", mce.joint_ce)
        return mce.mce_uds_score, mce.joint_ce
        # file.write("dependency list: " + str(mce.decomposition) + "\n")

        # mce_pure = MceWithoutJD.MceWithoutJd(self)
        # mce_pure.mce_pure_estimation()
        # eval = evaluation.JdEvaluation(mce.multiv_results, mce_pure.multiv_results, "J.D.", "No J.D.",
        #                               self.results_file)
        # eval.DimsVsBinsPlot()
        # eval.QuantitativeAnalysis()
        # JdEval.JdEvaluation(mce.multiv_results, mce_pure.multiv_results, "J.D.", "No J.D.", self.results_file)
        # .DimsVsBinsPlot()
        # JdEval.JdEvaluation(mce.multiv_results, mce_pure.multiv_results, "J.D.", "No J.D.").QuantitativeAnalysis()

    def uds(self):
        # uds1 = Uds.UdsVariations(self)
        # uds1.uds_pure()

        # uds2 = Uds.UdsVariations(self)
        # uds2.uds_jd()

        uds3 = Uds.UdsVariations(self)
        uds3.uds_extended_jd()
        # print("score: ", uds3.uds_score)
        # print("entropy: ", uds3.mce_score)
        # print("decomposition: ", uds3.resulting_combination)
        return uds3.uds_score, uds3.mce_score

    def compare(self):
        # estimation = EntropyEstimation(20, dataset, result_file)
        # print("dependency scores: \n")
        self.mce()
        self.uds()

if __name__ == "__main__":
    # file = open(path.abspath("../results/real_data/real_data_mce.txt"), 'w')
    file = open(path.abspath("../results/real_data/real_data_uds++1.txt"), 'w')
    # file_uds = open(path.abspath("../results/real_data/ real_data_uds.txt"), 'w')
    # file_uds1 = open(path.abspath("../results/real_data/ real_data_uds+.txt"), 'w')
    # file_uds2 = open(path.abspath("../results/real_data/ real_data_uds++.txt"), 'w')

    dataset_glass = path.abspath("../datasets/real_data/glass1.csv")
    dataset_diabetes = path.abspath("../datasets/real_data/diabetes1.csv")
    dataset_pendigits = path.abspath("../datasets/real_data/pendigits1.csv")
    dataset_diagnosis = path.abspath("../datasets/real_data/diagnosis1.csv")
    dataset_letter = path.abspath("../datasets/real_data/letter1.csv")
    dataset_wavenoise = path.abspath("../datasets/real_data/wavenoise1.csv")
    dataset_musk = path.abspath("../datasets/real_data/musk1.csv")

    def_nr_bins = 20
    # print("mce\n")
    print("glass")
    file.write("glass" + "\n")
    # file_uds2.write("glass" + "\n")
    # file_uds1.write("glass" + "\n")
    # file_uds2.write("glass" + "\n")
    start_time_glass = time.time()
    estimation = EntropyEstimation(def_nr_bins, dataset_glass, file)
    # score, entropy = estimation.mce()
    score, entropy = estimation.uds()
    time_glass = time.time() - start_time_glass
    # print("score: ", score)
    # print("entropy: ", entropy)
    # print("--- %s seconds ---" % time_glass, "\n")
    file.write("time: " + str(time_glass) + "\n")

    print("\ndiabetes")
    file.write("diabetes" + "\n")
    # file_uds2.write("diabetes" + "\n")
    start_time_diabites = time.time()
    estimation = EntropyEstimation(def_nr_bins, dataset_diabetes, file)
    # score, entropy = estimation.mce()
    score, entropy = estimation.uds()
    time_diabetes = time.time() - start_time_diabites
    # print("score: ", score)
    # print("entropy: ", entropy)
    # print("--- %s seconds ---" % time_diabetes, "\n")
    file.write("time: " + str(time_diabetes) + "\n")

    """
    print("pendigits")
    file.write("pendigits" + "\n")
    start_time_pendigits = time.time()
    estimation = EntropyEstimation(def_nr_bins, dataset_pendigits, file)
    # score, entropy = estimation.mce()
    score, entropy = estimation.uds()
    time_pendigits = time.time() - start_time_pendigits
    # print("score: ", score)
    print("entropy: ", entropy)
    print("--- %s seconds ---" % time_pendigits, "\n")
    file.write("time: " + str(time_pendigits) + "\n")"""

    print("\ndiagnosis")
    file.write("diagnosis" + "\n")
    start_time_diagnosis = time.time()
    estimation = EntropyEstimation(def_nr_bins, dataset_diagnosis, file)
    # score, entropy = estimation.mce()
    score, entropy = estimation.uds()
    time_diagnosis = time.time() - start_time_diagnosis
    # print("score: ", score)
    # print("entropy: ", entropy)
    # print("--- %s seconds ---" % time_diagnosis, "\n")
    file.write("time: " + str(time_diagnosis) + "\n")

    print("\nletter")
    file.write("letter" + "\n")
    start_time_letter = time.time()
    estimation = EntropyEstimation(def_nr_bins, dataset_letter, file)
    # score, entropy = estimation.mce()
    score, entropy = estimation.uds()
    time_letter = time.time() - start_time_letter
    # print("score: ", score)
    # print("entropy: ", entropy)
    # print("--- %s seconds ---" % time_letter, "\n")
    file.write("time: " + str(time_letter) + "\n")

    print("\nwavenoise")
    file.write("wavenoise" + "\n")
    start_time_wavenoise = time.time()
    estimation = EntropyEstimation(def_nr_bins, dataset_wavenoise, file)
    # score, entropy = estimation.mce()
    score, entropy = estimation.uds()
    time_wavenoise = time.time() - start_time_wavenoise
    # print("score: ", score)
    # print("entropy: ", entropy)
    # print("--- %s seconds ---" % time_wavenoise, "\n")
    file.write("time: " + str(time_wavenoise) + "\n")

    print("\nmusk")
    file.write("musk" + "\n")
    start_time_musk = time.time()
    estimation = EntropyEstimation(def_nr_bins, dataset_musk, file)
    # score, entropy = estimation.mce()
    score, entropy = estimation.uds()
    time_musk = time.time() - start_time_musk
    # print("score: ", score)
    # print("entropy: ", entropy)
    # print("--- %s seconds ---" % time_musk, "\n")
    file.write("time: " + str(time_musk) + "\n")
