import pandas as pd

data = pd.read_csv("/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/datasets/real_data/kidney_disease"
                   "/chronic_kidney_disease1.csv")

# print(data.iloc[:, 5])

j = 0
for i in data.iloc[:, 5]:
    if i == "normal":
        data.set_value(j, 'rbc', 1)
    else:
        data.set_value(j, 'rbc', 0)
    j += 1

j1 = 0
for i in data.iloc[:, 6]:
    if i == "normal":
        data.set_value(j1, 'pc', 1)
    else:
        data.set_value(j1, 'pc', 0)
    j1 += 1

j2 = 0
for i in data.iloc[:, 7]:
    if i == "present":
        data.set_value(j2, 'pcc', 1)
    else:
        data.set_value(j2, 'pcc', 0)
    j2 += 1

j3 = 0
for i in data.iloc[:, 8]:
    if i == "present":
        data.set_value(j3, 'ba', 1)
    else:
        data.set_value(j3, 'ba', 0)
    j3 += 1

j4 = 0
for i in data.iloc[:, 18]:
    if i == "yes":
        data.set_value(j4, 'htn', 1)
    else:
        data.set_value(j4, 'htn', 0)
    j4 += 1

j5 = 0
for i in data.iloc[:, 19]:
    if i == "yes":
        data.set_value(j5, 'dm', 1)
    else:
        data.set_value(j5, 'dm', 0)
    j5 += 1

j6 = 0
for i in data.iloc[:, 20]:
    if i == "yes":
        data.set_value(j6, 'cad', 1)
    else:
        data.set_value(j6, 'cad', 0)
    j6 += 1

j7 = 0
for i in data.iloc[:, 21]:
    if i == "good":
        data.set_value(j7, 'appet', 1)
    else:
        data.set_value(j7, 'appet', 0)
    j7 += 1

j8 = 0
for i in data.iloc[:, 22]:
    if i == "yes":
        data.set_value(j8, 'pe', 1)
    else:
        data.set_value(j8, 'pe', 0)
    j8 += 1

j9 = 0
for i in data.iloc[:, 23]:
    if i == "yes":
        data.set_value(j9, 'ane', 1)
    else:
        data.set_value(j9, 'ane', 0)
    j9 += 1

j10 = 0
for i in data.iloc[:, 24]:
    if i == "ckd":
        data.set_value(j10, 'class', 1)
    else:
        data.set_value(j10, 'class', 0)
    j10 += 1

# print(data)
data = data.reset_index(drop=True)
data.to_csv("/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/datasets/real_data/kidney_disease"
                   "/chronic_kidney_disease_num.csv")

