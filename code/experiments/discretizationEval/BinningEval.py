import numpy as np
import pandas as pd
from os import path
from DataGeneration import generator
from experiments import Estimation
from main import MceEstimation, Uds


def create_eval_table(rows, columns):

    """
    noise_level = 0
    print("\nnoise level: ", noise_level)
    network = generator.Dataset()
    network.generate_dataset(rows, columns, noise_level)
    print("dependencies: ", network.dependencies, "\n")
    dataset = path.abspath("dependency_network_noise.csv")"""
    dataset = path.abspath("../../DataGeneration/uds_dataset type1.csv")
    results_file = open(path.abspath("../../results/test.txt"), 'w')

    """
    print("true bins")
    for d in network.discretizations:
        print(d, ": ", len(network.discretizations[d])) """

    estimation = Estimation.EntropyEstimation(20, dataset, results_file)

    mce = MceEstimation.MCE(estimation, do_exp_check=False)
    mce.mce_estimation()

    print("\nmce")
    print(mce.decomposition, "\n")
    """
    for d in mce.final_discretizations:
        print(d, ": ", len(mce.final_discretizations[d]))"""

    """
    print("\nuds\n")
    uds1 = Uds.UdsVariations(estimation)
    uds1.uds_pure()

    for d in uds1.amount_of_bins_per_term:
        print(d, ":, ", uds1.amount_of_bins_per_term[d]) """
    """
    uds2 = Uds.UdsVariations(estimation)
    uds2.uds_extended()
    uds3 = Uds.UdsVariations(estimation)
    uds3.uds_jd()"""



if __name__ == "__main__":
    #print("conditioned sim: sum; new version + jd check")
    #print("redundancy check, cycles check, no add univariate")
    create_eval_table(100, 10)
    """
    print("\n100x6")
    for i in range(1):
        create_eval_table(100, 8)"""

    """
    print("\n1000x6")
    for i in range(1):
        create_eval_table(1000, 8)

    print("\n10 000x6")
    for i in range(1):
        create_eval_table(10000, 8)

    print("\n100 000x6")
    for i in range(1):
        create_eval_table(100000, 8)"""

    """
    print("\n1 000 000x6")
    for i in range(1):
        create_eval_table(1000000, 8)"""

    """

    print("\n100x10")
    for i in range(1):
        create_eval_table(100, 10)

    print("\n1000x10")
    for i in range(1):
        create_eval_table(1000, 10)

    print("\n10 000x10")
    for i in range(1):
        create_eval_table(10000, 10)

    print("\n100 000x10")
    for i in range(1):
        create_eval_table(100000, 10)"""
    """
    print("\n1 000 000x10")
    for i in range(1):
        create_eval_table(1000000, 10)"""
    """
    print("\n1000x15")
    for i in range(1):
        create_eval_table(1000, 15)

    print("\n10 000x15")
    for i in range(1):
        create_eval_table(10000, 15)

    print("\n100 000x15")
    for i in range(1):
        create_eval_table(100000, 15)"""
    """
    print("\n1 000 000x15")
    for i in range(1):
        create_eval_table(1000000, 15)"""
    """
    print("\n1000x20")
    for i in range(1):
        create_eval_table(1000, 20)

    print("\n10 000x20")
    for i in range(1):
        create_eval_table(10000, 20)

    print("\n100 000x20")
    for i in range(1):
        create_eval_table(100000, 20)"""
    """
    print("\n1 000 000x20")
    for i in range(1):
        create_eval_table(1000000, 20)"""

