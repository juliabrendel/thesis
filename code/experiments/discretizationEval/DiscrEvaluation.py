import operator

import collections
import matplotlib.pyplot as plt


class JdEvaluation:

    def __init__(self, multiv_results_method1, multiv_results_method2, method_name1, method_name2, results_file):
        self.multiv_results_method1 = multiv_results_method1
        self.multiv_results_method2 = multiv_results_method2
        self.results_file = results_file
        self.method1 = method_name1
        self.method2 = method_name2
        self.dims1 = {}
        self.bins1 = {}
        self.scores1 = {}
        self.dims2 = {}
        self.bins2 = {}
        self.scores2 = {}
        self.amount_of_combinations = min(len(multiv_results_method1), len(multiv_results_method2))

    def GetValues(self):
        #i = 0

        #TODO: add keys

        for key in self.multiv_results_method1:
            value = self.multiv_results_method1[key]
            given_dims_ids = value[0]
            score = value[1]
            number_of_bins = value[2]
            if isinstance(given_dims_ids, tuple):
                self.dims1[key] = len(given_dims_ids)
            else:
                self.dims1[key] = 1
            self.bins1[key] = number_of_bins
            self.scores1[key] = score

        """
        for value in self.multiv_results_method1.values():
            given_dims_ids = value[0]
            score = value[1]
            number_of_bins = value[2]
            if isinstance(given_dims_ids, tuple):
                self.dims1[i] = len(given_dims_ids)
            else:
                self.dims1[i] = 1
            self.bins1[i] = number_of_bins
            self.scores1[i] = score
            i += 1
        """
        #j = 0

        for key in self.multiv_results_method2:
            value = self.multiv_results_method2[key]
            given_dims_ids = value[0]
            score = value[1]
            number_of_bins = value[2]
            if isinstance(given_dims_ids, tuple):
                self.dims2[key] = len(given_dims_ids)
            else:
                self.dims2[key] = 1
            self.bins2[key] = number_of_bins
            self.scores2[key] = score

        """
        for value in self.multiv_results_method2.values():
            given_dims_ids = value[0]
            score = value[1]
            number_of_bins = value[2]
            if isinstance(given_dims_ids, tuple):
                self.dims2[j] = len(given_dims_ids)
            else:
                self.dims1[j] = 1
            self.bins2[j] = number_of_bins
            self.scores2[j] = score
            j += 1
        """

    def DimsVsBinsPlot(self):

        self.GetValues()

        sorted_dims_method1 = [t[1] for t in sorted(self.dims1.items(), key=operator.itemgetter(1))]
        sorted_bins_method1 = [self.bins1[t[0]] for t in sorted(self.dims1.items(), key=operator.itemgetter(1))]

        sorted_dims_method2 = [t[1] for t in sorted(self.dims2.items(), key=operator.itemgetter(1))]
        sorted_bins_method2 = [self.bins2[t[0]] for t in sorted(self.dims2.items(), key=operator.itemgetter(1))]

        plt.plot(sorted_dims_method2, sorted_bins_method2, 'b-', label=self.method2)
        plt.plot(sorted_dims_method1, sorted_bins_method1, 'r-', label=self.method1)
        plt.legend(bbox_to_anchor=(1.01, 1), loc=2, borderaxespad=0.)
        plt.xlabel('Number of given dimensions')
        plt.ylabel('Number of bins')
        plt.savefig("dimsVsBins.png")
        plt.close()

    def QuantitativeAnalysis(self):

        self.GetValues()

        average_diff_dims = 0
        average_diff_bins = 0
        average_diff_scores = 0

        self.results_file.write("id:         diff in dims         diff in bins         diff in score")

        for key1 in self.multiv_results_method1:
            for key2 in self.multiv_results_method2:
                if key1 == key2:
                    diff_dims = (1 - (self.dims1[key1]/self.dims2[key1]))*100
                    diff_bins = (1 - (self.bins1[key1] / self.bins2[key1])) * 100
                    diff_score = (1 - (self.scores1[key1] / self.scores2[key1])) * 100

                    average_diff_dims += (-1) * diff_dims
                    average_diff_bins += (-1) * diff_bins
                    average_diff_scores += (-1) * diff_score

                    if diff_dims > 0:
                        diff_dims = str("%.2f" % ((-1) * diff_dims)) + "%"
                    elif diff_dims < 0:
                        diff_dims = "+" + str("%.2f" % ((-1) * diff_dims)) + "%"
                    else:
                        diff_dims = str("%.2f" % diff_dims) + "%"

                    if diff_bins > 0:
                        diff_bins = str("%.2f" % ((-1) * diff_bins)) + "%"
                    elif diff_bins < 0:
                        diff_bins = "+" + str("%.2f" % ((-1) * diff_bins)) + "%"
                    else:
                        diff_bins = str("%.2f" % diff_bins) + "%"

                    if diff_score > 0:
                        diff_score = str("%.2f" % ((-1) * diff_score)) + "%"
                    elif diff_score < 0:
                        diff_score = "+" + str("%.2f" % ((-1) * diff_score)) + "%"
                    else:
                        diff_score = str("%.2f" % diff_score) + "%"

                    self.results_file.write("\n" + str(key1) + "      " + str(diff_dims) + "      " +
                                            str(diff_bins) + "      " + str(diff_score))
                    break

        """
        for i in range(0, self.amount_of_combinations):

            diff_dims = (1 - (self.dims1[i]/self.dims2[i]))*100
            diff_bins = (1 - (self.bins1[i]/self.bins2[i]))*100
            diff_score = (1 - (self.scores1[i]/self.scores2[i]))*100

            average_diff_dims += (-1) * diff_dims
            average_diff_bins += (-1) * diff_bins
            average_diff_scores += (-1) * diff_score

            if diff_dims > 0: diff_dims = str("%.2f" % ((-1) * diff_dims)) + "%"
            elif diff_dims < 0: diff_dims = "+" + str("%.2f" % ((-1)*diff_dims)) + "%"
            else: diff_dims = str("%.2f" % diff_dims) + "%"

            if diff_bins > 0: diff_bins = str("%.2f" % ((-1)*diff_bins)) + "%"
            elif diff_bins < 0: diff_bins = "+" + str("%.2f" % ((-1)*diff_bins)) + "%"
            else: diff_bins = str("%.2f" % diff_bins) + "%"

            if diff_score > 0: diff_score = str("%.2f" % ((-1)*diff_score)) + "%"
            elif diff_score < 0: diff_score = "+" + str("%.2f" % ((-1)*diff_score)) + "%"
            else: diff_score = str("%.2f" % diff_score) + "%"

            self.results_file.write("\n"+str(i+1) + "      " + str(diff_dims) + "      " +
                                    str(diff_bins) + "      " + str(diff_score)) """

        average_diff_dims /= self.amount_of_combinations
        average_diff_bins /= self.amount_of_combinations
        average_diff_scores /= self.amount_of_combinations
        self.results_file.write("\naverage:       {0}      {1}      {2}".format((str("%.2f" % average_diff_dims)+"%"),
                                                                                (str("%.2f" % average_diff_bins)+"%"),
                                                                                (str("%.2f" % average_diff_scores)+"%")))


#if __name__ == "__main__":
