#compare mce and mce without jd:
#1) nr. conditioned dimensions per term
#2) nr. bins per multivariate term
#3) additionaly: dep. network accuracy, precision/recall, entropy value, dep. score

#def mce_estimation(self, do_jd):
import math

from os import path

from DataGeneration import generator
from experiments import Estimation
from main import MceEstimation
from experiments import PrecisionRecallT as prTest
from experiments.dependencyGraph import basic_functions as basic


def eval_size(subspace_size, dep_type):
    rows = 10000
    #nr_dims = [10, 20, 30]
    nr_dims = [10, 20, 25, 30, 40, 50]
    percentage_out = 0.2

    av_acc_mce = []
    av_prob_mce = []
    av_cum_prob_mce = []
    av_precision_mce = []
    av_recall_mce = []

    av_acc_mce_nojd = []
    av_prob_mce_nojd = []
    av_cum_prob_mce_nojd = []
    av_precision_mce_nojd = []
    av_recall_mce_nojd = []

    noise = 10
    print("dep_type: ", dep_type)
    print("noise: ", noise)
    iterations = 2

    """
    acc_file = open("/Users/julia/Documents/studying/master/Master Thesis/thesis/code/experiments/discretizationEval/"
                    + str(dep_type) + "_sub_size" + str(subspace_size) + "/accuracy" + str(noise) + ".txt", "w+")
    pr_file = open("/Users/julia/Documents/studying/master/Master Thesis/thesis/code/experiments/discretizationEval/"
                   + str(dep_type) + "_sub_size" + str(subspace_size) + "/prob_n" + str(noise) + ".txt", "w+")
    ce_ds_file = open("/Users/julia/Documents/studying/master/Master Thesis/thesis/code/experiments/discretizationEval/"
                      + str(dep_type) + "_sub_size" + str(subspace_size) + "/ce_ds_n" + str(noise) + ".txt", "w+")
    jd_file = open("/Users/julia/Documents/studying/master/Master Thesis/thesis/code/experiments/discretizationEval/"
                    + str(dep_type) + "_sub_size" + str(subspace_size) + "/jd" + str(noise) + ".txt", "w+")"""

    for d in nr_dims:
        print("\nd: ", d)
        nr_outliers = int(math.floor(d * percentage_out))
        nr_subsp = math.ceil((d - nr_outliers)/subspace_size)
        dep_types_array = [dep_type] * nr_subsp

        average_acc_mce = 0
        average_prob_mce = 0
        average_cum_prob_mce = 0
        average_pr_mce = 0
        average_rec_mce = 0

        average_acc_mce_nojd = 0
        average_prob_mce_nojd = 0
        average_cum_prob_mce_nojd = 0
        average_pr_mce_nojd = 0
        average_rec_mce_nojd = 0

        for i in range(0, iterations):
            print("\n")
            data = generator.Dataset()
            data.generate_subspaces(rows, d, nr_subsp, subspace_size, dep_types_array, nr_outliers, noise)
            true_d = data.dependencies
            true_ce = round(data.mce_score, 10)
            true_ds = round(data.dependency_score, 10)
            print("true: ", true_d)
            dataset = path.abspath("subspace_network.csv")
            results_file = open(path.abspath("../../results/test.txt"), 'w')

            estimation = Estimation.EntropyEstimation(20, dataset, results_file)
            print("max: ",  estimation.data.max())
            print("min: ", estimation.data.min())
            mce = MceEstimation.MCE(estimation, do_exp_check=False)

            mce.mce_estimation(True)
            print("estimated mce: ", mce.decomposition)
            est_ce = round(mce.joint_ce, 10)
            est_ds = round(mce.mce_uds_score, 10)
            adjusted_d = estimation.data.shape[1]
            accuracy_mce, prob_mce, cum_prob_mce, pr_mce, r_mce = prTest.pr_r(true_d, mce.decomposition, adjusted_d)
            print("acc mce: ", accuracy_mce, "\n")

            mce_nojd = MceEstimation.MCE(estimation, do_exp_check=False)
            mce_nojd.mce_estimation(False)
            print("estimated mce, no jd: ", mce_nojd.decomposition)
            est_ce_nojd = round(mce_nojd.joint_ce, 10)
            est_ds_nojd = round(mce_nojd.mce_uds_score, 10)
            accuracy_mce_nojd, prob_mce_nojd, cum_prob_mce_nojd, pr_mce_nojd, r_mce_nojd = \
                prTest.pr_r(true_d, mce_nojd.decomposition, adjusted_d)
            print("acc mce no jd: ", accuracy_mce_nojd, "\n")

            print("multiv. info: ")
            for i in data.multiv_results:
                print(i)
            print("\nmce, multivariate info: ")
            for i in mce.multiv_results:
                print(i, ": ", mce.multiv_results[i])
            print("\nmce-jd, multivariate info: ")
            for j in mce_nojd.multiv_results:
                print(j, ": ", mce_nojd.multiv_results[j])

            average_acc_mce += accuracy_mce
            average_prob_mce += prob_mce
            average_cum_prob_mce += cum_prob_mce
            average_pr_mce += pr_mce
            average_rec_mce += r_mce

            average_acc_mce_nojd += accuracy_mce_nojd
            average_prob_mce_nojd += prob_mce_nojd
            average_cum_prob_mce_nojd += cum_prob_mce_nojd
            average_pr_mce_nojd += pr_mce_nojd
            average_rec_mce_nojd += r_mce_nojd

            print("\ntrue score: ", true_ds)
            print("mce: ", est_ds)
            print("mce no jd: ", est_ds_nojd, "\n")

            """
            ce_ds_file.write("\nnr dims: " + str(d) + "\n" + "nr outliers: " + str(nr_outliers) + "\n")
            ce_ds_file.write("i: " + str(i + 1))
            ce_ds_file.write("true ce: "+str(true_ce)+", mce: "+str(est_ce)+", mce_nojd: "+str(est_ce_nojd)+"\n")
            ce_ds_file.write("true score: "+str(true_ds)+", mce: "+str(est_ds)+", mce_nojd: "+str(est_ds_nojd)+"\n")"""

        average_acc = [d/iterations for d in [average_acc_mce, average_acc_mce_nojd]]
        av_acc_mce.append(average_acc[0])
        av_acc_mce_nojd.append(average_acc[1])

        average_pr = [d/iterations for d in [average_pr_mce, average_pr_mce_nojd]]
        av_precision_mce.append(average_pr[0])
        av_precision_mce_nojd.append(average_pr[1])

        average_r = [d/iterations for d in [average_rec_mce, average_rec_mce_nojd]]
        av_recall_mce.append(average_r[0])
        av_recall_mce_nojd.append(average_r[1])

        print("av. acc. mce: ", average_acc[0], "\nmce no jd: ", average_acc[1], "\n")

        average_p_mce = [d/iterations for d in [average_prob_mce, average_cum_prob_mce]]
        av_prob_mce.append(average_p_mce[0])
        av_cum_prob_mce.append(average_p_mce[1])

        average_p_mce_nojd = [d/iterations for d in [average_prob_mce_nojd, average_cum_prob_mce_nojd]]
        av_prob_mce_nojd.append(average_p_mce_nojd[0])
        av_cum_prob_mce_nojd.append(average_p_mce_nojd[1])

        """
        pr_file.write("nr dims: "+str(d)+"\n"+"nr outliers: "+str(nr_outliers)+"\n")
        pr_file.write("av. pr. of the network at random, mce: "+str(average_p_mce[0])+"\n")
        pr_file.write("av. pr. of the network or a better one at random, mce: "+str(average_p_mce[1])+"\n")
        pr_file.write("av. pr. of the network at random, mce no jd: "+str(average_p_mce_nojd[0])+"\n")
        pr_file.write("av. pr. of the network or a better one at random, mce no jd: "+str(average_p_mce_nojd[1])+"\n\n")

    pr_file.close()
    ce_ds_file.close()

    acc_file.write("dimensions: "+str(nr_dims)+"\n")
    acc_file.write("acc mce: "+str(av_acc_mce)+"\npr. mce: "+str(av_precision_mce)+"\nr. mce: "+str(av_recall_mce)+"\n")
    acc_file.write("acc mce no jd: "+str(av_acc_mce_nojd)+"\npr. mce no jd: "+str(av_precision_mce_nojd)+
                   "\nrecall mce no jd: "+str(av_recall_mce_nojd)+"\n")

    acc_file.close()"""

    file_name = str(dep_type) + "_20%_" + str(iterations) + "it_" + str(subspace_size) + "_JdEval"
    x_label = "Number of dimensions"
    y_label = "Average accuracy"
    basic.plot_reconstruction_acc_jd(nr_dims, [av_acc_mce, av_acc_mce_nojd], x_label, y_label, file_name)

if __name__ == "__main__":
    print("jd evaluation")
    size = 7
    dep_type = "linear"
    print("subspace size: ", size)
    eval_size(size, dep_type)
