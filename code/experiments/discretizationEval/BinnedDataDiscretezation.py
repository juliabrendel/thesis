import numpy as np
import pandas as pd
from os import path
from DataGeneration import generator
from main import read_data, preprocessing, JointDiscretization as jd, MceEstimation, Uds as uds
import math
import matplotlib.pyplot as plt
from experiments import Estimation

"""
binned matrix - [X_discr, X_cond]

"""


def compare_discretization(true_discr, empirical_discr):
    true_pos = 0
    false_pos = 0
    false_neg = 0

    for true_bin in true_discr:

        max_length = -math.inf
        max_intersection = []
        min_difference = []

        for approx_bin in empirical_discr:
            intersection = np.intersect1d(true_bin, approx_bin)

            if len(true_bin) >= len(approx_bin):
                #intersection = set(true_bin).intersection(set(approx_bin))
                # difference = set(true_bin).difference(set(approx_bin))
                difference = np.setdiff1d(true_bin, approx_bin)  # todo verify
            else:
                #intersection = set(approx_bin).intersection(set(true_bin))
                #difference = set(approx_bin).difference(set(true_bin))
                difference = np.setdiff1d(approx_bin, true_bin)

            # select the biggest intersection
            if len(intersection) > max_length:
                max_length = len(intersection)
                max_intersection = intersection
                min_difference = difference

        true_pos += len(max_intersection)
        false_pos += len(min_difference)
        false_neg += len(true_bin) - len(max_intersection)

    if true_pos + false_pos == 0:
        precision = 0
    else:
        precision = true_pos / (true_pos + false_pos)

    if true_pos + false_neg == 0:
        recall = 0
    else:
        recall = true_pos / (true_pos + false_neg)

    # print("precision = ", precision)
    # print("recall = ", recall, "\n")
    return precision, recall


def compare_discretization_pairwise(true_discr, empirical_discr, number_of_points):
    from itertools import combinations
    from scipy.special import binom

    true_pos = 0
    true_neg = 0
    false_pos = 0
    false_neg = 0

    for true_bin in true_discr:

        max_length = -math.inf
        max_intersection = []
        min_difference = []

        #true_pairs = [list(c) for c in combinations(true_bin, 2)]
        true_pairs = set(combinations(true_bin, 2))
        #print("true pairs: ", sorted(true_pairs))
        for approx_bin in empirical_discr:
            #app_pairs = [list(c) for c in combinations(approx_bin, 2)]
            if len(approx_bin) > 1: app_pairs = set(combinations(approx_bin, 2))
            else: app_pairs = set(approx_bin)
            #print("app pairs: ", sorted(app_pairs), "\n")
            intersection = true_pairs & app_pairs
            assert len(intersection) >= 0
            #print("intersection: ", sorted(intersection), " ", len(intersection))

            difference = app_pairs - true_pairs

            #print("difference: ", sorted(difference), " ", len(difference))
            assert len(difference) >= 0
            if len(intersection) > max_length:
                max_length = len(intersection)
                max_intersection = intersection
                min_difference = difference

        true_pos += len(max_intersection)
        false_pos += len(min_difference)
        false_neg += len(true_pairs) - len(max_intersection)
        true_neg += binom(number_of_points, 2) - true_pos - false_pos - false_neg

        assert true_pos >= 0, str(true_pos)
        assert false_pos >= 0, str(false_pos)
        assert false_neg >= 0, str(false_neg)
        assert true_neg >= 0, str(true_neg)

    accuracy = (true_pos + true_neg)/(true_pos + true_neg + false_pos + false_neg)

    if true_pos + false_pos == 0: precision = 0
    else: precision = true_pos/(true_pos + false_pos)
    if true_pos + false_neg == 0: recall = 0
    else: recall = true_pos/(true_pos + false_neg)

    assert (0 <= accuracy <= 1), str(accuracy)
    assert (0 <= precision <= 1), str(precision)
    assert (0 <= recall <= 1), str(recall)
    print("accuracy: ", accuracy)
    print("precision: ", precision)
    print("recall: ", recall)
    return accuracy


def discr_comparison_univariate(amount_of_iterations):
    # number_of_rows = 10000
    # bins_sizes = [1000, 3000, 3000, 3000]
    average_precision = 0
    average_recall = 0
    number_of_rows = 1000
    for i in range(amount_of_iterations):
        bins_sizes = [int(0.1 * number_of_rows), int(0.4 * number_of_rows), int(0.5 * number_of_rows)]

        true_bins = generator.generate_binnned_dimension(number_of_rows, bins_sizes)
        dataset = path.abspath("binned_data.csv")
        data = read_data.read(dataset)

        number_of_bins = 20
        initial_I = [pd.DataFrame(data).index]
        candidate_set_values, candidate_set_discrs, conditional_ces, counter_all_combinations = \
            preprocessing.generate_candidates(data, number_of_bins, initial_I)

        max_pair = max(candidate_set_values, key=candidate_set_values.get)
        discr = candidate_set_discrs[max_pair]
        precision, recall = compare_discretization(true_bins, discr)
        average_precision += precision
        average_recall += recall

    average_precision /= amount_of_iterations
    average_recall /= amount_of_iterations
    return average_precision, average_recall


# cce comparison for one binned dimension
# def compare_cce_accuracy(number_of_rows, amount_of_iterations, noise_level=None):
# w.r.t. ce - cce difference
def compare_cce_accuracy(univ_ce, true_cce, estimated_cce):
    # average_difference = 0
    true_diff = univ_ce - true_cce
    estimated_diff = univ_ce - estimated_cce
    # diff = min(true_diff, estimated_diff) / max(true_diff, estimated_diff)
    diff = -(1 - (estimated_diff / true_diff))

    return diff


def over_multiple_runs(repetitions, amount_of_iterations, number_of_rows, univariate=False, bivariate=False,
                       joint_distribution=False, number_of_columns=2):
    # average_precision = 0
    # average_recall = 0

    precision = []
    recall = []
    difference = []

    for i in range(repetitions):
        if univariate:
            av_precision, av_recall = discr_comparison_univariate(amount_of_iterations)
        elif bivariate:
            av_precision, av_recall, av_diff = discr_comparison2d(amount_of_iterations, number_of_rows,
                                                                  ce_value_test=True,
                                                                  joint_distribution=joint_distribution,
                                                                  number_of_columns=number_of_columns)

        if av_precision is not None:
            precision.append(av_precision)
            recall.append(av_recall)
            difference.append(av_diff)

    print("av. precision: ", precision)
    print("av. recall: ", recall)
    print("av. diff: ", difference)
    # average_precision /= number_of_iterations
    # average_recall /= number_of_iterations
    # print("average precision = ", average_precision)
    # print("average recall = ", average_recall)
    import matplotlib.pyplot as plt

    plt.subplot(211)
    plt.plot(list(range(1, len(precision) + 1)), precision, 'bo-', label="precision")
    plt.plot(list(range(1, len(precision) + 1)), recall, 'ro-', label="recall")
    plt.xlim(0, len(precision) + 1)
    plt.ylim(0, 1.5)
    plt.legend()
    plt.xlabel('Iterations')
    plt.ylabel('Average precision/recall')

    plt.subplot(212)
    plt.plot(list(range(1, len(precision) + 1)), difference, 'go-')
    plt.xlabel('Iterations')
    plt.ylabel('Average difference CCE, %')
    plt.xlim(0, len(precision) + 1)
    plt.ylim(min(difference) - 5, max(difference) + 5)

    plt.subplots_adjust(left=0.2, wspace=0.8, hspace=0.5)
    plt.savefig("prRecallCCENoNoise.png")
    plt.close()


# def discr_comparison2d(number_of_rows, amount_of_iterations, noise_level=None):
def discr_comparison2d(amount_of_iterations, number_of_rows, noise_level=None, all_bins=True, joint_distribution=False,
                       number_of_columns=2, ce_value_test=False):
    average_precision = 0
    average_recall = 0
    average_cce_diff = 0
    occurrences = amount_of_iterations

    for i in range(amount_of_iterations):
        print("i: ", i)

        if joint_distribution:
            # todo not all bins included?
            bins_sizes = [int(0.1 * number_of_rows), int(0.2 * number_of_rows), int(0.3 * number_of_rows),
                          int(0.4 * number_of_rows)]
            true_bins = generator.generate_jointly_disrtibuted(number_of_columns, bins_sizes, noise_level=noise_level)
            data_file = path.abspath("multivariate_gauss.csv")
        else:
            # add variations for a part of bins
            bins_sizes1 = [int(0.6 * number_of_rows), int(0.2 * number_of_rows), int(0.2 * number_of_rows)]
            bins_sizes2 = [int(0.4 * number_of_rows), int(0.6 * number_of_rows)]
            bins_sizes3 = [int(0.5 * number_of_rows), int(0.5 * number_of_rows)]
            #todo add factorization
            #bins_sizes1 = [int(0.1 * number_of_rows), int(0.2 * number_of_rows), int(0.3 * number_of_rows),
            #               int(0.4 * number_of_rows)]
            #bins_sizes2 = [int(0.3 * number_of_rows), int(0.2 * number_of_rows), int(0.5 * number_of_rows)]
            #bins_sizes3 = [int(0.5 * number_of_rows), int(0.5 * number_of_rows)]

            #true_bins = generator.generate_2dim_binned_data(number_of_rows, bins_sizes1, bins_sizes2, noise_level=noise_level)
            #true_bins = generator.generate_2dim_binned_data(number_of_rows, bins_sizes1, bins_sizes2, all_bins=all_bins,
            #                                                noise_level=noise_level)
            true_bins = generator.generate_univariately_binned_data(number_of_rows, number_of_columns,
                                                                    [bins_sizes1, bins_sizes2, bins_sizes3], all_bins=all_bins,
                                                                    noise_level=noise_level)
            data_file = path.abspath("binned_data.csv")

        #print(true_bins, "\n")
        data = read_data.read(data_file)
        print(data.shape)
        conditioned_dim = data[:, -1]
        true_empirical_cce = jd.conditional_ce_all_bins(conditioned_dim, true_bins)

        # compare to MCE
        res_file = open(path.abspath("../results/test.txt"), 'w')
        estimation = Estimation.EntropyEstimation(20, data_file, res_file)
        # print("univ ce: ", estimation.cumulative_entropies[2], "\n")

        """
        #comparison to MCE
        mceEst = MceEstimation.MCE(estimation, True)
        mceEst.mce_estimation()"""

        #comparison to UDS (pure)
        udsVariations1 = uds.UdsVariations(estimation)
        udsVariations1.uds_pure()
        #udsVariations1.uds_extended_jd() """

        """
        multiv_terms = [i for i in mceEst.final_discretizations.keys() if isinstance(i[1], tuple) and
                        i[0] == number_of_columns]
        print("final discrs: ", mceEst.final_discretizations.keys())"""

        multiv_terms = [i for i in udsVariations1.final_discretizations.keys() if len(i[1]) == number_of_columns]
        print("final discrs: ", udsVariations1.final_discretizations.keys())

        print("multiv terms: ", multiv_terms)
        # print("\nmultiv results: ", mceEst.multiv_results)

        if len(multiv_terms) == 0:
            occurrences -= 1
            print("did not occur\n")
            continue

        multiv_ = multiv_terms[0]
        #multiv_ = [t for t in multiv_terms if t[0] == number_of_columns + 1]
        #print(multiv_)

        #if len(multiv_) == 0:
        #    occurrences -= 1
        #    continue

        # TODO the same for univariate
        if ce_value_test:
            dim_id = multiv_[0]
            #cce_estimated = mceEst.multiv_results[dim_id][1]
            cce_estimated = udsVariations1.multiv_results[dim_id][1]
            diff = compare_cce_accuracy(estimation.cumulative_entropies[dim_id], true_empirical_cce, cce_estimated)
            average_cce_diff += diff

        #emp_discr = mceEst.final_discretizations[multiv_]
        emp_discr = udsVariations1.final_discretizations[multiv_]
        #print("true:\n", true_bins, "\n")
        #print("empirical:\n", emp_discr, "\n")
        precision, recall = compare_discretization(true_bins, emp_discr)
        print("precision: ", precision)
        print("recall: ", recall, "\n")
        average_precision += precision
        average_recall += recall

        # if multiv_ in udsVariations1.final_discretizations:
        #     emp_discr_udspure = udsVariations1.final_discretizations[multiv_]
        #     print("\ncomparison with uds pure\n")
        #     compare_discretization(true_bins, emp_discr_udspure)
        # else:
        #     print("the term did not occur in extended uds with jd")

        # discr comparison for each term (joint discr. case)
        """
        res_discrs = mceEst.final_discretizations.keys()
        for d in res_discrs:
            print("combination: ", d)
            emp_discr = mceEst.final_discretizations[d]
            print("amount of bins: ", len(emp_discr))
            precision, recall = compare_discretization(true_bins, emp_discr)
            print("precision: ", precision)
            print("recall: ", recall, "\n")"""

    if occurrences == 0:
        return None, None, None
    else:
        average_precision /= occurrences
        average_recall /= occurrences
        average_cce_diff /= occurrences
        average_cce_diff *= 100
        print("average precision = ", average_precision)
        print("average recall = ", average_recall)
        print("average cce diff = ", average_cce_diff, "%" "\n")

        return average_precision, average_recall, average_cce_diff


def precision_recall_vs_noise(amount_of_iterations, number_of_rows, joint_distribution=False, number_of_columns=2):
    # add no noise case for first exp
    #noise = [0, 1, 10, 20, 30, 40, 50, 80, 100, 150, 200, 250, 300]
    noise = [0, 1, 10, 15, 20, 30, 40, 50]
    acutual_noise = []
    # noise = [0]

    precision = []
    recall = []
    difference = []

    for n in noise:
        print("noise level: ", n)
        av_precision, av_recall, av_diff = discr_comparison2d(amount_of_iterations, number_of_rows,
                                                              noise_level=n, joint_distribution=joint_distribution,
                                                              number_of_columns=number_of_columns, ce_value_test=True)
        print("average precision: ", av_precision)
        print("average recall: ", av_recall)
        print("average difference: ", av_diff, "\n")
        # todo handle case if both are None
        if av_precision is not None:
            precision.append(av_precision)
            recall.append(av_recall)
            difference.append(av_diff)
            acutual_noise.append(n)

    import matplotlib.pyplot as plt

    plt.subplot(211)
    # plt.plot(list(range(1, len(precision)+1)), precision, 'bo-', label="precision")
    # plt.plot(list(range(1, len(precision)+1)), recall, 'ro-', label="recall")
    plt.plot(acutual_noise, precision, 'bo-', label="precision")
    plt.plot(acutual_noise, recall, 'ro-', label="recall")
    plt.xlim(0, max(acutual_noise) + 1)
    plt.ylim(0, 1.5)
    plt.legend()
    plt.xlabel('Amount of Gaussian noise')
    plt.ylabel('Average precision/recall')

    plt.subplot(212)
    # plt.plot(list(range(1, len(precision)+1)), difference, 'go-')
    plt.plot(acutual_noise, difference, 'go-')
    plt.xlim(0, max(acutual_noise) + 1)
    plt.ylim(min(difference) - 5, max(difference) + 5)
    plt.xlabel('Amount of Gaussian noise')
    plt.ylabel('Average difference CCE, %')

    plt.subplots_adjust(left=0.2, wspace=0.8, hspace=0.5)
    plt.savefig("UdsPrRcCceNoise1000x310It.png")
    plt.close()

    print(precision)
    print(recall)
    print(difference)
    print("\nactual noise: ", acutual_noise)
    # todo also for a part of the bins
    # todo the same plot for the ce value


if __name__ == "__main__":

    d1 = [[1, 3, 4, 5, 6]]
    #d2 = [[1, 3, 4, 5, 6]]
    d2 = [[1, 2]]

    compare_discretization_pairwise(d1, d2, 6)

    #precision_recall_vs_noise(10, 1000, number_of_columns=3)
    #discr_comparison2d(10, 1000, joint_distribution=True, number_of_columns=5, ce_value_test=True)

    # average_differences = []
    # sizes = [100, 200, 300]
    # sizes = [1000]
    # size = 100000
    # noise = [1, 5, 10, 15, 20, 25, 30, 50, 70, 100, 200]
    #noise = [0, 1, 10, 15, 20, 25, 30, 50, 70, 100, 150, 200]

    # for size in sizes:
    """
    for n in noise:
        print("noise level: ", n)
        discr_comparison2d(10, 1000, noise_level=n, joint_distribution=True, number_of_columns=3, ce_value_test=True)
        # todo also for a part of the bins
        # todo the same plot for the ce value
        #diff = true_discr_comparison(size, 10, n)
        #average_differences.append(diff) """

    # diff = true_discr_comparison(size, 30)

    """
    plt.plot(average_differences, sizes, 'bo')
    plt.xlabel('Average difference between the actual and the estimated CCE, %')
    plt.ylabel('Number of data points')
    plt.savefig("cceComparisonTwoDims.png")
    plt.close() """
