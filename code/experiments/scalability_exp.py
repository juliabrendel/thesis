from os import path
from experiments import Estimation
from main import MceEstimation, Uds
import time


def scal_dimensionality():
    dims = [10, 20, 30, 40, 50]
    data_dim10 = path.abspath("../datasets/synthetic/logpolynomial/k5/subspace_network5_logpol10.csv")
    data_dim20 = path.abspath("../datasets/synthetic/logpolynomial/k5/subspace_network5_logpol20.csv")
    data_dim30 = path.abspath("../datasets/synthetic/logpolynomial/k5/subspace_network5_logpol30.csv")
    data_dim40 = path.abspath("../datasets/synthetic/logpolynomial/k5/subspace_network5_logpol40.csv")
    data_dim50 = path.abspath("../datasets/synthetic/logpolynomial/k5/subspace_network5_logpol50.csv")
    data = [data_dim10, data_dim20, data_dim30, data_dim40, data_dim50]

    results_file1 = open(path.abspath("../results/scale_mce5.txt"), "w+")
    for dim in range(len(dims)):
        start_time = time.time()
        estimation = Estimation.EntropyEstimation(20, data[dim], results_file1)
        mce = MceEstimation.MCE(estimation, do_exp_check=False)
        mce.mce_estimation(True)
        end_time = time.time() - start_time
        print("mce time dim ", dims[dim], ": ", end_time, "\n")
        results_file1.write("mce time dim " + str(dims[dim]) + ": " + str(end_time) + "\n")

    results_file = open(path.abspath("../results/scale_uds++5.txt"), "w+")
    for dim in range(len(dims)):
        start_time = time.time()
        estimation = Estimation.EntropyEstimation(20, data[dim], results_file)
        uds = Uds.UdsVariations(estimation)
        uds.uds_extended_jd()
        # mce = MceEstimation.MCE(estimation, do_exp_check=False)
        # mce.mce_estimation(True)
        end_time = time.time() - start_time
        print("uds++ time dim ", dims[dim], ": ", end_time, "\n")
        results_file.write("uds++ time dim " + str(dims[dim]) + ": " + str(end_time) + "\n")


if __name__ == "__main__":
    scal_dimensionality()

