import numpy as np
from main import preprocessing
from scipy import integrate
# import scipy.integrate as integrate
import scipy.stats as stats
import math
from scipy.stats import multivariate_normal

"""
def check():
    X = np.random.normal(0, 1, 5000)
    min_x = min(X)
    max_x = max(X)
    value = integrate.quad(lambda x: stats.norm.pdf(x, 0, 1), -np.inf, np.inf)[0]"""


def univ_ce(X, m, sigma):
    ce_cdf = (-1)*integrate.quad(lambda x: stats.norm.cdf(x, m, sigma) * np.log2(stats.norm.cdf(x, m, sigma)),
                                 min(X), max(X))[0]
    return ce_cdf


def compare_univariate():
    X = np.random.normal(0, 1, 5000)
    #min_x = min(X)
    #max_x = max(X)

    ce_empirical = preprocessing.one_dim_ce(X)
    #ce_cdf = integrate.quad(lambda x: stats.norm.cdf(x, 0, 1) * np.log2(stats.norm.cdf(x, 0, 1)), min_x, max_x)[0] * (-1)
    ce_cdf = univ_ce(X, 0, 1)

    return math.fabs(ce_empirical - ce_cdf)


def univariate_pdf(x, m_x, sigma_x):
    pdf = stats.norm.pdf(x, m_x, sigma_x)
    return pdf


def bivariate_normal_pdf(x, y):

    m_x = 0.5
    m_y = 0.2
    sigma_xx = 2
    sigma_xy = 0.3
    sigma_yx = sigma_xy
    sigma_yy = 0.5

    m = [m_x, m_y]
    sigma = [[sigma_xx, sigma_xy], [sigma_yx, sigma_yy]]
    #data = np.dstack((x, y))
    data = [x, y]
    biv_pdf = multivariate_normal.pdf(data, m, sigma)

    return biv_pdf


def bivariate_normal_cdf():

    x_a = np.inf
    y_a = np.inf
    cdf = integrate.nquad(bivariate_normal_pdf, [[-np.inf, x_a], [-np.inf, y_a]])[0]


def conditional_normal_cdf(m_y, sigma_yy, x_a, y_a):

    pdf_y_a = univariate_pdf(y_a, m_y, sigma_yy)
    cdf_x_given_y = (integrate.quad(lambda x: bivariate_normal_pdf(x, y_a), -np.inf, x_a)[0]) / pdf_y_a
    value = cdf_x_given_y * np.log2(cdf_x_given_y)

    return value


def univ_cond_ce(m_y, sigma_yy, x, y_a):

    value = integrate.quad(lambda x_a: conditional_normal_cdf(m_y, sigma_yy, x_a, y_a), min(x), max(x))[0]
    return value


def cond_ce(x, y, m_y, sigma_yy):

    cond_ce_x_y = (-1)*integrate.quad(lambda y_a: univ_cond_ce(m_y, sigma_yy, x, y_a) * univariate_pdf(y_a, m_y, sigma_yy), min(y), max(y))[0]
    #print(cond_ce_x_y)
    return cond_ce_x_y


def compare_conditional():

    m_x = 0.5
    m_y = 0.2
    sigma_xx = 2
    sigma_xy = 0.3
    sigma_yx = sigma_xy
    sigma_yy = 0.5

    data_shape = (10000, 10000)
    x = np.random.rand(data_shape[0])
    y = np.random.rand(data_shape[1])
    #data = np.dstack((x, y))

    condintional = cond_ce(x, y, m_y, sigma_yy)
    #univariate = univ_ce(x, m_x, sigma_xx)
    univariate_empirical = preprocessing.one_dim_ce(x)

    import pandas as pd
    data = np.matrix([x, y]).T

    result, optimal_cce = preprocessing.compute_score(data, 0, 1, 10, [pd.DataFrame(data).index], 0, univariate_empirical, univariate=True)

    #print("h(X) = ", str(univariate))
    #print("h_emp(X) = ", str(univariate_empirical))
    #print("h_diff(X) = ", str(stats.norm.entropy(stats.norm.rvs())))
    print("h(X|Y) = ", str(condintional))
    print("h_emp(X|Y) = ", str(optimal_cce))


if __name__ == "__main__":
    """
    av_error = 0
    iterations = 1000
    for i in range(iterations):
        av_error += compare_univariate()

    av_error /= iterations
    print(av_error)"""

    compare_conditional()
