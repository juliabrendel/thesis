from unittest import TestCase
from experiments.PrecisionRecallT import sum_prob
import random
import numpy
import time
import matplotlib.pyplot as plt


class TestSum_prob(TestCase):
    def test_sum_prob(self):
        # result should be equal to 1
        T = random.randint(100, 200)
        F = random.randint(100, 200)
        tp = 0
        fp = F
        self.assertTrue(sum_prob(tp, fp, T, F) == 1)

    def test_run_time(self):
        times = list()
        n = 100
        x = numpy.arange(1, n)
        sizes = list()
        result = list()
        for i in x:
            T = random.randint(50, 100 * (i+1))
            F = random.randint(50, 100)
            tp = random.randint(0, T)
            fp = random.randint(0, F)
            start = time.time()
            result.append(sum_prob(tp, fp, T, F))
            sizes.append(T + F)
            end = time.time() - start
            times.append(end)
        plt.scatter(sizes, times, marker='+', c=['r' if numpy.isnan(p) else 'b' for p in result])
        plt.show()
