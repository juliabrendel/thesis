nr dims: 4
nr outliers: 0
av. pr. of the network at random, mce: 0.513333333333
av. pr. of the network or a better one at random, mce: 0.161515151515
av. pr. of the network at random, uds1,2,3: 0.166666666667, 0.175, 0.085
av. pr. of the network or a better one at random, uds1,2,3: 0.0265151515152, 0.0454545454545, 0.0119696969697

nr dims: 10
nr outliers: 2
av. pr. of the network at random, mce: 0.0855792070798
av. pr. of the network or a better one at random, mce: 0.00667145603605
av. pr. of the network at random, uds1,2,3: 0.301033040212, 0.286841482602, 0.220376464083
av. pr. of the network or a better one at random, uds1,2,3: 0.175841521901, 0.16354954141, 0.0701231982875

nr dims: 20
nr outliers: 4
av. pr. of the network at random, mce: 4.6697926433e-05
av. pr. of the network or a better one at random, mce: 5.98386012541e-07
av. pr. of the network at random, uds1,2,3: 0.0412687400775, 0.20898084666, 0.0589218851315
av. pr. of the network or a better one at random, uds1,2,3: 0.212269225944, 0.0596870945439, 0.00416759546611

nr dims: 30
nr outliers: 6
av. pr. of the network at random, mce: 2.89206246141e-09
av. pr. of the network or a better one at random, mce: 1.48814975857e-11
av. pr. of the network at random, uds1,2,3: 0.00219437186331, 0.176852924291, 0.000995981784198
av. pr. of the network or a better one at random, uds1,2,3: 0.225704820568, 0.0358019842175, 1.33290216406e-05

nr dims: 40
nr outliers: 8
av. pr. of the network at random, mce: 9.37475745212e-14
av. pr. of the network or a better one at random, mce: 2.70749677834e-16
av. pr. of the network at random, uds1,2,3: 6.20198572136e-05, 0.18409472929, 2.41241369032e-07
av. pr. of the network or a better one at random, uds1,2,3: 0.232169647636, 0.0184430975006, 1.37170092272e-09

nr dims: 50
nr outliers: 10
av. pr. of the network at random, mce: 2.8533078553e-17
av. pr. of the network or a better one at random, mce: nan
av. pr. of the network at random, uds1,2,3: 1.08724678483e-06, 0.224107297589, 5.55054027501e-07
av. pr. of the network or a better one at random, uds1,2,3: nan, nan, nan

