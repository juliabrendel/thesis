nr dims: 3
nr outliers: 0
av. pr. of the network at random, mce: 0.34
av. pr. of the network or a better one at random, mce: 0.568571428571
av. pr. of the network at random, uds: 0.2
av. pr. of the network or a better one at random, uds: 0.0380952380952

nr dims: 10
nr outliers: 2
av. pr. of the network at random, mce: 0.173304047044
av. pr. of the network or a better one at random, mce: 0.0164660203704
av. pr. of the network at random, uds: 0.401377386949
av. pr. of the network or a better one at random, uds: 0.0977333458783

nr dims: 20
nr outliers: 4
av. pr. of the network at random, mce: 3.13953564866e-05
av. pr. of the network or a better one at random, mce: 3.89714864218e-07
av. pr. of the network at random, uds: 0.257727327739
av. pr. of the network or a better one at random, uds: 0.112784633353

nr dims: 30
nr outliers: 6
av. pr. of the network at random, mce: 2.44877875772e-09
av. pr. of the network or a better one at random, mce: 1.31035508086e-11
av. pr. of the network at random, uds: 0.00135996667241
av. pr. of the network or a better one at random, uds: 0.250295612492

nr dims: 40
nr outliers: 8
av. pr. of the network at random, mce: 5.61415941322e-10
av. pr. of the network or a better one at random, mce: 2.14179805683e-12
av. pr. of the network at random, uds: 0.000239622973398
av. pr. of the network or a better one at random, uds: 0.225000195707

nr dims: 50
nr outliers: 10
av. pr. of the network at random, mce: 2.29144219976e-10
av. pr. of the network or a better one at random, mce: nan
av. pr. of the network at random, uds: 3.69233944071e-05
av. pr. of the network or a better one at random, uds: nan

