import math

import os
from os import path
import pandas as pd

from DataGeneration import generator
from experiments import Estimation
from main import MceEstimation, Uds
from experiments import PrecisionRecallT as prTest
from experiments.dependencyGraph import basic_functions as basic


def eval_size(subspace_size, dep_type):
    rows = 10000
    #nr_dims = [50]
    nr_dims = [10, 20, 30, 40, 50]
    #percentage_out = 0.2
    av_acc_mce = []
    av_prob_mce = []
    av_cum_prob_mce = []
    av_precision_mce = []
    av_recall_mce = []

    av_acc_uds = []
    av_prob_uds = []
    av_cum_prob_uds = []
    av_precision_uds = []
    av_recall_uds = []

    """
    av_acc_uds_jd = []
    av_prob_uds_jd = []
    av_cum_prob_uds_jd = []
    av_precision_uds_jd = []
    av_recall_uds_jd = []"""

    av_acc_uds_x_jd = []
    av_prob_uds_x_jd = []
    av_cum_prob_uds_x_jd = []
    av_precision_uds_x_jd = []
    av_recall_uds_x_jd = []

    noise = 10
    print("dep_type: ", dep_type)
    print("noise: ", noise)
    iterations = 5

    # acc_file = open("/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/experiments/subspaceNetwork/"
    #                + str(dep_type) + "_sub_size" + str(subspace_size) + "/accuracy" + str(noise) + ".txt", "w+")
    # pr_file = open("/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/experiments/subspaceNetwork/"
    #               + str(dep_type) + "_sub_size" + str(subspace_size) + "/prob_n" + str(noise) + "_new.txt", "w+")
    ce_ds_file = open("/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/experiments/subspaceNetwork/"
                      + str(dep_type) + "_sub_size" + str(subspace_size) + "/ce_ds_changed" + str(noise) + "_new.txt", "w+")

    for d in nr_dims:
        print("d: ", d)
        # nr_outliers = int(math.floor(d * percentage_out))
        # nr_subsp = math.ceil((d - nr_outliers) / subspace_size)
        # dep_types_array = [dep_type] * nr_subsp

        average_acc_mce = 0
        average_prob_mce = 0
        average_cum_prob_mce = 0
        average_pr_mce = 0
        average_rec_mce = 0

        average_acc_uds = 0
        average_prob_uds = 0
        average_cum_prob_uds = 0
        average_pr_uds = 0
        average_rec_uds = 0

        """
        average_acc_uds_jd = 0
        average_prob_uds_jd = 0
        average_cum_prob_uds_jd = 0
        average_pr_uds_jd = 0
        average_rec_uds_jd = 0"""

        average_acc_uds_x_jd = 0
        average_prob_uds_x_jd = 0
        average_cum_prob_uds_x_jd = 0
        average_pr_uds_x_jd = 0
        average_rec_uds_x_jd = 0

        directory = "/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/datasets/synthetic/linear/m"+str(d)+"/no_label"
        for filename in os.listdir(directory):
            dataset = directory + "/" + filename

            # for i in range(0, iterations):
            print("\n")
            # data = generator.Dataset()
            # data.generate_subspaces(rows, d, nr_subsp, subspace_size, dep_types_array, nr_outliers, noise)
            # true_d = data.dependencies
            # true_ce = round(data.mce_score, 10)
            # true_ds = round(data.dependency_score, 10)
            # print("true: ", true_d)
            # dataset = path.abspath("subspace_network.csv")
            results_file = open(path.abspath("../../results/test.txt"), 'w')

            estimation = Estimation.EntropyEstimation(20, dataset, results_file)
            print("max: ", estimation.data.max())
            print("min: ", estimation.data.min())
            mce = MceEstimation.MCE(estimation, do_exp_check=False)
            mce.mce_estimation(True)
            # no dependency maximization version
            # mce.mce_exp_version()
            print("estimated mce: ", mce.decomposition)
            est_ce = round(mce.joint_ce, 10)
            est_ds = round(mce.mce_uds_score, 10)
            adjusted_d = estimation.data.shape[1]
            # accuracy_mce, prob_mce, cum_prob_mce, pr_mce, r_mce = prTest.pr_r(true_d, mce.decomposition, adjusted_d)
            # print("acc mce: ", accuracy_mce)
            # print("precision: ", pr_mce)
            # print("recall: ", r_mce)

            uds1 = Uds.UdsVariations(estimation)
            uds1.uds_pure()
            uds1_ce = round(uds1.mce_score, 10)
            u_ds1 = round(uds1.uds_score, 10)
            # accuracy_uds1, prob_uds1, cum_prob_uds1, pr_uds1, r_uds1 = prTest.pr_r(true_d, uds1.resulting_combination,
            #                                                                       adjusted_d)
            # print("acc uds: ", accuracy_uds1, "\n")

            """
            uds2 = Uds.UdsVariations(estimation)
            uds2.uds_jd()
            uds2_ce = round(uds2.mce_score, 10)
            u_ds2 = round(uds2.uds_score, 10)
            accuracy_uds2, prob_uds2, cum_prob_uds2, pr_uds2, r_uds2 = prTest.pr_r(true_d, uds2.resulting_combination,
                                                                                   adjusted_d)
            print("acc uds jd: ", accuracy_uds2, "\n")"""

            uds3 = Uds.UdsVariations(estimation)
            uds3.uds_extended_jd()
            uds3_ce = round(uds3.mce_score, 10)
            u_ds3 = round(uds3.uds_score, 10)
            # accuracy_uds3, prob_uds3, cum_prob_uds3, pr_uds3, r_uds3 = prTest.pr_r(true_d, uds3.resulting_combination,
            #                                                                       adjusted_d)
            # print("acc uds jd: ", accuracy_uds3, "\n")

            """
            average_acc_mce += accuracy_mce
            average_prob_mce += prob_mce
            average_cum_prob_mce += cum_prob_mce
            average_pr_mce += pr_mce
            average_rec_mce += r_mce

            average_acc_uds += accuracy_uds1
            average_prob_uds += prob_uds1
            average_cum_prob_uds += cum_prob_uds1
            average_pr_uds += pr_uds1
            average_rec_uds += r_uds1"""

            """
            average_acc_uds_jd += accuracy_uds2
            average_prob_uds_jd += prob_uds2
            average_cum_prob_uds_jd += cum_prob_uds2
            average_pr_uds_jd += pr_uds2
            average_rec_uds_jd += r_uds2

            average_acc_uds_x_jd += accuracy_uds3
            average_prob_uds_x_jd += prob_uds3
            average_cum_prob_uds_x_jd += cum_prob_uds3
            average_pr_uds_x_jd += pr_uds3
            average_rec_uds_x_jd += r_uds3"""

            # print("true score: ", true_ds)
            print("mce score: ", est_ds)
            print("uds score: ", u_ds1)
            print("uds++ score: ", u_ds3)
            # print("true ce: ", true_ce)
            # print("mce ce: ", est_ce)

            # ce_ds_file.write("\nnr dims: " + str(d) + "\n" + "nr outliers: " + str(nr_outliers) + "\n")
            # ce_ds_file.write("i: " + str(i + 1))
            ce_ds_file.write("mce: " + str(est_ds) + "\n")
            ce_ds_file.write("uds: " + str(u_ds1) + "\n")
            ce_ds_file.write("uds++: " + str(u_ds3) + "\n")
            # ce_ds_file.write("true ce: " + str(true_ce) + ", mce: " + str(est_ce) + ", uds: " + str(uds1_ce) + "\n")
            # ce_ds_file.write("uds_jd: " + str(uds2_ce) + ", uds_x_jd: " + str(uds3_ce) + "\n")
            # ce_ds_file.write("true score: " + str(true_ds) + ", mce: " + str(est_ds) + ", uds: " + str(u_ds1) + "\n")
            # ce_ds_file.write("uds_jd: " + str(u_ds2) + ", uds_x_jd: " + str(u_ds3) + "\n")

        # average_acc = [d / iterations for d in
        #               [average_acc_mce, average_acc_uds, average_acc_uds_jd, average_acc_uds_x_jd]]
        # average_acc = [d / iterations for d in [average_acc_mce]]
        # av_acc_mce.append(average_acc[0])
        # av_acc_uds.append(average_acc[1])
        # av_acc_uds_jd.append(average_acc[2])
        # av_acc_uds_x_jd.append(average_acc[3])

        # average_pr = [d / iterations for d in [average_pr_mce, average_pr_uds, average_pr_uds_jd, average_pr_uds_x_jd]]
        # average_pr = [d / iterations for d in [average_pr_mce]]
        # av_precision_mce.append(average_pr[0])
        # av_precision_uds.append(average_pr[1])
        # av_precision_uds_jd.append(average_pr[2])
        # av_precision_uds_x_jd.append(average_pr[3])

        # average_r = [d / iterations for d in
        #              [average_rec_mce, average_rec_uds, average_rec_uds_jd, average_rec_uds_x_jd]]
        # average_r = [d / iterations for d in [average_rec_mce]]
        # av_recall_mce.append(average_r[0])
        # av_recall_uds.append(average_r[1])
        # av_recall_uds_jd.append(average_r[2])
        # av_recall_uds_x_jd.append(average_r[3])

        # print("av. acc. mce: ", average_acc[0], "\nuds: ", average_acc[1], "\nuds_jd: ", average_acc[2],
        #      "\nuds_x_jd: ", average_acc[3])

        # print("av. acc. mce: ", average_acc[0], "\nprecision: ", average_pr[0], "\nrecall: ", average_r[0])

        # average_p_mce = [d / iterations for d in [average_prob_mce, average_cum_prob_mce]]
        # av_prob_mce.append(average_p_mce[0])
        # av_cum_prob_mce.append(average_p_mce[1])

        # average_p_uds = [d / iterations for d in [average_prob_uds, average_cum_prob_uds,
        #                                          average_prob_uds_jd, average_cum_prob_uds_jd,
        #                                         average_prob_uds_x_jd, average_cum_prob_uds_x_jd]]
        # av_prob_uds.append(average_p_uds[0])
        # av_cum_prob_uds.append(average_p_uds[1])
        # av_prob_uds_jd.append(average_p_uds[2])
        # av_cum_prob_uds_jd.append(average_p_uds[3])
        # av_prob_uds_x_jd.append(average_p_uds[4])
        # av_cum_prob_uds_x_jd.append(average_p_uds[5])

        # pr_file.write("nr dims: " + str(d) + "\n" + "nr outliers: " + str(nr_outliers) + "\n")
        # pr_file.write("av. pr. of the network at random, mce: " + str(average_p_mce[0]) + "\n")
        # pr_file.write("av. pr. of the network or a better one at random, mce: " + str(average_p_mce[1]) + "\n")
        # pr_file.write("av. pr. of the network at random, uds1,2,3: " + str(average_p_uds[0]) +
        #               ", " + str(average_p_uds[2]) + ", " + str(average_p_uds[4]) + "\n")
        # pr_file.write("av. pr. of the network or a better one at random, uds1,2,3: " + str(average_p_uds[1]) +
        #              ", " + str(average_p_uds[3]) + ", " + str(average_p_uds[5]) + "\n\n")

        """
        acc_file.write("nr dims: " + str(d) + "\n")
        acc_file.write("acc mce: " + str(av_acc_mce) + "\npr. mce: " + str(av_precision_mce) +
                       "\nr. mce: " + str(av_recall_mce) + "\n")
        acc_file.write("acc uds: " + str(av_acc_uds) + "\npr. uds: " + str(av_precision_uds) +
                       "\nr. uds: " + str(av_recall_uds) + "\n")
        acc_file.write("acc uds jd: " + str(av_acc_uds_jd) + "\npr. uds jd: " + str(av_precision_uds_jd) +
                       "\nr. uds jd: " + str(av_recall_uds_jd) + "\n")
        acc_file.write("acc uds x jd: " + str(av_acc_uds_x_jd) + "\npr. uds x jd: " + str(av_precision_uds_x_jd) +
                       "\nr. uds x jd: " + str(av_recall_uds_x_jd) + "\n")"""

    # pr_file.close()
    ce_ds_file.close()
    # acc_file.close()

    """
    file_name = str(dep_type) + "_" + str(iterations) + "it_" + str(subspace_size) + "_new"
    x_label = "Number of dimensions"
    y_label = "Accuracy"
    basic.plot_reconstruction_acc(nr_dims, [av_acc_mce, av_acc_uds, av_acc_uds_jd, av_acc_uds_x_jd],
                                  x_label, y_label, file_name)"""


def eval_subspaces(dep_type):
    rows = 10000
    d = 30
    subsp_sizes = [2, 3, 4, 5, 6, 7, 8, 9, 10]

    # nr_dims = [subspace_size, 10, 20, 30, 40, 50]
    percentage_out = 0.2
    av_acc_mce = []
    av_prob_mce = []
    av_cum_prob_mce = []
    av_precision_mce = []
    av_recall_mce = []

    av_acc_uds = []
    av_prob_uds = []
    av_cum_prob_uds = []
    av_precision_uds = []
    av_recall_uds = []

    av_acc_uds_jd = []
    av_prob_uds_jd = []
    av_cum_prob_uds_jd = []
    av_precision_uds_jd = []
    av_recall_uds_jd = []

    av_acc_uds_x_jd = []
    av_prob_uds_x_jd = []
    av_cum_prob_uds_x_jd = []
    av_precision_uds_x_jd = []
    av_recall_uds_x_jd = []

    noise = 10
    print("dep_type: ", dep_type)
    #print("noise: ", noise)
    iterations = 10

    acc_file = open("/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/experiments/subspaceNetwork/"
                    + str(dep_type) + "/sub_dim" + "/accuracy" + ".txt", "w+")

    pr_file = open("/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/experiments/subspaceNetwork/"
                   + str(dep_type) + "/sub_dim" + "/prob_n" + ".txt", "w+")
    ce_ds_file = open("/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/experiments/subspaceNetwork/"
                      + str(dep_type) + "/sub_dim" + "/ce_ds_n" + ".txt", "w+")

    for subspace_size in subsp_sizes:

        print("size: ", subspace_size)
        nr_outliers = int(math.floor(d * percentage_out))
        nr_subsp = math.ceil((d - nr_outliers) / subspace_size)
        dep_types_array = [dep_type] * nr_subsp

        average_acc_mce = 0
        average_prob_mce = 0
        average_cum_prob_mce = 0
        average_pr_mce = 0
        average_rec_mce = 0

        average_acc_uds = 0
        average_prob_uds = 0
        average_cum_prob_uds = 0
        average_pr_uds = 0
        average_rec_uds = 0

        average_acc_uds_jd = 0
        average_prob_uds_jd = 0
        average_cum_prob_uds_jd = 0
        average_pr_uds_jd = 0
        average_rec_uds_jd = 0

        average_acc_uds_x_jd = 0
        average_prob_uds_x_jd = 0
        average_cum_prob_uds_x_jd = 0
        average_pr_uds_x_jd = 0
        average_rec_uds_x_jd = 0

        if subspace_size == 3:
            iterations = 3
        else:
            iterations = 10

        for i in range(0, iterations):
            print("\n")
            data = generator.Dataset()
            data.generate_subspaces(rows, d, nr_subsp, subspace_size, dep_types_array, nr_outliers, noise)
            true_d = data.dependencies
            true_ce = round(data.mce_score, 10)
            true_ds = round(data.dependency_score, 10)
            print("true: ", true_d)
            dataset = path.abspath("subspace_network.csv")
            results_file = open(path.abspath("../../results/test.txt"), 'w')

            estimation = Estimation.EntropyEstimation(20, dataset, results_file)
            print("max: ", estimation.data.max())
            print("min: ", estimation.data.min())
            mce = MceEstimation.MCE(estimation, do_exp_check=False)
            mce.mce_estimation(True)
            # no dependency maximization version
            # mce.mce_exp_version()
            print("estimated mce: ", mce.decomposition)
            est_ce = round(mce.joint_ce, 10)
            est_ds = round(mce.mce_uds_score, 10)
            adjusted_d = estimation.data.shape[1]
            accuracy_mce, prob_mce, cum_prob_mce, pr_mce, r_mce = prTest.pr_r(true_d, mce.decomposition, adjusted_d)
            print("acc mce: ", accuracy_mce)

            uds1 = Uds.UdsVariations(estimation)
            uds1.uds_pure()
            uds1_ce = round(uds1.mce_score, 10)
            u_ds1 = round(uds1.uds_score, 10)
            accuracy_uds1, prob_uds1, cum_prob_uds1, pr_uds1, r_uds1 = prTest.pr_r(true_d, uds1.resulting_combination,
                                                                                   adjusted_d)
            print("acc uds: ", accuracy_uds1, "\n")

            uds2 = Uds.UdsVariations(estimation)
            uds2.uds_jd()
            uds2_ce = round(uds2.mce_score, 10)
            u_ds2 = round(uds2.uds_score, 10)
            accuracy_uds2, prob_uds2, cum_prob_uds2, pr_uds2, r_uds2 = prTest.pr_r(true_d, uds2.resulting_combination,
                                                                                   adjusted_d)
            print("acc uds jd: ", accuracy_uds2, "\n")

            uds3 = Uds.UdsVariations(estimation)
            uds3.uds_extended_jd()
            uds3_ce = round(uds3.mce_score, 10)
            u_ds3 = round(uds3.uds_score, 10)
            accuracy_uds3, prob_uds3, cum_prob_uds3, pr_uds3, r_uds3 = prTest.pr_r(true_d, uds3.resulting_combination,
                                                                                   adjusted_d)
            print("acc uds jd: ", accuracy_uds3, "\n")

            average_acc_mce += accuracy_mce
            average_prob_mce += prob_mce
            average_cum_prob_mce += cum_prob_mce
            average_pr_mce += pr_mce
            average_rec_mce += r_mce

            average_acc_uds += accuracy_uds1
            average_prob_uds += prob_uds1
            average_cum_prob_uds += cum_prob_uds1
            average_pr_uds += pr_uds1
            average_rec_uds += r_uds1

            average_acc_uds_jd += accuracy_uds2
            average_prob_uds_jd += prob_uds2
            average_cum_prob_uds_jd += cum_prob_uds2
            average_pr_uds_jd += pr_uds2
            average_rec_uds_jd += r_uds2

            average_acc_uds_x_jd += accuracy_uds3
            average_prob_uds_x_jd += prob_uds3
            average_cum_prob_uds_x_jd += cum_prob_uds3
            average_pr_uds_x_jd += pr_uds3
            average_rec_uds_x_jd += r_uds3

            print("true score: ", true_ds)
            print("mce: ", est_ds)
            print("true ce: ", true_ce)
            print("mce: ", est_ce, "\n")
            # print("uds: ", u_ds1, "\n")

            ce_ds_file.write("\nsubspace_size: " + str(subspace_size) + "\n" + "nr outliers: " + str(nr_outliers) + "\n")
            ce_ds_file.write("i: " + str(i + 1) + "\n")
            ce_ds_file.write("true ce: " + str(true_ce) + "\n" + "mce: " + str(est_ce) + "\n" + "uds: " + str(uds1_ce) + "\n" )
            ce_ds_file.write("uds_jd: " + str(uds2_ce) + "\n" + "uds_x_jd: " + str(uds3_ce) + "\n")
            ce_ds_file.write("true score: " + str(true_ds) + "\n" + "mce: " + str(est_ds) + "\n" + "uds: " + str(u_ds1) + "\n")
            ce_ds_file.write("uds_jd: " + str(u_ds2) + "\n" + "uds_x_jd: " + str(u_ds3) + "\n")

        average_acc = [d / iterations for d in
                       [average_acc_mce, average_acc_uds, average_acc_uds_jd, average_acc_uds_x_jd]]
        # average_acc = [d / iterations for d in [average_acc_mce]]
        av_acc_mce.append(average_acc[0])
        av_acc_uds.append(average_acc[1])
        av_acc_uds_jd.append(average_acc[2])
        av_acc_uds_x_jd.append(average_acc[3])

        average_pr = [d / iterations for d in [average_pr_mce, average_pr_uds, average_pr_uds_jd, average_pr_uds_x_jd]]
        # average_pr = [d / iterations for d in [average_pr_mce]]
        av_precision_mce.append(average_pr[0])
        av_precision_uds.append(average_pr[1])
        av_precision_uds_jd.append(average_pr[2])
        av_precision_uds_x_jd.append(average_pr[3])

        average_r = [d / iterations for d in
                     [average_rec_mce, average_rec_uds, average_rec_uds_jd, average_rec_uds_x_jd]]
        # average_r = [d / iterations for d in [average_rec_mce]]
        av_recall_mce.append(average_r[0])
        av_recall_uds.append(average_r[1])
        av_recall_uds_jd.append(average_r[2])
        av_recall_uds_x_jd.append(average_r[3])

        print("av. acc. mce: ", average_acc[0], "\nuds: ", average_acc[1], "\nuds_jd: ", average_acc[2],
              "\nuds_x_jd: ", average_acc[3])

        # print("av. acc. mce: ", average_acc[0], "\nprecision: ", average_pr[0], "\nrecall: ", average_r[0])

        average_p_mce = [d / iterations for d in [average_prob_mce, average_cum_prob_mce]]
        av_prob_mce.append(average_p_mce[0])
        av_cum_prob_mce.append(average_p_mce[1])

        average_p_uds = [d / iterations for d in [average_prob_uds, average_cum_prob_uds,
                                                  average_prob_uds_jd, average_cum_prob_uds_jd,
                                                  average_prob_uds_x_jd, average_cum_prob_uds_x_jd]]
        av_prob_uds.append(average_p_uds[0])
        av_cum_prob_uds.append(average_p_uds[1])
        av_prob_uds_jd.append(average_p_uds[2])
        av_cum_prob_uds_jd.append(average_p_uds[3])
        av_prob_uds_x_jd.append(average_p_uds[4])
        av_cum_prob_uds_x_jd.append(average_p_uds[5])

        pr_file.write("subspace_size: " + str(subspace_size) + "\n" + "nr outliers: " + str(nr_outliers) + "\n")
        pr_file.write("av. pr. of the network at random, mce: " + str(average_p_mce[0]) + "\n")
        pr_file.write("av. pr. of the network or a better one at random, mce: " + str(average_p_mce[1]) + "\n")
        pr_file.write("av. pr. of the network at random, uds1,2,3: " + str(average_p_uds[0]) +
                      ", " + str(average_p_uds[2]) + ", " + str(average_p_uds[4]) + "\n")
        pr_file.write("av. pr. of the network or a better one at random, uds1,2,3: " + str(average_p_uds[1]) +
                      ", " + str(average_p_uds[3]) + ", " + str(average_p_uds[5]) + "\n\n")

    acc_file.write("nr dims: " + str(d) + "\n")
    acc_file.write("acc mce: " + str(av_acc_mce) + "\npr. mce: " + str(av_precision_mce) +
                   "\nr. mce: " + str(av_recall_mce) + "\n")
    acc_file.write("acc uds: " + str(av_acc_uds) + "\npr. uds: " + str(av_precision_uds) +
                   "\nr. uds: " + str(av_recall_uds) + "\n")
    acc_file.write("acc uds jd: " + str(av_acc_uds_jd) + "\npr. uds jd: " + str(av_precision_uds_jd) +
                   "\nr. uds jd: " + str(av_recall_uds_jd) + "\n")
    acc_file.write("acc uds x jd: " + str(av_acc_uds_x_jd) + "\npr. uds x jd: " + str(av_precision_uds_x_jd) +
                   "\nr. uds x jd: " + str(av_recall_uds_x_jd) + "\n")

    pr_file.close()
    ce_ds_file.close()
    acc_file.close()

    file_name = "SubDim" + str(dep_type) + "_" + str(iterations) + "it"
    x_label = "Subspace dimensionality"
    y_label = "Accuracy"
    basic.plot_reconstruction_acc(subsp_sizes, [av_acc_mce, av_acc_uds, av_acc_uds_jd, av_acc_uds_x_jd],
                                  x_label, y_label, file_name)


def eval_true_dir(dep_type):
    dir = "/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/datasets/synthetic/" + str(dep_type)
    dimensions = [10, 20, 40, 50]

    av_acc_mce = []
    av_prob_mce = []
    av_cum_prob_mce = []
    av_precision_mce = []
    av_recall_mce = []
    av_acc_uds = []
    av_prob_uds = []
    av_cum_prob_uds = []
    av_precision_uds = []
    av_recall_uds = []
    av_acc_uds_jd = []
    av_prob_uds_jd = []
    av_cum_prob_uds_jd = []
    av_precision_uds_jd = []
    av_recall_uds_jd = []
    av_acc_uds_x_jd = []
    av_prob_uds_x_jd = []
    av_cum_prob_uds_x_jd = []
    av_precision_uds_x_jd = []
    av_recall_uds_x_jd = []

    acc_file = open("/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/experiments/subspaceNetwork/"
                    + str(dep_type) + "/true_direction" + "/accuracy" + ".txt", "w+")

    for d in dimensions:
        print("\nd: ", d)

        if d == 10:
            true_d = [(2, (0, 1)), (5, (3, 4)), (8, (6, 7)), (9,)]
        elif d == 20:
            true_d = [(2, (0, 1)), (5, (3, 4)), (8, (6, 7)), (11, (9, 10)), (14, (12, 13)),
                      (17, (15, 16)), (18,), (19,)]
        elif d == 40:
            true_d = [(2, (0, 1)), (5, (3, 4)), (8, (6, 7)), (11, (9, 10)), (14, (12, 13)), (17, (15, 16)),
                      (20, (18, 19)), (23, (21, 22)), (26, (24, 25)), (29, (27, 28)), (32, (30, 31)), (33,), (34,),
                      (35,), (36,), (37,), (38,), (39,)]
        else:
            true_d = [(2, (0, 1)), (5, (3, 4)), (8, (6, 7)), (11, (9, 10)), (14, (12, 13)), (17, (15, 16)),
                      (20, (18, 19)), (23, (21, 22)), (26, (24, 25)), (29, (27, 28)), (32, (30, 31)), (35, (33, 34)),
                      (38, (36, 37)), (41, (39, 40)), (42,), (43,), (44,), (45,), (46,), (47,), (48,), (49,)]

        average_acc_mce = 0
        average_prob_mce = 0
        average_cum_prob_mce = 0
        average_pr_mce = 0
        average_rec_mce = 0

        average_acc_uds = 0
        average_prob_uds = 0
        average_cum_prob_uds = 0
        average_pr_uds = 0
        average_rec_uds = 0

        average_acc_uds_jd = 0
        average_prob_uds_jd = 0
        average_cum_prob_uds_jd = 0
        average_pr_uds_jd = 0
        average_rec_uds_jd = 0

        average_acc_uds_x_jd = 0
        average_prob_uds_x_jd = 0
        average_cum_prob_uds_x_jd = 0
        average_pr_uds_x_jd = 0
        average_rec_uds_x_jd = 0

        datasets = [dir + "/m" + str(d)+ "/no_label/subspace_network_" + str(dep_type)+"0.csv",
                    dir + "/m" + str(d) + "/no_label/subspace_network_" + str(dep_type) + "1.csv",
                    dir + "/m" + str(d) + "/no_label/subspace_network_" + str(dep_type) + "2.csv"]

        for data in datasets:
            results_file = open(path.abspath("../../results/mce_true_dir.txt"), 'w+')
            estimation = Estimation.EntropyEstimation(20, data, results_file)
            mce = MceEstimation.MCE(estimation, do_exp_check=False)
            mce.mce_estimation(True)
            print("estimated mce: ", mce.decomposition)
            adjusted_d = estimation.data.shape[1]
            accuracy_mce, prob_mce, cum_prob_mce, pr_mce, r_mce = \
                prTest.pr_r_directed(true_d, mce.decomposition, adjusted_d)
            print("acc mce: ", accuracy_mce)
            uds1 = Uds.UdsVariations(estimation)
            uds1.uds_pure()
            uds1_ce = round(uds1.mce_score, 10)
            u_ds1 = round(uds1.uds_score, 10)
            accuracy_uds1, prob_uds1, cum_prob_uds1, pr_uds1, r_uds1 = \
                prTest.pr_r_directed(true_d, uds1.resulting_combination, adjusted_d)
            print("acc uds: ", accuracy_uds1, "\n")

            uds2 = Uds.UdsVariations(estimation)
            uds2.uds_jd()
            uds2_ce = round(uds2.mce_score, 10)
            u_ds2 = round(uds2.uds_score, 10)
            accuracy_uds2, prob_uds2, cum_prob_uds2, pr_uds2, r_uds2 = \
                prTest.pr_r_directed(true_d, uds2.resulting_combination, adjusted_d)
            print("acc uds jd: ", accuracy_uds2, "\n")

            uds3 = Uds.UdsVariations(estimation)
            uds3.uds_extended_jd()
            uds3_ce = round(uds3.mce_score, 10)
            u_ds3 = round(uds3.uds_score, 10)
            accuracy_uds3, prob_uds3, cum_prob_uds3, pr_uds3, r_uds3 = \
                prTest.pr_r_directed(true_d, uds3.resulting_combination, adjusted_d)
            print("acc uds jd: ", accuracy_uds3, "\n")

            average_acc_mce += accuracy_mce
            average_prob_mce += prob_mce
            average_cum_prob_mce += cum_prob_mce
            average_pr_mce += pr_mce
            average_rec_mce += r_mce

            average_acc_uds += accuracy_uds1
            average_prob_uds += prob_uds1
            average_cum_prob_uds += cum_prob_uds1
            average_pr_uds += pr_uds1
            average_rec_uds += r_uds1

            average_acc_uds_jd += accuracy_uds2
            average_prob_uds_jd += prob_uds2
            average_cum_prob_uds_jd += cum_prob_uds2
            average_pr_uds_jd += pr_uds2
            average_rec_uds_jd += r_uds2

            average_acc_uds_x_jd += accuracy_uds3
            average_prob_uds_x_jd += prob_uds3
            average_cum_prob_uds_x_jd += cum_prob_uds3
            average_pr_uds_x_jd += pr_uds3
            average_rec_uds_x_jd += r_uds3

        average_acc = [d / 3 for d in
                       [average_acc_mce, average_acc_uds, average_acc_uds_jd, average_acc_uds_x_jd]]
        av_acc_mce.append(average_acc[0])
        av_acc_uds.append(average_acc[1])
        av_acc_uds_jd.append(average_acc[2])
        av_acc_uds_x_jd.append(average_acc[3])

        average_pr = [d / 3 for d in [average_pr_mce, average_pr_uds, average_pr_uds_jd, average_pr_uds_x_jd]]
        # average_pr = [d / iterations for d in [average_pr_mce]]
        av_precision_mce.append(average_pr[0])
        av_precision_uds.append(average_pr[1])
        av_precision_uds_jd.append(average_pr[2])
        av_precision_uds_x_jd.append(average_pr[3])

        average_r = [d / 3 for d in
                     [average_rec_mce, average_rec_uds, average_rec_uds_jd, average_rec_uds_x_jd]]
        # average_r = [d / iterations for d in [average_rec_mce]]
        av_recall_mce.append(average_r[0])
        av_recall_uds.append(average_r[1])
        av_recall_uds_jd.append(average_r[2])
        av_recall_uds_x_jd.append(average_r[3])

        print("av. acc. mce: ", average_acc[0], "\nuds: ", average_acc[1], "\nuds_jd: ", average_acc[2],
              "\nuds_x_jd: ", average_acc[3])

        # print("av. acc. mce: ", average_acc[0], "\nprecision: ", average_pr[0], "\nrecall: ", average_r[0])

        average_p_mce = [d / 3 for d in [average_prob_mce, average_cum_prob_mce]]
        av_prob_mce.append(average_p_mce[0])
        av_cum_prob_mce.append(average_p_mce[1])

        average_p_uds = [d / 3 for d in [average_prob_uds, average_cum_prob_uds,
                                         average_prob_uds_jd, average_cum_prob_uds_jd,
                                         average_prob_uds_x_jd, average_cum_prob_uds_x_jd]]
        av_prob_uds.append(average_p_uds[0])
        av_cum_prob_uds.append(average_p_uds[1])
        av_prob_uds_jd.append(average_p_uds[2])
        av_cum_prob_uds_jd.append(average_p_uds[3])
        av_prob_uds_x_jd.append(average_p_uds[4])
        av_cum_prob_uds_x_jd.append(average_p_uds[5])

    acc_file.write("nr dims: " + str(d) + "\n")
    acc_file.write("acc mce: " + str(av_acc_mce) + "\npr. mce: " + str(av_precision_mce) +
                   "\nr. mce: " + str(av_recall_mce) + "\n")
    acc_file.write("acc uds: " + str(av_acc_uds) + "\npr. uds: " + str(av_precision_uds) +
                   "\nr. uds: " + str(av_recall_uds) + "\n")
    acc_file.write("acc uds jd: " + str(av_acc_uds_jd) + "\npr. uds jd: " + str(av_precision_uds_jd) +
                   "\nr. uds jd: " + str(av_recall_uds_jd) + "\n")
    acc_file.write("acc uds x jd: " + str(av_acc_uds_x_jd) + "\npr. uds x jd: " + str(av_precision_uds_x_jd) +
                   "\nr. uds x jd: " + str(av_recall_uds_x_jd) + "\n")

    acc_file.close()


def eval_noise(dep_type):
    dir = "/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/datasets/synthetic/"+str(dep_type)+"/"
    noise_levels = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]

    av_acc_mce = []
    av_prob_mce = []
    av_cum_prob_mce = []
    av_precision_mce = []
    av_recall_mce = []
    av_acc_uds = []
    av_prob_uds = []
    av_cum_prob_uds = []
    av_precision_uds = []
    av_recall_uds = []
    av_acc_uds_x_jd = []
    av_prob_uds_x_jd = []
    av_cum_prob_uds_x_jd = []
    av_precision_uds_x_jd = []
    av_recall_uds_x_jd = []

    true_d = [(2, (0, 1)), (5, (3, 4)), (8, (6, 7)), (11, (9, 10)), (14, (12, 13)), (17, (15, 16)), (18,), (19,)]

    print("dep_type: ", dep_type)

    acc_file = open("/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/experiments/subspaceNetwork/"
                    + str(dep_type) + "/noise" + "/accuracy" + ".txt", "w+")
    pr_file = open("/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/experiments/subspaceNetwork/"
                   + str(dep_type) + "/noise" + "/probability" + ".txt", "w+")
    ce_ds_file = open("/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/experiments/subspaceNetwork/"
                      + str(dep_type) + "/noise" + "/ce_ds" + ".txt", "w+")

    for dir_ in os.listdir(dir):
        if os.path.isdir(os.path.join(os.path.abspath(dir), dir_)):
            print(dir_, "\n")
            # average values for this noise level, over 10 datasets

            pr_file.write("\nnoise level ++" + "\n")
            ce_ds_file.write("\nnoise level ++" + "\n")

            average_acc_mce = 0
            average_prob_mce = 0
            average_cum_prob_mce = 0
            average_pr_mce = 0
            average_rec_mce = 0

            average_acc_uds = 0
            average_prob_uds = 0
            average_cum_prob_uds = 0
            average_pr_uds = 0
            average_rec_uds = 0

            average_acc_uds_x_jd = 0
            average_prob_uds_x_jd = 0
            average_cum_prob_uds_x_jd = 0
            average_pr_uds_x_jd = 0
            average_rec_uds_x_jd = 0

            path1 = os.path.join(os.path.abspath(dir), dir_)
            for filename in os.listdir(path1):
                if filename.endswith(".csv"):
                    dataset = os.path.join(path1, filename)
                    #mce
                    results_file = open(path.abspath("../../results/mce_noise.txt"), 'w+')
                    estimation = Estimation.EntropyEstimation(20, dataset, results_file)
                    mce = MceEstimation.MCE(estimation, do_exp_check=False)
                    mce.mce_estimation(True)
                    print("estimated mce: ", mce.decomposition)
                    est_ce = round(mce.joint_ce, 10)
                    est_ds = round(mce.mce_uds_score, 10)
                    adjusted_d = estimation.data.shape[1]
                    accuracy_mce, prob_mce, cum_prob_mce, pr_mce, r_mce = prTest.pr_r(true_d, mce.decomposition, adjusted_d)
                    print("acc mce: ", round(accuracy_mce, 3), " precision: ", pr_mce, " recall: ", r_mce)
                    print(" entropy: ", est_ce, " score: ", est_ds, "\n")

                    #uds
                    uds1 = Uds.UdsVariations(estimation)
                    uds1.uds_pure()
                    uds1_ce = round(uds1.mce_score, 10)
                    u_ds1 = round(uds1.uds_score, 10)
                    accuracy_uds1, prob_uds1, cum_prob_uds1, pr_uds1, r_uds1 = \
                        prTest.pr_r(true_d, uds1.resulting_combination, adjusted_d)
                    print("acc uds: ", accuracy_uds1, " presicion: ", pr_uds1, " recall: ", r_uds1, "\n")

                    #uds++
                    uds3 = Uds.UdsVariations(estimation)
                    uds3.uds_extended_jd()
                    uds3_ce = round(uds3.mce_score, 10)
                    u_ds3 = round(uds3.uds_score, 10)
                    accuracy_uds3, prob_uds3, cum_prob_uds3, pr_uds3, r_uds3 = \
                        prTest.pr_r(true_d, uds3.resulting_combination, adjusted_d)
                    print("acc uds jd: ", accuracy_uds3, " presicion: ", pr_uds3, " recall: ", r_uds3, "\n")

                    average_acc_mce += accuracy_mce
                    average_prob_mce += prob_mce
                    average_cum_prob_mce += cum_prob_mce
                    average_pr_mce += pr_mce
                    average_rec_mce += r_mce
                    average_acc_uds += accuracy_uds1
                    average_prob_uds += prob_uds1
                    average_cum_prob_uds += cum_prob_uds1
                    average_pr_uds += pr_uds1
                    average_rec_uds += r_uds1
                    average_acc_uds_x_jd += accuracy_uds3
                    average_prob_uds_x_jd += prob_uds3
                    average_cum_prob_uds_x_jd += cum_prob_uds3
                    average_pr_uds_x_jd += pr_uds3
                    average_rec_uds_x_jd += r_uds3

                    ce_ds_file.write("\nentropy mce: "+str(est_ce)+", uds: "+str(uds1_ce)+", uds_x_jd: "+str(uds3_ce)+"\n")
                    ce_ds_file.write("\nds mce: " + str(est_ds) + ", uds: " + str(u_ds1) + ", uds_x_jd: " + str(u_ds3) + "\n")

            average_acc = [d/10 for d in [average_acc_mce, average_acc_uds, average_acc_uds_x_jd]]
            av_acc_mce.append(average_acc[0])
            av_acc_uds.append(average_acc[1])
            av_acc_uds_x_jd.append(average_acc[2])

            average_pr = [d/10 for d in [average_pr_mce, average_pr_uds, average_pr_uds_x_jd]]
            av_precision_mce.append(average_pr[0])
            av_precision_uds.append(average_pr[1])
            av_precision_uds_x_jd.append(average_pr[2])

            average_r = [d/10 for d in [average_rec_mce, average_rec_uds, average_rec_uds_x_jd]]
            av_recall_mce.append(average_r[0])
            av_recall_uds.append(average_r[1])
            av_recall_uds_x_jd.append(average_r[2])

            print("av. acc. mce: ", average_acc[0], "\nuds: ", average_acc[1], "\nuds_x_jd: ", average_acc[2])

            average_p_mce = [d/10 for d in [average_prob_mce, average_cum_prob_mce]]
            av_prob_mce.append(average_p_mce[0])
            av_cum_prob_mce.append(average_p_mce[1])

            average_p_uds = [d/10 for d in [average_prob_uds, average_cum_prob_uds,
                                            average_prob_uds_x_jd, average_cum_prob_uds_x_jd]]
            av_prob_uds.append(average_p_uds[0])
            av_cum_prob_uds.append(average_p_uds[1])
            av_prob_uds_x_jd.append(average_p_uds[2])
            av_cum_prob_uds_x_jd.append(average_p_uds[3])

            pr_file.write("av. pr. of the network at random, mce: " + str(average_p_mce[0]) + "\n")
            pr_file.write("av. pr. of the network or a better one at random, mce: " + str(average_p_mce[1]) + "\n")
            pr_file.write("av. pr. of the network at random, uds1,3: " + str(average_p_uds[0]) + ", " + str(average_p_uds[2]) + "\n")
            pr_file.write("av. pr. of the network or a better one at random, uds1,3: " + str(average_p_uds[1]) +
                          ", " + str(average_p_uds[3]) + "\n\n")

    acc_file.write("acc mce: "+str(av_acc_mce)+"\npr. mce: "+str(av_precision_mce)+"\nr. mce: "+str(av_recall_mce)+"\n")
    acc_file.write("acc uds: "+str(av_acc_uds)+"\npr. uds: "+str(av_precision_uds)+"\nr. uds: "+str(av_recall_uds)+"\n")
    acc_file.write("acc uds x jd: "+str(av_acc_uds_x_jd)+"\npr. uds x jd: "+str(av_precision_uds_x_jd)+"\nr. uds x jd: "
                   +str(av_recall_uds_x_jd)+"\n")

    pr_file.close()
    ce_ds_file.close()
    acc_file.close()

    file_name1 = "Acc_Noise" + str(dep_type)
    x_label = "Noise Level"
    y_label1 = "Accuracy"
    basic.plot_reconstruction_acc(noise_levels, [av_acc_mce, av_acc_uds, av_acc_uds_x_jd], x_label, y_label1, file_name1)

    file_name2 = "Precision_Noise" + str(dep_type)
    x_label = "Noise Level"
    y_label2 = "Precision"
    basic.plot_reconstruction_acc(noise_levels, [av_precision_mce, av_precision_uds, av_precision_uds_x_jd], x_label, y_label2,
                                  file_name2)

    file_name3 = "Recall_Noise" + str(dep_type)
    x_label = "Noise Level"
    y_label3 = "Recall"
    basic.plot_reconstruction_acc(noise_levels, [av_recall_mce, av_recall_uds, av_recall_uds_x_jd], x_label, y_label3,
                                  file_name3)


if __name__ == "__main__":
    print("true direction, linear")
    eval_true_dir('linear')
