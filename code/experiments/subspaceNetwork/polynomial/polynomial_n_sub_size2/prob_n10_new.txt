nr dims: 2
nr outliers: 0
av. pr. of the network at random, mce: 0.333333333333
av. pr. of the network or a better one at random, mce: 0.0833333333333
av. pr. of the network at random, uds1,2,3: 0.333333333333, 0.333333333333, 0.333333333333
av. pr. of the network or a better one at random, uds1,2,3: 0.0833333333333, 0.0833333333333, 0.0833333333333

nr dims: 10
nr outliers: 2
av. pr. of the network at random, mce: 0.00881098529045
av. pr. of the network or a better one at random, mce: 0.000490672479574
av. pr. of the network at random, uds1,2,3: 0.231281482114, 0.400678407054, 0.0599228518429
av. pr. of the network or a better one at random, uds1,2,3: 0.272286307847, 0.070018168784, 0.00463423221248

nr dims: 20
nr outliers: 4
av. pr. of the network at random, mce: 5.51344606203e-16
av. pr. of the network or a better one at random, mce: 3.06870049674e-18
av. pr. of the network at random, uds1,2,3: 0.0157652775638, 0.392924922717, 3.85110713641e-06
av. pr. of the network or a better one at random, uds1,2,3: 0.304258056219, 0.0402674511637, 4.98987819646e-08

nr dims: 30
nr outliers: 6
av. pr. of the network at random, mce: 8.28371856897e-25
av. pr. of the network or a better one at random, mce: 2.21003947707e-27
av. pr. of the network at random, uds1,2,3: 0.000422266357002, 0.328175851186, 1.18734069887e-09
av. pr. of the network or a better one at random, uds1,2,3: 0.314850023496, 0.021761158251, 7.42572958623e-12

nr dims: 40
nr outliers: 8
av. pr. of the network at random, mce: 1.03201824382e-30
av. pr. of the network or a better one at random, mce: 1.75112910218e-33
av. pr. of the network at random, uds1,2,3: 6.03048760678e-06, 0.37692360024, 8.13777311526e-12
av. pr. of the network or a better one at random, uds1,2,3: 0.319849437936, 0.0205632478391, 3.2818647552e-14

nr dims: 50
nr outliers: 10
av. pr. of the network at random, mce: 1.41945395276e-51
av. pr. of the network or a better one at random, mce: nan
av. pr. of the network at random, uds1,2,3: 5.34841905914e-08, 0.311128889169, 1.41300411284e-20
av. pr. of the network or a better one at random, uds1,2,3: nan, nan, nan

