nr dims: 3
nr outliers: 0
av. pr. of the network at random, mce: 0.533333333333
av. pr. of the network or a better one at random, mce: 0.161904761905
av. pr. of the network at random, uds1,2,3: 0.2, 0.16, 0.0933333333333
av. pr. of the network or a better one at random, uds1,2,3: 0.0380952380952, 0.04, 0.0152380952381

nr dims: 10
nr outliers: 2
av. pr. of the network at random, mce: 0.00126329115452
av. pr. of the network or a better one at random, mce: 4.04560821558e-05
av. pr. of the network at random, uds1,2,3: 0.401377386949, 0.243175958337, 0.00591231188346
av. pr. of the network or a better one at random, uds1,2,3: 0.0977333458783, 0.0239512641051, 0.000249011070321

nr dims: 20
nr outliers: 4
av. pr. of the network at random, mce: 6.05338662414e-07
av. pr. of the network or a better one at random, mce: 5.85604830281e-09
av. pr. of the network at random, uds1,2,3: 0.257727327739, 0.247504623677, 4.42653912995e-07
av. pr. of the network or a better one at random, uds1,2,3: 0.112784633353, 0.0146872626885, 5.14721491929e-09

nr dims: 30
nr outliers: 6
av. pr. of the network at random, mce: 8.67759426214e-17
av. pr. of the network or a better one at random, mce: 2.95743809092e-19
av. pr. of the network at random, uds1,2,3: 0.00135996667241, 0.268557262043, 2.07806704977e-08
av. pr. of the network or a better one at random, uds1,2,3: 0.250295612492, 0.0186851297344, 1.41213238355e-10

nr dims: 40
nr outliers: 8
av. pr. of the network at random, mce: 4.63663459561e-25
av. pr. of the network or a better one at random, mce: 8.68184825435e-28
av. pr. of the network at random, uds1,2,3: 0.000239622973398, 0.278922613183, 9.03062647812e-14
av. pr. of the network or a better one at random, uds1,2,3: 0.225000195707, 0.0130105452204, 3.30195685006e-16

nr dims: 50
nr outliers: 10
av. pr. of the network at random, mce: 1.10108299503e-31
av. pr. of the network or a better one at random, mce: nan
av. pr. of the network at random, uds1,2,3: 3.69233944071e-05, 0.247447258025, 3.9332936287e-19
av. pr. of the network or a better one at random, uds1,2,3: nan, nan, nan

