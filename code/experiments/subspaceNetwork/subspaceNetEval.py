import numpy as np
import pandas as pd
from os import path
from DataGeneration import generator
from experiments import Estimation
from main import MceEstimation, Uds

#['linear', 'linear_ratio', 'quadratic', 'polynomial_1', 'polynomial_n', 'polynomial_ratio', 'log_linear', 'log_pol', 'log_linear_ratio', 'log_pol_ratio']


def estimate_subspaces(rows, columns, nr_subspaces, dep_types_array, nr_ouliers, noise):
    network = generator.Dataset()
    network.generate_subspaces(rows, columns, nr_subspaces, dep_types_array, nr_ouliers, noise)
    print("dependencies: ", network.dependencies, "\n")
    dataset = path.abspath("subspace_network.csv")
    results_file = open(path.abspath("../../results/test.txt"), 'w')
    estimation = Estimation.EntropyEstimation(20, dataset, results_file)
    mce = MceEstimation.MCE(estimation, do_exp_check=False)
    mce.mce_estimation()
    print("\nmce")
    print(mce.decomposition, "\n")


def f():
    rows = 100000
    columns = 10
    nr_subspaces = 3
    nr_outliers = 1
    dep_types = ['polynomial_n']*nr_subspaces
    noise_level = 1
    print("size: ", rows, "x", columns, "\nnoise: ", noise_level, "\ndependency: ", dep_types, "\n")
    estimate_subspaces(rows, columns, nr_subspaces, dep_types, nr_outliers, noise_level)

if __name__ == "__main__":
    f()


    """
    rows = 10000
    columns = 6
    noise = 0
    dep_type1 = 'linear'
    dep_type2 = 'quadratic'
    dep_type3 = 'polynomial'
    dep_type4 = 'log'
    dep_type5 = 'exp'
    dep_type6 = 'cos'
    print("estimation: no add univariate")
    print("size: ", rows, "x", columns, "\nnoise: ", noise, "\ndependency: ", dep_type6, "\n")
    estimate(rows, columns, noise, dep_type6)"""