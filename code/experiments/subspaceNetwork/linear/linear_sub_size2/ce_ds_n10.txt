
nr dims: 2
nr outliers: 0
i: 1true ce: 4.35531280039e+46, mce: 4.35487733446e+47, uds: 4.35487733446e+47
true score: 1.0, mce: 0, uds: 0.0

nr dims: 2
nr outliers: 0
i: 2true ce: 16.4911540378, mce: 182.800098418, uds: 182.800098418
true score: 1.0, mce: 0.5990241551, uds: 0.0363006255

nr dims: 2
nr outliers: 0
i: 3true ce: 9.0939270085, mce: 58.02424461, uds: 132.881627587
true score: 1.0, mce: 0.6214229502, uds: 0.0660292312

nr dims: 2
nr outliers: 0
i: 4true ce: 21.3266191037, mce: 95.5887525022, uds: 95.5887525022
true score: 1.0, mce: 0.6247093397, uds: 0.0292926132

nr dims: 2
nr outliers: 0
i: 5true ce: 22.6415344853, mce: 42.0884223339, uds: 42.0884223339
true score: 1.0, mce: 0.4378419544, uds: 0.0193388971

nr dims: 2
nr outliers: 0
i: 6true ce: 62.7351852246, mce: 273.03543912, uds: 273.03543912
true score: 1.0, mce: 0.6651346931, uds: 0.0106022945

nr dims: 2
nr outliers: 0
i: 7true ce: 30.949353976, mce: 84.3956717647, uds: 217.920388573
true score: 1.0, mce: 0.741736169, uds: 0.0208546116

nr dims: 2
nr outliers: 0
i: 8true ce: 7.9243848795, mce: 49.6724688689, uds: 49.6724688689
true score: 1.0, mce: 0.521386473, uds: 0.0655788116

nr dims: 2
nr outliers: 0
i: 9true ce: 65.7240707399, mce: 78.8068440997, uds: 78.8068440997
true score: 1.0, mce: 0, uds: 0.0

nr dims: 2
nr outliers: 0
i: 10true ce: 76.98183409, mce: 643.325836898, uds: 643.325836898
true score: 1.0, mce: 0.6841764711, uds: 0.0088476139



nr dims: 10
nr outliers: 2
i: 1true ce: 282.165146955, mce: 813.13380484, uds: 931.654342215
true score: 1.0, mce: 0.6336666844, uds: 0.0

nr dims: 10
nr outliers: 2
i: 2true ce: 3.05681086914e+45, mce: 2.79334586554e+46, uds: 5.76764560262e+46
true score: 1.0, mce: 0.5508698896, uds: 0.0

nr dims: 10
nr outliers: 2
i: 3true ce: 589.023622809, mce: 3342.38516898, uds: 3944.23750984
true score: 1.0, mce: 0.8124081098, uds: 0.0002565675

nr dims: 10
nr outliers: 2
i: 4true ce: 426.045007242, mce: 1476.35638907, uds: 2141.92139837
true score: 1.0, mce: 0.6724615735, uds: 0.0

nr dims: 10
nr outliers: 2
i: 5true ce: 405.653438441, mce: 2273.00254673, uds: 2505.95764888
true score: 1.0, mce: 0.7885200048, uds: 0.0

nr dims: 10
nr outliers: 2
i: 6true ce: 302.12981599, mce: 1311.02107941, uds: 1424.43698897
true score: 1.0, mce: 0.712649281, uds: 0.0

nr dims: 10
nr outliers: 2
i: 7true ce: 4.54496192291e+56, mce: 2.72670451948e+57, uds: 2.72670451948e+57
true score: 1.0, mce: 0.7126603884, uds: 0.0

nr dims: 10
nr outliers: 2
i: 8true ce: 4.51153536224e+32, mce: 3.15775899395e+33, uds: 3.15775899395e+33
true score: 1.0, mce: 0.727130946, uds: 0.0

nr dims: 10
nr outliers: 2
i: 9true ce: 2.97660475695e+38, mce: 2.07533817811e+39, uds: 2.07533817811e+39
true score: 1.0, mce: 0.5098424794, uds: 0.0

nr dims: 10
nr outliers: 2
i: 10true ce: 243.435581111, mce: 492.66412809, uds: 803.254542863
true score: 1.0, mce: 0.6498372644, uds: 0.0019764126



nr dims: 20
nr outliers: 4
i: 1true ce: 7.28103175449e+61, mce: 1.60263822114e+62, uds: 6.69222962305e+62
true score: 1.0, mce: 0.8533570393, uds: 0.0

nr dims: 20
nr outliers: 4
i: 2true ce: 1.5981678853e+45, mce: 8.44485549963e+45, uds: 4.20351782983e+46
true score: 1.0, mce: 0.8306793638, uds: 0.0

nr dims: 20
nr outliers: 4
i: 3true ce: 5.83804980872e+51, mce: 2.88697892173e+52, uds: 3.04742477919e+53
true score: 1.0, mce: 0.9229443398, uds: 0.0

nr dims: 20
nr outliers: 4
i: 4true ce: 3.34065982843e+14, mce: 6.4508274297e+15, uds: 1.38275125325e+16
true score: 1.0, mce: 0.5466851496, uds: 0.0

nr dims: 20
nr outliers: 4
i: 5true ce: 1.09333394347e+17, mce: 2.9447087889e+17, uds: 3.11219278847e+17
true score: 1.0, mce: 0.1532016815, uds: 0.0

nr dims: 20
nr outliers: 4
i: 6true ce: 5.3180536441e+42, mce: 2.31273441473e+44, uds: 4.01502156838e+44
true score: 1.0, mce: 0.4296701598, uds: 0.0

nr dims: 20
nr outliers: 4
i: 7true ce: 5.23099434758e+22, mce: 4.70742420225e+23, uds: 4.70742420225e+23
true score: 1.0, mce: 0.6559007015, uds: 0.0

nr dims: 20
nr outliers: 4
i: 8true ce: 702.784596175, mce: 2384.80906717, uds: 2826.4685147
true score: 1.0, mce: 0.7053162117, uds: 0.0

nr dims: 20
nr outliers: 4
i: 9true ce: 3.64722097304e+37, mce: 2.18811388304e+38, uds: 2.18811388304e+38
true score: 1.0, mce: 0.7535570987, uds: 0.0

nr dims: 20
nr outliers: 4
i: 10true ce: 5.45354226994e+32, mce: 5.45299700374e+33, uds: 5.45299700374e+33
true score: 1.0, mce: 0.8114295179, uds: 0.0



nr dims: 30
nr outliers: 6
i: 1true ce: 2.41502941818e+25, mce: 4.42153560444e+26, uds: 1.60329912041e+27
true score: 1.0, mce: 0.7352972472, uds: 0.0

nr dims: 30
nr outliers: 6
i: 2true ce: 2.13457565329e+17, mce: 1.92092624539e+18, uds: 1.92092624539e+18
true score: 1.0, mce: 0.7554340446, uds: 0.0

nr dims: 30
nr outliers: 6
i: 3true ce: 1.0786471062e+32, mce: 7.54977523108e+32, uds: 7.54977523108e+32
true score: 1.0, mce: 0.7232080882, uds: 0.0

!!!
nr dims: 30
nr outliers: 6
i: 4true ce: 5.07085853218e+21, mce: 5.24247381951e+21, uds: 1.40865380299e+23
true score: 1.0, mce: 0.9987324847, uds: 0.0
!!!

nr dims: 30
nr outliers: 6
i: 5true ce: 1.02790450319e+47, mce: 5.13900869199e+47, uds: 5.13900869199e+47
true score: 1.0, mce: 0.7372205603, uds: 0.0

nr dims: 30
nr outliers: 6
i: 6true ce: 1.70846260115e+36, mce: 2.12028280582e+36, uds: 6.1917852251e+37
true score: 1.0, mce: 0.9931573821, uds: 0.0

nr dims: 30
nr outliers: 6
i: 7true ce: 2.38332296158e+40, mce: 2.44417472774e+41, uds: 4.53969835231e+41
true score: 1.0, mce: 0.4902900447, uds: 0.0

nr dims: 30
nr outliers: 6
i: 8true ce: 1.27212340726e+29, mce: 6.35998110573e+29, uds: 6.35998110573e+29
true score: 1.0, mce: 0.7188746477, uds: 0.0

nr dims: 30
nr outliers: 6
i: 9true ce: 3.46886117022e+26, mce: 1.48053884709e+27, uds: 2.96332899691e+27
true score: 1.0, mce: 0.5832961378, uds: 0.0

!!!
nr dims: 30
nr outliers: 6
i: 10true ce: 1.20333756994e+25, mce: 1.20370989697e+25, uds: 4.52512740044e+26
true score: 1.0, mce: 0.9999888159, uds: 0.0
!!!


nr dims: 40
nr outliers: 8
i: 1true ce: 5.97303544686e+51, mce: 2.32293006376e+52, uds: 2.56484967406e+52
true score: 1.0, mce: 0.7749187359, uds: 0.0

nr dims: 40
nr outliers: 8
i: 2true ce: 1.35868461162e+49, mce: 1.0868390363e+50, uds: 1.0868390363e+50
true score: 1.0, mce: 0.8396862954, uds: 0.0

nr dims: 40
nr outliers: 8
i: 3true ce: 1.10841306106e+31, mce: 9.14989812378e+31, uds: 6.3651425077e+32
true score: 1.0, mce: 0.8714231851, uds: 0.0

nr dims: 40
nr outliers: 8
i: 4true ce: 6.76317952678e+45, mce: 4.0575019896e+46, uds: 4.0575019896e+46
true score: 1.0, mce: 0.5321122084, uds: 0.0

nr dims: 40
nr outliers: 8
i: 5true ce: 1.61465344634e+31, mce: 7.52418041156e+31, uds: 7.9354181067e+31
true score: 1.0, mce: 0.0849049708, uds: 0.0

nr dims: 40
nr outliers: 8
i: 6true ce: 1.17610969689e+40, mce: 7.69899599903e+40, uds: 1.11476404869e+41
true score: 1.0, mce: 0.9946964022, uds: 0.0

nr dims: 40
nr outliers: 8
i: 7true ce: 1.09965842449e+34, mce: 3.14183633302e+35, uds: 5.44935497327e+35
true score: 1.0, mce: 0.4321681449, uds: 0.0

nr dims: 40
nr outliers: 8
i: 8true ce: 1.13379369678e+35, mce: 1.13368131169e+35, uds: 1.13368131169e+35
true score: 1.0, mce: 0.7745614579, uds: 0.0


nr dims: 40
nr outliers: 8
i: 9true ce: 1.89417308707e+48, mce: 2.89189239021e+49, uds: 1.25214373578e+50
true score: 1.0, mce: 0.7808564361, uds: 0.0

!!!
nr dims: 40
nr outliers: 8
i: 10true ce: 5.46106628902e+57, mce: 5.705952282e+57, uds: 1.02660507086e+59
true score: 1.0, mce: 0.9974749791, uds: 0.0
!!!