import matplotlib.pyplot as plt
import numpy as np


class Comparison:

    #def __init__(self, scores, results_file):
    def __init__(self, scores, names):
        self.scores = scores
        self.names = names
        self.number_of_variations = len(self.scores)
        #self.results_file = results_file

    def plot(self):

        cmap = plt.get_cmap('viridis')
        colors = cmap(np.linspace(0, 1, self.number_of_variations))
        #for score in range(0, self.number_of_variations):

        for i, (score, name, color) in enumerate(zip(self.scores, self.names, colors), 1):
            plt.plot(score, label=name, c=color)

        plt.legend()
        plt.savefig("scores_diagnosis.png")
        plt.close()

