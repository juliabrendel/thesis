import networkx as nx
from networkx.algorithms import dag
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


def create_adjacency_matrix(nr_dimensions, combinations):
    adj_matrix = np.full(shape=(nr_dimensions, nr_dimensions), fill_value=0)
    for term in combinations:
        if len(term) == 1:
            continue
        elif isinstance(term[1], tuple):
            for dim_id in term[1]:
                adj_matrix[dim_id][term[0]] = 1
        else:
            adj_matrix[term[1]][term[0]] = 1
            """
            for i in range(len(term)):
                for j in range(i+1, len(term)):
                    adj_matrix[term[i]][term[j]] = 1"""
    return adj_matrix


def create_graph(nr_dimensions, combinations, directed=True):
    adj_matrix = create_adjacency_matrix(nr_dimensions, combinations)
    if directed:
        graph = nx.from_numpy_matrix(adj_matrix, create_using=nx.DiGraph())
    else:
        graph = nx.from_numpy_matrix(adj_matrix)
    return graph


def plot_graph(nr_dimensions, combinations1, combinations2, file_name):
    # adj_matrix1 = create_adjacency_matrix(number_of_dimensions, combinations1)
    # G1 = nx.from_numpy_matrix(adj_matrix1, create_using=nx.DiGraph())
    G1 = create_graph(nr_dimensions, combinations1)
    plt.subplot(121)
    nx.draw_circular(G1, with_labels=True)
    # adj_matrix2 = create_adjacency_matrix(number_of_dimensions, combinations2)
    # G2 = nx.from_numpy_matrix(adj_matrix2, create_using=nx.DiGraph())
    G2 = create_graph(nr_dimensions, combinations2)
    plt.subplot(122)
    nx.draw(G2, with_labels=True)
    plt.show(block=False)
    plt.savefig(file_name + ".png", format="PNG")
    plt.close()


def get_connected_components(nr_dimensions, combinations):
    graph = create_graph(nr_dimensions, combinations, directed=False)
    return list(nx.connected_components(graph))


def has_cycle(nr_dimensions, combinations):
    graph = create_graph(nr_dimensions, combinations)
    return dag.has_cycle(graph)


# used for estimated dependencies
def reformat_dependencies(dependencies, noisy_case=False):

    if noisy_case: reformed = dependencies
    else:
        reformed = []
        for d in dependencies:
            if len(d) == 1: reformed.append(list(d))
            elif isinstance(d[1], tuple):
                new_d = [d[0]]
                new_d.extend([d_ for d_ in d[1]])
                reformed.append(sorted(new_d))
            else: reformed.append(sorted(list(d)))

    pruned = []
    for i in reformed:
        unique = True
        for j in reformed:
            if i == j: continue
            i_ = None
            j_ = None
            if len(j) > 1:
                if isinstance(j[1], tuple):
                    j_ = (j[0],) + j[1]
            if len(i) > 1:
                if isinstance(i[1], tuple):
                    i_ = (i[0],) + i[1]

            if i_ is not None and j_ is not None:
                inter = np.intersect1d(i_, j_)
                diff = np.setdiff1d(j_, i_)
            elif i_ is not None:
                inter = np.intersect1d(i_, j)
                diff = np.setdiff1d(j, i_)
            elif j_ is not None:
                inter = np.intersect1d(i, j_)
                diff = np.setdiff1d(j_, i)
            else:
                inter = np.intersect1d(i, j)
                diff = np.setdiff1d(j, i)

            flag = False
            if i_ is None and list(inter) == list(i):
                flag = True
            elif i_ is not None and sorted(list(inter)) == sorted(list(i_)):
                flag = True
            if flag:
                unique = False
                break
            elif len(inter) != 0:
                if len(diff) == 0: continue
                if len(i) > 1 and noisy_case: continue
                unique = False
                if noisy_case: break
                new = i.copy()
                new.extend(list(diff))
                new = sorted(new)
                if new not in pruned:
                    pruned.append(new)
                break
        if unique:
            pruned.append(i)
    return sorted(pruned)


def break_down(combination):
    modified_combination = []
    for c in combination:
        if len(c) == 1:
            modified_combination.append(c)
            continue
        if isinstance(c[1], tuple):
            for dim in c[1]:
                modified_combination.append((c[0], dim))
        else:
            modified_combination.append(c)
    return modified_combination


def plot_reconstruction_acc(x, y_arr, x_label, y_label, file_name, title=None, legend=True):
    plt.style.use('seaborn-white')
    plt.figure(figsize=(13, 10))
    ax = plt.axes()
    plt.gca().yaxis.grid(True, linestyle='--', linewidth=1, alpha=0.5, color='black')
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    ax.tick_params(labelsize=40)
    # plt.xlim(0, max)
    plt.ylim(0, 1)

    mce = y_arr[0]
    uds = y_arr[1]
    uds_jd = y_arr[2]
    uds_x_jd = y_arr[3]

    plt.plot(x, mce, linestyle='-', marker='o', markersize=15, label="Grip", linewidth=3)
    plt.plot(x, uds, linestyle='-', marker='v', markersize=15, label="UDS", linewidth=3)
    plt.plot(x, uds_jd, linestyle='-', marker="*", markersize=15,  label="UDS+", linewidth=3)
    plt.plot(x, uds_x_jd, linestyle='-', marker="^", markersize=15, label="UDS++", linewidth=3)
    plt.xlabel(x_label, fontsize=40)
    plt.ylabel(y_label, fontsize=40)
    if legend:
        plt.legend(loc='best', numpoints=1, fancybox=True, fontsize=30)
    if title is not None:
        plt.title(title, fontsize=22)
    plt.savefig(file_name + ".png")


def plot_relative_scores(x, y_arr, x_label, y_label, file_name, title=None, legend=True):
    plt.style.use('seaborn-white')
    plt.figure(figsize=(18.5, 14))
    ax = plt.axes()
    plt.gca().yaxis.grid(True, linestyle='--', linewidth=1, alpha=0.5, color='black')
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    # ax.spines["bottom"].set_visible(False)
    # ax.spines["left"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()

    plt.yticks(range(0, 451, 50), [str(x) + "%" for x in range(0, 451, 50)], fontsize=40)
    ax.tick_params(labelsize=40)
    plt.ylim(0, 451)

    mce = y_arr[0]
    uds = y_arr[1]
    uds_jd = y_arr[2]
    uds_x_jd = y_arr[3]

    plt.plot(x, mce, linestyle='-', marker='o', markersize=15, label="Grip", linewidth=3)
    plt.plot(x, uds, linestyle='-', marker='v', markersize=15, label="UDS", linewidth=3)
    plt.plot(x, uds_jd, linestyle='-', marker="*", markersize=15, label="UDS+", linewidth=3)
    plt.plot(x, uds_x_jd, linestyle='-', marker="^", markersize=15, label="UDS++", linewidth=3)

    plt.xlabel(x_label, fontsize=40)
    plt.ylabel(y_label, fontsize=40)
    if legend:
        plt.legend(loc='best', numpoints=1, fancybox=True, fontsize=30)
    if title is not None:
        plt.title(title, fontsize=22)
    plt.savefig(file_name + ".png")


def plot_runtime(x, y_arr, title=None):
    plt.style.use('seaborn-white')
    plt.figure(figsize=(16, 12))
    ax = plt.axes()
    # plt.gca().yaxis.grid(True, linestyle='--', linewidth=1)
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    ax.tick_params(labelsize=40)
    # plt.xlim(0, max)
    # plt.ylim(0, 1)

    mce = y_arr[0]
    uds_x_jd = y_arr[1]
    plt.plot(x, mce, linestyle='-', marker='.', label="Grip", markersize=25, linewidth=3)
    plt.plot(x, uds_x_jd, linestyle='-', marker="+", label="UDS++", markersize=25, linewidth=3)
    plt.xlabel("Number of dimensions", fontsize=40)
    plt.ylabel("Runtime (sec.)", fontsize=40)
    plt.legend(loc='best', numpoints=1, fancybox=True, fontsize=30)
    if title is not None:
        plt.title(title, fontsize=22)
    plt.savefig("runtime_mce_uds" + ".png")


def plot_reconstruction_acc_jd(x, y_arr, x_label, y_label, file_name, title=None):
    plt.style.use('seaborn-white')
    plt.figure(figsize=(10, 7.5))
    ax = plt.axes()
    plt.gca().yaxis.grid(True, linestyle='--', linewidth=1)
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    plt.xlim(0, max(x) + 1)
    plt.ylim(0, 1)

    mce = y_arr[0]
    mce_nojd = y_arr[1]

    plt.plot(x, mce, linestyle='-', marker='o', label="MCE")
    plt.plot(x, mce_nojd, linestyle='-', marker='v', label="MCE-jd")
    plt.xlabel(x_label, fontsize=16)
    plt.ylabel(y_label, fontsize=16)
    plt.legend(loc='best', numpoints=1, fancybox=True)
    if title is not None:
        plt.title(title, fontsize=22)
    plt.savefig(file_name + ".png")


def barplot_reconstruction_acc(x, y_arr, x_label, y_label, file_name, title=None):
    plt.style.use('seaborn-white')
    plt.figure(figsize=(10, 7.5))
    ax = plt.axes()
    # plt.gca().yaxis.grid(True, linestyle='--', linewidth=1)
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    plt.xlim(0, max(x) + 1)
    plt.ylim(0, 1)

    x_ = [0]
    x_.extend(x)
    n_groups = len(x)
    index = np.arange(n_groups)
    bar_width = 0.3
    opacity = 1

    plt.bar([i - (2/3)*bar_width for i in x], y_arr[0], width=bar_width, align='center', label='MCE') #,
                     #alpha=opacity,
                     #color='b',
                     #label='MCE')

    plt.bar([i + (2/3)*bar_width for i in x], y_arr[1], width=bar_width, align='center', label='UDS') #,
                     #alpha=opacity,
                     #color='g',
                     #label='UDS')

    plt.xlabel(x_label, fontsize=16)
    plt.ylabel(y_label, fontsize=16)
    plt.xticks(x_)
    plt.legend(loc='best', numpoints=1, fancybox=True)
    if title is not None:
        plt.title(title, fontsize=22)
    plt.savefig("Bar" + file_name + ".png")

if __name__ == "__main__":
    #nr_dims = [10, 20, 30, 40, 50]
    #mce = [84.77632808685303, 271.30586194992065, 591.0999369621277, 1000.9484288692474, 1368.6115610599518]
    #uds = [132.04197597503662, 531.63481092453, 1227.19043302536, 2227.1640751361847, 3485.512678861618]
    #plot_runtime(nr_dims, [mce, uds])
    sizes = [2, 4, 6, 8, 10]

    # mce_ce = [-0.87, -6.83, 7.54, 5.13, 6.42]
    # uds_ce = [15.91, 17.8, 20.96, 21.31, 20.87]
    # uds1_ce = [16.75, 18.56, 23.29, 21.61, 18.68]
    #     uds2_ce = [10.23, 9.18, 14.42, 12.49, 10.77]

    """
    mce_ds = [17.41, 11.41, 19.05, 14.97, 14.60]
    uds_ds = [-99.79, -99.87, -99.93, -99.95, -99.87]
    uds1_ds = [-95.24, -96.68, -98.37, -99.19, -99.08]
    uds2_ds = [-81.37, -80.46, -83.58, -84.88, -85.09]"""
    """
    mce_ce = [43.72, 120.73, 218.59, 207.37, 116.60]
    uds_ce = [112.90, 271.18, 422.43, 352.91, 261.11]
    uds1_ce = [112.90, 271.18, 422.44, 353.92, 261.12]
    uds2_ce = [112.65, 271.02, 422.44, 352.92, 260.96]"""

    # subspace experiment
    """
    mce_ce = [16.13, 11.45, 8.75, 5.9, -11.47]
    uds_ce = [33.01, 21.96, 14.66, 11.88, -23.43]
    uds1_ce = [80.65, 23.45, 14.40, 17.1, -14.28]
    uds2_ce = [30.94, 14.59, 8.25, 5.69, -24.39]"""
    """

    mce_ds = [-6.01, 9.07, 12.75, 12.54, 24.13]
    uds_ds = [-99.96, -99.58, -99.36, -98.72, -95.06]
    uds1_ds = [-99.05, -96.35, -91.99, -80.94, -78.18]
    uds2_ds = [-86.65, -82.33, -70.23, -64.46, -72.92]"""

    # polynomial
    mce_ce = [133.07, 94.78, 154.14, 119.16, 161.41]
    uds_ce = [200.4, 276.2, 216.02, 356.32, 406.70]
    uds1_ce = [201.39, 267.21, 216.03, 356.32, 335.79]
    uds2_ce = [175.83, 276.2, 216.02, 356.32, 406.7]

    # y_array = [mce_lin_ce, mce_pol_ce, mce_loglin_ce, mce_logpol_ce, uds_lin_ce, uds_pol_ce, uds_loglin_ce, uds_logpol_ce]

    plot_relative_scores(sizes, [mce_ce, uds_ce, uds1_ce, uds2_ce], "Subspace dimensionality",
                     "Error of the cumulative entropy estimation", "sub_ce_pol")

  #  plot_relative_scores(sizes, [mce_ds, uds_ds, uds1_ds, uds2_ds], "Subspace dimensionality",
  #                       "Error of the dependency score estimation", "sub_ds")
    # sizes = [2, 3, 4, 5, 6, 7, 8, 9, 10]
    # sizes = [2, 3, 4, 5, 6, 7, 8, 9, 10]
    # noises = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
    # accuracy = []
    # xlabel = "Subspace dimensionality"
    # ylabel_acc = "Accuracy"
    # file_name_acc = "sublin_acc_new"
    # plot_reconstruction_acc(sizes, accuracy, xlabel, ylabel_acc, file_name_acc, legend=False)
    """
    precision = []
    ylabel_pr = "Precision"
    file_name_pr = "sublpol_pr_new"
    plot_reconstruction_acc(sizes, precision, xlabel, ylabel_pr, file_name_pr, legend=False)
    recall = []
    ylabel_r = "Recall"
    file_name_r = "sublpol_r_new"
    plot_reconstruction_acc(sizes, recall, xlabel, ylabel_r, file_name_r, legend=False) """
    # noises = ['10%', '20%', '30%', '40%', '50%', '60%', '70%', '80%', '90%', '100%']
    """
    recalls = []
    xlabel = "Noise level"
    ylabel_precision = "Precision"
    file_name_precision = "noise_precision_logpol"
    precisions = []
    ylabel_recall = "Recall"
    file_name_recall = "noise_recall_logpol"
    # accuracy = []
    ylabel_acc = "Accuracy"
    file_name_acc = "noise_acc_logpol"
    plot_reconstruction_acc(noises, precisions, xlabel, ylabel_precision, file_name_precision)
    plot_reconstruction_acc(noises, recalls, xlabel, ylabel_recall, file_name_recall)
    # plot_reconstruction_acc(noises, accuracy, xlabel, ylabel_acc, file_name_acc) """

