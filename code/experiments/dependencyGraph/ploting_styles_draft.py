import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

plt.style.use('seaborn-white')
#sns.set_style("whitegrid")
#sns.set_style("ticks", {"xtick.major.size": 8, "ytick.major.size": 1})
#fig = plt.figure()
plt.figure(figsize=(10, 7.5))
ax = plt.axes()
plt.gca().yaxis.grid(True, linestyle='--', linewidth=0.5)
ax.spines["top"].set_visible(False)
#ax.spines["bottom"].set_visible(False)
ax.spines["right"].set_visible(False)
#ax.spines["left"].set_visible(False)
ax.get_xaxis().tick_bottom()
ax.get_yaxis().tick_left()


#plt.show()
x = np.linspace(0, 10, 1000)
#ax.plot(x, np.sin(x))
plt.plot(x, np.sin(x))
plt.xlim(0, 10)
plt.ylim(-1, 1)
plt.show()
