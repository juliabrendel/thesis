import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from os import path
from experiments import PrecisionRecallT as prTest
from DataGeneration import generator
from experiments.dependencyGraph import basic_functions as basic

"""
def plot_graph(nr_dimensions, combinations1, combinations2, file_name):
    #adj_matrix1 = create_adjacency_matrix(number_of_dimensions, combinations1)
    #G1 = nx.from_numpy_matrix(adj_matrix1, create_using=nx.DiGraph())
    G1 = basic.create_graph(nr_dimensions, combinations1)
    plt.subplot(121)
    nx.draw_circular(G1, with_labels=True)
    #adj_matrix2 = create_adjacency_matrix(number_of_dimensions, combinations2)
    #G2 = nx.from_numpy_matrix(adj_matrix2, create_using=nx.DiGraph())
    G2 = basic.create_graph(nr_dimensions, combinations2)
    plt.subplot(122)
    nx.draw(G2, with_labels=True)
    plt.show(block=False)
    plt.savefig(file_name + ".png", format="PNG")
    plt.close()"""


def reconstruction(rows, columns, noise_level, id):

    network = generator.Dataset()
    network.generate_dataset(rows, columns, noise_level)
    true_dependecies = network.dependencies
    #true_dependecies = generator.generate_dependency_network(rows, columns, noise_level=noise_level)
    dataset = path.abspath("dependency_network_noise.csv")
    #dataset = path.abspath("normal_with_correlations.csv")
    results_file = open(path.abspath("../../results/test.txt"), 'w')

    from experiments import Estimation
    estimation = Estimation.EntropyEstimation(20, dataset, results_file)
    number_of_dimensions = estimation.data.shape[1]

    #basic.plot_graph(number_of_dimensions, true_dependecies, "true_dep_network" + str(id))

    from main import MceEstimation, Uds
    mce = MceEstimation.MCE(estimation, do_exp_check=False)
    mce.mce_estimation()
    #reformed_mce = reformat_dependencies(mce.used_dimensions)
    #basic.plot_graph(number_of_dimensions, mce.used_dimensions, "mce_dep_network" + str(i))
    accuracy_mce, prob_mce1, prob_mce2 = prTest.pr_r(true_dependecies, mce.decomposition, number_of_dimensions)
    #accuracy = prTest.compare_connected_components(true_dependecies, reformed_mce)

    uds1 = Uds.UdsVariations(estimation)
    uds1.uds_pure()
    accuracy_uds, prob_uds1, prob_uds2 = prTest.pr_r(true_dependecies, uds1.resulting_combination, number_of_dimensions)
    #print("comb uds: ", uds1.resulting_combination, "\n")

    uds2 = Uds.UdsVariations(estimation)
    uds2.uds_jd()
    #reformed_uds_jd = reformat_dependencies(uds2.resulting_combination, noisy_case=True)
    accuracy_uds_jd, prob_uds_jd1, prob_uds_jd2 = prTest.pr_r(true_dependecies, uds2.resulting_combination, number_of_dimensions)
    #print("comb uds jd: ", uds2.resulting_combination, "\n")

    #basic.plot_graph(number_of_dimensions, uds.resulting_combination, "uds_dep_network" + str(i))
    accuracy = [accuracy_mce, accuracy_uds, accuracy_uds_jd]
    prob1 = [prob_mce1, prob_uds1, prob_uds_jd1]
    prob2 = [prob_mce2, prob_uds2, prob_uds_jd2]
    return accuracy, prob1, prob2


def plotReconstructionResults(rows, columns, iterations):
    #noise = [1, 5, 10, 15, 20, 30, 40, 50]
    noise = [1]
    accuracies_mce = []
    accuracies_uds = []
    accuracies_uds_jd = []

    for n in noise:
        av_acc_mce = 0
        av_prob_mce = 0
        av_sum_prob_mce = 0
        av_acc_uds = 0
        av_prob_uds = 0
        av_sum_prob_uds = 0
        av_acc_uds_jd = 0
        av_prob_uds_jd = 0
        av_sum_prob_uds_jd = 0

        av_accuracy = 0
        print("\nnoise level: ", n, "\n")
        for i in range(1, iterations+1):
            accuracy, prob1, prob2 = reconstruction(rows, columns, n, i)
            #accuracy = test_reconstruction(rows, columns, i)
            av_acc_mce += accuracy[0]
            av_prob_mce += prob1[0]
            av_sum_prob_mce += prob2[0]
            av_acc_uds += accuracy[1]
            av_prob_uds += prob1[1]
            av_sum_prob_uds += prob2[1]
            av_acc_uds_jd += accuracy[2]
            av_prob_uds_jd += prob1[2]
            av_sum_prob_uds_jd += prob2[2]

        av_acc_mce /= iterations
        av_prob_mce /= iterations
        av_sum_prob_mce /= iterations
        av_acc_uds /= iterations
        av_prob_uds /= iterations
        av_sum_prob_uds /= iterations
        av_acc_uds_jd /= iterations
        av_prob_uds_jd /= iterations
        av_sum_prob_uds_jd /= iterations
        accuracies_mce.append(av_acc_mce)
        accuracies_uds.append(av_acc_uds)
        accuracies_uds_jd.append(av_acc_uds_jd)

    plt.style.use('seaborn-white')
    plt.figure(figsize=(10, 7.5))
    ax = plt.axes()
    plt.gca().yaxis.grid(True, linestyle='--', linewidth=1)
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    plt.xlim(min(noise), max(noise) + 1)
    plt.ylim(0, 1)

    plt.plot(noise, accuracies_mce, linestyle='-', marker='o', label="MCE")
    plt.plot(noise, accuracies_uds, linestyle='-', marker='v', label="UDS")
    plt.plot(noise, accuracies_uds_jd, linestyle='-', marker='8', label="UDS++")
    plt.xlabel("Noise Level", fontsize=16)
    plt.ylabel("Average Accuracy", fontsize=16)
    plt.legend(loc='best', numpoints=1, fancybox=True)
    plt.savefig("ReconstructionAccuracy.png")
    plt.show()


def mceScoreComparison(rows, columns, noise_level):

    network = generator.Dataset()
    network.generate_dataset(rows, columns, noise_level)
    dataset = path.abspath("dependency_network_noise.csv")
    results_file = open(path.abspath("../../results/test.txt"), 'w')
    print("true mce: ", network.mce_score)
    print("true dependecies: ", network.dependencies)

    from experiments import Estimation
    estimation = Estimation.EntropyEstimation(20, dataset, results_file)
    #number_of_dimensions = estimation.data.shape[1]

    from main import MceEstimation, Uds
    mce = MceEstimation.MCE(estimation, do_exp_check=False)
    mce.mce_estimation()
    print("estimated: ", mce.joint_ce)
    #print("dependecies: ", mce.used_dimensions, "\n")

    uds1 = Uds.UdsVariations(estimation)
    uds2 = Uds.UdsVariations(estimation)
    uds3 = Uds.UdsVariations(estimation)
    uds4 = Uds.UdsVariations(estimation)
    uds1.uds_pure()
    print("uds pure: ", uds1.mce_score)
    #print("combination: ", sorted(uds1.resulting_combination), "\n")
    uds2.uds_extended()
    print("uds ext: ", uds2.mce_score)
    #print("combination: ", sorted(uds2.resulting_combination), "\n")
    uds3.uds_jd()
    print("uds jd (+): ", uds3.mce_score)
    #print("combination: ", sorted(uds3.resulting_combination), "\n")
    uds4.uds_extended_jd()
    print("uds ext jd (++): ", uds4.mce_score, "\n")
    #print("combination: ", sorted(uds4.resulting_combination), "\n")

    return network.mce_score, mce.joint_ce, uds1.mce_score, uds2.mce_score, uds3.mce_score, uds4.mce_score


def discretizationAssessment(rows, columns, noise_level):

    network = generator.Dataset()
    network.generate_dataset(rows, columns, noise_level)
    dataset = path.abspath("dependency_network_noise.csv")
    results_file = open(path.abspath("../../results/test.txt"), 'w')

    print("true mce: ", network.mce_score)
    print("true dependecies: ", network.dependencies)
    for i in network.discretizations:
        print(i)
        print(len(network.discretizations[i]), "\n")


#todo average over multiple runs?
def plotMceApproxResults(rows, columns):
    noise = [1, 5, 10, 15, 20, 30]
    #noise = [1]

    true_mces = []
    est_mces = []
    uds_mces = []
    uds_ext_mces = []
    uds_jd_mces = []
    uds_ext_jd_mces = []

    for n in noise:
        print("noise level: ", n, "\n")
        true, mce, uds, uds_ext, uds_jd, uds_ext_jd = mceScoreComparison(rows, columns, n)
        true_mces.append(true)
        est_mces.append(mce)
        uds_mces.append(uds)
        uds_ext_mces.append(uds_ext)
        uds_jd_mces.append(uds_jd)
        uds_ext_jd_mces.append(uds_ext_jd)

    plt.style.use('seaborn-white')
    plt.figure(figsize=(10, 7.5))
    ax = plt.axes()
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    plt.xlim(min(noise), max(noise))
    plt.plot(noise, true_mces, linestyle='--', marker='o', label="True values")
    plt.plot(noise, est_mces, linestyle='-', marker='o', label="MCE")
    plt.plot(noise, uds_mces, linestyle='-', marker='o', label="UDS")
    plt.plot(noise, uds_ext_mces, linestyle='-', marker='o', label="UDS ext.")
    plt.plot(noise, uds_jd_mces, linestyle='-', marker='o', label="UDS j.d.")
    plt.plot(noise, uds_ext_jd_mces, linestyle='-', marker='o', label="UDS ext. j.d.")
    plt.xlabel("Noise Level", fontsize=16)
    plt.ylabel("Average CE value", fontsize=16)
    plt.legend(loc='best', numpoints=1, fancybox=True)
    plt.title("Estimated entropy value for different levels of noise", fontsize=16)
    plt.savefig("EntropyApproximation.png")
    plt.show()


if __name__ == "__main__":

    #todo combine network reconstruction and entropy approximation plots
    #print("1000x10, 10 iterations, no implied depndencies, ", "\n")

    #mceScoreComparison(1000, 10, 1)
    plotMceApproxResults(10000, 10)
    #discretizationAssessment(1000, 10, 1)