import networkx as nx

G = nx.path_graph(4)
G.add_path([10, 11, 12])
s = sorted(nx.connected_components(G), key=len, reverse=True)
print(s)