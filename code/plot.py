import uds
from os import path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

data = uds.read_data(path.abspath('test_data.txt'))

cut = np.array(data[:, :2])
cut.sort(axis=0)
print(cut)
print("\n")
data_frame = pd.DataFrame(cut, columns=['X1', 'X2'])
data_frame1 = pd.DataFrame(cut[:5, :], columns=['X1', 'X2'])
print(data_frame1)
data_frame1.plot.scatter(x='X1', y='X2', s=2)
plt.show()

#print(pd.DataFrame(cut))
#cut_discr = pd.qcut(cut, 5, duplicates='drop')
