import numpy as np
import pandas as pd
from os import path
from scipy.stats import bernoulli, multivariate_normal
import random
import math
from sklearn.datasets import make_spd_matrix
from main import read_data, preprocessing as prep
import os

'''
def generate_random(rows, columns):
    column0 = np.float64(np.random.rand(rows) * 100)
    column1 = np.multiply(column0, np.full(rows, -2))
    column2 = np.add(column0, np.full(rows, -1))
    column3 = np.float64(np.random.rand(rows) * 100)
    column4 = np.add(column3, np.full(rows, -2))
    column5 = np.multiply(column3, column4)
    matrix = np.matrix([column0, column1, column2, column3, column4, column5])
    #np.insert(matrix, 0, random_column1, axis=1)
    np.savetxt("random.csv", np.float64(matrix.T), delimiter=" ") '''


def generate_random(rows, columns):
    matrix = np.full(shape=(rows, columns), fill_value=None)
    for i in range(0, rows):
        for j in range(0, columns):
            matrix[i][j] = random.uniform(-100, 100)

    np.savetxt("random.csv", matrix, delimiter=" ")
    return pd.DataFrame(matrix)


def generate_class_labels(rows):
    labels = np.empty(shape=rows)
    for i in range(0, rows):
        labels[i] = random.randint(0, 1)
    return pd.DataFrame(labels)


'''
def generate_bernoulli():

    column0 = bernoulli.rvs(1/2, size=1000)
    column1 = [int(not x) for x in column0]
    column2 = column0
    column3 = bernoulli.rvs(1/2, size=1000)
    column4 = bernoulli.rvs(4/5, size=1000)
    matrix = np.matrix([column0, column1, column2, column3, column4])
    np.savetxt("bernoulli.csv", np.float64(matrix.T), delimiter=" ") '''


def generate_gaussian(rows):
    # mean = (1, 2)
    # cov = [[1, 0], [0, 1]]
    # matrix = np.float64(np.random.multivariate_normal(mean, cov, size))

    # X1 is a function of X0
    # X2 is a function of X1
    # X4 is a function of X3
    # X5 is independent

    X0 = np.random.normal(0, 1, rows)
    # X1 = np.add(X0, np.full(rows, 10))
    X1 = np.add(X0, np.random.normal(0, 5))
    # X2 = np.add(X1, np.full(rows, -2))
    X2 = np.add(X1, np.random.normal(0, 0.5))
    X3 = np.add(X1, np.random.normal(0, 4.6))
    X4 = np.add(X2, X3)
    X5 = np.add(X0, np.random.normal(0, 14))
    X6 = np.random.normal(5, 2.5, rows)
    # X4 = np.add(X3, np.full(rows, 3))
    # X4 = 3 * X3
    X7 = np.multiply(X3, np.random.normal(0, 20))
    X8 = np.random.normal(12, 4, rows)
    matrix = np.matrix([X0, X1, X2, X3, X4, X5, X6, X7, X8])
    dependencies = [(1, 0), (2, (0, 1)), (3, (0, 1)), (4, (0, 1, 2, 3)), (5, (0, 1, 2, 3, 4)), (7, (0, 1, 2, 3, 4, 5))]
    print(dependencies, "\n")
    # matrix = np.float64(np.random.normal(0, 1, (rows, columns)))
    np.savetxt("normal_with_correlations.csv", matrix.T)
    return dependencies


def gaussian_no_corr(rows, columns):
    dims = []
    for i in range(0, columns):
        X = np.random.normal(0, 1, rows)
        dims.append(X)
    matrix = np.matrix(dims)
    np.savetxt("gauss.csv", matrix.T)


def generate_gaussian_uniform(rows):
    X0 = np.random.normal(0, 1, rows)
    X1 = np.random.uniform(1000, 2000, rows)
    # X1 = np.random.normal(2, 0.2, rows)
    # X2 = np.random.normal(-10, 3, rows)
    # X3 = np.random.normal(18, 2.5, rows)
    X2 = np.random.normal(-100, 5, rows)
    # X5 = np.random.normal(-24, 2, rows)
    # X6 = np.random.normal(0, 0.5, rows)
    # X7 = np.random.normal(-1, 2.8, rows)
    # X8 = np.random.normal(25, 0.8, rows)
    X3 = np.random.uniform(-100, 100, rows)
    X4 = np.random.normal(58, 10, rows)
    # matrix = np.matrix([X0, X1, X2, X3, X4, X5, X6, X7, X8, X9])
    # matrix = np.matrix([X0, X2, X3, X4, X7, X8, X9])
    matrix = np.matrix([X0, X1, X2, X3, X4])
    np.savetxt("normal_uniform.csv", matrix.T)


def generate_total_correlation(rows):
    X0 = np.random.uniform(0, 1, rows)
    # X1 = np.add(np.multiply(X0, X0), np.full(rows, -1))
    # X1 = np.add(X0, np.full(rows, -5))
    # X2 = np.multiply(X0, X1)
    # X2 = np.add(X0, np.full(rows, 1/3))
    # X3 = np.add(10*X0, X2)
    # X3 = np.add(X0, np.full(rows, 10))
    # X4 = -3.75*X2
    # X4 = np.add(X0, np.full(rows, -7))
    # matrix = np.matrix([X0, X0, X0, X0, X0, X0, X0, X0, X0, X0, X0, X0, X0, X0, X0])
    matrix = np.matrix([X0, X0, X0, X0])
    np.savetxt("total_correlation.csv", matrix.T)


def generate_uds_example(rows, columns, sigma, type):
    l = int(columns / 2)
    X = np.zeros((rows, columns))
    A = np.random.uniform(0, 1, (l, l))
    B = np.random.uniform(0, 0.5, (l, l))

    for i in range(0, rows):
        Z = np.random.normal(0, 1, l)
        W = np.zeros(l)

        for j in range(0, l):
            for k in range(0, l):
                X[i][j] += A[j][k] * Z[k]

        for j in range(0, l):
            for k in range(0, l):
                W[j] += B[j][k] * X[i][k]

        for j in range(0, l):
            if type == 0:
                X[i][j + l] = 2 * W[j] + 1 + np.random.normal(0, sigma)
            elif type == 1:
                X[i][j + l] = W[j] * W[j] - 2 * W[j] + np.random.normal(0, sigma)
            elif type == 2:
                X[i][j + l] = math.log(math.fabs(W[j] + 1)) + np.random.normal(0, sigma)
            elif type == 3:
                X[i][j + l] = math.sin(2 * W[j]) + np.random.normal(0, sigma)

    np.savetxt("uds_dataset type" + str(type) + ".csv", X)
    return X


# retruns 2 dimensions
def generate_binnned_dimension(rows, bins_sizes, noise_level=None):
    number_of_bins = len(bins_sizes)
    discr_dim = np.full(shape=rows, fill_value=None)
    # cond_dim = np.full(shape=rows, fill_value=None)

    low = -100
    high = -80

    pointer = 0
    for i in range(number_of_bins):
        bin_size = bins_sizes[i]

        # bin_discr = np.random.normal(np.random.randint(0, 5), np.random.random(), size=bin_size)
        bin_discr = np.random.normal(np.random.uniform(low, high), 1, size=bin_size)
        # bin_discr = np.random.uniform(low, high, size=bin_size)
        # bin_cond = np.add(np.multiply(2, bin_discr), 1)

        discr_dim.put(indices=range(pointer, pointer + bin_size), values=bin_discr)
        # cond_dim.put(indices=range(pointer, pointer+bin_size), values=bin_cond)
        pointer += bin_size

        diff = high - low
        low += 10 + diff
        high += 2 * diff

    if noise_level is not None and noise_level != 0:
        noise = np.random.normal(0, noise_level, rows)
        discr_dim = np.add(discr_dim, noise)

    cond_dim = np.log(np.float64(np.add(np.abs(discr_dim), 1)))

    matrix = np.matrix([discr_dim, cond_dim]).T
    df = pd.DataFrame(matrix)

    # permutate dimensions jointly
    shuffled = df.iloc[np.random.permutation(len(df))]
    # print(shuffled)
    ids = list(shuffled.index)
    # print(ids, "\n")

    shuffled_reset_index = shuffled.reset_index(drop=True)
    shuffled_reset_index["old_ids"] = ids
    # print(shuffled_reset_index, "\n")

    shuffled_bins = []
    pointer = 0
    for i in range(number_of_bins):
        b = []
        bin_size = bins_sizes[i]

        for j in range(pointer, pointer + bin_size):
            b.append(list(shuffled_reset_index.index[shuffled_reset_index['old_ids'] == j])[0])

        pointer += bin_size
        shuffled_bins.append(sorted(b))

    del shuffled_reset_index["old_ids"]
    np.savetxt("binned_data.csv", np.matrix(shuffled_reset_index))
    # np.savetxt("binned_data_bins.csv", shuffled_bins, fmt='%s')
    # print(shuffled_bins)
    return shuffled_bins


# todo partial discretization
def generate_jointly_disrtibuted(colomns, bins_sizes, noise_level=None, all_bins=True, return_matrix=False):
    rows = sum(bins_sizes)
    number_of_bins = len(bins_sizes)
    discr_data = None

    low = -100
    high = -80

    for i in range(number_of_bins):
        bin_size = bins_sizes[i]
        mean = ()
        for o in range(colomns):
            mean += (np.random.uniform(low, high),)
        sample = np.random.multivariate_normal(mean, make_spd_matrix(colomns), bin_size)
        if discr_data is None:
            discr_data = sample
        else:
            discr_data = np.append(discr_data, sample, axis=0)
        """diff = high - low
        low += 10 + diff
        high += 2 * diff"""
        low += np.random.uniform(10, 40)
        high += np.random.uniform(20, 35)

    # extract bins' ids
    cond_dim = np.sum(discr_data, axis=1).reshape((rows, 1))
    discr_data = np.append(discr_data, cond_dim, axis=1)
    # print(discr_data, "\n")

    if noise_level is not None and noise_level != 0:
        # noise = np.random.multivariate_normal((0, 0), [[noise_level, 0], [0, noise_level]], rows)
        mean = (0,) * (colomns + 1)
        covariance = np.diag(np.full(colomns + 1, noise_level))
        noise = np.random.multivariate_normal(mean, covariance, rows)
        discr_data = np.add(discr_data, noise)

    # cond_dim = np.multiply(discr_data[:, 0], discr_data[:, 1])
    # cond_dim = np.multiply(discr_data[:, 0], discr_data[:, 0])
    # df = pd.concat([pd.DataFrame(discr_data), pd.DataFrame(cond_dim)], axis=1)
    df = pd.DataFrame(discr_data)
    shuffled = df.iloc[np.random.permutation(len(df))]
    ids = list(shuffled.index)
    df_reset_index = shuffled.reset_index(drop=True)
    df_reset_index["old_ids"] = ids
    shuffled_bins = []
    pointer = 0

    for i in range(number_of_bins):
        b = []
        bin_size = bins_sizes[i]
        for j in range(pointer, pointer + bin_size):
            b.append(list(df_reset_index.index[df_reset_index['old_ids'] == j])[0])
        pointer += bin_size
        shuffled_bins.append(sorted(b))

    del df_reset_index["old_ids"]
    np.savetxt("multivariate_gauss.csv", np.matrix(df_reset_index))
    # return df_reset_index, shuffled_bins
    if return_matrix:
        return df_reset_index
    return shuffled_bins


class Dataset:
    def __init__(self):
        self.dataset = pd.DataFrame()
        self.shape = ()
        self.ces = {}
        self.dependencies = []
        self.outliers = []
        self.connected_components = []
        self.discretizations = []  # for each non-sigletone component
        self.ces_per_term = {}
        self.mce_score = 0
        self.mce_scores = []
        self.dependency_score = 0
        self.ds_numerator = 0
        self.ds_denominator = 0
        self.distributions = ["gaussian", "uniform", "poisson"]
        self.functions = ['linear', 'quadratic', 'polynomial', 'log', 'exp', 'sin']
        self.multiv_results = []

    def get_dependencies(self, sizes, outliers=False):
        if outliers:
            d = len(sizes)-1
        else:
            d = len(sizes)
        for i in range(0, d):
            if len(self.dependencies) == 0:
                d = (sizes[i]-1, tuple(range(0, sizes[i]-1)))
            else:
                s = sum(sizes[0: i])
                prev = self.dependencies[-1][0]
                d = (sizes[i] + prev, tuple(range(s, prev + sizes[i])))
            self.dependencies.append(d)
        if outliers:
            outliers = [(i,) for i in range(sum(sizes[0: -1]), sum(sizes))]
            self.dependencies.extend(outliers)

    def create_binning(self, number_of_dims, rows):
        bins_sizes = []
        for i in range(number_of_dims - 1):
            ps_ = []
            while sum(ps_) != 10:
                ps_ = [random.randint(1, 9) for i in range(0, random.randint(2, 5))]
            bins = [int((i / 10) * rows) for i in ps_]
            bins_sizes.append(bins)
        #print("binning: ", bins_sizes)
        return bins_sizes

    def linear(self, data):  # can be used for multiv. case
        n = data.shape[1] + 1  # number of parameters
        params = [random.randint(-9, 9) for i in range(0, n)]
        #print("linear, params: ", params, "\n")
        res = pd.DataFrame()
        for i in range(0, data.shape[1]):
            v = params[i] * data.iloc[:, i]
            if res.empty: res = v
            else: res += v
        res += params[-1]
        return pd.DataFrame(res)

    def linear_ratio(self, data):
        n = data.shape[1] + 2
        params = [random.randint(-9, 9) for i in range(0, n)]
        numerator_size = round(data.shape[1] / 2)
        res_n = pd.DataFrame()
        for i in range(0, numerator_size):
            v = params[i] * data.iloc[:, i]
            if res_n.empty: res_n = v
            else: res_n += v
        res_n += params[-1]
        res_d = pd.DataFrame()
        for j in range(numerator_size, data.shape[1]):
            v = params[j] * data.iloc[:, j]
            if res_d.empty: res_d = v
            else: res_d += v
        res_d += params[-2]
        res = res_n/res_d
        return pd.DataFrame(res)

    def quadratic(self, data):  # can be used for 2 variables
        nr_dims = data.shape[1]
        if nr_dims > 2:
            raise ValueError("quadratic not possible: too many dimensions")
        params = [random.randint(-9, 9) for i in range(0, 3)]
        #print("quadratic, params: ", params, "\n")
        if nr_dims == 1:
            dim = data.iloc[:, 0]
            res = params[0] * (dim ** 2) + params[1] * dim + params[2]
        else:
            dim1 = data.iloc[:, 0]
            dim2 = data.iloc[:, 1]
            res = params[0] * (dim1 ** 2) + params[1] * dim2 + params[2]
        return pd.DataFrame(res)

    #univariate
    def polynomial_1(self, data, power):
        params = [random.randint(-9, 9) for i in range(0, power+1)]
        dim = data.iloc[:, 0]
        res = pd.DataFrame()
        for i in range(1, power + 1):
            v = params[i]*(dim**i)
            if res.empty: res = v
            else: res += v
        res += params[-1]
        return pd.DataFrame(res)

    def polynomial_n(self, data):  # multiv
        n = data.shape[1] + 1
        params = [random.randint(-9, 9) for i in range(0, n)]
        res = pd.DataFrame()
        for i in range(1, data.shape[1] + 1):
            v = params[i - 1]*(data.iloc[:, i - 1] ** i)
            if res.empty: res = v
            else: res += v
        res += params[-1]
        return pd.DataFrame(res)

    def polynomial_ratio(self, data):
        n = data.shape[1] + 2
        params = [random.randint(-9, 9) for i in range(0, n)]
        numerator_size = round(data.shape[1] / 2)
        res_n = pd.DataFrame()
        powers = list(range(1, numerator_size+1))
        for i in range(0, numerator_size):
            v = params[i]*(data.iloc[:, i] ** powers[i])
            if res_n.empty: res_n = v
            else: res_n += v
        res_n += params[-1]
        res_d = pd.DataFrame()
        k = 0
        for j in range(numerator_size, data.shape[1]):
            v = params[j]*(data.iloc[:, j] ** powers[k])
            if res_d.empty: res_d = v
            else: res_d += v
            k += 1
        res_d += params[-2]
        res = res_n/res_d
        return pd.DataFrame(res)

    def log_linear(self, data):
        n = 2
        params = [random.randint(-100, 100) for i in range(0, n)]
        res = self.linear(data)
        res = res.apply(np.abs, axis=1)
        res = params[0]*res.apply(np.log2, axis=1) + params[1]
        return res

    def log_pol(self, data):
        n = 2
        params = [random.randint(-50, 50) for i in range(0, n)]
        res = self.polynomial_n(data)
        res = res.apply(np.abs, axis=1)
        res = params[0] * res.apply(np.log, axis=1) + params[1]
        return res

    #log multiplication?

    def log_linear_ratio(self, data):
        params = [random.randint(-9, 9) for i in range(0, 2)]
        res = self.linear_ratio(data)
        res = params[0]*res.apply(np.log2, axis=1) + params[1]
        return res

    def log_pol_ratio(self, data):
        params = [random.randint(-9, 9) for i in range(0, 2)]
        res = self.polynomial_ratio(data)
        res = params[0] * res.apply(np.log2, axis=1) + params[1]
        return res

    #['linear', 'linear_ratio', 'quadratic', 'polynomial_1', 'polynomial_n', 'polynomial_ratio', 'log_linear', 'log_pol', 'log_linear_ratio', 'log_pol_ratio']

    """
    def exponential(self, data):
        n = 2
        params = [random.randint(-9, 9) for i in range(0, n)]
        res = self.linear(data)
        res = res.apply(np.expm1, axis=1)
        return res"""

    """
    def sin(self, data):
        #n = data.shape[1]
        #params = [random.randint(-9, 9) for i in range(0, n)]
        #res = self.linear(data)
        res = pd.DataFrame(data.sum(axis=1))
        #res = res.apply(np.abs, axis=1)
        res = res.apply(np.tan, axis=1)
        #res = res.apply(np.cos, axis=1)
        return res"""

    def generate_subspaces(self, rows, columns, nr_subsp, subs_size, dep_types_array, nr_outl, noise_level, iteration, post_noise=False):
        if nr_subsp != len(dep_types_array):
            raise ValueError("amount of dependency types should equal amount of subspaces")
        nr_dep_dims = columns - nr_outl
        # subspace_size = math.ceil(nr_dep_dims/nr_subsp)
        # print("subspace size: ", subs_size)
        # print("nr outliers: ", nr_outl)
        if subs_size*nr_subsp != nr_dep_dims:
            nr_outl += nr_dep_dims - (subs_size*nr_subsp)
        # print("nr outliers: ", nr_outliers)
        # print("size: ", nr_outliers + nr_dep_dims)
        outliers = False
        sizes = [subs_size]*nr_subsp
        if nr_outl > 0:
            outliers = True
            sizes.append(nr_outl)
        self.get_dependencies(sizes, outliers=outliers)
        for i in range(0, nr_subsp):
            bins = self.create_binning(sizes[i], rows)
            distr = [d for d in np.random.choice(self.distributions, sizes[i] - 1)]
            dep_type = dep_types_array[i]
            subspace = self.gen_univ_binned_data(rows, sizes[i] - 1, bins, distr, dep_type, noise=noise_level, post_noise=post_noise)
            self.dataset = pd.concat([self.dataset, subspace], axis=1)
        if outliers:
            d_outliers = generate_random(rows, nr_outl)
            self.dataset = pd.concat([self.dataset, d_outliers], axis=1)
            for out_id in range(d_outliers.shape[1]):
                entropy = prep.one_dim_ce(d_outliers.iloc[:, out_id])
                self.ds_denominator += entropy
        for d_id in range(nr_dep_dims, self.dataset.shape[1]):
            value = prep.one_dim_ce(self.dataset.iloc[:, d_id])
            self.mce_scores.append(value)
        self.dataset = pd.concat([self.dataset, generate_class_labels(rows)], axis=1)
        self.dataset.columns = list(range(self.dataset.shape[1]))
        self.dataset = self.dataset.reset_index(drop=True)
        self.shape = self.dataset.shape
        print("shape: ", self.shape)
        # print("data size: ", self.shape)
        self.discretizations = dict(enumerate(self.discretizations))
        self.mce_score = sum(self.mce_scores)
        self.mce_scores = dict(enumerate(self.mce_scores))
        self.dependency_score = self.ds_numerator/self.ds_denominator
        # assert self.shape == (rows, columns), str(self.shape)
        print(self.dependencies)
        print(round(self.dependency_score, 10))
        print(round(self.mce_score, 10), "\n")
        self.dataset.to_csv("subspace_network_logpol" + str(iteration) + ".csv", index=False, index_label=False, sep=",")
        # np.savetxt("subspace_network.csv", np.matrix(self.dataset), delimiter=" ")

    def gen_univ_binned_data(self, rows, columns, bins_sizes, distributions, dep_type, noise=None, post_noise=False):
        a = np.random.uniform(-100, 100)
        b = np.random.uniform(-150, 50)
        # print("distributions: ", distributions)

        df = pd.DataFrame()
        for c in range(columns):
            discr_dim = None
            bins = bins_sizes[c]
            for j in range(len(bins)):
                bin_size = bins[j]
                distribution = distributions[c]
                if distribution == "gaussian":
                    mean = np.random.uniform(a, b)
                    variance = np.random.uniform(1, 10)
                    bin_discr = np.random.normal(mean, variance, bin_size)
                # if distribution == "lognormal":
                #    mean = np.random.uniform(a, b)
                #    variance = np.random.uniform(1, 10)
                #    bin_discr = np.random.lognormal(mean, variance, bin_size)
                if distribution == "uniform":
                    bin_discr = np.random.uniform(a, b, bin_size)
                if distribution == "poisson":
                    l = np.random.uniform(1, 100)
                    # print("poisson param: ", l)
                    bin_discr = np.random.poisson(l, bin_size)
                """
                if distribution == "exponential":
                    p = np.random.uniform(1, 100)
                    bin_discr = np.random.exponential(p, bin_size)"""
                if distribution == "binomial":
                    bin_discr = np.random.binomial(100, np.random.rand(), bin_size)
                if distribution == "random":
                    bin_discr = np.random.uniform(a, b, bin_size)
                if discr_dim is None:
                    discr_dim = bin_discr
                else:
                    discr_dim = np.append(discr_dim, bin_discr, axis=0)
                # if not all_bins and j == 0: continue
                # a += np.random.uniform(30, 60)
                # b += np.random.uniform(60, 70)
                a = b
                b += 50
            df = pd.concat([df, pd.DataFrame(discr_dim)], axis=1)

        if dep_type == 'linear':
            cond_dim = self.linear(df)
        elif dep_type == 'linear_ratio':
            cond_dim = self.linear_ratio(df)
        elif dep_type == 'quadratic':
            cond_dim = self.quadratic(df)
        elif dep_type == 'polynomial_1':
            power = random.randint(3, 5)
            cond_dim = self.polynomial_1(df, power)
        elif dep_type == 'polynomial_n':
            cond_dim = self.polynomial_n(df)
        elif dep_type == 'polynomial_ratio':
            cond_dim = self.polynomial_ratio(df)
        elif dep_type == 'log_linear':
            cond_dim = self.log_linear(df)
        elif dep_type == 'log_pol':
            cond_dim = self.log_pol(df)
        elif dep_type == 'log_linear_ratio':
            cond_dim = self.log_linear_ratio(df)
        elif dep_type == 'log_pol_ratio':
            cond_dim = self.log_pol_ratio(df)

        # adding noise only to the conditioned dimension
        if noise is not None and noise != 0 and not post_noise:
            noise = pd.DataFrame(np.random.normal(0, noise, rows))
            cond_dim += noise

        df = pd.concat([df, cond_dim], axis=1)
        df.columns = list(range(columns + 1))
        # multivariate noise
        """
        if noise_level is not None and noise_level != 0:
            mean = (0,) * (columns + 1)
            covariance = np.diag(np.full(columns + 1, noise_level))
            noise = pd.DataFrame(np.random.multivariate_normal(mean, covariance, rows))
            df += noise """

        # generating and binning the first discretized dimension
        """
        low1 = -100
        high1 = -80
        pointer1 = 0
        for i in range(number_of_bins1):
            bin_size = bins_sizes1[i]
            mean = np.random.uniform(low1, high1)
            bin_discr = np.random.normal(mean, 1, size=bin_size)
            #bin_cond = np.add(np.multiply(2, bin_discr), 1)
            discr_dim1.put(indices=range(pointer1, pointer1+bin_size), values=bin_discr)
            #cond_dim.put(indices=range(pointer, pointer+bin_size), values=bin_cond)
            pointer1 += bin_size
            if not all_bins and i == 0:
                # make the first and the second bin similar
                continue
            else:
                diff = high1 - low1
                low1 += 10 + diff
                high1 += 2 * diff
            #if i > 0:
            #    diff = high1 - low1
            #    low1 += 10 + diff
            #    high1 += 2 * diff
       # if noise_level is not None and noise_level != 0:
       #     noise = np.random.normal(0, noise_level, rows)
       #     discr_dim1 = np.add(discr_dim1, noise)
        df1 = pd.DataFrame(np.matrix(discr_dim1).T)
        # generating and binning the second discretized dimension
        low2 = -150
        high2 = -100
        pointer2 = 0
        for j in range(number_of_bins2):
            bin_size = bins_sizes2[j]
            bin_discr = np.random.normal(np.random.uniform(low2, high2), 1, size=bin_size)
            discr_dim2.put(indices=range(pointer2, pointer2+bin_size), values=bin_discr)
            if not all_bins:
                #cond_dim = np.full(shape=rows, fill_value=None)
                if j == 0:  # bins_sizes2[j] >= bins_sizes1[j]:
                    #values = np.add(np.multiply(0.5, discr_dim1[pointer2:pointer2+bin_size]), np.multiply(0.5, bin_discr))
                    values = bin_discr
                    #print("values: ", values, "\n")
                    cond_dim.put(indices=range(pointer2, pointer2+bin_size), values=values)
                else:
                    values_dim1 = discr_dim1[pointer2:pointer2+bin_size]
                    cond_dim.put(indices=range(pointer2, pointer2+bin_size), values=np.add(values_dim1, bin_discr))
                #print("cond. dim: ", cond_dim, "\n")
            pointer2 += bin_size
            low2 += np.random.uniform(10, 40)
            high2 += np.random.uniform(20, 35)
       # if noise_level is not None and noise_level != 0:
       #     noise = np.random.normal(0, noise_level, rows)
       #     discr_dim2 = np.add(discr_dim2, noise) 
        df2 = pd.DataFrame(np.matrix(discr_dim2).T)
        df2.columns = [1]
        #df = pd.concat([df1, df2], axis=1)
        df = pd.concat([df2, df1], axis=1)
        #TODO make conditional
        cond_dim = df.sum(axis=1)
        df = pd.concat([df, pd.DataFrame(cond_dim)], axis=1) """

        # todo add partial discretization case
        # shuffle the dataset
        shuffled = df.iloc[np.random.permutation(len(df))]
        ids = list(shuffled.index)
        df_reset_index = shuffled.reset_index(drop=True)
        df_reset_index["old_ids"] = ids
        discretization = []
        # bins_sizes - bins split for each of the discr.dimensions
        for bins in bins_sizes:
            number_of_bins = len(bins)
            pointer = 0
            shuffled_bins = []
            for i in range(number_of_bins):
                bin_size = bins[i]
                sorted_df = df_reset_index.sort_values(by=['old_ids'])
                ids = sorted_df.loc[sorted_df['old_ids'].isin(range(pointer, pointer+bin_size))].index
                shuffled_bins.append(ids)
                pointer += bin_size
            assert (sum([len(d) for d in shuffled_bins]) == rows), str(sum([len(d) for d in shuffled_bins]))
            discretization.append(shuffled_bins)
            self.discretizations.append(shuffled_bins)

        merged_bins = []
        for bins_id1 in range(len(discretization)):
            bins1 = discretization[bins_id1]
            for bins_id2 in range(bins_id1 + 1, len(discretization)):
                bins2 = discretization[bins_id2]
                inter = [i.intersection(j) for i in bins1 for j in bins2]
                inter = [d for d in inter if not d.size == 0]
                if len(merged_bins) == 0:
                    merged_bins = inter
                else:
                    inter1 = [i.intersection(j) for i in merged_bins for j in inter]
                    inter1 = [d for d in inter1 if not d.size == 0]
                    merged_bins = inter1

        self.discretizations.append(merged_bins)
        del df_reset_index["old_ids"]

        # check!
        if post_noise:
            print(df_reset_index.max(axis=1, skipna=True))
            print(df_reset_index.min)
            m_ = max(df_reset_index.max(axis=1, skipna=True), math.fabs(df_reset_index.min(axis=1, skipna=True)))
            noise_level = (noise/100)*m_
            df_reset_index = add_noise(df_reset_index, noise_level)

        for d_id in range(columns):  # only given columns
            # self.mce_score += prep.one_dim_ce(df_reset_index.iloc[:, d_id])
            self.mce_scores.append(prep.one_dim_ce(df_reset_index.iloc[:, d_id]))

        from main import JointDiscretization as jd  # ce for cond. column
        # self.mce_score += jd.conditional_ce_all_bins(df_reset_index.iloc[:, -1], merged_bins)
        shuffled_cond_dim = df_reset_index.iloc[:, -1]
        univ_ce = prep.one_dim_ce(shuffled_cond_dim)
        cond_ce = jd.conditional_ce_all_bins(shuffled_cond_dim, merged_bins)
        self.mce_scores.append(cond_ce)
        self.ds_numerator += (univ_ce - cond_ce)
        self.ds_denominator += univ_ce
        # temp_multiv_res[c_cond_dim] = [jd_comb, joint_cond_ce, nr_bins]
        self.multiv_results.append([cond_ce, len(merged_bins)])
        # compute univariate ce, add to the denominator, add difference to the numerator

        """
        #get disc. w.r.t. the points' ids
        shuffled_bins2 = []
        pointer2 = 0
        for j in range(number_of_bins2):
            b = []
            bin_size = bins_sizes2[j]

            for k in range(pointer2, pointer2 + bin_size):
                b.append(list(df_reset_index.index[df_reset_index['old_ids'] == k])[0])

            pointer2 += bin_size
            shuffled_bins2.append(pd.Index(sorted(b)))

        # all bins included in conditioned dimension
        #if all_bins:

        #bins ids
        shuffled_bins1 = []
        pointer1 = 0
        for i in range(number_of_bins1):
            b = []
            bin_size = bins_sizes1[i]

           # if not all_bins and i == 0:
           #     b = [idx for idx in shuffled_bins2[0]]  # !!!!
           # if (not all_bins and i > 1) or all_bins:
           #     for j in range(pointer1, pointer1 + bin_size):
           #         b.append(list(df_reset_index.index[df_reset_index['old_ids'] == j])[0])

            for j in range(pointer1, pointer1 + bin_size):
                b.append(list(df_reset_index.index[df_reset_index['old_ids'] == j])[0])

            pointer1 += bin_size
            #if len(b) != 0: shuffled_bins1.append(pd.Index(sorted(b)))
            shuffled_bins1.append(pd.Index(sorted(b)))

        discr_ = [i.intersection(j) for i in shuffled_bins1 for j in shuffled_bins2]
        merged_bins = [d for d in discr_ if not d.size == 0]

        del df_reset_index["old_ids"]
        #data = pd.concat([df_reset_index, pd.DataFrame(cond_dim)], axis=1) """
        """
        if not all_bins:
            b1 = merged_bins[0]
            b2 = merged_bins[1]
            merged_bins.remove(b1)
            merged_bins.remove(b2)
            merged_bins.append(b1.append(b2))"""

        np.savetxt("binned_data.csv", np.matrix(df_reset_index))
        return df_reset_index


# use this function for noise experiment
def add_noise(data, noise_level):
    # dataset = read_data.read(data_file)
    # rows = dataset.shape[0]
    # columns = dataset.shape[1]
    mean_v = data.values.mean()
    # print(mean_v)
    cov_value = mean_v * noise_level
    mean = (0,) * data.shape[1]
    covariance = np.diag(np.full(data.shape[1], cov_value))
    noise = pd.DataFrame(np.random.multivariate_normal(mean, covariance, data.shape[0]))
    data_noise = pd.DataFrame(data.values + noise.values)
    return data_noise
    # np.savetxt(data_file + "noise_" + str(noise_level), dataset)


def generate_noisy_data():
    dir_linear = "/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/datasets/synthetic/polynomial/m20/"
    noise_levels = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]

    for nl in noise_levels:
        for filename in os.listdir(dir_linear):
            if filename.endswith(".csv"):
                dataset = pd.read_csv(dir_linear + "/" + filename, usecols=range(0, 20))
                noisy_data = add_noise(dataset, nl).reset_index(drop=True)
                noisy_data.to_csv(str(nl) + filename, header=False, index=False)


if __name__ == "__main__":
    # dir_linear = "/Users/julia/Documents/studying/master/Master_Thesis/thesis/code/datasets/synthetic/linear/noise/"
    # for filename in os.listdir(dir_linear):
    #    if os.path.isdir(os.path.join(os.path.abspath(dir_linear), filename)):
    #        for filename1 in os.listdir(os.path.join(os.path.abspath(dir_linear), filename)):
    #            print(filename1)

    # generate_noisy_data()

    # generate_subspaces(self, rows, columns, nr_subsp, subs_size, dep_types_array, nr_outl, noise_level,
    #                   post_noise=False):

    rows = 10000
    columns = [10, 20, 30, 40, 50]

    for c in columns:
        nr_outliers = int(math.ceil(c * 0.2))
        subspace_size = 5
        nr_subsp = math.ceil((c - nr_outliers) / subspace_size)
        dep_types_array = ["log_pol"] * nr_subsp
        noise = 10

        data = Dataset()
        data.generate_subspaces(rows, c, nr_subsp, subspace_size, dep_types_array, nr_outliers, noise, c)





