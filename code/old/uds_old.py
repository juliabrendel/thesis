#!/usr/bin/python3.5.2

import numpy as np
import pandas as pd
from os import path
import math

'''
def read_data(path_to_file):
    data = np.loadtxt(path_to_file, dtype=float)
    return data'''


def rank_each_dimension(data):
    ranked_data = np.zeros(shape=data.shape, dtype=int)

    for i in range(0, data.shape[1]):
        dimension = data[:, i]
        indices = {}

        for index, point in enumerate(dimension):
            indices[index] = float(point)
            # indices[list(dimension).index(point)] = float(point)

        sorted_dimension = sorted(dimension)

        for j in sorted_dimension:
            index_of_j = int(list(indices.keys())[list(indices.values()).index(float(j))])
            ranked_data[sorted_dimension.index(float(j))][i] = int(index_of_j)
            del indices[index_of_j]

    return ranked_data


def one_dim_ce_light(dimension):
    cumulative_entropy = 0
    dimension = sorted(dimension)

    for i in range(1, len(dimension) - 1):
        cumulative_entropy += (dimension[i] - dimension[i - 1]) * (i / len(dimension)) * np.log(i / len(dimension))

    cumulative_entropy = (-1) * cumulative_entropy

    if len(dimension) == 0:
        print("dimension has no points!")
        cumulative_entropy = None

    return cumulative_entropy


# oneDimCE -  for every dimension calculate its CE, save into a map
def one_dim_ce(dimension, dimension_index):
    cumulative_entropy = 0
    # sorting elements in the ascending order
    dimension = sorted(dimension)
    # print(dimension)

    for i in range(1, len(dimension) - 1):
        cumulative_entropy += (dimension[i] - dimension[i - 1]) * (i / len(dimension)) * np.log(i / len(dimension))

    cumulative_entropy = (-1) * cumulative_entropy
    candidate_set[dimension_index + 1] = cumulative_entropy
    return cumulative_entropy, candidate_set

'''
def shannon_entropy(dataset):
    # print("dataset = " + str(dataset))
    number_of_points = dataset.shape[0]
    df = pd.DataFrame(dataset)
    df1 = pd.DataFrame(df.groupby(df.columns.tolist()).size())
    df2 = pd.Series(df1.values[:, -1], name=df1.columns[-1])

    entropy = 0

    for x in df2:
        entropy -= (x / number_of_points) * math.log2(x / number_of_points)

    # print("shannon entropy = " + str(entropy) + "\n")
    return entropy'''


# TODO: make multivariate case
def univariate_shannon_entropy_discretized(dimension, discretization, prev_dims=None, prev_discrs=None):

    if prev_dims is not None and prev_discrs is not None:
        dimension = join_dimensions(prev_dims.append(dimension))
        discretization = join_discretizations(prev_discrs, discretization, len(prev_dims))

    entropy = 0
    discretized_dim = []

    for interval in discretization:
        bin = []
        for point in dimension:
            if interval[0] < point <= interval[1]:
                bin.append(point)
        discretized_dim.append(bin)

    for bin in discretized_dim:
        entropy -= (len(bin) / len(dimension)) * math.log2(len(bin) / len(dimension))

    # print("discretized shannon entropy = " + str(entropy) + "\n")
    return entropy

'''
def multivariate_shannon_entropy(prev_dims, prev_discrs, new_dim, new_discr):
    
    joined_dimensions = join_dimensions(prev_dims.append(new_dim))
    
    joined_discrs = join_discretizations(prev_discrs, new_discr, len(prev_dims))
    
    entropy = 0
    discretized_dims = []
    
    for interval in joined_discrs:
        hypercube = []
        for point in joined_dimensions:
            if interval[] 
'''
# changed
# TODO: add to the candidate set
# TODO: add the same method for a bunch of bins
'''
def conditional_ce(conditioned_dim, current_bin, discretized_dim, bin_support):
    """
    print("cond dim: " + str(conditioned_dim))
    print("current bin: " + str(current_bin))
    print("discretized_dim: " + str(discretized_dim))
    print("bin_support: " + str(bin_support)) """

    cond_cumulative_entropy = 0
    conditioned_dim = sorted(conditioned_dim)
    points_current_bin = []
    for point in conditioned_dim:
        if current_bin[0] < point <= current_bin[1]:
            points_current_bin.append(point)

    print("points: " + str(points_current_bin) + "\n")

    if len(points_current_bin) != 0:
        cond_cumulative_entropy += one_dim_ce_light(points_current_bin) * (bin_support / len(discretized_dim))
    else:
        cond_cumulative_entropy = None  # avoiding empty conditioned bins issue

    return cond_cumulative_entropy'''


# TODO: avoid using one_dim_ce_light
def conditional_ce(conditioned_dim, discretized_dim, bin_support, current_bin=None, merged_bin_points_ids=None):
    cond_cumulative_entropy = 0
    conditioned_dim = sorted(conditioned_dim)
    points = []
    if current_bin is not None:
        # points = np.where(np.logical_and(conditioned_dim > current_bin[0], conditioned_dim <= current_bin[1]))
        for point in conditioned_dim:
            if current_bin[0] < point <= current_bin[1]:
                points.append(point)

    if merged_bin_points_ids is not None:
        points = [conditioned_dim[idx] for idx in merged_bin_points_ids]

    if len(points) != 0:
        cond_cumulative_entropy += one_dim_ce_light(points) * (bin_support / len(discretized_dim))
    else:
        cond_cumulative_entropy = None

    return cond_cumulative_entropy


def ce_per_dimension(data):
    cumulative_entropies = {}

    # compute CE for each dimension, add to the dictionary
    for i in range(0, data.shape[1]):
        cumulative_entropy, candidate_set = one_dim_ce(data[:, i], i)
        cumulative_entropies[i + 1] = cumulative_entropy

    # sort dimensions in the descending order of CEs
    # ordered_cumulative_entropies = sorted(cumulative_entropies.items(), key=lambda kv: float(kv[1]), reverse=True)
    ordered_dimensions = sorted(cumulative_entropies, key=cumulative_entropies.get, reverse=True)

    # for j in range(1, len(ordered_cumulative_entropies) + 1):
    #    ordered_dimensions[j] = (ordered_cumulative_entropies[j-1])

    return cumulative_entropies, ordered_dimensions


def form_initial_bins(data, order_of_columns, dim_x_index, number_of_bins):
    index_dim_x = order_of_columns[dim_x_index - 1] - 1
    index_dim_y = order_of_columns[dim_x_index - 2] - 1

    conditioned = sorted(data[:, index_dim_x])
    discretized = sorted(data[:, index_dim_y])

    y_discretized = pd.qcut(discretized, number_of_bins, duplicates='drop')
    x_discretized = pd.qcut(conditioned, number_of_bins, duplicates='drop')

    return x_discretized, y_discretized


def join_bins(bins):
    new_left_value = bins[0][0]
    new_right_value = bins[len(bins) - 1][1]
    joined_bin = [new_left_value, new_right_value]
    return joined_bin


# TODO: previous dims and dicrs should be given multivariatly
# returns the common points
def merge(previous_dims, previous_discrs, new_dim, new_bin):

    #print("previous dims = " + str(previous_dims))

    #print("merge function, new_bin: " + str(new_bin))
    common_points_ids = []
    joined_bin = join_bins(previous_discrs)
    #print("joined bin = " + str(joined_bin) + "\n")

    for idx in range(0, len(new_dim)):
        if new_bin[0] < new_dim[idx] <= new_bin[1]:
            #print("joined_bin[0] = " + str(joined_bin[0]))
            #print("joined_bin[1] = " + str(joined_bin[1]))
            #print("previous dims[idx] = " + str(previous_dims[idx]) + "\n")
            if joined_bin[0] < previous_dims[idx] <= joined_bin[1]:
                common_points_ids.append(idx)

    return common_points_ids


def join_discretizations(discr_old, discr_new, dimensionality):
    discr_joint = []
    # correction_const = 0.0001
    if len(discr_old) == len(discr_new):

        for bin_index in range(0, len(discr_old)):

            if dimensionality == 1:
                # bin_left_side = (discr_old[bin_index][0], discr_new[bin_index][0] - correction_const)
                bin_left_side = (discr_old[bin_index][0], discr_new[bin_index][0])
                bin_right_side = (discr_old[bin_index][1], discr_new[bin_index][-1])
                merged_bin = [bin_left_side, bin_right_side]
                discr_joint.append(merged_bin)

            else:
                tuple_left_side = discr_old[bin_index][0]
                tuple_right_side = discr_old[bin_index][1]
                # tuple_left_side += (discr_new[bin_index][0] - correction_const,)
                tuple_left_side += (discr_new[bin_index][0],)
                tuple_right_side += (discr_new[bin_index][-1],)
                merged_bin = [tuple_left_side, tuple_right_side]
                discr_joint.append(merged_bin)

    return discr_joint


# correct structure for dimensions?
# result should be in tuples
def join_dimensions(dimensions):

    #print("join dims, dims = " + str(dimensions))
    number_of_points = len(dimensions[0])
    joined_dimensions = []

    for p_idx in range(0, number_of_points):
        tuple_point = ()
        for d_idx in range(0, len(dimensions)):
            tuple_point += (dimensions[d_idx][p_idx],)
        joined_dimensions.append(tuple_point)

    return joined_dimensions


# TODO: if len(merged_bin_points_ids) != 0: - make this check for multivariate case
# TODO: change, that previous dims are taken in the function, not through the arguments
def compute_f_val_b(data, order_of_columns, number_of_bins, dim_cond_index, conditioned_dim, discretized_dim, sumLog,
                    previous_dicrs=None, previous_dims=None):
    # def compute_f_val_b(data, order_of_columns, number_of_bins, dim_cond_index, conditioned_dim, discretized_dim, sumLog):

    f = np.full(shape=(number_of_bins, number_of_bins), fill_value=None, dtype=float)
    val = np.full(shape=(number_of_bins, number_of_bins), fill_value=None, dtype=float)
    final_bins = np.empty(shape=(number_of_bins, number_of_bins), dtype=list)

    # initial equal-frequency discretizations of both X and Y
    cond_discretized, discr_discretized = form_initial_bins(data, order_of_columns, dim_cond_index, number_of_bins)

    # indices of points which fall into those bins
    bin_indices_discr = discr_discretized.codes
    bin_indices_cond = cond_discretized.codes

    initial_bins = []
    initial_bins_cond = []
    merged_bins_points_ids = []
    s = []
    bins_support = []

    for b in discr_discretized.categories:
        interval = [b.left, b.right]
        initial_bins.append(interval)

    # print("initial_bins: " + str(initial_bins))
    '''
    for b in cond_discretized.categories:
        interval = [b.left, b.right]
        initial_bins_cond.append(interval)'''

    # print("initial_bins_cond: " + str(initial_bins_cond))
    '''
    if previous_dicrs is None:
        for b in cond_discretized.categories:
            interval = [b.left, b.right]
            initial_bins_cond.append(interval)
            # else:'''

    for b in cond_discretized.categories:
        interval = [b.left, b.right]
        initial_bins_cond.append(interval)

    # TODO: recalculate s each time for multivariate case
    '''
    for i in range(0, number_of_bins):
        count = 0
        for j in range(0, i + 1):
            for ind in bin_indices_discr:
                if ind == j:
                    count += 1
        s.append(count)
    #print("s: " + str(s)) '''

    if previous_dicrs is None:
        for i in range(0, number_of_bins):
            count = 0
            for j in range(0, i + 1):
                for ind in bin_indices_discr:
                    if ind == j:
                        count += 1
            s.append(count)
    else:
        #print("previous discrs: " + str(previous_dicrs))
        count = 0
        for i in range(0, number_of_bins):
            #bins_to_join = []
            for j in range(0, i + 1):
                #print("initial bin j: " + str(initial_bins[j]))
                #bins_to_join.append(initial_bins[j])
                bins_to_join = initial_bins[j]
                #print("bins to join: " + str(bins_to_join))
            count += len(merge(previous_dims, previous_dicrs, discretized_dim, bins_to_join))
            s.append(count)
    '''
    for i in range(0, number_of_bins):
        count = 0
        for b in bin_indices_discr:
            if b == i:
                count += 1
        bins_support.append(count)
    #print("bins support: " + str(bins_support)) '''
    if previous_dicrs is None:
        for i in range(0, number_of_bins):
            count = 0
            for b in bin_indices_discr:
                if b == i:
                    count += 1
            bins_support.append(count)
    else:
        for i in range(0, number_of_bins):
            merged_bin = merge(previous_dims, previous_dicrs, discretized_dim, initial_bins[i])
            merged_bins_points_ids.append(merged_bin)
            count = len(merged_bin)
            bins_support.append(count)

    # print("initial bins")
    # print(str(initial_bins) + "\n")

    # print("bin to merge")
    # print(str(initial_bins_cond[0]))

    # TODO: does not make sense if discr dim is multidimensional
    # TODO: bin indices should condsider merged discr of two dimensions!!!
    # conditioned_dim_bins = discretize_conditioned_dimension(bin_indices_discr, conditioned_dim)

    # TODO: doesn't make if discr dim is one dimension
    joint = join_discretizations(initial_bins, initial_bins_cond, 1)

    # print("joint")
    # print(str(joint) + "\n")

    for j in range(0, number_of_bins):

        # TODO: add extension of previously discretized dimensions
        bins_to_join_discr_dim = [initial_bins[j]]
        bins_to_join_cond_dim = [initial_bins_cond[j]]
        joined_support = bins_support[j]

        if previous_dicrs is not None:
            merged_points_ids_cond = merged_bins_points_ids[j]

        for i in range(j, number_of_bins):

            if j == i:
                '''
                cond_ce = conditional_ce(conditioned_dim, discretized_dim, bins_support[i],
                                         current_bin=initial_bins_cond[i]) '''
                if previous_dicrs is None:
                    cond_ce = conditional_ce(conditioned_dim, discretized_dim, bins_support[i],
                                             current_bin=initial_bins_cond[i])
                else:
                    cond_ce = conditional_ce(conditioned_dim, discretized_dim, bins_support[i],
                                             merged_bin_points_ids=merged_bins_points_ids[i])

            else:
                bins_to_join_discr_dim.append(initial_bins[i])
                joined_bin_discr = join_bins(bins_to_join_discr_dim)
                joined_support += bins_support[i]

                if previous_dicrs is None:

                    bins_to_join_cond_dim.append(initial_bins_cond[i])
                    joined_bin_cond = join_bins(bins_to_join_cond_dim)
                    cond_ce = conditional_ce(conditioned_dim, discretized_dim, joined_support, current_bin=joined_bin_cond)

                # for point in conditioned_dim_bins[i]:
                #    bins_to_join_cond_dim.append(point)
                # cond_ce = one_dim_conditioned_ce(bins_to_join_cond_dim, discretized_dim, joined_support)
                #cond_ce = conditional_ce(conditioned_dim, discretized_dim, joined_support, current_bin=joined_bin_cond)

                else:
                    for p in merged_bins_points_ids[i]:
                        merged_points_ids_cond.append(p)
                    cond_ce = conditional_ce(conditioned_dim, discretized_dim, joined_support, merged_bin_points_ids=merged_points_ids_cond)

            # print("cond_ce = " + str(cond_ce)) #!!!!
            f[j][i] = cond_ce

            if j == 0:
                val[j][i] = f[j][i]
                if i == 0:
                    final_bins[j][i] = [initial_bins[0]]
                else:
                    final_bins[j][i] = [joined_bin_discr]

    for l in range(1, number_of_bins):

        for i in range(l, number_of_bins):
            omega = {}

            for j in range(l - 1, i):
                o = ((s[i] - s[j]) / s[i]) * f[j + 1][i] + (s[j] / s[i]) * val[l - 1][j]
                #print("((" + str(s[i]) + "-" + str(s[j]) + ") / " + str(s[i]) + ") * " + str(f[j+1][i]) + " +
                # (" + str(s[j]) + " / " + str(s[i]) + ") * " + str(val[l-1][j]))
                #print("o = " + str(o))

                omega[j] = o

            min_omega = min(sorted(omega.values()))
            pos = min(omega, key=omega.get)
            val[l][i] = min_omega

            if final_bins[l][i] is not None:

                new_bins = []
                for m in range(0, len(final_bins[l][i])):
                    new_bins.append(final_bins[l][i][m])

                for n in range(0, len(final_bins[l - 1][pos])):
                    new_bins.append(final_bins[l - 1][pos][n])

                final_bins[l][i] = new_bins

            else:
                if final_bins[l - 1][pos] is not None:

                    new_bins = []
                    for n in range(0, len(final_bins[l - 1][pos])):
                        new_bins.append(final_bins[l - 1][pos][n])

                    final_bins[l][i] = new_bins

            union_a_k = [initial_bins[k] for k in range(pos + 1, i + 1)]

            if final_bins[l][i] is not None:

                new_bins = []
                for m in range(0, len(final_bins[l][i])):
                    new_bins.append(final_bins[l][i][m])

                new_bins.append(join_bins(union_a_k))
                final_bins[l][i] = new_bins

            else:
                final_bins[l][i] = join_bins(union_a_k)

    regul_dict = {}
    for l in range(0, number_of_bins):
        # TODO: check if it is optimal
        # for i in range(l, number_of_bins):

        entropy = univariate_shannon_entropy_discretized(discretized_dim, final_bins[l][number_of_bins - 1])

        # reg_value = val[l][number_of_bins-1] / one_dim_ce_light(discretized_dim) +\
        #            shannon_entropy(np.array(discretized_dim)) / (math.log2(number_of_bins) + sumLog)
        reg_value = val[l][number_of_bins - 1] / one_dim_ce_light(discretized_dim) + entropy/(math.log2(number_of_bins)
                                                                                              + sumLog)

        #print("delta = " + str(l))
        #print("entropy = " + str(entropy))
        #print("reg value = " + str(reg_value) + "\n")

        regul_dict[l] = reg_value

    delta_min = min(regul_dict, key=regul_dict.get)

    return val, final_bins, delta_min


def compute_score_one_dim(data, dim_x, conditioned, discretized,  number_of_bins, sumLog, previous_dicrs=None, previous_dims=None):
    ces, order_of_columns, candidate_set = ce_per_dimension(data)

    #conditioned = sorted(data[:, order_of_columns[dim_x - 1] - 1])
    # print("conditioned = " + str(order_of_columns[dim_x - 1] - 1))
    #discretized = sorted(data[:, order_of_columns[dim_x - 2] - 1])
    # print("discretized =  " + str(order_of_columns[dim_x - 2] - 1) + "\n")

    # two_dims = join_dimensions([discretized, conditioned])

    val, final_bins, delta_min = \
        compute_f_val_b(data, order_of_columns, number_of_bins, dim_x, conditioned, discretized, sumLog, previous_dicrs, previous_dims)

    print(val)
    #print("\n")
    #print("min delta = " + str(delta_min))
    #print("min value = " + str(val[delta_min][number_of_bins - 1]) + "\n")

    result = [ces[order_of_columns[dim_x - 1]] - val[delta_min][number_of_bins - 1],
              math.log2(len(final_bins[delta_min][number_of_bins - 1]))]

    print("CE = " + str(ces[order_of_columns[dim_x - 1]]))
    #print("result = " + str(result))
    return result, final_bins[delta_min][number_of_bins - 1]


def compute_score(data, number_of_bins):

    ces, order_of_columns, candidate_set = ce_per_dimension(data)
    print("order of columns: " + str(order_of_columns) + "\n")
    number_of_dimensions = data.shape[1]

    sumCES = 0
    sumScore = 0
    sumLog = 0
    I = None
    I_discretization = None

    for column_id in order_of_columns:

        print("id:" + str(order_of_columns.index(column_id)))

        if order_of_columns.index(column_id) > 1:
            I = join_dimensions(I)

        conditioned = sorted(data[:, order_of_columns[column_id - 1] - 1])
        discretized = sorted(data[:, order_of_columns[column_id - 2] - 1])

        print("discretized = " + str(discretized))

        res, bins = compute_score_one_dim(data, column_id, conditioned, discretized, number_of_bins, sumLog, previous_dims=I, previous_dicrs=I_discretization)
        print("res = " + str(res))
        print("bins = " + str(bins))

        if I is not None and order_of_columns.index(column_id) > 1:
            I.append(discretized)
            print("I = " + str(I))
            #I = join_dimensions(I)
            #print("dims are joined")
        elif I is None:
            I = discretized
        else:
            I = [I, discretized]

        if I_discretization is not None:
            I_discretization = join_discretizations(I_discretization, bins, order_of_columns.index(column_id))
        else:
            I_discretization = bins

        #print("I = " + str(I))
        print("bins = " + str(bins))
        print("I dicrs = " + str(I_discretization) + "\n")
        sumLog += res[1]

# implement two ways of discretization - uds and point/expectation version

# def one_dim_conditional_discretization(conditioned_dimension, discretize_dimension, initial_number_of_bins):


# TODO: sort canidate set w.r.t. ce values
candidate_set = {}
'''data = read_data(path.abspath('test_data.txt'))
rank_each_dimension(data)

sumLog = 0

# ! dim can start only from the second
#score = compute_score_one_dim(data, 2, 4, sumLog)

compute_score(data, 5)'''
