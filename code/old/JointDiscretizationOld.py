'''
def create_bins_cond_dim(cond_dim, given_dim, modified_discretization):
    cond_dim_discretization = []

    for b in modified_discretization:
        cond_dim_bin = []
        for point_id in range(0, len(given_dim)):
            if b[0] < given_dim[point_id] <= b[1]:
                cond_dim_bin.append(cond_dim[point_id])
        cond_dim_discretization.append(cond_dim_bin)

    # not an interval, but a list of points
    return cond_dim_discretization '''

'''
def bin_support(dimension, bin):
    support = 0
    for point in dimension:
        if bin[0] < point <= bin[1]:
            support += 1

    return support'''

'''
def discr_support(discretization, dimension=[], intervals=True):
    support_arr = []

    for b in discretization:
        if intervals:
            b_support = bin_support(dimension, b)
        else:
            b_support = len(b)
        support_arr.append(b_support)

    return support_arr'''

'''
def join_dimensions(dimensions):

    number_of_points = len(dimensions[0])
    joined_dimensions = []

    for p_idx in range(0, number_of_points):
        tuple_point = ()
        for d_idx in range(0, len(dimensions)):
            tuple_point += (dimensions[d_idx][p_idx],)
        joined_dimensions.append(tuple_point)

    return joined_dimensions'''

'''
def discretization_in_points(dimension, discretization):
    discr_points = []
    discretization = sorted(discretization)

    for bin in discretization:
        b_points = []
        for point in dimension:
            if bin[0] < point <= bin[1]:
                b_points.append(point)
        discr_points.append(b_points)

    return discr_points'''

# todo print('optimal disc', len([[min([data[prev][i] for i in ind]), max([data[prev][i] for i in ind])] for ind in discs[opt_l]]), '\n')
'''
def discr_points_to_intervals(discretization):
    discr_intervals = []
    margin = 0.00000000999999

    for b in discretization:
        new_min_b = ()
        if discretization.index(b) == 0:
            if isinstance(b[0], tuple):
                if len(b) == 1:
                    for point in b[0]:
                        point -= margin
                        new_min_b += (point,)
                    b_interval = [new_min_b, b[0]]
                else:
                    for point in min(b):
                        point -= margin
                        new_min_b += (point,)
                    b_interval = [new_min_b, max(b)]
            else:
                b_interval = [min(b) - margin, max(b)]
        else:
            if len(b) == 1:
                if isinstance(b[0], tuple):
                    for point in b[0]:
                        point -= margin
                        new_min_b += (point,)
                    b_interval = [new_min_b, b[0]]
                else:
                    b_interval = [min(b) - margin, max(b)]
            else:
                idx = discretization.index(b)
                b_interval = [discr_intervals[idx - 1][1], max(b)]
        discr_intervals.append(b_interval)

    return discr_intervals'''

'''
# create multivariate representation of a discretization
def project_discr(discr_dimension, discretization, interval_discr=True):
    projection = []

    if interval_discr:
        discr_in_points = discretization_in_points(discr_dimension, discretization)
    else:
        discr_in_points = discretization

    # for bin in discretization:
    for bin in discr_in_points:

        projected_bin = []
        for point_idx in range(0, len(discr_dimension)):
            if discr_dimension[point_idx] in bin:
                if len(projection) != 0 and point_idx not in projection[-1]:
                    projected_bin.append(point_idx)
                elif len(projection) == 0:
                    projected_bin.append(point_idx)
        projection.append(projected_bin)

    return projection'''

# todo use intersection instead
'''
# TODO: project the new dim first on other dims without discrs, project old dim on the new one without discr, then merge
def merge_projections(projection_prev_discr, projection_new_discr, cond_dim=None, projected_dims=None, single_bin_projection=False):
    len_prev_discr = len(projection_prev_discr)
    support = discr_support(projection_prev_discr, intervals=False)
    s = sum(support)
    len_new_discr = len(projection_new_discr)

    joined = []

    if single_bin_projection:
        b_new_discr = projection_new_discr[0]
        for bin_prev_discr in projection_prev_discr:

            if len(bin_prev_discr) == 0 or len(b_new_discr) == 0:
                continue

            if bin_prev_discr[0] < b_new_discr[0]:
                cut_off_bin = []
                new_bin_prev_discr = []
                for idx in bin_prev_discr:
                    if idx < b_new_discr[0]:
                        cut_off_bin.append(idx)
                    else:
                        new_bin_prev_discr.append(idx)

                joined.append(cut_off_bin)
                bin_prev_discr = new_bin_prev_discr

            if len(bin_prev_discr) >= len(b_new_discr):
                common_points = sorted(list(set(bin_prev_discr).intersection(b_new_discr)))
                difference = sorted(list(set(bin_prev_discr).difference(b_new_discr)))
                difference1 = sorted(list(set(b_new_discr).difference(bin_prev_discr)))
            else:
                common_points = sorted(list(set(b_new_discr).intersection(bin_prev_discr)))
                difference = sorted(list(set(b_new_discr).difference(bin_prev_discr)))
                difference1 = sorted(list(set(bin_prev_discr).difference(b_new_discr)))

            if common_points not in joined and len(common_points) != 0:
                add_common = True
                if len(joined) != 0:
                    for bin in joined:
                        if set(common_points).issubset(set(bin)):
                            add_common = False
                            common = sorted(list(set(bin).intersection(common_points)))
                            diff = sorted(list(set(bin).difference(common_points)))
                            joined.remove(bin)
                            if len(common) != 0 and common not in joined:
                                joined.append(common)
                            if len(diff) != 0 and diff not in joined:
                                joined.append(diff)
                if add_common:
                    joined.append(common_points)

            if difference not in joined and len(difference) != 0:
                add_diff = True
                if len(joined) != 0:
                    for bin in joined:
                        if set(bin).issubset(set(difference)):
                            add_diff = False
                if add_diff:
                    joined.append(difference)

            if difference1 not in joined and len(difference1) != 0:
                add_diff = True
                if len(joined) != 0:
                    for bin in joined:
                        if set(bin).issubset(set(difference1)):
                            add_diff = False
                if add_diff:
                    joined.append(difference1)

        joined_support = discr_support(joined, intervals=False)
        #return joined

    # TODO: one bin gets lost
    else:
        if len_prev_discr == len_new_discr:
            number_of_bins = len_prev_discr

            for b_id in range(0, number_of_bins):
                bin_prev_discr = projection_prev_discr[b_id]
                b_new_discr = projection_new_discr[b_id]

                if len(bin_prev_discr) >= len(b_new_discr):
                    common_points = sorted(list(set(bin_prev_discr).intersection(b_new_discr)))
                    difference = sorted(list(set(bin_prev_discr).difference(b_new_discr)))
                else:
                    common_points = sorted(list(set(b_new_discr).intersection(bin_prev_discr)))
                    difference = sorted(list(set(b_new_discr).difference(bin_prev_discr)))

                if common_points not in joined and len(common_points) != 0:
                    add_common = True
                    if len(joined) != 0:
                        for bin in joined:
                            if set(common_points).issubset(set(bin)):
                                add_common = False
                                common = sorted(list(set(bin).intersection(common_points)))
                                diff = sorted(list(set(bin).difference(common_points)))
                                joined.remove(bin)
                                if len(common) != 0 and common not in joined:
                                    joined.append(common)
                                if len(diff) != 0 and diff not in joined:
                                    joined.append(diff)
                    if add_common:
                        joined.append(common_points)

                if difference not in joined and len(difference) != 0:
                    add_diff = True
                    if len(joined) != 0:
                        for bin in joined:
                            if set(bin).issubset(set(difference)):
                                add_diff = False
                    if add_diff:
                        joined.append(difference)

        elif len_prev_discr == 1 or len_new_discr == 1:

            if len_prev_discr > len_new_discr:
                number_of_bins = len_prev_discr
                b_new_discr = projection_new_discr[0]

                for b_id in range(0, number_of_bins):
                    bin_prev_discr = projection_prev_discr[b_id]
                    common_points = sorted(list(set(b_new_discr).intersection(bin_prev_discr)))

                    if common_points not in joined and len(common_points) != 0:
                        add_common = True
                        if len(joined) != 0:
                            for bin in joined:
                                if set(common_points).issubset(set(bin)):
                                    add_common = False
                                    common = sorted(list(set(bin).intersection(common_points)))
                                    diff = sorted(list(set(bin).difference(common_points)))
                                    joined.remove(bin)
                                    if len(common) != 0 and common not in joined:
                                        joined.append(common)
                                    if len(diff) != 0 and diff not in joined:
                                        joined.append(diff)
                        if add_common:
                            joined.append(common_points)

            else:
                number_of_bins = len_new_discr
                bin_prev_discr = projection_prev_discr[0]

                for b_id in range(0, number_of_bins):
                    b_new_discr = projection_new_discr[b_id]
                    common_points = sorted(list(set(bin_prev_discr).intersection(b_new_discr)))

                    if common_points not in joined and len(common_points) != 0:
                        add_common = True
                        if len(joined) != 0:
                            for bin in joined:
                                if set(common_points).issubset(set(bin)):
                                    add_common = False
                                    common = sorted(list(set(bin).intersection(common_points)))
                                    diff = sorted(list(set(bin).difference(common_points)))
                                    joined.remove(bin)
                                    if len(common) != 0 and common not in joined:
                                        joined.append(common)
                                    if len(diff) != 0 and diff not in joined:
                                        joined.append(diff)
                        if add_common:
                            joined.append(common_points)
        else:
            if len_prev_discr < len_new_discr:
                number_of_bins = len_prev_discr

                for b_id in range(0, number_of_bins):
                    bin_prev_discr = projection_prev_discr[b_id]
                    iterator = iter(projection_new_discr)

                    while iterator.__length_hint__() > 0:
                        common_points = sorted(list(set(bin_prev_discr).intersection(next(iterator))))

                        if common_points not in joined and len(common_points) != 0:
                            add_common = True
                            if len(joined) != 0:
                                for bin in joined:
                                    if set(common_points).issubset(set(bin)):
                                        add_common = False
                                        common = sorted(list(set(bin).intersection(common_points)))
                                        diff = sorted(list(set(bin).difference(common_points)))
                                        joined.remove(bin)
                                        if len(common) != 0 and common not in joined:
                                            joined.append(common)
                                        if len(diff) != 0 and diff not in joined:
                                            joined.append(diff)
                            if add_common:
                                joined.append(common_points)
            else:
                number_of_bins = len_new_discr

                for b_id in range(0, number_of_bins):
                    b_new_discr = projection_new_discr[b_id]
                    iterator = iter(projection_prev_discr)

                    while iterator.__length_hint__() > 0:
                        common_points = sorted(list(set(b_new_discr).intersection(next(iterator))))

                        if common_points not in joined and len(common_points) != 0:
                            add_common = True
                            if len(joined) != 0:
                                for bin in joined:
                                    if set(common_points).issubset(set(bin)):
                                        add_common = False
                                        common = sorted(list(set(bin).intersection(common_points)))
                                        diff = sorted(list(set(bin).difference(common_points)))
                                        joined.remove(bin)
                                        if len(common) != 0 and common not in joined:
                                            joined.append(common)
                                        if len(diff) != 0 and diff not in joined:
                                            joined.append(diff)
                            if add_common:
                                joined.append(common_points)

    if cond_dim is not None:

        joined_points = []
        cond_dim = sorted(cond_dim)

        for b in joined:
            points_b = []
            for point_id in b:
                points_b.append(cond_dim[point_id])

            joined_points.append(points_b)  # merged discr. w.r.t. conditioned dim

        if projected_dims is None:
            return sorted(joined_points)

    if projected_dims is not None:
        joined_tuples = []
        for b in joined:
            points_b = []
            for point_id in b:
                points_tuple = ()
                for dim in projected_dims:
                    points_tuple += (dim[point_id],)  # TODO: check the condition
                points_b.append(points_tuple)
            joined_tuples.append(points_b)  # discr. in tuples w.r.t. projected dims.
        return sorted(joined_points), sorted(joined_tuples)
    else:
        return sorted(joined)  # return just the points ids '''

# todo intersection??
'''
def modify_discretization(opt_discr, current_bin_id, previous_step_discr=[]):
    bins_to_merge1 = []
    bins_to_merge2 = []
    current_bin = opt_discr[current_bin_id]

    # creating discretization, which contains only of current bin, all others - merged
    for bin_id in range(0, len(opt_discr)):

        if bin_id < current_bin_id:
            bins_to_merge1.append(opt_discr[bin_id])

        elif bin_id > current_bin_id:
            bins_to_merge2.append(opt_discr[bin_id])

        elif bin_id == 0:
            continue

    if len(bins_to_merge1) != 0 and len(bins_to_merge2) != 0:
        current_bin_discr = [preprocessing.join_bins(bins_to_merge1), opt_discr[current_bin_id],
                             preprocessing.join_bins(bins_to_merge2)]

    elif len(bins_to_merge1) == 0 and len(bins_to_merge2) != 0:
        current_bin_discr = [opt_discr[current_bin_id], preprocessing.join_bins(bins_to_merge2)]

    elif len(bins_to_merge1) != 0 and len(bins_to_merge2) == 0:
        current_bin_discr = [preprocessing.join_bins(bins_to_merge1), opt_discr[current_bin_id]]

    current_bin_position = current_bin_discr.index(current_bin)

    # print("\n" + "current bin: " + str(opt_discr[current_bin_id]))
    # print("current bin discr: " + str(current_bin_discr))

    modified = []

    if len(previous_step_discr) == 1:
        modified = current_bin_discr
        return modified

    for b in previous_step_discr:

        if b[0] < current_bin[0] and b[1] < current_bin[1]:
            modified.append(b)

        elif b[0] < current_bin[0] and b[1] == current_bin[1]:
            if [b[0], current_bin[0]] in current_bin_discr:
                modified.append([b[0], current_bin[0]])

            if [b[0], current_bin[0]] not in previous_step_discr and [b[0], current_bin[0]] not in current_bin_discr and \
                            [b[0], current_bin[0]] not in opt_discr:
                modified.append([b[0], current_bin[0]])

            for b_opt in opt_discr:
                if (b_opt in previous_step_discr or b_opt in current_bin_discr) and b_opt not in modified:
                    modified.append(b_opt)
                elif len(modified) != 0 and modified[-1][1] <= b_opt[0] <= current_bin[0]:
                    modified.append(b_opt)
                elif b_opt[1] >= current_bin[1]:
                    break

        elif b[1] == current_bin[1]:
            sliced_bins = current_bin_discr[0:current_bin_position + 1]
            for sliced_b in sliced_bins:
                add_bin = True
                for i in range(0, len(modified)):
                    if sliced_b[0] == modified[i][0]:
                        add_bin = False
                        break
                if add_bin:
                    modified.append(sliced_b)

        elif b[0] == current_bin[0] and b[1] > current_bin[1]:
            for b_opt in opt_discr:
                if len(modified) == 0:
                    modified.append(b_opt)
                elif modified[-1][1] < b_opt[1] <= b[1] and (
                                b_opt in previous_step_discr or b_opt in current_bin_discr):
                    modified.append(b_opt)

            sliced_bins = current_bin_discr[current_bin_discr.index(modified[-1]) + 1: len(previous_step_discr)+1]
            for sliced_b in sliced_bins:
                if modified[-1][1] < b[1] < sliced_b[1]:
                    modified.append([modified[-1][1], b[1]])
                    if modified[-1][1] == previous_step_discr[-1][0]:
                        modified.append(previous_step_discr[-1])
                else:
                    modified.append(sliced_b)

        elif b[0] == current_bin[0] and b[1] < current_bin[1]:
            modified.append(current_bin)

        elif b[0] < current_bin[0] and b[1] > current_bin[1]:
            if len(modified) != 0 and modified[-1][1] < current_bin[0] and [modified[-1][1],
                                                                            current_bin[0]] in opt_discr:
                modified.append([modified[-1][1], current_bin[0]])
            elif [b[0], current_bin[0]] in opt_discr:
                modified.append([b[0], current_bin[0]])

            for curr_b in opt_discr:
                if curr_b == current_bin and len(current_bin_discr) > len(previous_step_discr):
                    if b == previous_step_discr[0] and current_bin_discr[current_bin_discr.index(curr_b)+1][0] >= b[1]:  # TODO: check the last condition
                        sliced_bins = current_bin_discr[
                                      current_bin_discr.index(curr_b): len(current_bin_discr)+1]
                        for sliced_b in sliced_bins:
                            modified.append(sliced_b)
                    elif b != previous_step_discr[0] and current_bin_discr[current_bin_discr.index(curr_b)+1][0] >= b[0]:
                        sliced_bins = current_bin_discr[
                                      current_bin_discr.index(curr_b): len(current_bin_discr)+1]
                        for sliced_b in sliced_bins:
                            modified.append(sliced_b)
                        break

                # TODO 0 or 1
                if len(modified) != 0 and modified[-1][1] < curr_b[0] and curr_b not in previous_step_discr and curr_b \
                        not in current_bin_discr and curr_b not in opt_discr:
                    modified.append([modified[-1][1], curr_b[0]])  # 0 or 1!
                    continue

                add_bin = True
                if len(modified) == 0:
                    if curr_b not in current_bin_discr and curr_b not in previous_step_discr:
                        modified.append(min(current_bin_discr[0], previous_step_discr[0]))
                    else:
                        modified.append(curr_b)
                for i in range(0, len(modified)):
                    if curr_b[0] == modified[i][0]:
                        add_bin = False
                        break

                    if add_bin and curr_b == current_bin:
                        modified.append(curr_b)
                        break

                    if add_bin and (curr_b[0] != modified[-1][1] or curr_b[1] != current_bin[0]):
                        if modified[-1][1] < current_bin[0]:
                            modified.append([modified[-1][1], current_bin[0]])
                            break

                    if add_bin and curr_b not in modified and \
                            (len(modified) == 0 or previous_step_discr.index(b) + 1 == len(previous_step_discr)) \
                            and modified[-1][1] < curr_b[0]:
                        modified.append(curr_b)
                        break

                    if add_bin and curr_b not in modified and previous_step_discr.index(b) + 1 < len(
                            previous_step_discr):
                        next_bin = previous_step_discr[previous_step_discr.index(b)+1]
                        if modified[-1][1] == curr_b[0] and curr_b[1] == next_bin[0]:
                            modified.append(curr_b)
                            break

        elif b[0] > current_bin[0] and b[1] > current_bin[1]:
            if len(modified) != 0:
                if modified[-1][1] < b[0]:
                    modified.append([modified[-1][1], b[0]])

                if modified[-1][1] == b[0]:
                    modified.append(b)
            elif b > current_bin and current_bin in opt_discr:
                modified.append(current_bin)

        elif len(modified) != 0 and modified[-1][1] != opt_discr[-1][1]:
            sliced_bins = current_bin_discr[current_bin_position + 1: len(current_bin_discr)]

            for sliced_b in sliced_bins:
                if modified[-1][1] == sliced_b[0] and sliced_b[0] >= previous_step_discr[-1][0]:
                    modified.append(sliced_b)

    return modified '''


# discretization are given in points ids
def joint_discr(data, cond_dim_id, given_dims_ids, discretizations, significance_threshold):
    # conditioned_dim = sorted(data[:, cond_dim_id])
    conditioned_dim = data[:, cond_dim_id]
    number_of_points = len(conditioned_dim)
    # given_dims = []  # given dims are stored in a list, one after another

    # given_dims = pd.DataFrame(data[:, dim_id] for dim_id in given_dims_ids).T
    '''
    for dim_id in given_dims_ids:
        #dim = sorted(data[:, dim_id])
        dim = data[:, dim_id]
        given_dims.append(dim)'''
    '''
    bin_iterators = []  # iterator for each dim.discr.; iteration over bins
    for discr in discretizations:
        it = discr.itertuples()
        #it = iter(discr)
        bin_iterators.append(it)'''

    # do while there are cut points

    current_combination = (cond_dim_id, {})  # ("x0", {"x1": [], "x2": []}), current_combination combination

    do_joint_discr = True
    insignificant_counter = 0
    bin_counter = 0
    # subextension = False
    # subextension_indices = []

    while do_joint_discr:
        cond_ces = {}

        for dim in range(len(given_dims_ids)):  # iterate over the set of given dimensions
            # for dim_id in given_dims_ids:

            dim_id = given_dims_ids[dim]
            # given_dim = given_dims[dim]  # current_combination dimension in points
            discr = pd.DataFrame(discretizations[dim]).dropna(axis=0)

            discr_length = len(discr)  # number of bins
            if bin_counter > discr_length - 1:
                continue
            b = discr.T[bin_counter][dim]

            # bin_iterator = bin_iterators[dim_id_idx]  # iterator over of discr(bins) of current_combination dimension

            if dim_id not in list(current_combination[1].keys()):
                # create new bin iterator, if the prev. is over. TODO: check if this dim. with the next bin are already included!
                '''
                try:
                    b = next(bin_iterator)
                except StopIteration:
                    bin_iterator = iter(discretizations[dim_id_idx])
                    b = next(bin_iterator)'''

                # index of the current_combination bin of current_combination given dim
                # index_of_bin = list(discretizations[dim_id_idx]).index(b)
                index_of_bin = bin_counter

                if discr_length == 1:
                    transformed_discr = [b]
                    # transformed_discr = discretizations[dim_id_idx]
                else:
                    if index_of_bin == 0:
                        discr_r = discr.T[1][dim]
                        for i in range(2, discr_length):
                            # for i in discr[2:].T:
                            discr_r = discr_r.union(discr.T[i][dim])
                        transformed_discr = [b, discr_r]

                        # transformed_discr = [b, [discr.T[1][dim].union(discr.T[i][dim]) for i in discr[2:].T][0]]

                        # transformed_discr =
                        # transformed_discr = [b, preprocessing.join_bins(
                        #    discretizations[dim_id_idx][index_of_bin + 1:discr_length])]

                    elif 0 < index_of_bin < discr_length - 1:
                        discr_l = discr.T[0][dim]

                        for i in range(0, index_of_bin):
                            # for i in discr[0:index_of_bin-1]:
                            discr_l = discr_l.union(discr.T[i][dim])

                        discr_r = discr.T[index_of_bin + 1][dim]
                        for i in range(index_of_bin + 1, discr_length):
                            # for i in discr[index_of_bin + 1:].T:
                            discr_r = discr_r.union(discr.T[i][dim])

                        transformed_discr = [discr_l, b, discr_r]

                        # transformed_discr = \
                        #    [[discr.T[0][dim].union(discr.T[i][dim]) for i in discr[0:index_of_bin + 1].T][0], b,
                        #     [discr.T[index_of_bin + 1][dim].union(discr.T[i][dim]) for i in
                        #      discr[index_of_bin + 1:].T][0]]

                        # transformed_discr = [preprocessing.join_bins(discretizations[dim_id_idx][0:index_of_bin]), b,  # !
                        #                     preprocessing.join_bins(
                        #                        discretizations[dim_id_idx][index_of_bin + 1:discr_length])]
                    else:
                        discr_l = discr.T[0][dim]
                        for i in range(0, index_of_bin):
                            # for i in discr[0:index_of_bin - 1]:
                            discr_l = discr_l.union(discr.T[i][dim])
                        transformed_discr = [discr_l, b]
                        # transformed_discr = [
                        #    [discr.T[0][dim].union(discr.T[i][dim]) for i in discr[0:index_of_bin].T][0], b]
                        # transformed_discr = [preprocessing.join_bins(discretizations[dim_id_idx][0:discr_length - 1]), b]
                assert (sum([len(d) for d in transformed_discr]) == number_of_points), "some points are missing"

                if len(current_combination[1]) == 0:
                    cond_ce = conditional_ce_all_bins(conditioned_dim, transformed_discr)
                    # print(dim_id)
                    cond_ces[dim_id] = {cond_ce: transformed_discr}

                    # w.r.t. point bins
                    # cond_dim_bins = create_bins_cond_dim(conditioned_dim, given_dim, transformed_discr)
                    # w.r.t. interval bins
                    # bins_support = discr_support(transformed_discr, dimension=given_dim)
                    # print("bins sum: " + str(sum(bins_support)))
                    # cond CE given one cut point (2 bins)
                    # cond_ce = conditional_ce_all_bins(cond_dim_bins, number_of_points, bins_support)

                else:
                    incl_dim_id = next(iter(current_combination[1]))
                    prev_discr = list(current_combination[1][incl_dim_id].values())[0]

                    discr_ = [i.intersection(j) for i in prev_discr for j in transformed_discr]
                    merged_discr = [d for d in discr_ if len(d) != 0]

                    assert (sum([len(d) for d in merged_discr]) == number_of_points), \
                        "some points are missing, dim in non empty curr.c."

                    cond_ce = conditional_ce_all_bins(conditioned_dim, merged_discr)

                    if incl_dim_id == dim_id:
                        cond_ces[dim_id] = {cond_ce: merged_discr}
                    elif isinstance(incl_dim_id, tuple) and dim_id in incl_dim_id:
                        cond_ces[incl_dim_id] = {cond_ce: merged_discr}
                    elif isinstance(incl_dim_id, tuple) and dim_id not in incl_dim_id:
                        cond_ces[incl_dim_id + (dim_id,)] = {cond_ce: merged_discr}
                    else:
                        cond_ces[(incl_dim_id, dim_id)] = {cond_ce: merged_discr}

                    """
                    if isinstance(incl_dim_id, tuple) and dim_id not in incl_dim_id:

                        #projected_ds = [sorted(data[:, i]) for i in incl_dim_id]
                        #c1 = len(projected_ds)
                        # projection_included_dim = project_discr(join_dimensions(projected_ds), used_bins)
                        # projection_new_dim = project_discr(given_dim, transformed_discr)

                        #projected_ds.append(sorted(data[:, dim_id]))
                        #c2 = len(projected_ds)

                        #merged_projection, merged_discr = merge_projections(projection_included_dim, projection_new_dim,
                        #                                                    cond_dim=conditioned_dim,
                        #                                                    projected_dims=projected_ds)

                        # bins_support = discr_support(merged_projection, intervals=False)
                        # print("bins sum: " + str(sum(bins_support)))
                        cond_ce = conditional_ce_all_bins(merged_projection, number_of_points, bins_support)
                        cond_ces[incl_dim_id + (given_dims_ids[dim_id_idx],)] = {
                            cond_ce: sorted(discr_points_to_intervals(merged_discr))}

                    elif isinstance(incl_dim_id, tuple) and dim_id in incl_dim_id:
                        # project current dim bin on the tuple, merge, calc ce (if it is already included into the tuple)

                        # extract the corresponding discr. out of the used_bins
                        prev_discr_dim_id = []
                        index = incl_dim_id.index(dim_id)

                        for ub in used_bins:
                            if len(prev_discr_dim_id) != 0:
                                extracted_bin = [prev_discr_dim_id[-1][1], ub[1][index]]
                            else:
                                extracted_bin = [ub[0][index], ub[1][index]]

                            if abs(extracted_bin[0] - extracted_bin[1]) > 1:
                                if extracted_bin not in prev_discr_dim_id:
                                    prev_discr_dim_id.append(extracted_bin)

                        # check if this bin has already been included
                        if b in prev_discr_dim_id:
                            continue

                        # index_of_bin = list(discretizations[dim_id_idx]).index(b)
                        # optimal_discr = discretizations[dim_id_idx]
                        modified_discr = modify_discretization(optimal_discr, index_of_bin, prev_discr_dim_id)

                        # projected_ds = [sorted(data[:, i]) for i in incl_dim_id]

                        # projection_included_dim = project_discr(join_dimensions(projected_ds), used_bins)
                        # projection_new_dim = project_discr(given_dim, modified_discr)
                        merged_projection, merged_discr = merge_projections(projection_included_dim, projection_new_dim,
                                                                            cond_dim=conditioned_dim,
                                                                            projected_dims=projected_ds)

                        # bins_support = discr_support(merged_projection, intervals=False)
                        # print("bins sum: " + str(sum(bins_support)))
                        cond_ce = conditional_ce_all_bins(merged_projection, number_of_points, bins_support)
                        cond_ces[incl_dim_id] = {cond_ce: sorted(discr_points_to_intervals(merged_discr))}

                    else:   
                        # projection_included_dim = project_discr(sorted(data[:, incl_dim_id]), used_bins)
                        # projection_new_dim = project_discr(given_dim, transformed_discr)
                        if incl_dim_id == dim_id:
                        # merged_projection, merged_discr = merge_projections(projection_included_dim, projection_new_dim,
                        #                                                    cond_dim=conditioned_dim,
                        #                                                    projected_dims=[sorted(data[:, incl_dim_id])])
                        else:
                        merged_projection, merged_discr = merge_projections(projection_included_dim, projection_new_dim,
                                                                            cond_dim=conditioned_dim,
                                                                            projected_dims=[sorted(data[:, incl_dim_id]),
                                                                                            sorted(data[:, dim_id])])
                        bins_support = discr_support(merged_projection, intervals=False)
                        print("bins sum: " + str(sum(bins_support)))
                        cond_ce = conditional_ce_all_bins(merged_projection, number_of_points, bins_support)

                        if incl_dim_id == given_dims_ids[dim_id_idx]:
                            cond_ces[given_dims_ids[dim_id_idx]] = {
                                cond_ce: sorted(discr_points_to_intervals(merged_discr))}
                        else:
                            cond_ces[(incl_dim_id, given_dims_ids[dim_id_idx])] = {
                                cond_ce: sorted(discr_points_to_intervals(merged_discr))}"""
            else:
                prev_discr = list(current_combination[1][dim_id].values())[0]
                merged_discr = [i.intersection(j) for j in prev_discr for i in
                                [b, pd.DataFrame(data).index.difference(b)]]

                transformed_discr = [d for d in merged_discr if len(d) != 0]
                assert (sum([len(d) for d in transformed_discr]) == number_of_points), \
                    "some points are missing, dim not in curr.c."

                cond_ce = conditional_ce_all_bins(conditioned_dim, transformed_discr)
                cond_ces[dim_id] = {cond_ce: transformed_discr}

                '''
                try:
                    b = next(bin_iterator)
                except StopIteration:
                    bin_iterator = iter(discretizations[dim_id_idx])
                    b = next(bin_iterator)'''
                # index_of_bin = bin_counter
                # index_of_bin = list(discretizations[dim_id_idx]).index(b)
                # optimal_discr = discretizations[dim_id_idx]
                '''
                if len(optimal_discr) != 1:
                    modified_discr = modify_discretization(optimal_discr, index_of_bin, prev_discr)
                if len(optimal_discr) == 1 and index_of_bin == 0:
                    modified_discr = optimal_discr
                cond_dim_bins = create_bins_cond_dim(conditioned_dim, given_dim, modified_discr)
                bins_support = discr_support(modified_discr, dimension=given_dim)'''
                # print("bins sum: " + str(sum(bins_support)))
                # cond_ce = conditional_ce_all_bins(cond_dim_bins, number_of_points, bins_support)
                # cond_ces[given_dims_ids[dim_id_idx]] = {cond_ce: sorted(modified_discr)}

        # pick the minimum one
        bin_counter += 1
        values = [list(ce.keys())[0] for ce in cond_ces.values()]

        if len(values) == 0:
            return current_combination[1]

        ces = preprocessing.ce_per_dimension(data)

        min_cond_ce = min(values)
        # !!!!!!!
        # if min_cond_ce == ces[cond_dim_id] or np.math.fabs(min_cond_ce - ces[cond_dim_id]) < 0.1 * ces[cond_dim_id]:
        if min_cond_ce == ces[cond_dim_id]:
            cond_ces.clear()
            insignificant_counter += 1
            if insignificant_counter > 1:
                do_joint_discr = False
            continue

        # take all min values
        indices = [i for i, v in enumerate(values) if v == min_cond_ce]

        '''
        if len(indices) > 1:
            # extend each candidate, choose the best
            subextension = True

            for i in indices:
                candidate = list(cond_ces.items())[i][0]
                if isinstance(candidate, tuple):
                    subextension_indices.append(candidate[-1])
                else:
                    subextension_indices.append(candidate) '''

        if len(indices) > 1:
            candidates_indices = []
            ces_indices = {}

            for i in indices:
                candidate = list(cond_ces.items())[i][0]
                if isinstance(candidate, tuple):
                    candidates_indices.append(candidate[-1])
                else:
                    candidates_indices.append(candidate)

            # TODO: place outside of the loop
            for idx in ces:
                if idx in candidates_indices:
                    ces_indices[idx] = ces[idx]

            # if combination is empty - add the most informative dimension, else - the least informative dimension
            if len(current_combination[1]) == 0:
                next_candidate = max(ces_indices.items(), key=itemgetter(1))[0]
            else:
                next_candidate = min(ces_indices.items(), key=itemgetter(1))[0]

            if isinstance(list(cond_ces.items())[0][0], tuple) or isinstance(list(cond_ces.items())[1][0], tuple):
                for item in list(cond_ces.keys()):
                    if isinstance(item, tuple):
                        if item[-1] == next_candidate:
                            chosen_dim_id = item
                            break
            else:
                chosen_dim_id = next_candidate
        else:
            min_value = {}
            # todo change to get key by value
            for c in cond_ces.values():
                if list(c.keys())[0] == min_cond_ce:
                    min_value[min_cond_ce] = c[min_cond_ce]
                    break

            chosen_dim_id = list(cond_ces.keys())[
                list(cond_ces.values()).index(min_value)]

            # if len(current_combination[1]) == 0:
            # print("ce of given dimension " + str(cond_dim_id) + ": " + str(ces[cond_dim_id]))
            # print("jcce: " + str(min_cond_ce))
            # print("cce of the pair " + str(cond_dim_id) + ", " + str(chosen_dim_id) + " : "
            #      + str(cces[(cond_dim_id, chosen_dim_id)]) + "\n")

        # significance check: is the decrease significant w.r.t. to the previous step?
        # else:
        if len(current_combination[1]) != 0:
            cce_value_prev_step = list(list(current_combination[1].values())[0].keys())[0]

            # TODO: sign. check.
            # TODO: take the next min if sign. check failed
            # if not min_cond_ce < significance_threshold * cce_value_prev_step:
            if not min_cond_ce < cce_value_prev_step:
                # insignificant_counter += 1
                # cond_ces.clear()
                # if insignificant_counter > 1:
                do_joint_discr = False
                # continue
                # else:
                #    insignificant_counter = 0

        if isinstance(chosen_dim_id, tuple):
            for dim_id in chosen_dim_id:
                if dim_id in current_combination[1]:
                    current_combination[1].pop(dim_id)

            if len(list(current_combination[1].keys())) != 0:
                if isinstance(list(current_combination[1].keys())[0], tuple):
                    if (i in chosen_dim_id for i in list(current_combination[1].keys())[0]):
                        current_combination[1].clear()

        current_combination[1][chosen_dim_id] = cond_ces[chosen_dim_id]
        # print("current_combination: " + str(current_combination) + "\n")

    # return ce value and multidim. bins
    return current_combination[1]