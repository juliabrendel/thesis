"""
while candidate_set or ces:

    next_candidates = get_candidates(next_dim_id, candidate_set)

    # TODO: change to taking the most significant first

    if len(next_candidates) == 0:
        if len(ces) != 0:
            max_ce = max(ces.values())
        else:
            break


        next_dim_id = list(ces.keys())[list(ces.values()).index(max_ce)]
        joint_ce += max_ce
        used_dimensions.append((next_dim_id,))
        remove_reverse((next_dim_id,), candidate_set, ces)

        # TODO: modify this function
        remove_candidates((next_dim_id,), number_of_dims, candidate_set)
        ces.pop(next_dim_id)

    else:

        #TODO: do this initially
        next_dim_id = get_most_significant_candidate(next_candidates, candidate_set)

        # TODO: expectation check, assume passed
        # TODO: remove ordering from this function

        # TODO!!! don't extend it completely
        next_candidate = extend_candidate(next_dim_id[0], candidate_set, data)

        if len(next_candidate[1]) == 0:
            joint_ce += sorted_ces[next_candidate[0]]
            used_dimensions.append(next_candidate)
            remove_reverse((next_candidate[0],), candidate_set, ces)

            # TODO: modify this function
            remove_candidates((next_dim_id,), number_of_dims, candidate_set)

        if len(next_candidate[1]) == 1:
            t = (next_candidate[0], next_candidate[1][0])
            joint_ce += conditional_ces[t]
            used_dimensions.append((next_candidate[0], [next_candidate[1][0]]))
            remove_reverse((next_candidate[0], (next_candidate[1][0],)), candidate_set, ces)

            # TODO: modify this function
            remove_candidates((next_candidate[0], (next_candidate[1][0],)), number_of_dims, candidate_set)

            if compute_full_score:
                uds_score_numerator += ces[next_candidate[0]] - conditional_ces[t]
                uds_score_denominator += ces[next_candidate[0]]

        if len(next_candidate[1]) > 1:

            candidates_discrs = pd.DataFrame(candidate_set[next_candidate[0], d_id][1] for d_id in next_candidate[1]).T
            '''
            for d_id in next_candidate[1]:
                pair = (next_candidate[0], d_id)
                candidates_discrs.append(candidate_set[pair][1])'''

            joint_discr_combination = jd.joint_discr(data, next_candidate[0], next_candidate[1],
                                                     candidates_discrs, significance_threshold, conditional_ces)

            #joint_discr_combination = jd.joint_discr_light(data, next_candidate[0], next_candidate[1],
            #                                               candidates_discrs, significance_threshold, conditional_ces)

            #TODO: add to a temporal candidate set

            if len(joint_discr_combination) == 0:
                joint_ce += ces[next_candidate[0]]
                used_dimensions.append((next_candidate[0],))
                remove_reverse((next_candidate[0],), candidate_set, ces)

                # TODO: modify this function
                remove_candidates((next_candidate[0],), number_of_dims, candidate_set)

            else:
                joint_cond_ce = list(list(joint_discr_combination.values())[0].keys())[0]
                used_combination = list(joint_discr_combination.keys())[0]
                discretization = list(list(joint_discr_combination.values())[0].values())[0]
                amount_of_bins = len(discretization)

                results_file.write("\nnumber of bins: " + str(amount_of_bins) + "\n")
                results_file.write("h(X" + str(str(next_candidate[0])) + ") = " + str(ces[next_candidate[0]]) + "\n")
                results_file.write("h(X" + str(str(next_candidate[0])) + "|X" + str(used_combination) + ") = " + str(
                    joint_cond_ce) + "\n")

                if isinstance(used_combination, tuple):
                    used_combination = sorted(used_combination)
                else:
                    used_combination = (used_combination,)

            # TODO: add conditioned dims one by one, exclude if exp check is not passed

                joint_ce += joint_cond_ce
                remove_reverse((next_candidate[0], used_combination), candidate_set, ces)

                # TODO: modify this function
                remove_candidates((next_candidate[0], used_combination), number_of_dims, candidate_set)

                if isinstance(used_combination, tuple):
                    used_combination = list(used_combination)

                used_dimensions.append((next_candidate[0], used_combination))

            if compute_full_score:
                if len(joint_discr_combination) != 0:
                    uds_score_numerator += ces[next_candidate[0]] - joint_cond_ce
                uds_score_denominator += ces[next_candidate[0]]

            # remove_candidates(next_candidate, number_of_dims)

        # remove the conditioned dim from CEs to avoid adding singletons

        if len(ces) != 0 and next_candidate[0] in ces:
            ces.pop(next_candidate[0])

    del next_candidates[:] """

# prune singletons
'''
cond_dims_set = []
for dim_id in used_dimensions:
    if len(dim_id) > 1:
        cond_dims_set.append(dim_id[0])
        for d in dim_id[1]:
            cond_dims_set.append(d)

cond_dims_set = set(cond_dims_set)

for dim_id in range(0, number_of_dims):
    if dim_id not in cond_dims_set:
        if (dim_id,) in used_dimensions:
            used_dimensions.remove((dim_id,))
        joint_ce -= preprocessing.ce_per_dimension(data)[dim_id]'''
"""
if compute_full_score:
    if uds_score_denominator == 0:
        uds_score = 0
    else:
        uds_score = uds_score_numerator / uds_score_denominator
    results_file.write("\nMCEE_UDS = " + str(uds_score) + "\n")"""