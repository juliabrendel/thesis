# TODO: split into preprocessing and optimal_discretization
from operator import itemgetter

import pandas as pd

from main import preprocessing


# cond_dim_discr in points-view, not in interval_view
def conditional_ce_all_bins(conditioned_dim, discretization):
    cond_cumulative_entropy = 0
    for b in discretization:
        points = [conditioned_dim[point_id] for point_id in b]
        if len(points) != len(b):
            print("lengths do not correspond")
        cond_cumulative_entropy += preprocessing.one_dim_ce(points) * (len(b) / len(conditioned_dim))
    return cond_cumulative_entropy


def transform(discretization, bin_id, dim_id):
    discr_length = len(discretization)
    b = discretization.T[bin_id][dim_id]
    if discr_length == 1:
        transformed_discr = [b]
    else:
        if bin_id == 0:
            discr_r = discretization.T[1][dim_id]
            for i in range(2, discr_length):
                discr_r = discr_r.union(discretization.T[i][dim_id])
            transformed_discr = [b, discr_r]

        elif 0 < bin_id < discr_length - 1:
            discr_l = discretization.T[0][dim_id]
            for i in range(0, bin_id):
                discr_l = discr_l.union(discretization.T[i][dim_id])
            discr_r = discretization.T[bin_id + 1][dim_id]
            for i in range(bin_id + 1, discr_length):
                discr_r = discr_r.union(discretization.T[i][dim_id])
            transformed_discr = [discr_l, b, discr_r]

        else:
            discr_l = discretization.T[0][dim_id]
            for i in range(0, bin_id):
                discr_l = discr_l.union(discretization.T[i][dim_id])
            transformed_discr = [discr_l, b]

    # number_of_points = sum([len(d) for d in discretization])
    c = []
    for i in range(0, discr_length):
        d = discretization.iloc[i, :][dim_id]
        length = len(d)
        c.append(length)

    number_of_points = sum(c)
    # number_of_points = sum([len(row[0]) for index, row in discretization.iterrows()])
    assert (sum([len(d) for d in transformed_discr]) == number_of_points), str(sum([len(d) for d in transformed_discr]))
    return transformed_discr


# sort dimension and bins w.r.t. their informativeness
def sort_bins(conditioned_dim, given_dims_ids, discretizations):
    from collections import OrderedDict
    import operator
    res = {}

    for dim_idx in range(len(given_dims_ids)):
        dim_id = given_dims_ids[dim_idx]
        bins = {}
        discr = pd.DataFrame(discretizations[dim_idx]).dropna(axis=0)
        discr_length = len(discr)

        for b_id in range(discr_length):
            if discr_length == 2 and b_id == 1: continue
            transformed_discr = transform(discr, b_id, dim_idx)
            cond_ce = conditional_ce_all_bins(conditioned_dim, transformed_discr)
            bins[b_id] = cond_ce

        sorted_bins = OrderedDict(sorted(bins.items(), key=operator.itemgetter(1)))
        # min_values[dim_id] = min(sorted_bins.values())
        res[dim_id] = sorted_bins
    return res

"""
# main function
# discretizations are given in points ids
def joint_discr(data, cond_dim_id, given_dims_ids, discretizations):
    conditioned_dim = data[:, cond_dim_id]
    number_of_points = len(conditioned_dim)
    jd_combination = {}  # ("x0", {"x1": [], "x2": []}), jd_combination combination
    do_joint_discr = True
    insignificant_counter = 0
    bin_counter = 0
    ces = preprocessing.ce_per_dimension(data)

    while do_joint_discr:
        cond_ces = {}
        for dim in range(len(given_dims_ids)):  # iterate over the set of given dimensions
            dim_id = given_dims_ids[dim]
            discr = pd.DataFrame(discretizations[dim]).dropna(axis=0)
            discr_length = len(discr)  # number of bins
            if bin_counter > discr_length - 1: continue  # if no bins left in this discretization - continue to the next
            b = discr.T[bin_counter][dim]  # bin
            # bin_iterator = bin_iterators[dim_id_idx]  # iterator over of discr(bins) of jd_combination dimension

            if dim_id not in list(jd_combination.keys()):   # if this dim. is not incl. into jd yet
                # create new bin iterator, if the prev. is over.
                index_of_bin = bin_counter  # index of the current bin
                # modify the discr. correspondingly
                transformed_discr = transform(discr, index_of_bin, dim)
                # update CEs list depending on if the dim. has already been incl. to jd
                if len(jd_combination) == 0:
                    cond_ce = conditional_ce_all_bins(conditioned_dim, transformed_discr)
                    cond_ces[dim_id] = {cond_ce: transformed_discr}
                else:
                    incl_dim_id = next(iter(jd_combination))
                    prev_discr = list(jd_combination[incl_dim_id].values())[0]
                    discr_ = [i.intersection(j) for i in prev_discr for j in transformed_discr]
                    merged_discr = [d for d in discr_ if len(d) != 0]
                    assert (sum([len(d) for d in merged_discr]) == number_of_points), str(sum([len(d) for d in merged_discr]))
                    cond_ce = conditional_ce_all_bins(conditioned_dim, merged_discr)
                    if incl_dim_id == dim_id: cond_ces[dim_id] = {cond_ce: merged_discr}
                    elif isinstance(incl_dim_id, tuple) and dim_id in incl_dim_id:
                        cond_ces[incl_dim_id] = {cond_ce: merged_discr}
                    elif isinstance(incl_dim_id, tuple) and dim_id not in incl_dim_id:
                        cond_ces[incl_dim_id + (dim_id,)] = {cond_ce: merged_discr}
                    else:
                        cond_ces[(incl_dim_id, dim_id)] = {cond_ce: merged_discr}

            else:  # if this dim. is already included into jd
                prev_discr = list(jd_combination[dim_id].values())[0]
                merged_discr = [i.intersection(j) for j in prev_discr for i in [b, pd.DataFrame(data).index.difference(b)]]
                transformed_discr = [d for d in merged_discr if len(d) != 0]
                assert (sum([len(d) for d in transformed_discr]) == number_of_points), str(sum([len(d) for d in transformed_discr]))
                cond_ce = conditional_ce_all_bins(conditioned_dim, transformed_discr)
                cond_ces[dim_id] = {cond_ce: transformed_discr}

        # update the bin counter
        bin_counter += 1
        # pick that dim. which results in the max decrease of the CE
        values = [list(ce.keys())[0] for ce in cond_ces.values()]
        if len(values) == 0: return jd_combination
        min_cond_ce = min(values)
        # if min_cond_ce == ces[cond_dim_id] or np.math.fabs(min_cond_ce - ces[cond_dim_id]) < 0.1 * ces[cond_dim_id]:
        # if the CCE value is NOT lower than the CE value - insignificant, discard, continue
        if min_cond_ce == ces[cond_dim_id]:
            cond_ces.clear()
            insignificant_counter += 1
            # if insignificant occurs more than once - stop, return
            if insignificant_counter > 1: do_joint_discr = False
            continue
        indices = [i for i, v in enumerate(values) if v == min_cond_ce]
        if len(indices) > 1:
            candidates_indices = []
            ces_indices = {}
            for i in indices:
                candidate = list(cond_ces.items())[i][0]
                if isinstance(candidate, tuple): candidates_indices.append(candidate[-1])
                else: candidates_indices.append(candidate)
            for idx in ces:
                if idx in candidates_indices: ces_indices[idx] = ces[idx]
            # if combination is empty - add the most informative dimension, else - the least informative dimension
            if len(jd_combination) == 0: next_candidate = max(ces_indices.items(), key=itemgetter(1))[0]
            else: next_candidate = min(ces_indices.items(), key=itemgetter(1))[0]
            if isinstance(list(cond_ces.items())[0][0], tuple) or isinstance(list(cond_ces.items())[1][0], tuple):
                for item in list(cond_ces.keys()):
                    if isinstance(item, tuple):
                        if item[-1] == next_candidate:
                            chosen_dim_id = item
                            break
            else: chosen_dim_id = next_candidate
        else:
            min_value = {}
            for c in cond_ces.values():
                if list(c.keys())[0] == min_cond_ce:
                    min_value[min_cond_ce] = c[min_cond_ce]
                    break
            chosen_dim_id = list(cond_ces.keys())[list(cond_ces.values()).index(min_value)]
        # significance check: is the decrease significant w.r.t. to the previous step?
        if len(jd_combination) != 0:
            cce_value_prev_step = list(list(jd_combination.values())[0].keys())[0]
            if not min_cond_ce < cce_value_prev_step: do_joint_discr = False
            '''
            else:
                #print("decrease, %: ", 1 - (min_cond_ce/cce_value_prev_step))
                print("previous step: ", cce_value_prev_step)
                print("current: ", min_cond_ce, "\n")'''
        if isinstance(chosen_dim_id, tuple):
            for dim_id in chosen_dim_id:
                if dim_id in jd_combination: jd_combination.pop(dim_id)
            if len(list(jd_combination.keys())) != 0:
                if isinstance(list(jd_combination.keys())[0], tuple):
                    if (i in chosen_dim_id for i in list(jd_combination.keys())[0]): jd_combination.clear()
        jd_combination[chosen_dim_id] = cond_ces[chosen_dim_id]  # update jd
    # return ce value and multidim. bins
    return jd_combination  # each term is a dict.: {dim_id: {cond_ce: discr}}"""

# includes bins sorting
def joint_discr(data, cond_dim_id, given_dims_ids, discretizations):
    conditioned_dim = data[:, cond_dim_id]
    number_of_points = len(conditioned_dim)
    jd_combination = {}  # ("x0", {"x1": [], "x2": []}), jd_combination combination
    do_joint_discr = True
    insignificant_counter = 0
    ces = preprocessing.ce_per_dimension(data)
    # select best first cut
    # iterate over dimensions, select always the most informative bin
    sorted_ = sort_bins(conditioned_dim, given_dims_ids, discretizations)
    # get starting dimension
    min_ = {}
    for d_id in sorted_:
        min_[d_id] = min(sorted_[d_id].values())

    # check if values are the same, if yes, choose based on the univariate ce
    min_v = min(min_.values())
    # start_dim = list(min_.keys())[list(min_.values()).index(min_v)]  # old
    # new
    min_dims = [k for k, v in min_.items() if v == min_v]
    # start_dim = list(min_ces.keys())[list(min_ces.values()).index(max(min_ces.values()))]  # old
    picked_dims = []
    if len(min_dims) == 1:
        start_dim = min_dims[0]
        start_bin_id = list(sorted_[start_dim].keys())[0]
        start_dim_id = given_dims_ids.index(start_dim)
        d_ = pd.DataFrame(discretizations[start_dim_id]).dropna(axis=0)
        transformed_discr = transform(d_, start_bin_id, start_dim_id)
        picked_dims.append(start_dim)
    elif len(min_dims) > 1:
        min_ces = {d: ces[d] for d in min_dims}
        max_ce = max(min_ces.values())
        # pick the one with the highest univariate cumulative entropy
        min_dims1 = [k for k, v in min_ces.items() if v == max_ce]
        if len(min_dims1) == 1:
            start_dim = min_dims1[0]
            start_bin_id = list(sorted_[start_dim].keys())[0]
            start_dim_id = given_dims_ids.index(start_dim)
            d_ = pd.DataFrame(discretizations[start_dim_id]).dropna(axis=0)
            transformed_discr = transform(d_, start_bin_id, start_dim_id)
            picked_dims.append(start_dim)
        else:
            picked_dims = min_dims1
            transformed_discr = None
            for d in min_dims1:
                d_idx = given_dims_ids.index(d)
                discr = pd.DataFrame(discretizations[d_idx]).dropna(axis=0)
                bin_id = list(sorted_[d].keys())[0]
                t_discr = transform(discr, bin_id, d_idx)
                if transformed_discr is not None:
                    d_ = [i.intersection(j) for i in transformed_discr for j in t_discr]
                    transformed_discr = [d for d in d_ if len(d) != 0]
                else:
                    transformed_discr = t_discr
                assert (sum([len(d) for d in transformed_discr]) == number_of_points), str(
                    sum([len(d) for d in transformed_discr]))
    else:
        transformed_discr = discretizations[cond_dim_id]
        print("trans. discr assigned to the initial")
        print("len min dims: ", len(min_dims), "\n")

    for d in picked_dims:
        # start_bin_id = list(sorted_[d].keys())[0]
        # start_dim_id = given_dims_ids.index(d)
        # d_ = pd.DataFrame(discretizations[start_dim_id]).dropna(axis=0)
        # transformed_discr = transfrom(d_, start_bin_id, start_dim_id)
        number_of_bins_sd = max(list(sorted_[d].keys()))
        sorted_[d].pop(list(sorted_[d].keys())[0])  # delete the used bin!
        # check if the left over bins are redundant
        if len(sorted_[d]) == 2 and \
                ((list(sorted_[d].keys())[0] == 0 and list(sorted_[d].keys())[1] == number_of_bins_sd) or
                     (list(sorted_[d].keys())[0] == number_of_bins_sd and list(sorted_[d].keys())[1] == 0)):
            sorted_[d].clear()

    cond_ce = conditional_ce_all_bins(conditioned_dim, transformed_discr)
    if len(picked_dims) == 1:
        jd_combination[picked_dims[0]] = {cond_ce: transformed_discr}
    else:
        jd_combination[tuple(picked_dims)] = {cond_ce: transformed_discr}

    # if len(sorted_[start_dim]) == 3 and start_bin_id == 1: sorted_[start_dim].clear()
    # else: sorted_[start_dim].pop(list(sorted_[start_dim].keys())[0])  # delete the used bin!

    while do_joint_discr:
        cond_ces = {}
        for dim in range(len(given_dims_ids)):  # iterate over the set of given dimensions
            dim_id = given_dims_ids[dim]
            discr = pd.DataFrame(discretizations[dim]).dropna(axis=0)
            # discr_length = len(discr)  # number of bins
            # all bins of this discretization have been included into jd
            if len(sorted_[dim_id]) == 0: continue
            bin_id = list(sorted_[dim_id].keys())[0]
            b = discr.T[bin_id][dim]

            if dim_id not in list(jd_combination.keys()):  # if this dim. is not incl. into jd yet
                # modify the discr. correspondingly
                transformed_discr = transform(discr, bin_id, dim)
                # update CEs list depending on if the dim. has already been incl. to jd
                if len(jd_combination) == 0:
                    cond_ce = conditional_ce_all_bins(conditioned_dim, transformed_discr)
                    cond_ces[dim_id] = {cond_ce: transformed_discr}
                else:
                    incl_dim_id = next(iter(jd_combination))
                    prev_discr = list(jd_combination[incl_dim_id].values())[0]
                    discr_ = [i.intersection(j) for i in prev_discr for j in transformed_discr]
                    merged_discr = [d for d in discr_ if len(d) != 0]
                    assert (sum([len(d) for d in merged_discr]) == number_of_points), str(
                        sum([len(d) for d in merged_discr]))
                    cond_ce = conditional_ce_all_bins(conditioned_dim, merged_discr)
                    cond_ces[dim_id] = {cond_ce: merged_discr}

            else:  # if this dim. is already included into jd
                # overlap in bins cannot happen, since used bins are always deleted at the end!
                prev_discr = list(jd_combination[dim_id].values())[0]
                merged_discr = [i.intersection(j) for j in prev_discr for i in
                                [b, pd.DataFrame(data).index.difference(b)]]
                transformed_discr = [d for d in merged_discr if len(d) != 0]
                assert (sum([len(d) for d in transformed_discr]) == number_of_points), str(
                    sum([len(d) for d in transformed_discr]))
                cond_ce = conditional_ce_all_bins(conditioned_dim, transformed_discr)
                cond_ces[dim_id] = {cond_ce: transformed_discr}

        # pick that dim. which results in the max decrease of the CE
        values = [list(ce.keys())[0] for ce in cond_ces.values()]
        if len(values) == 0:
            return jd_combination

        min_cond_ce = min(values)
        # if min_cond_ce == ces[cond_dim_id] or np.math.fabs(min_cond_ce - ces[cond_dim_id]) < 0.1 * ces[cond_dim_id]:
        # if the CCE value is NOT lower than the CE value - insignificant, discard, continue
        if min_cond_ce == ces[cond_dim_id]:
            cond_ces.clear()
            insignificant_counter += 1
            if insignificant_counter > 1:  # if insignificant occurs more than once - stop, return
                do_joint_discr = False
            continue

        # get all min values
        indices = [i for i, v in enumerate(values) if v == min_cond_ce]
        # optimize
        picked_dims1 = []
        if len(indices) > 1:
            candidates_indices = []
            ces_indices = {}
            for i in indices:
                candidate = list(cond_ces.items())[i][0]
                if isinstance(candidate, tuple):
                    candidates_indices.append(candidate[-1])
                else:
                    candidates_indices.append(candidate)
            # for idx in ces:
            #    if idx in candidates_indices: ces_indices[idx] = ces[idx]
            for i in candidates_indices:
                ces_indices[i] = ces[i]

            # next_candidate = min(ces_indices.items(), key=itemgetter(1))[0]  # old
            min_ce = min(ces_indices.values())
            min_dims_ = [k for k, v in ces_indices.items() if v == min_ce]
            if len(min_dims_) == 1:
                chosen_dim_id = min_dims_[0]
                chosen_value = cond_ces[chosen_dim_id]
                picked_dims1.append(chosen_dim_id)
            else:
                picked_dims1 = min_dims_
                transformed_discr = None
                for d in min_dims_:
                    d_idx = given_dims_ids.index(d)
                    discr = pd.DataFrame(discretizations[d_idx]).dropna(axis=0)
                    bin_id = list(sorted_[d].keys())[0]
                    t_discr = transform(discr, bin_id, d_idx)
                    if transformed_discr is not None:
                        d_ = [i.intersection(j) for i in transformed_discr for j in t_discr]
                        transformed_discr = [d for d in d_ if len(d) != 0]
                    else:
                        transformed_discr = t_discr
                    assert (sum([len(d) for d in transformed_discr]) == number_of_points), str(
                        sum([len(d) for d in transformed_discr]))

                if len(jd_combination) != 0:
                    incl_dim_id = next(iter(jd_combination))
                    prev_discr = list(jd_combination[incl_dim_id].values())[0]
                    discr_ = [i.intersection(j) for i in prev_discr for j in transformed_discr]
                    merged_discr = [d for d in discr_ if len(d) != 0]
                    assert (sum([len(d) for d in merged_discr]) == number_of_points), str(
                        sum([len(d) for d in merged_discr]))
                    min_cond_ce = conditional_ce_all_bins(conditioned_dim, merged_discr)
                    chosen_value = {cond_ce: merged_discr}

                    # if combination is empty add the most informative dimension, else the least informative dimension
                    # if len(jd_combination) == 0:
                    # todo change
                    # next_candidate = max(ces_indices.items(), key=itemgetter(1))[0]
                    # else:
                    # todo change
                    # next_candidate = min(ces_indices.items(), key=itemgetter(1))[0]

                    # if isinstance(list(cond_ces.items())[0][0],tuple)orisinstance(list(cond_ces.items())[1][0],tuple):
                    # for item in list(cond_ces.keys()):
                    # if isinstance(item, tuple):
                    # if item[-1] == next_candidate:
                    # chosen_dim_id = item
                    # break
                    # else:
                    # chosen_dim_id = next_candidate
        else:
            min_value = {}
            for c in cond_ces.values():
                if list(c.keys())[0] == min_cond_ce:
                    min_value[min_cond_ce] = c[min_cond_ce]
                    break
            chosen_dim_id = list(cond_ces.keys())[list(cond_ces.values()).index(min_value)]
            chosen_value = cond_ces[chosen_dim_id]
            picked_dims1.append(chosen_dim_id)

        # significance check: is the decrease significant w.r.t. to the previous step?
        if len(jd_combination) != 0:
            cce_value_prev_step = list(list(jd_combination.values())[0].keys())[0]
            # if not min_cond_ce < significance_threshold * cce_value_prev_step:
            if not min_cond_ce < cce_value_prev_step:
                # print("jd: no ce decrease")
                cond_ces.clear()
                # # # change: stop immediately if insignificant!
                insignificant_counter += 1
                # remove the current bin s.t. it is not picked again on the next step
                for d in picked_dims1:
                    chosen_bin_id = list(sorted_[d].keys())[0]
                    if max(list(sorted_[d].keys())) == 2 and chosen_bin_id == 1:
                        sorted_[d].clear()
                    else:
                        sorted_[d].pop(list(sorted_[d].keys())[0])
                if insignificant_counter > 1:  # if insignificant occurs more than once - stop, return
                    do_joint_discr = False
                continue
        # chosen_value = cond_ces[chosen_dim_id]
        for d in picked_dims1:
            chosen_bin_id = list(sorted_[d].keys())[0]
            if max(list(sorted_[d].keys())) == 2 and chosen_bin_id == 1:
                sorted_[d].clear()
            else:
                sorted_[d].pop(list(sorted_[d].keys())[0])  # delete the used bin!
        prev_dim_id = list(jd_combination.keys())[0]
        if len(picked_dims1) == 1:
            chosen_dim_id = picked_dims1[0]
        else:
            chosen_dim_id = tuple(picked_dims1)
        if chosen_dim_id != prev_dim_id:
            if isinstance(prev_dim_id, tuple) and chosen_dim_id not in prev_dim_id:
                if isinstance(chosen_dim_id, tuple):
                    chosen_dim_id += list(jd_combination.keys())[0]
                else:
                    chosen_dim_id = (chosen_dim_id,) + list(jd_combination.keys())[0]
            elif isinstance(prev_dim_id, tuple) and chosen_dim_id in prev_dim_id:
                chosen_dim_id = prev_dim_id
            elif not isinstance(prev_dim_id, tuple) and isinstance(chosen_dim_id, tuple):
                chosen_dim_id += (prev_dim_id,)
            else:
                chosen_dim_id = (prev_dim_id, chosen_dim_id)
        jd_combination.clear()
        jd_combination[chosen_dim_id] = chosen_value
    # return ce value and multidim. bins
    return jd_combination  # each term is a dict.: {dim_id: {cond_ce: discr}}


"""
def joint_discr_light(data, cond_dim_id, given_dims_ids, discretizations, significance_threshold, cces):
    conditioned_dim = sorted(data[:, cond_dim_id])
    length = len(conditioned_dim)
    given_dims = []  # given dims are stored in a list, one after another
    '''
    ces = preprocessing.ce_per_dimension(data)

    ces_slice = {}
    for ce in ces:
        if ce in given_dims_ids:
            ces_slice[ce] = ces[ce]

    ces_slice = sorted(ces_slice.items(), key=itemgetter(1))
    print(ces_slice)
    ids_sorted_by_ces = [ce[0] for ce in ces_slice]
    print(ids_sorted_by_ces) '''

    for dim_id in given_dims_ids:
        dim = sorted(data[:, dim_id])
        given_dims.append(dim)

    do_joint_discr = True
    insignificant_counter = 0
    # subextension = False
    # subextension_indices = []

    cond_ces = {}

    for dim_id_idx in range(0, len(given_dims_ids)):  # iterate over the set of given dimensions

        dim_id = given_dims_ids[dim_id_idx]
        given_dim = given_dims[dim_id_idx]  # current_combination dimension in points

        discr_length = len(discretizations[dim_id_idx])  # number of bins
        discretization = discretizations[dim_id_idx]

        if len(cond_ces) == 0:
            cond_ces[dim_id] = {cces[(cond_dim_id, dim_id)]: discretization}

        else:
            incl_dim_id = list(cond_ces.keys())[0]
            used_bins = list(cond_ces[incl_dim_id].values())[0]

            if isinstance(incl_dim_id, tuple) and dim_id not in incl_dim_id:

                projected_ds = [sorted(data[:, i]) for i in incl_dim_id]
                c1 = len(projected_ds)
                projection_included_dim = project_discr(join_dimensions(projected_ds), used_bins)
                if projection_included_dim[-1][-1] != data.shape[0]-1:
                    projection_included_dim[-1].append(data.shape[0]-1)
                if projection_included_dim[0][0] != 0:
                    projection_included_dim[0].append(0)

                projection_new_dim = project_discr(given_dim, discretization)
                if projection_new_dim[-1][-1] != data.shape[0] - 1:
                    projection_new_dim[-1].append(data.shape[0] - 1)
                if projection_new_dim[0][0] != 0:
                    projection_new_dim[0].append(0)

                projected_ds.append(sorted(data[:, dim_id]))
                c2 = len(projected_ds)

                merged_projection, merged_discr = merge_projections(projection_included_dim, projection_new_dim,
                                                                    cond_dim=conditioned_dim,
                                                                    projected_dims=projected_ds)

                bins_support = discr_support(merged_projection, intervals=False)
                # print("bins sum: " + str(sum(bins_support)))
                cond_ce = conditional_ce_all_bins(merged_projection, length, bins_support)

                cce_value_prev_step = list(list(cond_ces.values())[0].keys())[0]

                if cond_ce == cce_value_prev_step:
                    continue

                cond_ces[incl_dim_id + (given_dims_ids[dim_id_idx],)] = \
                    {cond_ce: sorted(discr_points_to_intervals(merged_discr))}

            #elif isinstance(incl_dim_id, tuple) and dim_id in incl_dim_id:
            else:
                projection_included_dim = project_discr(sorted(data[:, incl_dim_id]), used_bins)
                if projection_included_dim[-1][-1] != data.shape[0]-1:
                    projection_included_dim[-1].append(data.shape[0]-1)
                if projection_included_dim[0][0] != 0:
                    projection_included_dim[0].append(0)

                projection_new_dim = project_discr(given_dim, discretization)
                if projection_new_dim[-1][-1] != data.shape[0] - 1:
                    projection_new_dim[-1].append(data.shape[0] - 1)
                if projection_new_dim[0][0] != 0:
                    projection_new_dim[0].append(0)

                merged_projection, merged_discr = merge_projections(projection_included_dim,
                                                                    projection_new_dim,
                                                                    cond_dim=conditioned_dim,
                                                                    projected_dims=[sorted(data[:, incl_dim_id]),
                                                                                    sorted(data[:, dim_id])])

                bins_support = discr_support(merged_projection, intervals=False)
                # print("bins sum: " + str(sum(bins_support)))
                cond_ce = conditional_ce_all_bins(merged_projection, length, bins_support)

                cce_value_prev_step = list(list(cond_ces.values())[0].keys())[0]

                if cond_ce == cce_value_prev_step:
                    continue

                cond_ces.clear()
                cond_ces[(incl_dim_id, dim_id)] = {cond_ce: sorted(discr_points_to_intervals(merged_discr))}

    return cond_ces"""
