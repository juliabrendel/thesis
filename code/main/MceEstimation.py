from collections import OrderedDict

import numpy as np
import pandas as pd

import experiments.PrecisionRecallT as pr
from main import JointDiscretization as jd
from main import preprocessing as prep
from main import expectation_check as exp
from experiments.dependencyGraph import basic_functions as basic


class MCE:
    def __init__(self, estimation, do_exp_check):
        self.data = estimation.data
        self.nr_bins = estimation.number_of_bins
        self.nr_dims = self.data.shape[1]
        self.results_file = estimation.results_file
        self.cumulative_entropies = estimation.cumulative_entropies
        self.candidates_ncmi = estimation.candidate_set_values.copy()
        self.candidates_discr = estimation.candidate_set_disrs.copy()
        self.conditional_ces = estimation.conditional_ces
        self.decomposition = [(d,) for d in range(self.nr_dims)]
        self.outliers = set(range(self.nr_dims))
        # self.outliers = [d for d in range(self.nr_dims)]
        self.search_history = {i: {} for i in range(self.nr_dims)}
        self.multiv_results = {}
        self.used_discretizations = {}
        self.final_discretizations = {}
        self.mce_scores = estimation.mce_scores
        self.do_exp_check = do_exp_check

    def remove_reverse(self, next_candidate):
        reverse = next_candidate[::-1]
        if reverse in self.candidates_ncmi:
            self.candidates_ncmi.pop(reverse)

    def creates_cycle(self, current_term, next_term, decomposition):
        test_dependencies = decomposition.copy()
        test_dependencies.remove(current_term)
        test_dependencies.append(next_term)
        has_cycle = basic.has_cycle(self.nr_dims, test_dependencies)
        return has_cycle

    def jd(self, cond_dim, given_dims):
        discretizations = pd.DataFrame([self.used_discretizations[(cond_dim, d)] for d in given_dims])
        discretizations = discretizations.T
        joint_discr_combination = jd.joint_discr(self.data, cond_dim, given_dims, discretizations)
        return joint_discr_combination

    def extend(self, cond_dim, given_dims):
        discretization = [pd.DataFrame(self.data).index]
        for dim in given_dims:
            d = self.used_discretizations[(cond_dim, dim)]
            extension = prep.extend_I(discretization, d)
            discretization = extension
        ce_value = jd.conditional_ce_all_bins(self.data[:, cond_dim], discretization)
        return discretization, ce_value

    def compute_ncmi(self, c_cond_dim, cond_ce):
        ce = self.cumulative_entropies[c_cond_dim]
        cmi = (ce - cond_ce) / ce
        return cmi

    def find_redundant(self, candidate):
        test_decomp = self.decomposition.copy()
        test_decomp.append(candidate)
        pairwise = basic.break_down(test_decomp)
        red = []
        for d1 in pairwise:
            for d2 in pairwise:
                if d1 == d2: continue
                if len(d1) == 1 or len(d2) == 1: continue
                red_ = None
                if d1[0] == d2[1]:
                    red_ = (d2[0], d1[1])
                elif d1[1] == d2[0]:
                    red_ = (d1[0], d2[1])
                if red_ in pairwise:
                    if len(np.intersect1d(red_, candidate)) == 0: continue
                    if red_ in red: continue
                    red.append(red_)
        return red

    def local_ncmi(self, candidate, decom_=None, multiv_res_=None):
        if decom_ is None and multiv_res_ is None:
            decom = self.decomposition
            multiv_res = self.multiv_results
        else:
            decom = decom_
            multiv_res = multiv_res_
        # get relevant terms from decomposition
        # get connected components
        cc = basic.get_connected_components(self.nr_dims, decom)
        # get relevant connected component
        rel_c = [c for c in cc if len(c) != 1 and (candidate[0] in c or candidate[1] in c)]

        """
        c = []
        for c_ in cc:
            if len(c_) != 1:
                if candidate[0] in c_ or candidate[1] in c_:
                    c.append(c_) """

        if len(rel_c) == 0:
            return 0
        # if candidate has to do with multiple c.c.
        if len(rel_c) > 1:
            rel_c = rel_c[0].union(rel_c[1])
        else:
            rel_c = rel_c[0]
        # get all relevant dependencies
        relevant = [i for i in decom if len(i) != 1 and len(np.intersect1d(basic.break_down([i]), list(rel_c))) != 0]
        nominator = 0
        denominator = 0

        """
        add_univariate = True
        for i in relevant:
            if candidate[0] == i[0]: add_univariate = False"""

        for i in relevant:
            ce = self.cumulative_entropies[i[0]]
            if len(i) == 1:
                denominator += ce
                continue
            if i[0] in multiv_res:
                cond_ce = multiv_res[i[0]][1]
            else:
                cond_ce = self.conditional_ces[i]
            nominator += ce - cond_ce
            denominator += ce
        """
        if add_univariate:
            print("relevant: ", relevant, " candidate: ", candidate)
            denominator += self.cumulative_entropies[candidate[0]] """
        ncmi = nominator / denominator
        return ncmi

    """
    def rollback(self, redundant):
        temp_decom = self.decomposition.copy()
        temp_candidate_set = self.search_candidate_set.copy()
        temp_multiv_res = self.multiv_results.copy()
        temp_final_discrs = self.final_discretizations.copy()

        for r in redundant:
            temp_cond_dim = r[0]
            temp_final_discrs.pop(temp_cond_dim)  # remove previous discr.
            temp_candidate_set.pop(temp_cond_dim)  # remove ce value from search C

            if r in temp_decom:  # if univariate
                temp_decom.remove(r)  # not included to multiv. results
                temp_decom.append((temp_cond_dim,))
                temp_candidate_set[temp_cond_dim] = self.cumulative_entropies[
                    temp_cond_dim]  # add prev. iteration value
            else:
                term_to_remove = [i for i in temp_decom if i[0] == temp_cond_dim][0]  # existing term
                temp_decom.remove(term_to_remove)
                temp_multiv_res.pop(temp_cond_dim)
                t_ = list(term_to_remove[1])
                t_.remove(r[1])
                if len(t_) == 1: t_ = t_[0]
                else: t_ = tuple(t_)
                term_to_be = (temp_cond_dim, t_)  # potential term

                if term_to_be in self.search_history[temp_cond_dim].keys():  # if this term occurred before
                    info = self.search_history[temp_cond_dim][term_to_be]  # get ce, discr
                    ce_value = info[0]
                    discr = info[1]
                    temp_decom.append(term_to_be)  # add to the temporal decomposition
                    temp_multiv_res[temp_cond_dim] = [term_to_be[1], ce_value, len(discr)]

                else:
                    temp_given_dims = term_to_be[1]

                    if not isinstance(temp_given_dims, tuple):  # univariate
                        ce_value = self.conditional_ces[term_to_be]
                        discr = self.candidates_discr[term_to_be]
                        temp_decom.append(term_to_be)  # add to the temporal decomposition
                        temp_multiv_res[temp_cond_dim] = [term_to_be[1], ce_value, len(discr)]

                    else:
                        joint_discr_combination = self.jd(temp_cond_dim, temp_given_dims)
                        if len(joint_discr_combination) == 0: print("jd is empty!")
                        ce_value = list(list(joint_discr_combination.values())[0].keys())[0]
                        discr = list(list(joint_discr_combination.values())[0].values())[0]
                        jd_combination = list(joint_discr_combination.keys())[0]
                        temp_decom.append((temp_cond_dim, jd_combination))  # add to the temporal decomposition
                        temp_multiv_res[temp_cond_dim] = [jd_combination, ce_value, len(discr)]
                        self.search_history[temp_cond_dim][(temp_cond_dim, jd_combination)] = [ce_value, discr]

                temp_candidate_set[temp_cond_dim] = ce_value
                temp_final_discrs[temp_cond_dim] = discr

        return temp_decom, temp_candidate_set, temp_final_discrs, temp_multiv_res"""

    def compute_dependency_score(self):
        print("outliers: ", self.outliers)
        uds_score_numerator = 0
        uds_score_denominator = 0
        for dim in self.search_candidate_set:
            curr_diff = self.cumulative_entropies[dim] - self.search_candidate_set[dim]
            if curr_diff != 0:
                uds_score_numerator += curr_diff
                uds_score_denominator += self.cumulative_entropies[dim]
        for out in self.outliers:
            uds_score_denominator += self.cumulative_entropies[out]

        if uds_score_numerator == 0:
            self.mce_uds_score = 0
        else:
            self.mce_uds_score = uds_score_numerator / uds_score_denominator

    def mce_estimation(self, do_jd):
        self.results_file.write("MCE\n")
        # self.results_file.write("candidate set: " + str(list(self.candidates_ncmi.keys())) + "\n")
        self.multiv_terms = 0
        self.worse_than_expected = 0
        self.search_candidate_set = self.cumulative_entropies.copy()

        # while the candidate set is not empty
        while self.candidates_ncmi:
            remove_rev = True
            next_candidate = self.candidates_ncmi.popitem(last=False)[0]
            c_cond_dim = next_candidate[0]
            c_given_dim = next_candidate[1]
            c_discr = self.candidates_discr[next_candidate]
            self.used_discretizations[next_candidate] = c_discr
            temp_decom = self.decomposition.copy()
            temp_outliers = self.outliers.copy()
            temp_candidate_set = self.search_candidate_set.copy()
            temp_final_discrs = self.final_discretizations.copy()
            temp_multiv_res = self.multiv_results.copy()
            # r_check = True  # redundancy check true
            update_info = True
            redundant = self.find_redundant(next_candidate)  # if 1 element returned - redundant, discard
            if not isinstance(redundant, list) and next_candidate == redundant:
                continue
            if isinstance(redundant, list) and next_candidate in redundant:
                continue
            # if len(redundant) == 0:
            #    r_check = False
            # elif next_candidate in redundant:
            #    continue  # discard next candidate
            # dependency score of the connected component
            current_score = self.local_ncmi(next_candidate)
            # if r_check:
            # compute ncmi for current decomposition
            # rollback - copy and modify current decomposition
            # temp_decom, temp_candidate_set, temp_final_discrs, temp_multiv_res = self.rollback(redundant)

            current = [v for i, v in enumerate(temp_decom) if v[0] == c_cond_dim and len(v) > 1]
            # if this candidate is already in the current combination - do joint discretization
            if len(current) != 0:
                current_term = current[0]
                if isinstance(current_term[1], tuple):
                    given_dims = list(current_term[1])
                else:
                    given_dims = [current_term[1]]
                given_dims.append(c_given_dim)
                # check if adding this candidate would create cycles
                creates_cycle = self.creates_cycle(current_term, (c_cond_dim, tuple(given_dims)), temp_decom)
                if creates_cycle:
                    # print("creates cycle: ", (c_cond_dim, tuple(given_dims)))
                    continue
                if do_jd:
                    joint_discr_combination = self.jd(c_cond_dim, given_dims)
                    if len(joint_discr_combination) == 0: continue
                    joint_cond_ce = list(list(joint_discr_combination.values())[0].keys())[0]
                    jd_comb = list(joint_discr_combination.keys())[0]
                    joint_discretization = list(list(joint_discr_combination.values())[0].values())[0]
                    nr_bins = len(joint_discretization)
                    if isinstance(jd_comb, tuple):
                        if c_given_dim not in jd_comb:
                            remove_rev = False
                    """
                    # expectation check
                    if self.do_exp_check:
                        better_than_expected = exp.expected_ce(joint_discretization, self.data[:, c_cond_dim], joint_cond_ce)
                        self.multiv_terms += 1
                        if not better_than_expected:
                            print("worse than expected, joint discretization discarded")
                            self.worse_than_expected += 1
                            remove_rev = False
                            self.remove_candidate(next_candidate, remove_rev)
                            continue"""
                else:
                    # term extension
                    joint_discretization, joint_cond_ce = self.extend(c_cond_dim, given_dims)
                    nr_bins = len(joint_discretization)
                    jd_comb = tuple(given_dims)

                if joint_cond_ce < temp_candidate_set[c_cond_dim]:
                    temp_candidate_set.pop(c_cond_dim)
                    temp_decom.remove(current_term)
                    for j in jd_comb:
                        temp_outliers.discard(j)
                    temp_decom.append((c_cond_dim, jd_comb))
                    assert (c_cond_dim, jd_comb) in temp_decom, str((c_cond_dim, jd_comb)) + "\n" + str(temp_decom)
                    temp_multiv_res[c_cond_dim] = [jd_comb, joint_cond_ce, nr_bins]
                    temp_candidate_set[c_cond_dim] = joint_cond_ce
                    if c_cond_dim in temp_final_discrs: temp_final_discrs.pop(c_cond_dim)
                    temp_final_discrs[c_cond_dim] = joint_discretization
                    self.search_history[c_cond_dim][(c_cond_dim, jd_comb)] = [joint_cond_ce, joint_discretization]

                    new_score = self.local_ncmi(next_candidate, decom_=temp_decom, multiv_res_=temp_multiv_res)
                    if not new_score > current_score:
                        # candidate discarded
                        update_info = False
                        remove_rev = False
                        # if r_check:
                        # redundant kept
                        #    print("redundant kept, candidate removed")
                        # else:
                        # candidate added
                    #    if r_check:
                    #        print("redundnat removed, candidate added")

                    if update_info:
                        self.decomposition = temp_decom
                        self.outliers = temp_outliers
                        self.search_candidate_set = temp_candidate_set
                        self.final_discretizations = temp_final_discrs
                        self.multiv_results = temp_multiv_res
                else:
                    # candidate and all variable changes discarded, redundant term as well as reverse are kept
                    remove_rev = False
            else:
                # check if adding this candidate would create cycles
                creates_cycle = self.creates_cycle((c_cond_dim,), next_candidate, temp_decom)
                if creates_cycle:
                    # print("created cycles")
                    continue

                """
                # expectation check
                if self.do_exp_check:
                    self.multiv_terms += 1
                    cond_ce = self.conditional_ces[next_candidate]
                    better_than_expected = exp.expected_ce(c_discr, self.data[:, c_cond_dim], cond_ce)
                    if not better_than_expected:
                        self.worse_than_expected += 1
                        remove_rev = False
                        print("worse than expected, discretization discarded")
                        self.remove_candidate(next_candidate, remove_rev)
                        continue"""

                if self.conditional_ces[next_candidate] < temp_candidate_set[c_cond_dim]:
                    temp_candidate_set.pop(c_cond_dim)
                    temp_decom.remove((c_cond_dim,))
                    temp_outliers.discard(c_cond_dim)
                    temp_outliers.discard(c_given_dim)
                    temp_decom.append(next_candidate)
                    if c_cond_dim in temp_final_discrs:
                        temp_final_discrs.pop(c_cond_dim)
                    temp_final_discrs[c_cond_dim] = c_discr
                    temp_candidate_set[c_cond_dim] = self.conditional_ces[next_candidate]
                    self.search_history[c_cond_dim][next_candidate] = [self.conditional_ces[next_candidate], c_discr]
                    new_score = self.local_ncmi(next_candidate, decom_=temp_decom, multiv_res_=temp_multiv_res)
                    if not new_score > current_score:
                        update_info = False
                        remove_rev = False
                        # if r_check:
                        # redundant kept
                        #    print("redundant kept, candidate removed")
                        # else:
                        # candidate added
                    #    if r_check:
                    #        print("redundnat removed, candidate added")

                    if update_info:
                        self.decomposition = temp_decom
                        self.outliers = temp_outliers
                        self.search_candidate_set = temp_candidate_set
                        self.final_discretizations = temp_final_discrs
                        self.multiv_results = temp_multiv_res
                else:
                    # candidate and all variable changes discarded, redundant term as well as reverse are kept
                    remove_rev = False
            if remove_rev:
                self.remove_reverse(next_candidate)

        # compute dependency score
        self.compute_dependency_score()
        self.joint_ce = sum(self.search_candidate_set.values())
        self.mce_scores.append(list(np.cumsum(list(self.search_candidate_set.values()))))
        self.mce_scores_check = self.search_candidate_set

        # write results into a file
        for key in self.multiv_results:
            res = self.multiv_results[key]
            res_cond_dim = key
            res_used_combination = res[0]
            res_joint_cond_ce = res[1]
            res_number_of_bins = res[2]
            self.results_file.write("\nnumber of bins: " + str(res_number_of_bins) + "\n")
            self.results_file.write(
                "h(X" + str(str(res_cond_dim)) + ") = " + str(self.cumulative_entropies[res_cond_dim]) + "\n")
            self.results_file.write(
                "h(X" + str(str(res_cond_dim)) + "|X" + str(res_used_combination) + ") = " + str(
                    res_joint_cond_ce) + "\n")

        self.results_file.write("mce_uds_score: " + str(self.mce_uds_score) + "\n\n")
        self.results_file.write(str(self.data.shape) + "\n\n")
        self.results_file.write("h(X0,...,X" + str(self.nr_dims - 1) + ") = ")

        for combination in sorted(self.decomposition):
            if len(combination) > 1:
                self.results_file.write("h(X" + str(combination[0]) + "|")
                self.results_file.write("X" + str(combination[1]))
                self.results_file.write(") + ")
            else:
                self.results_file.write("h(X" + str(combination[0]) + ") + ")
        self.results_file.write(" = " + str(self.joint_ce) + "\n\n")

    def mce_exp_version(self):
        self.multiv_terms = 0
        # self.worse_than_expected = 0
        self.search_candidate_set = self.cumulative_entropies.copy()

        # while the candidate set is not empty
        while self.candidates_ncmi:
            remove_rev = True
            next_candidate = self.candidates_ncmi.popitem(last=False)[0]
            c_cond_dim = next_candidate[0]
            c_given_dim = next_candidate[1]
            c_discr = self.candidates_discr[next_candidate]
            self.used_discretizations[next_candidate] = c_discr

            current = [v for i, v in enumerate(self.decomposition) if v[0] == c_cond_dim and len(v) > 1]
            # if this candidate is already in the current combination - do joint discretization
            if len(current) != 0:
                current_term = current[0]
                if isinstance(current_term[1], tuple):
                    given_dims = list(current_term[1])
                else:
                    given_dims = [current_term[1]]
                given_dims.append(c_given_dim)
                # check if adding this candidate would create cycles
                creates_cycle = self.creates_cycle(current_term, (c_cond_dim, tuple(given_dims)), self.decomposition)
                if creates_cycle:
                    print("creates cycle: ", (c_cond_dim, tuple(given_dims)))
                    continue
                joint_discr_combination = self.jd(c_cond_dim, given_dims)
                if len(joint_discr_combination) == 0: continue
                joint_cond_ce = list(list(joint_discr_combination.values())[0].keys())[0]
                jd_comb = list(joint_discr_combination.keys())[0]
                joint_discretization = list(list(joint_discr_combination.values())[0].values())[0]
                nr_bins = len(joint_discretization)
                if isinstance(jd_comb, tuple):
                    if c_given_dim not in jd_comb:
                        remove_rev = False
                if joint_cond_ce < self.search_candidate_set[c_cond_dim]:
                    self.search_candidate_set.pop(c_cond_dim)
                    self.decomposition.remove(current_term)
                    self.decomposition.append((c_cond_dim, jd_comb))
                    assert (c_cond_dim, jd_comb) in self.decomposition, str((c_cond_dim, jd_comb)) + "\n" + str(
                        self.decomposition)
                    self.multiv_results[c_cond_dim] = [jd_comb, joint_cond_ce, nr_bins]
                    self.search_candidate_set[c_cond_dim] = joint_cond_ce
                    if c_cond_dim in self.final_discretizations: self.final_discretizations.pop(c_cond_dim)
                    self.final_discretizations[c_cond_dim] = joint_discretization
                else:
                    # candidate and all variable changes discarded, redundant term as well as reverse are kept
                    remove_rev = False

            else:
                # check if adding this candidate would create cycles
                creates_cycle = self.creates_cycle((c_cond_dim,), next_candidate, self.decomposition)
                if creates_cycle:
                    print("created cycles")
                    continue
                if self.conditional_ces[next_candidate] < self.search_candidate_set[c_cond_dim]:
                    self.search_candidate_set.pop(c_cond_dim)
                    self.decomposition.remove((c_cond_dim,))
                    self.decomposition.append(next_candidate)
                    if c_cond_dim in self.final_discretizations: self.final_discretizations.pop(c_cond_dim)
                    self.final_discretizations[c_cond_dim] = c_discr
                    self.search_candidate_set[c_cond_dim] = self.conditional_ces[next_candidate]
                else:
                    # candidate and all variable changes discarded, redundant term as well as reverse are kept
                    remove_rev = False
            if remove_rev:
                self.remove_reverse(next_candidate)

        # compute dependency score
        self.compute_dependency_score()
        self.joint_ce = sum(self.search_candidate_set.values())
        self.mce_scores.append(list(np.cumsum(list(self.search_candidate_set.values()))))
        self.mce_scores_check = self.search_candidate_set
