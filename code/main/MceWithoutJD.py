import numpy as np
import pandas as pd

import experiments.PrecisionRecallT as pr
from main import JointDiscretization as jd, preprocessing
from main import expectation_check as exp


class MceWithoutJd:
    def __init__(self, estimation, do_exp_check):

        self.data = estimation.data
        self.number_of_bins = estimation.number_of_bins
        self.number_of_dims = self.data.shape[1]
        self.results_file = estimation.results_file
        self.cumulative_entropies = estimation.cumulative_entropies
        self.candidate_set = estimation.candidate_set_values.copy()
        self.candidate_set_discr = estimation.candidate_set_disrs.copy()
        self.conditional_ces = estimation.conditional_ces
        self.used_dimensions = [(d,) for d in range(self.number_of_dims)]
        self.multiv_results = {}
        self.used_discretizations = {}
        self.mce_scores = estimation.mce_scores
        self.do_exp_check = do_exp_check

    def remove_candidate(self, next_dim_id, remove_rev):
        self.used_discretizations[next_dim_id] = self.candidate_set_discr[next_dim_id]
        self.candidate_set.pop(next_dim_id)
        self.candidate_set_discr.pop(next_dim_id)
        if remove_rev:
            reverse = next_dim_id[::-1]
            if reverse in self.candidate_set:
                self.candidate_set.pop(reverse)
                self.candidate_set_discr.pop(reverse)

    def mce_pure_estimation(self):

        #print("mce no jd")
        self.results_file.write("\nMCE no joint discretization\n")
        self.results_file.write("candidate set: " + str(list(self.candidate_set.keys())) + "\n")
        uds_score_numerator = 0
        uds_score_denominator = 0
        temp_candidate_set = self.cumulative_entropies.copy()
        candidates = self.candidate_set.copy()

        while self.candidate_set:
            #add_singletone = True
            remove_rev = True
            max_ce = max(self.candidate_set.values())
            next_dim_id = list(self.candidate_set.keys())[list(self.candidate_set.values()).index(max_ce)]
            cond_dim = next_dim_id[0]
            candidate = [v for i, v in enumerate(self.used_dimensions) if v[0] == next_dim_id[0] and len(v) > 1]

            # if this candidate is already in the current combination
            if len(candidate) != 0:
                used_d = candidate[0][1]

                #!!!!!!!!!!!!!!!
                if isinstance(used_d, tuple):
                    discr = [pd.DataFrame(self.data).index]
                    # discr = used_discretizations[(cond_dim, used_d[0])]
                    for d in used_d:
                        ext = preprocessing.extend_I(discr, self.used_discretizations[(cond_dim, d)])
                        discr = ext
                    used_d += (next_dim_id[1],)
                    used_combination = used_d
                else:
                    discr = self.used_discretizations[candidate[0]]
                    used_combination = (candidate[0][1], next_dim_id[1])
                #!!!!!!!!!!!!!!!!!

                #!!!!!!!!!!!!!!!
                ext = preprocessing.extend_I(discr, self.candidate_set_discr[next_dim_id])
                amount_of_bins = len(ext)
                # print(amount_of_bins)
                joint_cond_ce = jd.conditional_ce_all_bins(self.data[:, cond_dim], ext)
                #!!!!!!!!!!!!!!!

                # expectation check
                if self.do_exp_check:
                    better_than_expected = exp.expected_ce(ext, self.data[:, cond_dim], joint_cond_ce)
                    # print(better_than_expected)
                    if not better_than_expected:
                        print("worse than expected, discretization discarded")
                        self.remove_candidate(next_dim_id, remove_rev)
                        continue

                #significance check
                if joint_cond_ce < temp_candidate_set[next_dim_id[0]]:
                    temp_candidate_set[next_dim_id[0]] = joint_cond_ce
                    self.multiv_results[cond_dim] = [used_combination, joint_cond_ce, amount_of_bins]
                    self.used_dimensions.remove(candidate[0])
                    self.used_dimensions.append((cond_dim, used_combination))

                #add_singletone = False

                # TODO: add conditioned dims one by one, exclude if exp check is not passed

            else:
                #if add_singletone:
                if self.do_exp_check:
                    discretization = self.candidate_set_discr[next_dim_id]
                    cond_ce = self.conditional_ces[next_dim_id]
                    #print("actual: ", str(cond_ce))
                    better_than_expected = exp.expected_ce(discretization, self.data[:, cond_dim], cond_ce)
                    #print(better_than_expected)
                    if not better_than_expected:
                        print("worse than expected, discretization discarded")
                        self.remove_candidate(next_dim_id, remove_rev)
                        continue

                if self.conditional_ces[next_dim_id] < temp_candidate_set[next_dim_id[0]]:
                    temp_candidate_set.pop(next_dim_id[0])
                    self.used_dimensions.remove((next_dim_id[0],))
                    self.used_dimensions.append(next_dim_id)
                    temp_candidate_set[next_dim_id[0]] = self.conditional_ces[next_dim_id]

            self.remove_candidate(next_dim_id, remove_rev)
            """
            used_discretizations[next_dim_id] = self.candidate_set_discr[next_dim_id]
            self.candidate_set.pop(next_dim_id)
            self.candidate_set_discr.pop(next_dim_id)
            if remove_rev:
                reverse = next_dim_id[::-1]
                if reverse in self.candidate_set:
                    self.candidate_set.pop(reverse)
                    self.candidate_set_discr.pop(reverse)"""

        #if compute_full_score:
        for dim in temp_candidate_set:
            curr_diff = self.cumulative_entropies[dim] - temp_candidate_set[dim]
            # print(dim)
            # print(str(ces[dim]), " - ", str(temp_candidate_set[dim]), " = ", str(curr_diff), "\n")
            uds_score_numerator += curr_diff
            if curr_diff != 0:
                uds_score_denominator += self.cumulative_entropies[dim]

        if uds_score_numerator == 0:
            mce_uds_score = 0
        else:
            mce_uds_score = uds_score_numerator / uds_score_denominator

        for key in self.multiv_results:
            res = self.multiv_results[key]
            res_cond_dim = key
            res_used_combination = res[0]
            res_joint_cond_ce = res[1]
            res_number_of_bins = res[2]

            self.results_file.write("\nnumber of bins: "+str(res_number_of_bins)+"\n")
            self.results_file.write(
                "h(X"+str(str(res_cond_dim))+") = "+str(self.cumulative_entropies[res_cond_dim])+"\n")
            self.results_file.write(
                "h(X"+str(str(res_cond_dim))+"|X"+str(res_used_combination)+") = "+str(res_joint_cond_ce)+"\n")

        self.joint_ce = sum(temp_candidate_set.values())
        self.mce_scores.append(list(np.cumsum(list(temp_candidate_set.values()))))

        precision, recall = pr.pr_r(candidates, self.used_dimensions, transitivity=True)
        self.results_file.write("\nprecision: " + str(precision) + ", recall: " + str(recall) + "\n\n")
        self.results_file.write("mce_uds_score: " + str(mce_uds_score) + "\n\n")

        for combination in sorted(self.used_dimensions):
            if len(combination) > 1:
                self.results_file.write("h(X" + str(combination[0]) + "|")
                self.results_file.write("X" + str(combination[1]))
                self.results_file.write(") + ")
            else:
                self.results_file.write("h(X" + str(combination[0]) + ") + ")
        self.results_file.write(" = " + str(self.joint_ce) + "\n\n")
