def significance_check(old_value, new_value, threshold):

    check_passed = False

    if old_value - new_value > threshold:
        check_passed = True

    return check_passed
