from operator import itemgetter

import numpy as np
import pandas as pd
import math


# oneDimCE -  for every dimension calculate its CE, save into a map
def one_dim_ce(dimension):
    cumulative_entropy = 0
    dimension = sorted(dimension)

    for i in range(0, len(dimension)-1):
        cumulative_entropy += (dimension[i+1] - dimension[i]) * ((i+1) / len(dimension)) * math.log2((i+1) / len(dimension))

    cumulative_entropy = (-1) * cumulative_entropy
    return cumulative_entropy


#TODO: add intersection!
def shannon_entropy(I, number_of_points):
    entropy = 0
    for bin in I:
        l = len(bin)
        entropy -= (l / number_of_points) * math.log2(l / number_of_points)
    return entropy


#given_dim_bin - a bin of dimension Y w.r.t. the points' ids
# current_bin w.r.t. the conditioned_dim
#def conditional_ce(conditioned_dim, discretized_dim, current_bin=None):
def conditional_ce(conditioned_dim, given_dim_bin, I):

    #conditioned_dim = sorted(conditioned_dim)
    #intercestion = point_ids.intersection(c)
    #dataloc = data.loc[point_ids.intersection(c), dim]
    #points = [conditioned_dim[point_id] for point_id in given_dim_bin]

    #sum([len(c) / len(conditioned_dim) * one_dim_ce([conditioned_dim[point_id] for point_id in given_dim_bin.intersection(c)]) for c in I])

    cond_cumulative_entropy = 0
    for discr in I:
        points = [conditioned_dim[point_id] for point_id in given_dim_bin.intersection(discr)]
        cond_cumulative_entropy += one_dim_ce(points) * (len(discr) / len(conditioned_dim))

    return cond_cumulative_entropy


def rank_dimensions(cumulative_entropies):
    return sorted(cumulative_entropies, key=cumulative_entropies.get, reverse=True)


def ce_per_dimension(data):
    cumulative_entropies = {}

    # compute CE for each dimension, add to the dictionary
    for i in range(0, data.shape[1]):
        cumulative_entropy = one_dim_ce(data[:, i])
        cumulative_entropies[i] = cumulative_entropy

    # sort dimensions in the descending order of CEs
    # ordered_cumulative_entropies = sorted(cumulative_entropies.items(), key=lambda kv: float(kv[1]), reverse=True)
    #ordered_dimensions = sorted(cumulative_entropies, key=cumulative_entropies.get, reverse=True)

    return cumulative_entropies


def sort_dims_asc_ces(ces):
    sorted_ces = sorted(ces.items(), key=itemgetter(1))
    sorted_dimensions = [ce[0] for ce in sorted_ces]
    return sorted_dimensions


def sort_dims_desc_ces(ces):
    sorted_ces = sorted(ces.items(), key=itemgetter(1), reverse=True)
    sorted_dimensions = [ce[0] for ce in sorted_ces]
    return sorted_dimensions


#def form_initial_bins(discretized, number_of_bins):
def form_initial_bins(data, dim_id, number_of_bins):

    ranked_data = pd.DataFrame(data).rank(method='first')
    #y_discretized = pd.qcut(discretized, number_of_bins, precision=15, duplicates='drop')
    #discretized = pd.DataFrame(discretized).rank(method='first')
    y_discretized = pd.qcut(ranked_data[dim_id], number_of_bins)
    return y_discretized

'''
def join_bins(bins):
    new_left_value = bins[0][0]
    new_right_value = bins[len(bins) - 1][1]
    joined_bin = [new_left_value, new_right_value]
    # print("\n" + str(joined_bin) + "\n")
    return joined_bin'''

'''
def discr_to_bins_of_ids(dimension, discretization):
    discr_ids = []

    for b in discretization:
        b_id = []
        for point_id in range(0, len(dimension)):
            if dimension[point_id] in b:
                if len(discr_ids) == 0:
                    b_id.append(point_id)
                elif point_id not in discr_ids[-1]:
                    b_id.append(point_id)
        discr_ids.append(b_id)

    if len(discr_ids) == 0:
        print("discr ids is empty")

    return discr_ids'''

'''
def project_i_on_dim(I, dimension_to_project_on, data):
    prev_dims_ids = list(I.keys())
    prev_dims = []

    bins_cond_dim = []
    merged_discr_tuples = []
    for dim_id in prev_dims_ids:
        dim = sorted(data[:, dim_id])
        prev_dims.append(dim)
        current_discr = I[dim_id]

        if prev_dims_ids.index(dim_id) == 0:
            # creates bins of points for cond.d. w.r.t. to the discr. of previous dimension
            bins_cond_dim = jd.create_bins_cond_dim(dimension_to_project_on, dim, current_discr)
            check1 = len(bins_cond_dim)
            b_support = jd.discr_support(bins_cond_dim, intervals=False)
            s = sum(b_support)

        if prev_dims_ids.index(dim_id) > 0:
            projection_current_discr = jd.project_discr(dim, current_discr)

            if prev_dims_ids.index(dim_id) == 1:
                prev_dim_id = prev_dims_ids[prev_dims_ids.index(dim_id) - 1]
                prev_discr = I[prev_dim_id]
                prev_dim = sorted(data[:, prev_dim_id])
                projection_prev_discr = jd.project_discr(prev_dim, prev_discr)

            else:
                projection_prev_discr = jd.project_discr(jd.join_dimensions(prev_dims[0:prev_dims_ids.index(dim_id)]),
                                                         merged_discr_tuples, interval_discr=False)

            # if prev_dims_ids.index(dim_id) > 1:
            projected_dims = prev_dims[0: prev_dims_ids.index(dim_id) + 1]
            bins_cond_dim.clear()

            if len(projection_prev_discr) == 0:
                print("projection prev discr is empty")
            if len(projection_current_discr) == 0:
                print("projection current discr is empty")

            if len(projected_dims) == 1:
                bins_cond_dim = jd.merge_projections(projection_prev_discr, projection_current_discr,
                                                     cond_dim=dimension_to_project_on)
                check2 = len(bins_cond_dim)
            else:
                bins_cond_dim, merged_discr_tuples = jd.merge_projections(projection_prev_discr,
                                                                          projection_current_discr,
                                                                          cond_dim=dimension_to_project_on,
                                                                          projected_dims=projected_dims)
                check3 = len(bins_cond_dim)

    return bins_cond_dim '''


def extend_I(I, discr):
    discr_ = [i.intersection(j) for i in I for j in discr]

    # todo python361
    # return [d for d in disc_ if not d.empty]
    # todo python342
    return [d for d in discr_ if not d.size == 0]


#def compute_f_val_b(data, number_of_bins, dim_cond_index, dim_discr_index, ce_cond_dim, I, e):
def compute_f_val_b(data, number_of_bins, dim_cond_index, dim_discr_index, ce_cond_dim, I, e, early_stopping=False):

    f = np.full(shape=(number_of_bins, number_of_bins), fill_value=0.0, dtype=float)
    val = np.full(shape=(number_of_bins, number_of_bins), fill_value=0.0, dtype=float)
    final_bins = np.empty(shape=(number_of_bins, number_of_bins), dtype=list)
    #final_bins_support = []

    # initial equal-frequency discretizations of X
    conditioned_dim = data[:, dim_cond_index]
    #discretized_dim = data[:, dim_discr_index]

    number_of_points = len(conditioned_dim)
    dimension_binned = form_initial_bins(data, dim_discr_index, number_of_bins)
    dimension_binned = dimension_binned.cat.rename_categories(
        [i for i in range(number_of_bins)]).reindex(pd.Series(dimension_binned).index)

    #print(dimension_binned)

    s = dimension_binned.value_counts().sort_index().cumsum()

    for j in range(0, number_of_bins):
        for i in range(j, number_of_bins):
            current_bin = dimension_binned[np.logical_and(dimension_binned <= i, dimension_binned >= j)].index
            cond_ce = conditional_ce(conditioned_dim, current_bin, I)

            f[j][i] = cond_ce

            #early stopping
            if j == 1:
                if early_stopping:
                    if min(f[0][0], f[0][-1])/max(f[0][0], f[0][-1]) >= 0.95:
                        #print("stopped")
                        return ce_cond_dim, None, None
            if j == 0:
                val[0][i] = f[0][i]
                final_bins[0][i] = [current_bin]

    for l in range(1, number_of_bins):
        for i in range(l, number_of_bins):
            omega = {}

            for j in range(l - 1, i):
                o = ((s[i] - s[j]) / s[i]) * f[j + 1][i] + (s[j] / s[i]) * val[l - 1][j]

                omega[j] = o

            min_omega = min(sorted(omega.values()))
            pos = min(omega, key=omega.get)
            val[l][i] = min_omega

            discr = final_bins[l-1][pos].copy()
            union_bins = dimension_binned[np.logical_and(dimension_binned <= i, dimension_binned >= pos + 1)].index
            #union_bins = [p_id for p_id in dimension_binned[np.logical_and(dimension_binned <= i, dimension_binned >= pos+1)].index]
            discr.append(union_bins)
            final_bins[l][i] = discr

    regularization = {}
    regularization_I = {}
    log_number_of_bins = math.log2(number_of_bins) + e

    for l in range(0, number_of_bins):
        reg_I = extend_I(I, final_bins[l][number_of_bins - 1])
        entropy = shannon_entropy(reg_I, number_of_points)
        if ce_cond_dim == 0: reg_value = entropy / log_number_of_bins
        elif log_number_of_bins == 0: reg_value = val[l][number_of_bins - 1] / ce_cond_dim
        else: reg_value = (val[l][number_of_bins - 1] / ce_cond_dim) + (entropy / log_number_of_bins)
        regularization[l] = reg_value
        regularization_I[l] = reg_I

    delta_min = min(regularization, key=regularization.get)
    return val[delta_min][number_of_bins - 1], final_bins[delta_min][number_of_bins - 1], regularization_I[delta_min]


def compute_score(data, dim_x, dim_y, number_of_bins, I, e, ce_x, univariate=False, early_stopping=False):
    optimal_cce, final_bins, I = compute_f_val_b(data, number_of_bins, dim_x, dim_y, ce_x, I, e, early_stopping=early_stopping)
    #result = [ce_x - optimal_cce, final_bins]
    result = [(ce_x - optimal_cce)/ce_x, final_bins]  # normalized version
    if univariate: return result, optimal_cce
    else: return result, optimal_cce, I


#def generate_candidates(data, number_of_bins, signficance_threshold, I, e):
#def generate_candidates(data, number_of_bins, I, e=0):
def generate_candidates(data, number_of_bins, I, e=0, early_stopping=False):
    candidates_ncmi = {}
    candidates_discr = {}
    conditional_ces = {}
    ces = ce_per_dimension(data)
    number_of_points = data.shape[0]
    number_of_dimensions = data.shape[1]
    counter = 0
    counter_all_combinations = 0
    for i in range(0, number_of_dimensions):
        for j in range(0, number_of_dimensions):
            counter += 1
            if i != j:
                #threshold_value = ces[i] * signficance_threshold
                res, optimal_cce = compute_score(data, i, j, number_of_bins, I, e,  ces[i], univariate=True, early_stopping=early_stopping)
                #res, optimal_cce = compute_score(data, i, j, number_of_bins, I, e, ces[i], univariate=True)
                #ce_diff = res[0]
                norm_cmi = res[0]
                discr = res[1]
                counter_all_combinations += 1
                # TODO: substitute with significance check
                #if optimal_cce < threshold_value:  # optimal cce should be much smaller than ces[i]
                if optimal_cce < ces[i]:
                    #candidate_set_values[i, j] = ce_diff
                    candidates_ncmi[i, j] = norm_cmi
                    candidates_discr[i, j] = discr
                    conditional_ces[i, j] = optimal_cce
                """
                #expectation check
                from main import expectation_check as exp
                if discr is not None:
                    exp.expected_ce(discr, data[:, i], optimal_cce)"""
    # sort the candidate set in the descending order of values (the most significant are placed first)
    from collections import OrderedDict
    candidates_ncmi = OrderedDict(sorted(candidates_ncmi.items(), key=itemgetter(1), reverse=True))
    #candidates_discr = OrderedDict(sorted(candidates_discr.items(), key=itemgetter(1), reverse=True))
    #print("normalized candidate set")
    #print(candidates_ncmi, "\n")
    return candidates_ncmi, candidates_discr, conditional_ces, counter_all_combinations


"""
            if j == i:
                # I - prev. discretizations
                # project each discr onto cond.d., project new bin onto cond.d., merge, calculate ce
                if len(I) == 0:

                    # TODO: bins correspondence problem
                    cond_ce = conditional_ce(conditioned_dim, discretized_dim, current_bin=initial_bins_cond[i])
                else:
                    # project the new bin onto cond dim, merge with bins_cond_dim, calculate ce_all_bins
                    projection_current_bin = jd.project_discr(discretized_dim,
                                                              [initial_bins[i]])  # initial bins or initial bins cond?
                    cond_dim_projection_current_bin = [[conditioned_dim[point_id] for point_id in projection_current_bin[0]]]
                    if len(cond_dim_projection_current_bin) == 0:
                        print("cond dim projection current bin is empty")
                    c1 = len(bins_cond_dim)
                    check_support = jd.discr_support(bins_cond_dim, intervals=False)
                    check_sum = sum(len(b) for b in bins_cond_dim)
                    merged_bins_cond_dim = jd.merge_projections(discr_to_bins_of_ids(conditioned_dim, bins_cond_dim),
                                                                discr_to_bins_of_ids(conditioned_dim,
                                                                                     cond_dim_projection_current_bin),
                                                                cond_dim=conditioned_dim, single_bin_projection=True)
                    c2 = len(merged_bins_cond_dim)
                    merged_bins_support = jd.discr_support(merged_bins_cond_dim, intervals=False)
                    final_bins_support = merged_bins_support
                    s1 = sum(merged_bins_support)

                    cond_ce = jd.conditional_ce_all_bins(merged_bins_cond_dim, len(conditioned_dim),
                                                         merged_bins_support)

                    # b = [conditioned_dim[point_id] for point_id in projection_current_bin[0]]
                    # for point_id in projection_current_bin[0]:
                    #    b.append(conditioned_dim[point_id])
            else:
                if len(initial_bins) > i and indicator_discr_dim:
                    bins_to_join_discr_dim.append(initial_bins[i])
                elif len(initial_bins) > i and not indicator_discr_dim:
                    bins_to_join_discr_dim = [initial_bins[i]]

                joined_bin_discr = join_bins(bins_to_join_discr_dim)

                if len(I) == 0:
                    joined_support += bins_support_discr[i]

                    if len(initial_bins_cond) > i and indicator_cond_dim:
                        bins_to_join_cond_dim.append(initial_bins_cond[i])
                    elif len(initial_bins_cond) > i and not indicator_cond_dim:
                        bins_to_join_cond_dim = [initial_bins_cond[i]]

                    joined_bin_cond = join_bins(bins_to_join_cond_dim)
                    cond_ce = conditional_ce(conditioned_dim, discretized_dim, current_bin=joined_bin_cond)

                else:
                    projection_joined_bin = jd.project_discr(discretized_dim, [joined_bin_discr])
                    cond_dim_projection_joined_bin = [
                        [conditioned_dim[point_id] for point_id in projection_joined_bin[0]]]
                    if len(cond_dim_projection_joined_bin) == 0:
                        print("cond dim projection joined bin is empty")
                    c1 = len(bins_cond_dim)
                    merged_bins_cond_dim = jd.merge_projections(discr_to_bins_of_ids(conditioned_dim, bins_cond_dim),
                                                                discr_to_bins_of_ids(conditioned_dim,
                                                                                     cond_dim_projection_joined_bin),
                                                                cond_dim=conditioned_dim, single_bin_projection=True)
                    c2 = len(merged_bins_cond_dim)
                    merged_bins_support = jd.discr_support(merged_bins_cond_dim, intervals=False)
                    final_bins_support = merged_bins_support
                    s1 = sum(merged_bins_support)

                    cond_ce = jd.conditional_ce_all_bins(merged_bins_cond_dim, len(conditioned_dim),
                                                         merged_bins_support) """

#if __name__ == "__main__":


