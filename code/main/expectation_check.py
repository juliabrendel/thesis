import math
from scipy.stats import hypergeom
import numpy as np
from main import JointDiscretization as jd

"""
[M, n, N] = [10, 5, 5]
rv = hypergeom(M, n, N)
pmf = rv.pmf(5)
print(pmf) """

"""
def expected_ce(binning, ce_value, total_size):

    # binning = [size_bin_1,...,size_bin_k]
    #population_size = len(conditioned_dimension)
    number_of_bins = len(binning)
    print("number_of_bins = ", str(number_of_bins))
    expected = 0
    for i in range(number_of_bins):
        bin_size = len(binning[i])
        p = hypergeom(total_size, bin_size, bin_size).pmf(bin_size)
        print("p = ", str(p))
        expected -= p * (bin_size / total_size) * math.log2(bin_size / total_size)

    print("expected ce = ", str(expected))
    print("actual_ce = ", str(ce_value))
    if ce_value <= expected:
        print("passed")"""


def cond_ce(discretization, conditioned_dim):

    cond_cumulative_entropy = 0
    for bin in discretization:
        points = [conditioned_dim[point_id] for point_id in bin]
        from main import preprocessing
        cond_cumulative_entropy += preprocessing.one_dim_ce(points) * (len(bin) / len(conditioned_dim))

    return cond_cumulative_entropy


def expected_ce(discretization, conditioned_dim, ce_value):
    """
    actual = jd.conditional_ce_all_bins(conditioned_dim, discretization)
    if actual != ce_value:
        print("wrong ce value")"""

    expected = 0
    iterations = 1000
    for i in range(0, iterations):
        permutated = np.random.permutation(conditioned_dim)
        #cce = cond_ce(discretization, permutated)
        cce = jd.conditional_ce_all_bins(permutated, discretization)
        expected += cce

    expected /= iterations

    #print("\nactual value = ", str(ce_value))
    #print("expected value = ", str(expected))
    if ce_value < expected:
        #print("better than expected")
        return True
    else:
        #print("\nactual value = ", str(ce_value))
        #print("expected value = ", str(expected))
        print("%.20f" % (expected - ce_value))
        return False
