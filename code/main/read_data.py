import pandas as pd
from numpy import genfromtxt


def read(path_to_file):
    data = genfromtxt(path_to_file, delimiter=",")
    # data = genfromtxt(path_to_file, delimiter=';', usecols=range(0, 10))
    # data = genfromtxt(path_to_file, delimiter=";")
    # data = pd.read_csv(path_to_file)
    # data = pd.read_csv(path_to_file, delimiter=' ', header=None)
    # data = data.loc[:, :3]
    return data
