import math
import numpy as np
import pandas as pd

import experiments.PrecisionRecallT as prTest
from main import JointDiscretization as jd, expectation_check as exp, preprocessing


class UdsVariations:

    def __init__(self, estimation):
        self.data = estimation.data
        self.number_of_bins = estimation.number_of_bins
        self.number_of_dims = self.data.shape[1]
        self.results_file = estimation.results_file
        self.cumulative_entropies = estimation.cumulative_entropies
        self.ordered_dimensions = estimation.ordered_dimensions
        self.I = estimation.initial_I
        self.candidate_set = estimation.candidate_set_values
        self.ce_values = estimation.ce_values
        self.total_score_values = estimation.total_score_values
        self.mce_scores = estimation.mce_scores
        self.uds_score = 0
        self.final_discretizations = {}
        self.multiv_results = {}
        self.resulting_combination = []
        self.amount_of_bins_per_term = {}

    def verify_result(self):

        full_id_set = list(range(self.number_of_dims))
        res_id_set = [i[0] for i in self.resulting_combination]
        diff = np.setdiff1d(full_id_set, res_id_set)
        assert (len(diff) == 0), str(diff)

    def uds_pure(self):
        self.results_file.write("\nUDS_pure\n")
        e = 0
        uds_score_numerator = 0
        uds_score_denominator = 0
        scores = []
        self.mce_score = max(self.cumulative_entropies.values())
        scores.append(self.mce_score)
        # mce_score = 0
        previous_dims = []
        I = self.I.copy()
        for dim_idx in range(1, len(self.ordered_dimensions)):
            current_id = self.ordered_dimensions[dim_idx]
            previous_id = self.ordered_dimensions[dim_idx - 1]
            previous_dims.append(previous_id)
            res, optimal_cce, I_new = \
                preprocessing.compute_score(self.data, current_id, previous_id, self.number_of_bins, I, e,
                                            self.cumulative_entropies[current_id])

            discr = res[1]
            final_number_of_bins = len(discr)
            # expectation check
            """"
            better_than_expected = exp.expected_ce(discr, self.data[:, current_id], optimal_cce)
            if not better_than_expected:
                print("worse than expected\n")"""
            self.mce_score += optimal_cce
            scores.append(self.mce_score)
            self.final_discretizations[(current_id, tuple(previous_dims))] = discr
            self.multiv_results[current_id] = [previous_dims, optimal_cce, final_number_of_bins]
            e += math.log2(len(discr))
            I = I_new
            # todo add for other variations
            self.amount_of_bins_per_term[current_id] = len(I)
            self.results_file.write("amount of bins of the discretized dimension: " + str(len(res[1])) + "\n")
            self.results_file.write("resulting amount of bins of the current dimension: " + str(len(I)) + "\n")
            self.results_file.write("h(X" + str(self.ordered_dimensions[dim_idx]) + ") = " +
                                    str(self.cumulative_entropies[self.ordered_dimensions[dim_idx]]) + "\n")
            self.results_file.write("h(X" + str(self.ordered_dimensions[dim_idx]) + "|X" + str(previous_dims) + ") = " +
                                    str(optimal_cce) + "\n\n")
            uds_score_numerator += res[0]
            uds_score_denominator += self.cumulative_entropies[self.ordered_dimensions[dim_idx]]
        self.results_file.write("h(X0,...,X" + str(self.number_of_dims-1) + ") = " + str(self.mce_score) + "\n")
        self.uds_score = uds_score_numerator / uds_score_denominator
        self.results_file.write("\nUDS_pure = " + str(self.uds_score) + "\n")
        # !!!
        # resulting_combination = []
        for idx in range(len(self.ordered_dimensions)):
            if idx == 0:
                t = (self.ordered_dimensions[idx],)
            else:
                t = (self.ordered_dimensions[idx], tuple(self.ordered_dimensions[0:idx]))
            self.resulting_combination.append(t)
        self.ce_values.append(self.mce_score)
        self.total_score_values.append(self.uds_score)
        self.mce_scores.append(scores)
        self.verify_result()

    def uds_extended(self):
        self.results_file.write("\n\nUDS Extended \n\n")
        uds_score_numerator = 0
        uds_score_denominator = 0
        scores = []
        self.mce_score = max(self.cumulative_entropies.values())
        scores.append(self.mce_score)
        # mce_score = 0
        previous_dims = []
        #I = {}
        for dim_idx in range(1, len(self.ordered_dimensions)):
            current_id = self.ordered_dimensions[dim_idx]
            previous_id = self.ordered_dimensions[dim_idx - 1]
            temp_e = 0
            temp_I = self.I.copy()
            if len(previous_dims) != 0:
                for prev_dim_id in previous_dims:
                    temp_res, temp_optimal_cce, I = \
                        preprocessing.compute_score(self.data, current_id, prev_dim_id, self.number_of_bins,
                                                    temp_I, temp_e, self.cumulative_entropies[current_id])
                    temp_I = I
                    temp_e += math.log2(len(temp_res[1]))

            res, optimal_cce, I_new = \
                preprocessing.compute_score(self.data, current_id, previous_id, self.number_of_bins, temp_I, temp_e,
                                            self.cumulative_entropies[current_id])
            discr = res[1]
            final_number_of_bins = len(discr)
            # expectation check
            """
            better_than_expected = exp.expected_ce(discr, self.data[:, current_id], optimal_cce)
            if not better_than_expected:
                print("worse than expected\n")"""

            self.mce_score += optimal_cce
            scores.append(self.mce_score)
            previous_dims.append(previous_id)
            self.final_discretizations[(current_id, tuple(previous_dims))] = discr
            self.multiv_results[current_id] = [previous_dims, optimal_cce, final_number_of_bins]

            self.results_file.write("amount of bins of the discretized dimension: " + str(len(res[1])) + "\n")
            self.results_file.write("resulting amount of bins of the current dimension: " + str(len(temp_I)) + "\n")
            self.results_file.write("h(X" + str(self.ordered_dimensions[dim_idx]) + ") = " +
                               str(self.cumulative_entropies[self.ordered_dimensions[dim_idx]]) + "\n")
            self.results_file.write("h(X" + str(self.ordered_dimensions[dim_idx]) + "|X" + str(previous_dims) + ") = " +
                               str(optimal_cce) + "\n\n")
            uds_score_numerator += res[0]
            uds_score_denominator += self.cumulative_entropies[self.ordered_dimensions[dim_idx]]

        self.results_file.write("h(X0,...,X" + str(self.number_of_dims-1) + ") = " + str(self.mce_score) + "\n")
        self.uds_score = uds_score_numerator / uds_score_denominator
        self.results_file.write("\nUDS_extended = " + str(self.uds_score) + "\n")

        #!!!
        #resulting_combination = []
        for idx in range(len(self.ordered_dimensions)):
            if idx == 0:
                t = (self.ordered_dimensions[idx],)
            else:
                t = (self.ordered_dimensions[idx], tuple(self.ordered_dimensions[0:idx]))
            self.resulting_combination.append(t)
        self.ce_values.append(self.mce_score)
        self.total_score_values.append(self.uds_score)
        self.mce_scores.append(scores)
        self.verify_result()

    def uds_extended_jd(self):
        nr_dep = 0
        self.results_file.write("\nMCE with ordered dimensions/UDS with JD\n")
        self.mce_score = 0
        scores = []
        initial_I = self.I.copy()
        I = []
        uds_score_numerator = 0
        uds_score_denominator = 0
        given_dim = self.ordered_dimensions[0]
        cond_dim = self.ordered_dimensions[1]
        most_informative_dim = given_dim
        ce_most_inf_dim = self.cumulative_entropies[most_informative_dim]
        self.mce_score += ce_most_inf_dim
        scores.append(self.mce_score)
        self.resulting_combination.append((given_dim,))

        res, cce = preprocessing.compute_score(self.data, cond_dim, given_dim, self.number_of_bins, initial_I, 0,
                                               self.cumulative_entropies[cond_dim], univariate=True)
        self.mce_score += cce
        scores.append(self.mce_score)
        self.resulting_combination.append((cond_dim, given_dim))
        # self.results.write("\nh(X" + str(most_informative_dim) + ") = " + str(ce_most_inf_dim) + "\n\n")
        # self.results.write("h(X" + str(cond_dim) + "|X" + str(given_dim) + ") = " + str(cce) + "\n\n")
        uds_score_numerator += self.cumulative_entropies[cond_dim] - cce
        # print(str(self.cumulative_entropies[self.ordered_dimensions[cond_dim]]), " - ", str(cce), " = ",
        #      str(self.cumulative_entropies[cond_dim] - cce))
        uds_score_denominator += self.cumulative_entropies[cond_dim]
        I.append(given_dim)
        self.final_discretizations[(cond_dim, tuple(I))] = res[1]

        for dim_idx in range(2, len(self.ordered_dimensions)):
            I.append(self.ordered_dimensions[dim_idx - 1])
            candidates_discrs = []

            for d_id in I:
                # recalculate discretization
                cond_dim_id = self.ordered_dimensions[dim_idx]
                res, cce = preprocessing.compute_score(self.data, cond_dim_id, d_id, self.number_of_bins, initial_I, 0,
                                                       self.cumulative_entropies[cond_dim_id], univariate=True)
                discr = res[1]
                candidates_discrs.append(discr)

            # j.d. w.r.t. recalculated disretizations
            joint_discr_combination = jd.joint_discr(self.data, cond_dim_id, I, pd.DataFrame(candidates_discrs).T)
            if len(joint_discr_combination) == 0:
                self.resulting_combination.append((cond_dim_id,))
                self.mce_score += self.cumulative_entropies[cond_dim_id]
                scores.append(self.mce_score)
                """
                self.results_file.write("\nnumber of bins: 0 \n")
                self.results_file.write(
                    "h(X" + str(cond_dim_id) + ") = " + str(
                        self.cumulative_entropies[cond_dim_id]) + "\n")
                self.results_file.write("h(X" + str(cond_dim_id) + "|X" + str(I) + ") = "
                                + str(self.cumulative_entropies[cond_dim_id]) + "\n")"""
                continue

            joint_cce = list(list(joint_discr_combination.values())[0].keys())[0]
            used_combination = list(joint_discr_combination.keys())[0]
            print(used_combination)
            if isinstance(used_combination, tuple):
                nr_dep += len(used_combination)
            else:
                nr_dep += 1
            print(nr_dep, "\n")
            discretization = list(list(joint_discr_combination.values())[0].values())[0]
            amount_of_bins = len(list(list(joint_discr_combination.values())[0].values())[0])
            self.mce_score += joint_cce
            scores.append(self.mce_score)
            self.resulting_combination.append((cond_dim_id, used_combination))
            self.final_discretizations[(cond_dim_id, used_combination)] = discretization
            self.multiv_results[cond_dim] = [used_combination, joint_cce, amount_of_bins]
            uds_score_numerator += self.cumulative_entropies[cond_dim_id] - joint_cce
            uds_score_denominator += self.cumulative_entropies[cond_dim_id]
            """
            self.results.write("\nnumber of bins: " + str(amount_of_bins) + "\n")
            self.results.write(
                "h(X" + str(cond_dim_id) + ") = " + str(self.cumulative_entropies[cond_dim_id]) + "\n")
            self.results.write("h(X" + str(cond_dim_id) + "|X" + str(used_combination) + ") = "
                          + str(joint_cce) + "\n")"""

        self.results_file.write("\nh(X0,...,X" + str(self.number_of_dims - 1) + ") = " + str(self.mce_score) + "\n")
        if uds_score_numerator == 0: self.uds_score = 0
        else: self.uds_score = uds_score_numerator / uds_score_denominator
        self.results_file.write("\nUDS_jd = " + str(self.uds_score) + "\n")
        self.ce_values.append(self.mce_score)
        self.total_score_values.append(self.uds_score)
        self.mce_scores.append(scores)
        self.verify_result()

    def uds_jd(self):

        self.results_file.write("\nUDS pure with JD\n")
        self.mce_score = 0
        scores = []
        initial_I = self.I.copy()
        candidate_set = self.candidate_set.copy()
        I = []
        candidates_discrs = []
        #!!!
        #resulting_combination = []
        uds_score_numerator = 0
        uds_score_denominator = 0
        given_dim = self.ordered_dimensions[0]
        cond_dim = self.ordered_dimensions[1]
        most_informative_dim = given_dim
        ce_most_inf_dim = self.cumulative_entropies[most_informative_dim]
        self.mce_score += ce_most_inf_dim
        scores.append(self.mce_score)
        self.resulting_combination.append((given_dim,))

        #res, cce = preprocessing.compute_score(self.data, cond_dim, given_dim, self.number_of_bins, initial_I, 0,
        #                                       univariate=True)
        #mce_score += cce
        #print(mce_score)
        #resulting_combination.append((given_dim, cond_dim))

        # self.results.write("\nh(X" + str(most_informative_dim) + ") = " + str(ce_most_inf_dim) + "\n\n")
        # self.results.write("h(X" + str(cond_dim) + "|X" + str(given_dim) + ") = " + str(cce) + "\n\n")

        # if compute_full_score:
        #uds_score_numerator += self.cumulative_entropies[cond_dim] - cce
        #uds_score_denominator += self.cumulative_entropies[cond_dim]

        #I.append(given_dim)
        #candidates_discrs.append(res[1])

        for dim_idx in range(1, len(self.ordered_dimensions)):

            cond_dim_id = self.ordered_dimensions[dim_idx]
            given_dim_id = self.ordered_dimensions[dim_idx-1]
            res, cce = preprocessing.compute_score(self.data, cond_dim_id, given_dim_id, self.number_of_bins, initial_I,
                                                   0, self.cumulative_entropies[cond_dim_id], univariate=True)
            discr = res[1]
            candidates_discrs.append(discr)
            I.append(given_dim_id)

            if len(I) == 1:
                self.mce_score += cce
                scores.append(self.mce_score)
                self.resulting_combination.append((cond_dim_id, given_dim_id))
                uds_score_numerator += self.cumulative_entropies[cond_dim_id] - cce
                uds_score_denominator += self.cumulative_entropies[cond_dim_id]
                continue

            joint_discr_combination = jd.joint_discr(self.data, cond_dim_id, I, pd.DataFrame(candidates_discrs).T)
            if len(joint_discr_combination) == 0:

                self.resulting_combination.append((cond_dim_id,))
                self.mce_score += self.cumulative_entropies[cond_dim_id]
                scores.append(self.mce_score)
                self.results_file.write("\nnumber of bins: 0 \n")
                self.results_file.write(
                    "h(X" + str(cond_dim_id) + ") = " + str(
                        self.cumulative_entropies[cond_dim_id]) + "\n")
                self.results_file.write("h(X" + str(cond_dim_id) + "|X" + str(I) + ") = "
                                + str(self.cumulative_entropies[cond_dim_id]) + "\n")
                continue

            joint_cce = list(list(joint_discr_combination.values())[0].keys())[0]
            used_combination = list(joint_discr_combination.keys())[0]
            amount_of_bins = len(list(list(joint_discr_combination.values())[0].values())[0])
            self.mce_score += joint_cce
            scores.append(self.mce_score)
            self.resulting_combination.append((cond_dim_id, used_combination))
            uds_score_numerator += self.cumulative_entropies[cond_dim_id] - joint_cce
            uds_score_denominator += self.cumulative_entropies[cond_dim_id]
            self.multiv_results[cond_dim] = [used_combination, joint_cce, amount_of_bins]

            self.results_file.write("\nnumber of bins: " + str(amount_of_bins) + "\n")
            self.results_file.write(
                "h(X" + str(cond_dim_id) + ") = " + str(self.cumulative_entropies[cond_dim_id]) + "\n")
            self.results_file.write("h(X" + str(cond_dim_id) + "|X" + str(used_combination) + ") = "
                          + str(joint_cce) + "\n")

        self.results_file.write("\nh(X0,...,X" + str(self.number_of_dims - 1) + ") = " + str(self.mce_score) + "\n")
        if uds_score_numerator == 0: self.uds_score = 0
        else: self.uds_score = uds_score_numerator / uds_score_denominator
        self.results_file.write("\nUDS_pure_jd = " + str(self.uds_score) + "\n")
        #precision/recall w.r.t. the candidate set
        #precision, recall = prTest.test(candidate_set, self.resulting_combination, transitivity=True)
        #self.results_file.write("\nprecision: " + str(precision) + ", recall: " + str(recall) + "\n\n")
        self.ce_values.append(self.mce_score)
        self.total_score_values.append(self.uds_score)
        self.mce_scores.append(scores)
        self.verify_result()
