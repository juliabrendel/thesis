MCE

number of bins: 4
h(X1) = 48.76702377
h(X1|X(2, 0)) = 7.34622728197

number of bins: 3
h(X35) = 553.286068156
h(X35|X(34, 33)) = 60.5755851534

number of bins: 6
h(X6) = 31.2919848028
h(X6|X(8, 7)) = 11.4544682465

number of bins: 5
h(X38) = 132.580221634
h(X38|X(37, 36)) = 51.3566133376

number of bins: 6
h(X11) = 111.105872717
h(X11|X(9, 10)) = 35.7257737746

number of bins: 4
h(X12) = 43.8821328061
h(X12|X(14, 13)) = 7.34984019077
mce_uds_score: 0.474199638693

(10001, 51)

h(X0,...,X50) = h(X0) + h(X1|X(2, 0)) + h(X2) + h(X3) + h(X4) + h(X5|X4) + h(X6|X(8, 7)) + h(X7) + h(X8) + h(X9) + h(X10) + h(X11|X(9, 10)) + h(X12|X(14, 13)) + h(X13) + h(X14) + h(X15) + h(X16|X17) + h(X17) + h(X18|X20) + h(X19) + h(X20) + h(X21) + h(X22) + h(X23) + h(X24|X26) + h(X25) + h(X26) + h(X27) + h(X28) + h(X29|X27) + h(X30|X32) + h(X31) + h(X32) + h(X33) + h(X34) + h(X35|X(34, 33)) + h(X36) + h(X37) + h(X38|X(37, 36)) + h(X39) + h(X40|X41) + h(X41) + h(X42) + h(X43) + h(X44) + h(X45) + h(X46) + h(X47) + h(X48) + h(X49) + h(X50) +  = 2979.44840726


UDS_pure
amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X2) = 402.212191381
h(X2|X[35]) = 402.212191381

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X20) = 335.523520442
h(X20|X[35, 2]) = 335.523520442

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X8) = 211.906245667
h(X8|X[35, 2, 20]) = 211.906245667

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X14) = 200.702487674
h(X14|X[35, 2, 20, 8]) = 200.702487674

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X5) = 197.820920714
h(X5|X[35, 2, 20, 8, 14]) = 197.820920714

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X38) = 132.580221634
h(X38|X[35, 2, 20, 8, 14, 5]) = 132.580221634

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X32) = 116.469698432
h(X32|X[35, 2, 20, 8, 14, 5, 38]) = 116.469698432

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X26) = 115.6230689
h(X26|X[35, 2, 20, 8, 14, 5, 38, 32]) = 115.6230689

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X41) = 114.719920673
h(X41|X[35, 2, 20, 8, 14, 5, 38, 32, 26]) = 114.719920673

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X11) = 111.105872717
h(X11|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41]) = 111.105872717

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X23) = 75.8616715628
h(X23|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11]) = 75.8616715628

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X46) = 72.5033218511
h(X46|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23]) = 72.5033218511

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X43) = 72.4267216749
h(X43|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46]) = 72.4267216749

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X49) = 72.4261994019
h(X49|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43]) = 72.4261994019

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X48) = 72.3425669178
h(X48|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49]) = 72.3425669178

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X45) = 72.2352675658
h(X45|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48]) = 72.2352675658

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X42) = 72.1898649577
h(X42|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45]) = 72.1898649577

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X47) = 71.9552079177
h(X47|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42]) = 71.9552079177

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X44) = 71.5831346372
h(X44|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47]) = 71.5831346372

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X29) = 62.3991353617
h(X29|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44]) = 62.3991353617

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X33) = 57.1465147247
h(X33|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29]) = 57.1465147247

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X36) = 53.7168468984
h(X36|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33]) = 53.7168468984

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X1) = 48.76702377
h(X1|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36]) = 48.76702377

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X34) = 46.4781991748
h(X34|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1]) = 46.4781991748

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X12) = 43.8821328061
h(X12|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34]) = 43.8821328061

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X17) = 37.2934146309
h(X17|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12]) = 37.2934146309

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X0) = 36.1949856237
h(X0|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17]) = 36.1949856237

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X18) = 34.5706412914
h(X18|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0]) = 34.5706412914

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X16) = 34.2285680788
h(X16|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18]) = 34.2285680788

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X40) = 34.1690451083
h(X40|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16]) = 34.1690451083

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X31) = 31.3875177861
h(X31|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40]) = 31.3875177861

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X6) = 31.2919848028
h(X6|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31]) = 31.2919848028

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X24) = 30.7582545274
h(X24|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6]) = 30.7582545274

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X7) = 29.1506557401
h(X7|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24]) = 29.1506557401

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X4) = 28.8391644651
h(X4|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7]) = 28.8391644651

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X13) = 27.0697175605
h(X13|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4]) = 27.0697175605

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X10) = 25.3412652288
h(X10|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13]) = 25.3412652288

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X30) = 19.2314240472
h(X30|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13, 10]) = 19.2314240472

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X9) = 14.6853142927
h(X9|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13, 10, 30]) = 14.6853142927

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X19) = 14.3318119213
h(X19|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13, 10, 30, 9]) = 14.3318119213

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X28) = 14.0518656105
h(X28|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13, 10, 30, 9, 19]) = 14.0518656105

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X3) = 13.0297219986
h(X3|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13, 10, 30, 9, 19, 28]) = 13.0297219986

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X27) = 12.1700004419
h(X27|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13, 10, 30, 9, 19, 28, 3]) = 12.1700004419

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X37) = 11.2306786568
h(X37|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13, 10, 30, 9, 19, 28, 3, 27]) = 11.2306786568

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X39) = 9.80641999676
h(X39|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13, 10, 30, 9, 19, 28, 3, 27, 37]) = 9.80641999676

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X15) = 8.74946990864
h(X15|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13, 10, 30, 9, 19, 28, 3, 27, 37, 39]) = 8.74946990864

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X22) = 6.70537914625
h(X22|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13, 10, 30, 9, 19, 28, 3, 27, 37, 39, 15]) = 6.70537914625

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X25) = 6.10423339629
h(X25|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13, 10, 30, 9, 19, 28, 3, 27, 37, 39, 15, 22]) = 6.10423339629

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X21) = 5.4168688492
h(X21|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13, 10, 30, 9, 19, 28, 3, 27, 37, 39, 15, 22, 25]) = 5.4168688492

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X50) = 0.507355258772
h(X50|X[35, 2, 20, 8, 14, 5, 38, 32, 26, 41, 11, 23, 46, 43, 49, 48, 45, 42, 47, 44, 29, 33, 36, 1, 34, 12, 17, 0, 18, 16, 40, 31, 6, 24, 7, 4, 13, 10, 30, 9, 19, 28, 3, 27, 37, 39, 15, 22, 25, 21]) = 0.507355258772

h(X0,...,X50) = 3974.17978398

UDS_pure = 0.0

UDS pure with JD

number of bins: 1
h(X20) = 335.523520442
h(X20|X35) = 335.523520442

number of bins: 1
h(X8) = 211.906245667
h(X8|X35) = 211.906245667

number of bins: 1
h(X14) = 200.702487674
h(X14|X35) = 200.702487674

number of bins: 1
h(X5) = 197.820920714
h(X5|X35) = 197.820920714

number of bins: 1
h(X38) = 132.580221634
h(X38|X35) = 132.580221634

number of bins: 1
h(X32) = 116.469698432
h(X32|X35) = 116.469698432

number of bins: 1
h(X26) = 115.6230689
h(X26|X35) = 115.6230689

number of bins: 1
h(X41) = 114.719920673
h(X41|X35) = 114.719920673

number of bins: 1
h(X11) = 111.105872717
h(X11|X35) = 111.105872717

number of bins: 1
h(X23) = 75.8616715628
h(X23|X35) = 75.8616715628

number of bins: 1
h(X46) = 72.5033218511
h(X46|X35) = 72.5033218511

number of bins: 1
h(X43) = 72.4267216749
h(X43|X35) = 72.4267216749

number of bins: 1
h(X49) = 72.4261994019
h(X49|X35) = 72.4261994019

number of bins: 1
h(X48) = 72.3425669178
h(X48|X35) = 72.3425669178

number of bins: 1
h(X45) = 72.2352675658
h(X45|X35) = 72.2352675658

number of bins: 1
h(X42) = 72.1898649577
h(X42|X35) = 72.1898649577

number of bins: 1
h(X47) = 71.9552079177
h(X47|X35) = 71.9552079177

number of bins: 1
h(X44) = 71.5831346372
h(X44|X35) = 71.5831346372

number of bins: 1
h(X29) = 62.3991353617
h(X29|X35) = 62.3991353617

number of bins: 1
h(X33) = 57.1465147247
h(X33|X35) = 57.1465147247

number of bins: 1
h(X36) = 53.7168468984
h(X36|X35) = 53.7168468984

number of bins: 1
h(X1) = 48.76702377
h(X1|X35) = 48.76702377

number of bins: 1
h(X34) = 46.4781991748
h(X34|X35) = 46.4781991748

number of bins: 1
h(X12) = 43.8821328061
h(X12|X35) = 43.8821328061

number of bins: 1
h(X17) = 37.2934146309
h(X17|X35) = 37.2934146309

number of bins: 1
h(X0) = 36.1949856237
h(X0|X35) = 36.1949856237

number of bins: 1
h(X18) = 34.5706412914
h(X18|X35) = 34.5706412914

number of bins: 1
h(X16) = 34.2285680788
h(X16|X35) = 34.2285680788

number of bins: 1
h(X40) = 34.1690451083
h(X40|X35) = 34.1690451083

number of bins: 1
h(X31) = 31.3875177861
h(X31|X35) = 31.3875177861

number of bins: 1
h(X6) = 31.2919848028
h(X6|X35) = 31.2919848028

number of bins: 1
h(X24) = 30.7582545274
h(X24|X35) = 30.7582545274

number of bins: 1
h(X7) = 29.1506557401
h(X7|X35) = 29.1506557401

number of bins: 1
h(X4) = 28.8391644651
h(X4|X35) = 28.8391644651

number of bins: 1
h(X13) = 27.0697175605
h(X13|X35) = 27.0697175605

number of bins: 1
h(X10) = 25.3412652288
h(X10|X35) = 25.3412652288

number of bins: 1
h(X30) = 19.2314240472
h(X30|X35) = 19.2314240472

number of bins: 1
h(X9) = 14.6853142927
h(X9|X35) = 14.6853142927

number of bins: 1
h(X19) = 14.3318119213
h(X19|X35) = 14.3318119213

number of bins: 1
h(X28) = 14.0518656105
h(X28|X35) = 14.0518656105

number of bins: 1
h(X3) = 13.0297219986
h(X3|X35) = 13.0297219986

number of bins: 1
h(X27) = 12.1700004419
h(X27|X35) = 12.1700004419

number of bins: 1
h(X37) = 11.2306786568
h(X37|X35) = 11.2306786568

number of bins: 1
h(X39) = 9.80641999676
h(X39|X35) = 9.80641999676

number of bins: 1
h(X15) = 8.74946990864
h(X15|X35) = 8.74946990864

number of bins: 1
h(X22) = 6.70537914625
h(X22|X35) = 6.70537914625

number of bins: 1
h(X25) = 6.10423339629
h(X25|X35) = 6.10423339629

number of bins: 1
h(X21) = 5.4168688492
h(X21|X35) = 5.4168688492

number of bins: 1
h(X50) = 0.507355258772
h(X50|X35) = 0.507355258772

h(X0,...,X50) = 3974.17978398

UDS_pure_jd = 0

MCE with ordered dimensions/UDS with JD

h(X0,...,X50) = 3593.16725285

UDS_jd = 0.111378067482
67.95823971

UDS_jd = 0.0914012307549
