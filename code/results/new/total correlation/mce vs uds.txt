(5000, 5)

X0 = np.random.uniform(0, 1, rows)
X1 = np.add(np.multiply(X0, X0), np.full(rows, -1))
X2 = np.multiply(X0, X1)
X3 = np.add(10*X0, X2)
X4 = -3.75*X2

MCE
threshold preprocessing = 1
threshold discretization = 1
number of all combinations = 20
size of the candidate set: 14
candidates set: [(0, 1), (0, 3), (1, 0), (1, 3), (2, 0), (2, 1), (2, 3), (2, 4), (3, 0), (3, 1), (4, 0), (4, 1), (4, 2), (4, 3)]

number of bins: 5
h(X4) = 0.584916169104
h(X4|X(2, 1)) = 0.165121837814

number of bins: 3
h(X1) = 0.322447550481
h(X1|X(3, 0)) = 0.10674668844

MCEE_UDS = 0.700375361737
h(X0,...,X4) = h(X0) + h(X1|X0 X3) + h(X2) + h(X3) + h(X4|X1 X2) = 4.27098903169


UDS Pure
amount of bins of the discretized dimension: 3
resulting amount of bins of the current dimension: 3
h(X4) = 0.584916169104
h(X4|X[3]) = 0.265468587663

amount of bins of the discretized dimension: 2
resulting amount of bins of the current dimension: 5
h(X0) = 0.359881272327
h(X0|X[3, 4]) = 0.0890475441546

amount of bins of the discretized dimension: 7
resulting amount of bins of the current dimension: 9
h(X1) = 0.322447550481
h(X1|X[3, 4, 0]) = 0.0195932696723

amount of bins of the discretized dimension: 5
resulting amount of bins of the current dimension: 11
h(X2) = 0.126990527889
h(X2|X[3, 4, 0, 1]) = 0.00802130034752

h(X0,...,X4) = 3.89437940706

UDS_pure = 0.725920982208

UDS generates too many bins