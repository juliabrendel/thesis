UDS Pure, no rediscretizations

amount of bins of the discretized dimension: 2
resulting amount of bins of the current dimension: 2
h(X0) = 37.6239432022
h(X0|X[7]) = 28.7997121173

amount of bins of the discretized dimension: 4
resulting amount of bins of the current dimension: 8
h(X2) = 37.5414955964
h(X2|X[7, 0]) = 8.80536948811

amount of bins of the discretized dimension: 4
resulting amount of bins of the current dimension: 14
h(X6) = 37.1736655957
h(X6|X[7, 0, 2]) = 7.48905269591

amount of bins of the discretized dimension: 5
resulting amount of bins of the current dimension: 43
h(X1) = 34.8503873706
h(X1|X[7, 0, 2, 6]) = 17.9867886809

amount of bins of the discretized dimension: 17
resulting amount of bins of the current dimension: 232
h(X5) = 34.8404010994
h(X5|X[7, 0, 2, 6, 1]) = 8.10920661978

amount of bins of the discretized dimension: 20
resulting amount of bins of the current dimension: 482
h(X8) = 33.219247676
h(X8|X[7, 0, 2, 6, 1, 5]) = 0.591773853071

amount of bins of the discretized dimension: 12
resulting amount of bins of the current dimension: 554
h(X9) = 32.7604180885
h(X9|X[7, 0, 2, 6, 1, 5, 8]) = 0.0474625371395

amount of bins of the discretized dimension: 8
resulting amount of bins of the current dimension: 565
h(X4) = 31.4810772202
h(X4|X[7, 0, 2, 6, 1, 5, 8, 9]) = 0.0103741443308

amount of bins of the discretized dimension: 4
resulting amount of bins of the current dimension: 567
h(X3) = 29.772471537
h(X3|X[7, 0, 2, 6, 1, 5, 8, 9, 4]) = 0.00319510920001

h(X0,...,X9) = 71.8429352458

UDS_pure = 0.767696393362
