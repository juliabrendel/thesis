(569, 31)

MCEE
threshold preprocessing = 1
threshold discretization = 1
number of all combinations = 930
size of the candidates set: 930

number of bins: 4
h(X7) = 39.9706328784
h(X7|X(27, 2)) = 21.3162439604

number of bins: 2
h(X1) = 34.8503873706
h(X1|X21) = 23.4245237825

number of bins: 9
h(X2) = 37.5414955964
h(X2|X(0, 20, 22)) = 14.3818128079

number of bins: 2
h(X4) = 31.4810772202
h(X4|X24) = 23.1941446965

number of bins: 4
h(X26) = 35.7730054671
h(X26|X(6, 25)) = 20.0732084808

number of bins: 10
h(X22) = 35.8968371844
h(X22|X(3, 23, 20)) = 10.9208957296

number of bins: 4
h(X25) = 31.7200240063
h(X25|X(5, 29)) = 19.1554838254

number of bins: 7
h(X3) = 29.772471537
h(X3|X(0, 2, 7)) = 11.7441670175

number of bins: 5
h(X10) = 17.4886780469
h(X10|X(12, 13, 23)) = 9.84688679232

number of bins: 7
h(X30) = 0.421940482329
h(X30|X(22, 27, 12)) = 0.0878156057217

number of bins: 6
h(X13) = 11.6576394666
h(X13|X(12, 23)) = 5.96542489884

number of bins: 5
h(X16) = 12.8335070012
h(X16|X(15, 6)) = 7.22379331102

number of bins: 4
h(X12) = 16.076607929
h(X12|X(22, 23)) = 11.4933082648

MCEE_UDS = 0.410480869029
h(X0,...,X30) = h(X0) + h(X1|X21) + h(X2|X0 X20 X22) + h(X3|X0 X2 X7) + h(X4|X24) + h(X5) + h(X6) + h(X7|X2 X27) + h(X8) + h(X9) + h(X10|X12 X13 X23) + h(X11) + h(X12|X22 X23) + h(X13|X12 X23) + h(X14) + h(X15) + h(X16|X6 X15) + h(X17) + h(X18) + h(X19) + h(X20) + h(X21) + h(X22|X3 X20 X23) + h(X23) + h(X24) + h(X25|X5 X29) + h(X26|X6 X25) + h(X27) + h(X28) + h(X29) + h(X30|X12 X22 X27) +  = 736.414092564

