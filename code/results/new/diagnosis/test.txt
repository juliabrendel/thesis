(569, 31)

MCE
threshold preprocessing = 1
threshold discretization = 1
number of all combinations = 930
size of the candidate set: 178
candidates set: [(7, 3), (30, 26), (0, 20), (7, 12), (0, 7), (24, 4), (20, 7), (2, 22), (0, 10), (3, 7), (27, 2), (26, 6), (23, 7), (30, 13), (6, 7), (30, 0), (30, 28), (7, 6), (17, 7), (30, 23), (20, 30), (25, 26), (6, 25), (13, 7), (13, 20), (2, 27), (22, 3), (3, 2), (7, 10), (23, 10), (3, 23), (10, 13), (6, 0), (7, 5), (6, 23), (10, 23), (0, 30), (7, 22), (6, 26), (13, 10), (12, 22), (27, 22), (13, 23), (3, 12), (22, 12), (6, 13), (5, 27), (15, 16), (6, 16), (13, 0), (1, 21), (22, 23), (20, 3), (25, 5), (23, 22), (27, 6), (23, 3), (2, 12), (23, 12), (10, 3), (7, 2), (30, 27), (12, 2), (17, 6), (13, 3), (12, 13), (20, 0), (2, 23), (22, 7), (27, 5), (26, 7), (22, 10), (30, 20), (2, 7), (6, 30), (20, 10), (20, 12), (22, 0), (27, 0), (3, 22), (30, 2), (6, 20), (5, 15), (10, 20), (6, 27), (12, 23), (0, 2), (22, 30), (13, 22), (27, 26), (2, 30), (22, 13), (3, 0), (6, 2), (5, 26), (15, 19), (26, 25), (7, 20), (22, 20), (13, 12), (12, 20), (27, 20), (22, 27), (21, 1), (3, 10), (27, 25), (23, 2), (30, 3), (2, 13), (0, 23), (30, 5), (4, 24), (10, 0), (12, 3), (0, 22), (13, 2), (20, 22), (26, 30), (7, 27), (2, 20), (0, 12), (23, 20), (2, 3), (29, 25), (6, 5), (30, 6), (7, 0), (12, 0), (7, 13), (7, 30), (20, 27), (27, 30), (23, 27), (27, 3), (30, 12), (5, 7), (25, 29), (5, 6), (0, 13), (30, 22), (19, 15), (25, 27), (26, 16), (0, 3), (17, 16), (23, 30), (22, 2), (2, 0), (3, 20), (10, 12), (6, 3), (5, 25), (16, 15), (6, 22), (12, 10), (10, 22), (7, 23), (20, 13), (27, 23), (16, 6), (3, 13), (3, 30), (2, 10), (30, 10), (30, 25), (25, 28), (20, 23), (26, 27), (7, 26), (20, 2), (26, 22), (25, 6), (27, 7), (26, 5), (23, 0), (23, 13), (30, 7), (10, 2)]

number of bins: 6
h(X22) = 35.8968371844
h(X22|X(20, 23)) = 12.2147154111

number of bins: 5
h(X0) = 37.6239432022
h(X0|X(3, 2)) = 15.2567486033

number of bins: 7
h(X0) = 37.6239432022
h(X0|X(3, 2, 20)) = 14.7977934985

number of bins: 7
h(X0) = 37.6239432022
h(X0|X(3, 2, 23)) = 14.670825576

number of bins: 7
h(X0) = 37.6239432022
h(X0|X(3, 2, 22)) = 14.2397015516

number of bins: 4
h(X27) = 53.498107294
h(X27|X(7, 6)) = 30.6829340914

number of bins: 4
h(X27) = 53.498107294
h(X27|X(7, 26)) = 28.6690953907

number of bins: 4
h(X7) = 39.9706328784
h(X7|X(22, 2)) = 26.3381138353

number of bins: 4
h(X7) = 39.9706328784
h(X7|X(22, 30)) = 25.4607220208

number of bins: 7
h(X20) = 37.0222872934
h(X20|X(13, 23)) = 11.2954729293

number of bins: 4
h(X26) = 35.7730054671
h(X26|X(6, 25)) = 20.0732084808

number of bins: 4
h(X7) = 39.9706328784
h(X7|X(22, 5)) = 21.5013653841

number of bins: 13
h(X22) = 35.8968371844
h(X22|X(13, 23, 20)) = 11.2283509154

number of bins: 6
h(X2) = 37.5414955964
h(X2|X(13, 22)) = 13.4957878799

number of bins: 6
h(X6) = 37.1736655957
h(X6|X(5, 30)) = 18.2513221763

number of bins: 4
h(X5) = 34.8404010994
h(X5|X(25, 26)) = 22.6556828666

number of bins: 4
h(X5) = 34.8404010994
h(X5|X(25, 27)) = 21.6853059273

number of bins: 4
h(X5) = 34.8404010994
h(X5|X(25, 15)) = 21.5284749393

number of bins: 6
h(X3) = 29.772471537
h(X3|X(13, 7)) = 18.5466632826

number of bins: 6
h(X17) = 26.125162261
h(X17|X(7, 6)) = 16.565896419

number of bins: 8
h(X17) = 26.125162261
h(X17|X(7, 16, 6)) = 15.5301076421

number of bins: 6
h(X12) = 16.076607929
h(X12|X(10, 13)) = 6.99106757199

number of bins: 6
h(X23) = 26.1752047627
h(X23|X(30, 7)) = 16.1866554689

number of bins: 6
h(X15) = 25.9597004783
h(X15|X(16, 19)) = 14.9912965437

number of bins: 7
h(X3) = 29.772471537
h(X3|X(13, 30, 7)) = 17.7258751112

number of bins: 7
h(X23) = 26.1752047627
h(X23|X(30, 10, 7)) = 15.2930676722

number of bins: 4
h(X10) = 17.4886780469
h(X10|X(22, 20)) = 13.0812216073

number of bins: 4
h(X10) = 17.4886780469
h(X10|X(22, 0)) = 13.0493808719

number of bins: 4
h(X25) = 31.7200240063
h(X25|X(29, 28)) = 23.4558550954

number of bins: 4
h(X30) = 0.421940482329
h(X30|X(20, 27)) = 0.0951814666458

number of bins: 7
h(X30) = 0.421940482329
h(X30|X(20, 27, 12)) = 0.0911812884552
h(X0,...,X30) = h(X0|X[2, 3, 22] ) + h(X2|X[13, 22] ) + h(X3|X[7, 13, 30] ) + h(X5|X[15, 25] ) + h(X6|X[5, 30] ) + h(X7|X[5, 22] ) + h(X10|X[0, 22] ) + h(X12|X[10, 13] ) + h(X13|X0 ) + h(X15|X[16, 19] ) + h(X17|X[6, 7, 16] ) + h(X20|X[13, 23] ) + h(X21|X1 ) + h(X22|X[13, 20, 23] ) + h(X23|X[7, 10, 30] ) + h(X24|X4 ) + h(X25|X[28, 29] ) + h(X26|X[6, 25] ) + h(X27|X[7, 26] ) + h(X30|X[12, 20, 27] ) +  = 0


UDS Pure
amount of bins of the discretized dimension: 2
resulting amount of bins of the current dimension: 2
h(X7) = 39.9706328784
h(X7|X[27]) = 24.3786595025

amount of bins of the discretized dimension: 3
resulting amount of bins of the current dimension: 5
h(X21) = 39.73301159
h(X21|X[27, 7]) = 26.0760378163

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 5
h(X0) = 37.6239432022
h(X0|X[27, 7, 21]) = 26.7320698875

amount of bins of the discretized dimension: 5
resulting amount of bins of the current dimension: 20
h(X2) = 37.5414955964
h(X2|X[27, 7, 21, 0]) = 5.99927968386

amount of bins of the discretized dimension: 7
resulting amount of bins of the current dimension: 38
h(X24) = 37.4206013355
h(X24|X[27, 7, 21, 0, 2]) = 6.66970394505

amount of bins of the discretized dimension: 5
resulting amount of bins of the current dimension: 102
h(X6) = 37.1736655957
h(X6|X[27, 7, 21, 0, 2, 24]) = 10.1321839324

amount of bins of the discretized dimension: 3
resulting amount of bins of the current dimension: 147
h(X20) = 37.0222872934
h(X20|X[27, 7, 21, 0, 2, 24, 6]) = 3.49552227554

amount of bins of the discretized dimension: 7
resulting amount of bins of the current dimension: 210
h(X22) = 35.8968371844
h(X22|X[27, 7, 21, 0, 2, 24, 6, 20]) = 0.994534583548

amount of bins of the discretized dimension: 8
resulting amount of bins of the current dimension: 263
h(X26) = 35.7730054671
h(X26|X[27, 7, 21, 0, 2, 24, 6, 20, 22]) = 1.38991166923

amount of bins of the discretized dimension: 20
resulting amount of bins of the current dimension: 465
h(X1) = 34.8503873706
h(X1|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26]) = 1.12160989677

amount of bins of the discretized dimension: 12
resulting amount of bins of the current dimension: 552
h(X5) = 34.8404010994
h(X5|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1]) = 0.0321084029741

amount of bins of the discretized dimension: 7
resulting amount of bins of the current dimension: 563
h(X8) = 33.219247676
h(X8|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5]) = 0.0132865844327

amount of bins of the discretized dimension: 4
resulting amount of bins of the current dimension: 568
h(X9) = 32.7604180885
h(X9|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8]) = 0.00271425529604

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 568
h(X25) = 31.7200240063
h(X25|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9]) = 3.75123198594e-05

amount of bins of the discretized dimension: 2
resulting amount of bins of the current dimension: 568
h(X4) = 31.4810772202
h(X4|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25]) = 0.00157845106622

amount of bins of the discretized dimension: 2
resulting amount of bins of the current dimension: 569
h(X3) = 29.772471537
h(X3|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X28) = 27.1869662631
h(X28|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X23) = 26.1752047627
h(X23|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X17) = 26.125162261
h(X17|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X15) = 25.9597004783
h(X15|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X11) = 25.8778912644
h(X11|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X29) = 24.837701206
h(X29|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X18) = 22.5549886703
h(X18|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X14) = 20.2794517807
h(X14|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X10) = 17.4886780469
h(X10|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18, 14]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X12) = 16.076607929
h(X12|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18, 14, 10]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X19) = 15.2976335291
h(X19|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18, 14, 10, 12]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X16) = 12.8335070012
h(X16|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18, 14, 10, 12, 19]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X13) = 11.6576394666
h(X13|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18, 14, 10, 12, 19, 16]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X30) = 0.421940482329
h(X30|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18, 14, 10, 12, 19, 16, 13]) = 0.0

h(X0,...,X30) = 160.537345693

UDS_pure = 0.87250746283


UDS Extended 

h(X27) = 53.498107294

amount of bins of the discretized dimension: 2
resulting amount of bins of the current dimension: 1
h(X7) = 39.9706328784
h(X7|X[27]) = 24.3786595025

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X21) = 39.73301159
h(X21|X[27, 7]) = 39.73301159

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 2
h(X0) = 37.6239432022
h(X0|X[27, 7, 21]) = 28.7997121173

amount of bins of the discretized dimension: 4
resulting amount of bins of the current dimension: 4
h(X2) = 37.5414955964
h(X2|X[27, 7, 21, 0]) = 7.43723070509

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X24) = 37.4206013355
h(X24|X[27, 7, 21, 0, 2]) = 37.4206013355

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 11
h(X6) = 37.1736655957
h(X6|X[27, 7, 21, 0, 2, 24]) = 18.1318719873

amount of bins of the discretized dimension: 3
resulting amount of bins of the current dimension: 26
h(X20) = 37.0222872934
h(X20|X[27, 7, 21, 0, 2, 24, 6]) = 6.37926513853

amount of bins of the discretized dimension: 4
resulting amount of bins of the current dimension: 48
h(X22) = 35.8968371844
h(X22|X[27, 7, 21, 0, 2, 24, 6, 20]) = 2.63459734448

amount of bins of the discretized dimension: 4
resulting amount of bins of the current dimension: 40
h(X26) = 35.7730054671
h(X26|X[27, 7, 21, 0, 2, 24, 6, 20, 22]) = 5.11651702058

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 2
h(X1) = 34.8503873706
h(X1|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26]) = 23.4245237825

amount of bins of the discretized dimension: 20
resulting amount of bins of the current dimension: 272
h(X5) = 34.8404010994
h(X5|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1]) = 0.912284197046

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X8) = 33.219247676
h(X8|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5]) = 33.219247676

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X9) = 32.7604180885
h(X9|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8]) = 32.7604180885

amount of bins of the discretized dimension: 3
resulting amount of bins of the current dimension: 550
h(X25) = 31.7200240063
h(X25|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9]) = 0.0233106900388

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 2
h(X4) = 31.4810772202
h(X4|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25]) = 23.1941446965

amount of bins of the discretized dimension: 3
resulting amount of bins of the current dimension: 552
h(X3) = 29.772471537
h(X3|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4]) = 0.00769275311412

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X28) = 27.1869662631
h(X28|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3]) = 27.1869662631

amount of bins of the discretized dimension: 2
resulting amount of bins of the current dimension: 526
h(X23) = 26.1752047627
h(X23|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28]) = 0.0442071115015

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 568
h(X17) = 26.125162261
h(X17|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23]) = 0.0024369529877

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X15) = 25.9597004783
h(X15|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17]) = 25.9597004783

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X11) = 25.8778912644
h(X11|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15]) = 25.8778912644

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 2
h(X29) = 24.837701206
h(X29|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11]) = 19.6619723981

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X18) = 22.5549886703
h(X18|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29]) = 22.5549886703

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 1
h(X14) = 20.2794517807
h(X14|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18]) = 20.2794517807

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X10) = 17.4886780469
h(X10|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18, 14]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X12) = 16.076607929
h(X12|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18, 14, 10]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 2
h(X19) = 15.2976335291
h(X19|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18, 14, 10, 12]) = 11.7368499102

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X16) = 12.8335070012
h(X16|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18, 14, 10, 12, 19]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 569
h(X13) = 11.6576394666
h(X13|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18, 14, 10, 12, 19, 16]) = 0.0

amount of bins of the discretized dimension: 1
resulting amount of bins of the current dimension: 4
h(X30) = 0.421940482329
h(X30|X[27, 7, 21, 0, 2, 24, 6, 20, 22, 26, 1, 5, 8, 9, 25, 4, 3, 28, 23, 17, 15, 11, 29, 18, 14, 10, 12, 19, 16, 13]) = 0.15820093784

h(X0,...,X30) = 490.533861686

UDS_extended = 0.479454469267
