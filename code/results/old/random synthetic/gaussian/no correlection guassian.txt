(5000, 5)

MCEE 

threshold preprocessing = 1
threshold discretization = 1
number of all combinations = 20
size of the candidates set: 20

number of bins: 3
h(X0) = 0.882889570317
h(X0|X(4, 1)) = 0.357839209073

number of bins: 3
h(X4) = 0.888212226534
h(X4|X(1, 3)) = 0.358725899287

h(X0,...,X4) = h(X0|X1, X4) + h(X1) + h(X2) + h(X3) + h(X4|X1, X3) = 3.44829220185
MCEE_UDS = 0.595412804824

detects 4 wrong dependencies, probably owing to the high univariate CE values


UDS Pure, no rediscretizations

h(X1) = 0.90592274925
h(X1|X2) = 0.121416088677

amount of bins: 5
h(X3) = 0.903011960214
h(X3|X1, X2) = 0.263329984573

amount of bins: 7
h(X4) = 0.888212226534
h(X4|X1, X2, X3) = 0.201173853852

amount of bins: 8
h(X0) = 0.882889570317
h(X0|X1, X2, X3, X4) = 0.199224124662

h(X0,...,X4) = h(X2) + h(X1|X2) + h(X3|X1, X2) + h(X4|X1, X2, X3) + h(X0|X1, X2, X3, X4) = 1.70793643579
UDS_pure = 0.780688255447

quite misleading, according to UDS each combination of dimensions exhibit a significant dependency


UDS Extended 

h(X2) = 0.922792384026

amount of bins: 4
h(X3) = 0.903011960214
h(X3|X[1, 2]) = 0.3700801443

amount of bins: 4
h(X4) = 0.888212226534
h(X4|X[1, 2, 3]) = 0.358725899287

amount of bins: 4
h(X0) = 0.882889570317
h(X0|X[1, 2, 3, 4]) = 0.357839209073

h(X0,...,X4) = 2.00943763669
UDS_extended = 0.593642847172
higher score, dependency


MCEE with ordered dimensions/UDS with JD

number of bins: 3
h(X3) = 0.903011960214
h(X3|X1) = 0.3700801443, number of bins: 3
the same score as uds extended returns for h(X3|X[1, 2]) - X2 is redundant


number of bins: 3
h(X4) = 0.888212226534
h(X4|X(1, 3)) = 0.358725899287, number of bins: 3
the same value as mcee returns the same score as uds extended returns for h(X4|X[1, 2, 3])


number of bins: 3
h(X0) = 0.882889570317
h(X0|X(4, 1)) = 0.357839209073, number of bins: 3
the same value as mcee returns, the same score as uds extended returns for h(X0|X[1, 2, 3, 4]), so j.d. actually prunes redundant dimensions

h(X0,...,X4) = h(X0|X1, X4) + h(X2) + h(X3|X1) + (X4|X1, X3) = 2.13085372536
UDS_jd = 0.662556138965