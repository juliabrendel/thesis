(5000, 6)

MCEE 

threshold preprocessing = 1
threshold discretization = 1
number of all combinations = 30
size of the candidates set: 30

number of bins: 3
h(X0) = 0.902687938273
h(X0|X1) = 0.362277811983

number of bins: 3
h(X1) = 0.902687938273
h(X1|X2) = 0.362277811983

number of bins: 3
h(X2) = 0.902687938273
h(X2|X0) = 0.362277811983

h(X0,...,X5) = h(X0|X1) + h(X1|X2) + h(X2|X0) + h(X3) + h(X4) + h(X5) = 9.19492434626
MCEE_UDS = 0.598667716026


UDS Pure, no rediscretizations

h(X3) = 2.23236322253
h(X3|X[5]) = 0.301560589902

amount of bins: 4
h(X4) = 2.23236322253
h(X4|X[3, 5]) = 0.734090517144

amount of bins: 5
h(X1) = 0.902687938273
h(X1|X[3, 4, 5]) = 0.243183271171

amount of bins: 6
h(X2) = 0.902687938273
h(X2|X[1, 3, 4, 5]) = 0.183907020417

amount of bins: 9
h(X0) = 0.902687938273
h(X0|X[1, 2, 3, 4, 5]) = 0.132211345075

h(X0,...,X5) = 5.23831720897
UDS_pure = 0.777638452273


UDS Extended 

h(X5) = 3.64336446526

amount of bins: 3
h(X4) = 2.23236322253
h(X4|X[3, 5]) = 0.920604431067

amount of bins: 3
h(X1) = 0.902687938273
h(X1|X[3, 4, 5]) = 0.362277811983
the same score as mcee for h(X1|X2)

amount of bins: 3
h(X2) = 0.902687938273
h(X2|X[1, 3, 4, 5]) = 0.362277811983

amount of bins: 3
h(X0) = 0.902687938273
h(X0|X[1, 2, 3, 4, 5]) = 0.362277811983
the same value as mcee returns for h(X0|X1): X2, X3, X4, X5 are redundant 

h(X0,...,X5) = 5.65080233227
UDS_extended = 0.593671184325

better w.r.t. to the UDS pure, results in a smaller number of bins and higher scores
Detects wrong dependencies


MCEE with ordered dimensions/UDS with JD

number of bins: 3
h(X4) = 2.23236322253
h(X4|X3) = 0.920604431067
the same score as uds extended h(X4|X[3, 5]) -> X5 is redundant


number of bins: 3
h(X1) = 0.902687938273
h(X1|X4) = 0.362277811983
uds extended h(X1|X[3, 4, 5]), mcee h(X1|X2)


number of bins: 3
h(X2) = 0.902687938273
h(X2|X1) = 0.362277811983
uds extended h(X2|X[1, 3, 4, 5]) -> X3, X4, X5 are redundant


number of bins: 3
h(X0) = 0.902687938273
h(X0|X1) = 0.362277811983

h(X0,...,X5) = h(X0|X1) + h(X2|X1) + h(X1|X4) + h(X4|X3) + h(X5) = 5.95236292218
UDS_jd = 0.678089227029

h(X4|X3), h(X2|X1), h(X0|X1) - correct
h(X1|X4) - wrong
h(X2|X0) - missing
