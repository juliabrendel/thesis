UDS, no rediscretizations

h(X8) = 27.6882515172

h(X0|X7) = 3.53754756979


bins' support: [199, 228, 113, 29]
amount of bins: 4
h(X2|X0, X7) = 7.76182276117


bins' support: [58, 141, 228, 86, 27, 29]
amount of bins: 6
h(X6|X0, X2, X7) = 5.92805478


bins' support: [58, 141, 115, 113, 86, 26, 30]
amount of bins: 7
h(X5|X0, X2, X6, X7) = 4.98058657344


bins' support: [58, 141, 115, 113, 86, 28, 28]
amount of bins: 7
h(X1|X0, X2, X5, X6, X7) = 4.35137949588


bins' support: [58, 56, 86, 114, 114, 85, 27, 1, 28]
amount of bins: 9
h(X8|X0, X1, X2, X5, X6, X7) = 3.97899519442


bins' support: [29, 29, 56, 85, 115, 113, 86, 27, 29]
amount of bins: 9
h(X9|X0, X1, X2, X5, X6, X7, X8) = 3.79422010218


bins' support: [29, 29, 56, 85, 115, 60, 53, 86, 27, 29]
amount of bins: 10
h(X4|X0, X1, X2, X5, X6, X7, X8, X9) = 3.02298085078


bins' support: [29, 29, 56, 85, 57, 58, 59, 54, 86, 27, 29]
amount of bins: 11
h(X3|X0, X1, X2, X4, X5, X6, X7, X8, X9) = 3.0008814953


bins' support: [29, 29, 56, 85, 58, 57, 60, 53, 56, 30, 27, 29]
amount of bins: 12
h(X11|X0, X1, X2, X3, X4, X5, X6, X7, X8, X9) = 2.46834011969


bins' support: [29, 29, 56, 85, 57, 58, 59, 54, 56, 30, 27, 29]
amount of bins: 12
h(X14|X0, X1, X2, X3, X4, X5, X6, X7, X8, X9, X11) = 1.97852826198


bins' support: [30, 28, 56, 85, 57, 58, 59, 54, 28, 28, 30, 27, 29]
amount of bins: 13
h(X10|X0, X1, X2, X3, X4, X5, X6, X7, X8, X9, X11, X14) = 1.76504432605


bins' support: [29, 29, 56, 85, 57, 58, 59, 54, 28, 28, 30, 27, 29]
amount of bins: 13
h(X12|X0, X1, X2, X3, X4, X5, X6, X7, X8, X9, X10, X11, X14) = 1.64226109413


bins' support: [30, 28, 57, 84, 57, 58, 59, 54, 28, 28, 30, 27, 29]
amount of bins: 13
h(X13|X0, X1, X2, X3, X4, X5, X6, X7, X8, X9, X10, X11, X12, X14) = 1.4721993701

h(X0,...,X14) = 77.3710935121