real data, no bins sorting

mce

glass
main file, data size:  (214, 7)
outliers:  {3}
score:  0.297230954925
entropy:  106.501813688
--- 36.53645992279053 seconds --- 

diabetes
main file, data size:  (768, 8)
outliers:  {0, 1, 2, 5, 6, 7}
score:  0.0393825855395
entropy:  271.601224781
--- 60.20931100845337 seconds --- 

pendigits
main file, data size:  (7494, 16)
outliers:  {0, 1, 2, 4, 6, 8, 9, 12}
score:  0.138706468865
entropy:  513.196850914
--- 360.1505391597748 seconds --- 

diagnosis
main file, data size:  (569, 30)
outliers:  {8, 9, 11, 14, 17, 18, 19, 28, 29}
score:  0.293055014406
entropy:  730.481276352
--- 1314.753695011139 seconds --- 

letter
main file, data size:  (20000, 16)
outliers:  {5, 7, 8, 9, 11, 12, 13, 14, 15}
score:  0.0836693996619
entropy:  560.23547014
--- 933.0622010231018 seconds --- 

wavenoise
main file, data size:  (5000, 40)
outliers:  {0, 1, 2, 3, 4, 5, 8, 9, 10, 11, 12, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39}
score:  0.0135380576056
entropy:  1532.35502605
--- 1064.544014930725 seconds --- 