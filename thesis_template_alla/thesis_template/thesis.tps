[FormatInfo]
Type=TeXnicCenterProjectSessionInformation
Version=2

[SessionInfo]
ActiveTab=2
FrameCount=9
ActiveFrame=0

[Frame0]
Columns=1
Rows=1
Flags=2
ShowCmd=3
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-4
MaxPos.y=-23
NormalPos.left=22
NormalPos.top=22
NormalPos.right=951
NormalPos.bottom=382
Class=CLatexEdit
Document=thesis.tex

[Frame0_Row0]
cyCur=494
cyMin=10

[Frame0_Col0]
cxCur=1063
cxMin=10

[Frame0_View0,0]
Cursor.row=193
Cursor.column=66
TopSubLine=178

[Frame1]
Columns=1
Rows=1
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-4
MaxPos.y=-23
NormalPos.left=44
NormalPos.top=44
NormalPos.right=973
NormalPos.bottom=404
Class=CLatexEdit
Document=Presections\declaration.tex

[Frame1_Row0]
cyCur=313
cyMin=10

[Frame1_Col0]
cxCur=901
cxMin=10

[Frame1_View0,0]
Cursor.row=0
Cursor.column=0
TopSubLine=0

[Frame2]
Columns=1
Rows=1
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-4
MaxPos.y=-23
NormalPos.left=66
NormalPos.top=66
NormalPos.right=995
NormalPos.bottom=426
Class=CLatexEdit
Document=Presections\abstract.tex

[Frame2_Row0]
cyCur=313
cyMin=10

[Frame2_Col0]
cxCur=901
cxMin=10

[Frame2_View0,0]
Cursor.row=9
Cursor.column=590
TopSubLine=6

[Frame3]
Columns=1
Rows=1
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-4
MaxPos.y=-23
NormalPos.left=88
NormalPos.top=88
NormalPos.right=1017
NormalPos.bottom=448
Class=CLatexEdit
Document=Presections\acknowledgement.tex

[Frame3_Row0]
cyCur=313
cyMin=10

[Frame3_Col0]
cxCur=901
cxMin=10

[Frame3_View0,0]
Cursor.row=11
Cursor.column=179
TopSubLine=0

[Frame4]
Columns=1
Rows=1
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-4
MaxPos.y=-23
NormalPos.left=44
NormalPos.top=44
NormalPos.right=973
NormalPos.bottom=404
Class=CLatexEdit
Document=Bibliography.bib

[Frame4_Row0]
cyCur=313
cyMin=10

[Frame4_Col0]
cxCur=901
cxMin=10

[Frame4_View0,0]
Cursor.row=0
Cursor.column=0
TopSubLine=0

[Frame5]
Columns=1
Rows=1
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-4
MaxPos.y=-23
NormalPos.left=22
NormalPos.top=22
NormalPos.right=951
NormalPos.bottom=382
Class=CLatexEdit
Document=Chapters\Chapter8.tex

[Frame5_Row0]
cyCur=313
cyMin=10

[Frame5_Col0]
cxCur=901
cxMin=10

[Frame5_View0,0]
Cursor.row=561
Cursor.column=1
TopSubLine=773

[Frame6]
Columns=1
Rows=1
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-4
MaxPos.y=-23
NormalPos.left=44
NormalPos.top=44
NormalPos.right=973
NormalPos.bottom=404
Class=CLatexEdit
Document=Chapters\Chapter5.tex

[Frame6_Row0]
cyCur=313
cyMin=10

[Frame6_Col0]
cxCur=901
cxMin=10

[Frame6_View0,0]
Cursor.row=72
Cursor.column=547
TopSubLine=183

[Frame7]
Columns=1
Rows=1
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-4
MaxPos.y=-23
NormalPos.left=66
NormalPos.top=66
NormalPos.right=995
NormalPos.bottom=426
Class=CLatexEdit
Document=Chapters\Chapter7.tex

[Frame7_Row0]
cyCur=313
cyMin=10

[Frame7_Col0]
cxCur=901
cxMin=10

[Frame7_View0,0]
Cursor.row=129
Cursor.column=7
TopSubLine=217

[Frame8]
Columns=1
Rows=1
Flags=0
ShowCmd=1
MinPos.x=-1
MinPos.y=-1
MaxPos.x=-4
MaxPos.y=-23
NormalPos.left=88
NormalPos.top=88
NormalPos.right=1017
NormalPos.bottom=448
Class=CLatexEdit
Document=Chapters\Chapter2.tex

[Frame8_Row0]
cyCur=313
cyMin=10

[Frame8_Col0]
cxCur=901
cxMin=10

[Frame8_View0,0]
Cursor.row=138
Cursor.column=1
TopSubLine=253

