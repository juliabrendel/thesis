import java.util.*;

public class DataPoint 
{
	public int universalID;									
	public int localID;										
	public double[] measures;
	
	public ArrayList<Integer> neighbors;					
	public ArrayList<Double> distToNeighbors;				
	public double lrd;										
	public String classID;										
	
	public DataPoint(int numMeasures)
	{
		if (numMeasures > 0)
			measures = new double[numMeasures];
	}
	
	public DataPoint(DataPoint p, int numMeasures)
	{
		this.universalID = p.universalID;
		this.localID = p.localID;
		
		if (numMeasures > 0)
			measures = new double[numMeasures];
		
		for (int i = 0; i < numMeasures; i++)
			measures[i] = p.measures[i];
	}
	
	public static double distanceLNorm(int expo, ArrayList<Integer> dims, DataPoint p, DataPoint q)
	{
		int numMeasuresCols = dims.size();
		double dist = 0;
		int curCol;
		for (int i = 0; i < numMeasuresCols; i++)
		{
			curCol = dims.get(i).intValue();
			dist += Math.pow(Math.abs(p.measures[curCol] - q.measures[i]), expo);
		}
		
		return Math.pow(dist, 1.0 / expo);
	}
	
	public static double distanceLNorm(int expo, DataPoint p, DataPoint q)
	{
		int numMeasuresCols = p.measures.length;
		double dist = 0;
		for (int i = 0; i < numMeasuresCols; i++)
			dist += Math.pow(Math.abs(p.measures[i] - q.measures[i]), expo);
		
		return Math.pow(dist, 1.0 / expo);
	}
	
	public static double distanceLNorm(int expo, DataPoint p, DataPoint q, int start, int end)
	{
		double dist = 0;
		for (int i = start; i <= end; i++)
			dist += Math.pow(Math.abs(p.measures[i] - q.measures[i]), expo);
		
		return Math.pow(dist, 1.0 / expo);
	}
}