import java.util.*;

public class Constants 
{
	//public static int INIT_BIN_COUNT = Integer.MAX_VALUE;
	public static int LOG_BASE = 2;
	public static int LP_NORM = 2;
	public static double MTC = 0;
	public static final double MAX_ERROR = 0.00001;
	
	public static int[][] DISC_DATA = null;
	public static double[] CRES = null;
	public static ArrayList<String> CLASS_LABELS;
	
	public static String FILE_INPUT; 					
	public static String FILE_RUNTIME_OUTPUT;
	public static String FILE_DATA_OUTPUT;
	public static String FILE_CP_OUTPUT;
	public static String FILE_SUBSPACE;
	public static String FILE_GROUND_SUBSPACE;
	public static String FILE_GRAPH;
	public static String FILE_B_OUTPUT;
	public static String FILE_N_OUTPUT;
	public static String FILE_C_OUTPUT;
	
	public static int NUM_ROWS;
	public static int NUM_MEASURE_COLS;
	public static String FIELD_DELIMITER = ";";
	//public static double MAX_VAL = 100;
	public static boolean MICORG = false;
	public static boolean DISCARD_FIRST_ROW = false;
	public static double ALPHA = 0.5;
	public static int CLUMPS = Integer.MAX_VALUE;
	public static double FACTOR = 1;
	
	public static int METHOD = 0;
	public static final int MAC = 0;
	public static final int MSTMAC = 1;
	public static final int MSTMIC = 2;
	public static final int MultiMIC = 3;
	public static final int DCOR = 4;
}