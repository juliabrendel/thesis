import java.util.*;

public class MicroBin 
{
	public double lowerBound;
	public double upperBound;
	public ArrayList<Integer> pointIDs;
	public ArrayList<Integer> dims;
	
	public MicroBin(double lowerBound, double upperBound)
	{
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		pointIDs = new ArrayList<Integer>();
		dims = new ArrayList<Integer>();
	}
}