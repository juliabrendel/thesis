import java.io.*;
import java.util.*;

public class DataReader 
{
	public static void readData(String fileName, DataMatrix dataMatrix) throws Exception
	{
		try
		{
			Scanner scanner = new Scanner(new FileInputStream(fileName));
			
			String s = null;
			int rows = dataMatrix.rows;
			int cols = dataMatrix.cols;
			String[] vals = new String[cols + 1];
			DataPoint newPoint = null;
			int count = 0;
			
			Constants.CRES  = new double[cols];
			Constants.CLASS_LABELS = new ArrayList<String>();
			int numClasses;
			boolean labelExists;
			String tmpLabel = null;
			
			for (int r = 0; r < rows; r++)
			{
				s = scanner.nextLine();
				if (s.contains("?"))
					continue;
				
				vals = s.split(Constants.FIELD_DELIMITER);
	        	if (vals.length != cols + 1)
	        	{
	        		System.out.println(s);
	        		throw new Exception("Invalid input!");
	        	}
	        	s = null;
	        	
		        newPoint = new DataPoint(cols);
		        newPoint.universalID = count;
		        
		        // update the list of class labels
		        newPoint.classID = new String(vals[cols]);
		        numClasses = Constants.CLASS_LABELS.size();
		        labelExists = false;
		        for (int j = 0; j < numClasses; j++)
		        {
		        	tmpLabel = Constants.CLASS_LABELS.get(j);
		        	if (tmpLabel.equals(newPoint.classID))
		        	{
		        		labelExists = true;
		        		break;
		        	}
		        }
		        if (!labelExists)
		        	Constants.CLASS_LABELS.add(new String(newPoint.classID));
		        
		        for (int i = 0; i < cols; i++)
		        	newPoint.measures[i] = Double.parseDouble(vals[i]);
		        
		        dataMatrix.data.add(newPoint);
		        vals = null;
		        
		        count++;
		    } // end while
			rows = count;
			Constants.NUM_ROWS = count;
			
			double[] dimData = new double[rows];
			SortedObject[] so = new SortedObject[cols];
			for (int i = 0; i < cols; i++)
			{
				for (int j = 0; j < rows; j++)
				{
					dimData[j] = dataMatrix.data.get(j).measures[i];
				}
				Constants.CRES[i] = computeCRE(dimData, false);
				so[i] = new SortedObject(i, Constants.CRES[i]);
			}
			
			scanner.close();
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}
	
	public static double computeCRE(double[] vals, boolean hasSorted)
	{
		if (vals.length <= 1)
			return 0;
			
		int num_items = vals.length;
		if (hasSorted == false)
			Arrays.sort(vals);
		
		double cre = 0;
		double logBase = Math.log(Constants.LOG_BASE);
		for (int i = 0; i < num_items - 1; i++)
			if (vals[i + 1] != vals[i])
				cre += (vals[i + 1] - vals[i]) * ((i + 1) / (1.0 * num_items)) * Math.log((i + 1) / (1.0 * num_items)) / logBase;
		
		return -cre;
	}
}